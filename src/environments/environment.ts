// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  defaultLang: 'ru',
  lang: [
    {name: 'RU', value: 'ru'},
    {name: 'EN', value: 'en'},
    {name: 'UZ', value: 'uz'}
  ],
  googleMapsApiKey: 'AIzaSyBCTLbKUAze03q-lWfkr-UiD2lOiP6TUuA',
  baseURL: 'https://api.smartbooking.uz',
  // tslint:disable-next-line:max-line-length
  appToken: 'Basic WGpDQllzcG5XMm9xaWZqbEdxcTJnVEZmSGlMaFZuRmVSN3dpMzlkcmpRZEwzcGxjSFB0RGxmVllCSjVFOlVkalB4ZWFOZmpISXRySmFzYjVXM2JFUVRtQ0tWV0hXV0FNcGhIZTNLdWZneDY4UjU0M2c4YzdBb1hvVQ=='
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
