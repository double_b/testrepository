import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

// Environments
import {environment} from '../environments/environment';

// Services
import {KeysEnum} from '@app/core/constants/keys.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
    const lang = localStorage.getItem(KeysEnum.LANG)
      ? localStorage.getItem(KeysEnum.LANG)
      : environment.defaultLang;
    this.translate.use(lang);
  }
}
