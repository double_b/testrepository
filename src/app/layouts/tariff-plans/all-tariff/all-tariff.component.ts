import {Component, OnDestroy, OnInit} from '@angular/core';
import {TariffPlansService} from '../tariff-plans.service';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {Subscription} from 'rxjs';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-all-tariff',
  templateUrl: './all-tariff.component.html',
  styleUrls: ['./all-tariff.component.scss']
})
export class AllTariffComponent implements OnInit, OnDestroy {

  routesEnum = new RoutesEnum();
  isVisibleDelete = false;
  dataToDelete: TariffPlanModel = null;
  deleteLoading = false;
  deleteTariffSub: Subscription = null;

  constructor(private notificationService: NotificationService,
              public tariffPlansService: TariffPlansService,
              private router: Router) {
  }

  ngOnInit() {
    this.tariffPlansService.getTariffPlans();
  }

  ngOnDestroy() {
    if (this.deleteTariffSub != null) {
      this.deleteTariffSub.unsubscribe();
    }
  }

  editPlan(planId: number) {
    this.router.navigate(
      [RoutesEnum.HOTEL, this.tariffPlansService.getPropertyId(), RoutesEnum.TARIFF_PLANS, RoutesEnum.EDIT],
      {queryParams: {id: planId}})
      .finally();
  }

  prepareDataToDelete(data) {
    this.dataToDelete = data;
    this.isVisibleDelete = true;
  }

  cancelDelete() {
    this.dataToDelete = null;
    this.isVisibleDelete = false;
  }

  deleteElm() {
    this.deleteLoading = true;
    this.deleteTariffSub = this.tariffPlansService.deleteTariffPlan(this.dataToDelete).subscribe(res => {
      this.notificationService.successMessage('Тарифный план успешно удалён');
      this.isVisibleDelete = false;
      this.deleteLoading = false;
    }, error => {
      this.notificationService.errorMessage('Неизвестная ошибка, попробуйте позже');
      this.isVisibleDelete = false;
      this.deleteLoading = false;
    });
  }

  /**
   * просто для модалки возвращаем название выбранного тарифа
   * обработка нужна, чтоб не выдавало null вместо названия
   */
  getDeletedItemName(): string {
    return this.dataToDelete === null ? '' : this.dataToDelete.name;
  }

  getChessRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.tariffPlansService.getPropertyId()}/${RoutesEnum.CHESS}`;
  }

  getTariffCreateRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.tariffPlansService.getPropertyId()}/${RoutesEnum.TARIFF_PLANS}/${RoutesEnum.CREATE}`;
  }
}
