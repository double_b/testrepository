import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {TariffModule} from '../../core/models/commun';
import {TariffPlansApiService} from '@app/shared/services/tariff-plans-api/tariff-plans-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {map} from 'rxjs/operators';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';

@Injectable({
  providedIn: 'root'
})
export class TariffPlansService implements OnDestroy {

  isLoading = false;

  private readonly _tariffPlans = new BehaviorSubject<TariffPlanModel[]>([]);
  readonly $tariffPlans = this._tariffPlans.asObservable();
  getTariffPlansSub: Subscription = null;

  constructor(private tariffPlansApiService: TariffPlansApiService, private roomsService: RoomsService) {
  }

  /**
   * получение списка тарифов (с фильтрацией на стороне сервера)
   * если используется сортировка - то один из видов сортировка нужно устанавливать как false
   * @param searchByName - название тарифа (для поиска по названию) (опционально)
   * @param sortById - сортировка по id (опционально)
   * @param sortByName - сортировка по названию (опционально)
   * @param reverseSort - для обратной сортировки (в запрос перед значением добавляется - (-id, -name)) (опционально)
   */
  getTariffPlans(searchByName: string = null,
                 sortById: boolean = false,
                 sortByName: boolean = false,
                 reverseSort: boolean = false) {
    this.isLoading = true;
    this.getTariffPlansSub =
      this.tariffPlansApiService.getTariffPlans(this.getPropertyId(), searchByName, sortById, sortByName, reverseSort).subscribe(res => {
        this._tariffPlans.next([]);
        this._tariffPlans.next(res);
        this.isLoading = false;
      });
  }

  getTariffPlan(planId: number): Observable<TariffPlanModel> {
    return this.tariffPlansApiService.getTariffPlan(this.getPropertyId(), planId).pipe(
      map((res) => {
        // сервер отправляет 1/0 вместо true/false
        // поэтому переопределяем значения
        res.breakfast = res.breakfast === 1;
        res.lunch = res.lunch === 1;
        res.dinner = res.dinner === 1;
        return res;
      })
    );
  }

  createTariffPlan(plan: TariffPlanModel): Observable<TariffPlanModel> {
    return this.tariffPlansApiService.createTariffPlan(this.getPropertyId(), plan).pipe(
      map(res => {
        const list: TariffPlanModel[] = [...this._tariffPlans.getValue(), res];
        this._tariffPlans.next([]);
        this._tariffPlans.next(list);
        return res;
      })
    );
  }

  editTariffPlan(plan: TariffPlanModel): Observable<TariffPlanModel> {
    return this.tariffPlansApiService.editTariffPlan(this.getPropertyId(), plan).pipe(
      map(res => {
        const _tariff = this._tariffPlans.getValue().find(item => item.id === res.id);
        const index = this._tariffPlans.getValue().indexOf(_tariff);
        this._tariffPlans.getValue()[index] = res;
        return res;
      })
    );
  }

  deleteTariffPlan(plan: TariffPlanModel): Observable<any> {
    return this.tariffPlansApiService.deleteTariffPlan(this.getPropertyId(), plan).pipe(
      map((_) => {
        const _tariffs = this._tariffPlans.getValue();
        const _tariff = _tariffs.find(item => item.id === plan.id);
        const index = _tariffs.indexOf(_tariff);
        _tariffs.splice(index, 1);
        this._tariffPlans.next([]);
        this._tariffPlans.next(_tariffs);
        return _;
      }));
  }

  getPropertyId(): number {
    const hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelId);
  }

  getRoomTypes(): Observable<RoomsTypesListModel[]> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId()).pipe(
      map((_) => _));
  }

  ngOnDestroy(): void {
    if (this.getTariffPlansSub != null) {
      this.getTariffPlansSub.unsubscribe();
    }
  }
}
