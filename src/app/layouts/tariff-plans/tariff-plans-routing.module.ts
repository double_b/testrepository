import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AllTariffComponent} from './all-tariff/all-tariff.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {TariffFormComponent} from '@app/layouts/tariff-plans/tariff-form/tariff-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: RoutesEnum.LIST
  },
  {
    path: RoutesEnum.LIST,
    component: AllTariffComponent
  },
  {
    path: RoutesEnum.CREATE,
    component: TariffFormComponent
  },
  {
    path: RoutesEnum.EDIT,
    component: TariffFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TariffPlansRoutingModule {
}
