import {Component, OnDestroy, OnInit} from '@angular/core';
import {TariffPlansService} from '@app/layouts/tariff-plans/tariff-plans.service';
import {Subscription} from 'rxjs';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {TariffPlanFormModel} from '@app/layouts/tariff-plans/tariff-form/tariff-plan-form.model';
import {Utils} from '@app/shared/helpers/utils';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location} from '@angular/common';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-tariff-form',
  templateUrl: './tariff-form.component.html',
  styleUrls: ['./tariff-form.component.scss']
})
export class TariffFormComponent implements OnInit, OnDestroy {
  // для отображение ошибок валидации
  nutritionError = false;
  roomTypesError = false;
  nameError = false;

  isEdit = false;
  title = 'NEW_TARIFF.CREATE_TITLE';
  roomTypes: RoomsTypesListModel[] = [];

  // подписки нужны, чтоб при дестрое отписываться
  // других ф-й они не выполняют
  roomTypesSub: Subscription = null;
  tariffPlanSub: Subscription = null;
  submitTariffPlansSub: Subscription = null;

  tariffPlan: TariffPlanFormModel = new TariffPlanFormModel();

  constructor(public tariffPlansService: TariffPlansService,
              private router: Router,
              public location: Location,
              private notificationService: NotificationService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.roomTypesSub = this.tariffPlansService.getRoomTypes().subscribe(res => {
      this.roomTypes.push(...res);
      this.activatedRoute.queryParams.subscribe((params: Params) => {
        if (params.id) {
          this.isEdit = true;
          this.title = 'NEW_TARIFF.EDIT_TITLE';
          this.getTariffPlanById(params.id);
        }
      });
    });
  }

  getTariffPlanById(planId: number) {
    this.tariffPlanSub = this.tariffPlansService.getTariffPlan(planId).subscribe(res => {
        this.tariffPlan.id = res.id;
        this.tariffPlan.property_id = res.property_id;
        this.tariffPlan.name = res.name;
        this.tariffPlan.breakfast = res.breakfast;
        this.tariffPlan.lunch = res.lunch;
        this.tariffPlan.dinner = res.dinner;
        this.tariffPlan.min_stay = res.min_stay;
        this.tariffPlan.min_advance_res = res.min_advance_res;
        res.room_types.forEach(item => {
          this.roomTypes.find(i => i.id === item.id).selected = true;
        });
        // переключатель Да/нет для раздела питания
        this.tariffPlan.nutrition = this.tariffPlan.breakfast || this.tariffPlan.lunch || this.tariffPlan.dinner;
        // чекбокс Все включено для раздела питания
        this.tariffPlan.allInclusive = this.tariffPlan.breakfast && this.tariffPlan.lunch && this.tariffPlan.dinner;
        // переключатель Да/нет для раздела мин кол-во дней проживания
        // если кол-во дней в тарифе == 1, ставим Нет, так как мин кол-во и так == 1
        // точно также обрабатываем переключатель Дней до заезда, только мин кол-во == 0
        this.tariffPlan.minDaysCheck = this.tariffPlan.min_stay > 1;
        this.tariffPlan.beforeDaysCheck = this.tariffPlan.min_advance_res > 0;
      }
    );
  }

  ngOnDestroy() {
    if (this.roomTypesSub != null) {
      this.roomTypesSub.unsubscribe();
    }
    if (this.tariffPlanSub != null) {
      this.tariffPlanSub.unsubscribe();
    }
    if (this.submitTariffPlansSub != null) {
      this.submitTariffPlansSub.unsubscribe();
    }
  }

  /**
   * просто возращает на вьюшку название тарифа
   * чтоб не отображалось undefined когда объект пустой
   */
  getTariffName(): string {
    return this.isEdit ? this.tariffPlan.name : '';
  }

  /**
   * переключение чекбоксов питания
   * @param isAllInclusive - true если было выбрано Все включено
   */
  onNutritionSwitch(isAllInclusive: boolean = false) {
    // переопределяем значения (если это необходимо)
    // так как из-за 1/0 вместо true/false иногда могут возникать аномальные поведения чекбоксов
    this.switchNutritionType();
    // если питание было полностью выключено и если не режим редактирования
    // сбрасываем все чекбоксы
    this.nutritionError = false;
    if (!this.tariffPlan.nutrition && !this.isEdit) {
      this.tariffPlan.breakfast = false;
      this.tariffPlan.lunch = false;
      this.tariffPlan.dinner = false;
      this.tariffPlan.dinner = false;
      return;
    }
    if (isAllInclusive) {
      this.tariffPlan.breakfast = this.tariffPlan.allInclusive;
      this.tariffPlan.lunch = this.tariffPlan.allInclusive;
      this.tariffPlan.dinner = this.tariffPlan.allInclusive;
    } else {
      this.tariffPlan.allInclusive = !!(this.tariffPlan.breakfast && this.tariffPlan.lunch && this.tariffPlan.dinner);
    }
  }

  /**
   * при выключении мин дней проживания сбрасываем счетчик
   */
  onMinDaysSwitch() {
    if (!this.tariffPlan.minDaysCheck) {
      this.tariffPlan.min_stay = 1;
    }
  }

  /**
   * при выключении мин дней бронирования сбрасываем счетчик
   */
  onBeforeDaysSwitch() {
    if (!this.tariffPlan.beforeDaysCheck) {
      this.tariffPlan.min_advance_res = 0;
    }
  }

  submit() {
    if (!this.validateSubmit()) {
      return;
    }
    // очищаем на всякий случай типы номеров
    // и заполняем id-ишниками выбранных типов
    this.tariffPlan.room_types = [];
    this.roomTypes.forEach(item => {
      if (item.selected) {
        // @ts-ignore
        this.tariffPlan.room_types.push(item.id);
      }
    });
    // если питание выключено, то просто выключаем все типы
    // этот код нужен, так как в режиме редактирования значения насильно удерживаются даже при переключении на Нет
    if (!this.tariffPlan.nutrition) {
      this.tariffPlan.breakfast = false;
      this.tariffPlan.lunch = false;
      this.tariffPlan.dinner = false;
    } else {
      // сервер требует 1/0 вместо true/false
      // поэтому переопределяем значения
      this.tariffPlan.breakfast = this.tariffPlan.breakfast ? 1 : 0;
      this.tariffPlan.lunch = this.tariffPlan.lunch ? 1 : 0;
      this.tariffPlan.dinner = this.tariffPlan.dinner ? 1 : 0;
    }
    const finalTariff: TariffPlanModel = this.tariffPlan;
    if (this.isEdit) {
      this.submitTariffPlansSub = this.tariffPlansService.editTariffPlan(finalTariff).subscribe(res => {
        this.notificationService.successMessage('Изменения успешно сохранены');
        this.router.navigate(
          [RoutesEnum.HOTEL, this.tariffPlansService.getPropertyId(), RoutesEnum.TARIFF_PLANS, RoutesEnum.LIST]).finally();
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка, попробуйте позже');
      });
    } else {
      this.submitTariffPlansSub = this.tariffPlansService.createTariffPlan(finalTariff).subscribe(res => {
        this.notificationService.successMessage('Тарифный план успешно создан');
        this.router.navigate(
          [RoutesEnum.HOTEL, this.tariffPlansService.getPropertyId(), RoutesEnum.TARIFF_PLANS, RoutesEnum.LIST]).finally();
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка, попробуйте позже');
      });
    }
  }

  validateSubmit(): boolean {
    let result = true;
    // если питание не выключено
    // то проверяем виды питания
    if (this.tariffPlan.nutrition) {
      if (!this.tariffPlan.breakfast && !this.tariffPlan.lunch && !this.tariffPlan.dinner) {
        this.nutritionError = true;
        result = false;
      }
    }
    // ищем хотя бы один выбранный тип номеров
    if (!this.roomTypes.some(item => item.selected)) {
      this.roomTypesError = true;
      result = false;
    }
    // проверяем название тарифа
    if (!Utils.isExist(this.tariffPlan.name)) {
      this.nameError = true;
      result = false;
    }
    return result;
  }

  switchNutritionType() {
    if (typeof this.tariffPlan.breakfast === 'number') {
      this.tariffPlan.breakfast = this.tariffPlan.breakfast === 1;
    }
    if (typeof this.tariffPlan.lunch === 'number') {
      this.tariffPlan.lunch = this.tariffPlan.lunch === 1;
    }
    if (typeof this.tariffPlan.dinner === 'number') {
      this.tariffPlan.dinner = this.tariffPlan.dinner === 1;
    }
  }

  /**
   * css-класс для степпера питания
   */
  getNutritionStatus(): string {
    if (this.nutritionError) {
      return 'error';
    }
    if (!this.tariffPlan.nutrition) {
      return 'process';
    } else if (!this.tariffPlan.breakfast && !this.tariffPlan.lunch && !this.tariffPlan.dinner) {
      return 'wait';
    } else {
      return 'process';
    }
  }

  /**
   * css-класс для степпера типов номеров
   */
  getRoomTypesStatus(): string {
    if (this.roomTypesError) {
      return 'error';
    }
    let result = 'wait';
    for (const item of this.roomTypes) {
      if (item.selected) {
        result = 'process';
        break;
      }
    }
    return result;
  }

  /**
   * css-класс для степпера названия
   */
  getNameStatus(): string {
    if (this.nameError) {
      return 'error';
    }
    return Utils.isExist(this.tariffPlan.name) ? 'process' : 'wait';
  }
}
