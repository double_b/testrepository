import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';

export class TariffPlanFormModel extends TariffPlanModel {
  nutrition: boolean|number = false;
  allInclusive: boolean|number = false;
  minDaysCheck = false;
  beforeDaysCheck = false;
}
