import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NzAlertModule,
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzEmptyModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzInputNumberModule,
  NzModalModule,
  NzPopoverModule,
  NzRadioModule,
  NzStepsModule,
  NzTableModule
} from 'ng-zorro-antd';
import {RouterModule} from '@angular/router';
import {AllTariffComponent} from './all-tariff/all-tariff.component';
import {SharedModule} from '@app/core/shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TariffPlansRoutingModule} from './tariff-plans-routing.module';
import {TariffFormComponent} from './tariff-form/tariff-form.component';

@NgModule({
  declarations: [AllTariffComponent, TariffFormComponent],
  imports: [
    CommonModule,
    NzGridModule,
    NzCardModule,
    RouterModule,
    NzButtonModule,
    SharedModule,
    NzStepsModule,
    NzPopoverModule,
    NzIconModule,
    NzFormModule,
    ReactiveFormsModule,
    NzRadioModule,
    NzCheckboxModule,
    NzInputNumberModule,
    NzInputModule,
    NzAlertModule,
    NzTableModule,
    NzDatePickerModule,
    FormsModule,
    NzEmptyModule,
    NzModalModule,
    TariffPlansRoutingModule
  ],
  exports: []
})
export class TariffPlansModule {
}
