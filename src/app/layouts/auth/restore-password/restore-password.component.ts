import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

// Services
import {RestoredService} from '@app/shared/services/auth/restored.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RolesEnum} from '@app/core/constants/roles.enum';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent {
  restoreForm: FormGroup;
  isLoading = false;

  isRestoreByEmail = true;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private router: Router,
    private restoredService: RestoredService,
    private notificationService: NotificationService
  ) {
    this.restoreForm = this.fb.group({
      email: [null, [Validators.required]],
    });
  }

  onSwitch() {
    this.isRestoreByEmail = !this.isRestoreByEmail;
    this.restoreForm.reset();
  }

  submitForm() {
    this.isLoading = true;
    if (this.isRestoreByEmail) {
      this.restoredService.isRestoreByEmail = true;
      const {email} = this.restoreForm.value;
      this.restoredService.restoredPasswordByEmail({email, role: RolesEnum.HOTELIER})
        .subscribe((res) => {
            this.isLoading = false;
            localStorage.setItem(KeysEnum.EMAIL, email);
            this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CHANGE_PASS]).then(() => {
              this.notificationService.successMessage('Код подтверждения был отправлен на указанную почту');
            });
          },
          error => {
            this.onError(error);
          });
    } else {
      const phone = this.restoreForm.value.email;
      this.restoredService.isRestoreByEmail = false;
      this.restoredService.restoredPasswordByPhone({phone, role: RolesEnum.HOTELIER})
        .subscribe((res) => {
            this.isLoading = false;
            localStorage.setItem(KeysEnum.PHONE, phone);
            this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CHANGE_PASS]).then(() => {
              this.notificationService.successMessage(res.message);
            });
          },
          error => {
            this.onError(error);
          });
    }
  }

  onError(error) {
    if (error.status === 403) {
      this.isLoading = false;
      this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CHANGE_PASS, error.error.seconds]).then(() => {
        this.notificationService.errorMessage('Код был отправлен раннее', 'Проверьте почту.');
      });
    }
    if (error.status === 422) {
      this.isLoading = false;
      this.notificationService.errorMessage('Указаны неверные данные');
    }
  }

  getLoginRoute(): string {
    return `/${RoutesEnum.AUTH}/${RoutesEnum.LOGIN}`;
  }

}
