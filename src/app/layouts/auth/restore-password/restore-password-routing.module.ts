import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {RestorePasswordComponent} from '@app/layouts/auth/restore-password/restore-password.component';

const routes: Routes = [
  {
    path: '',
    component: RestorePasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestorePasswordRoutingModule { }
