import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Routing module
import {RestorePasswordRoutingModule} from './restore-password-routing.module';
// Components
import {RestorePasswordComponent} from './restore-password.component';
import {SharedModule} from '@app/shared/shared.module';


@NgModule({
  declarations: [RestorePasswordComponent],
  imports: [
    CommonModule,
    RestorePasswordRoutingModule,
    SharedModule
  ]
})
export class RestorePasswordModule {
}
