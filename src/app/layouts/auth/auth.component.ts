import {Component, OnInit, NgZone} from '@angular/core';
import {Router} from '@angular/router';

// Services
import {AuthService} from '@src/app/shared/services/auth/auth.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  routesEnum = new RoutesEnum();
  isLoading = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private zone: NgZone
  ) {
  }

  ngOnInit(): void {
    this.validateUser();
    this.zone.run((e) => e);
  }

  // Проверяю авторизирован ли ранее пользаватель
  validateUser() {
    this.authService.isAuthenticated()
      ? this.router.navigate([RoutesEnum.HOTEL, RoutesEnum.LIST])
      : this.router.navigate([RoutesEnum.AUTH, RoutesEnum.LOGIN]).finally();
  }
}
