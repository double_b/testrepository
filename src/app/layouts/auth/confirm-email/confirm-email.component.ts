import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

// Services
import {RegisterService} from '@app/shared/services/auth/register.service';
import {RolesEnum} from '@app/core/constants/roles.enum';
import {AuthService} from '@app/shared/services/auth/auth.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {AuthDtoModel} from '@app/shared/services/auth/auth-dto.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  confirmForm: FormGroup;
  isLoading = false;
  timer = 120;
  minuteLabel = 0;
  secondLabel = 0;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private router: Router,
    private registerService: RegisterService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {
    this.confirmForm = fb.group({
      token: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.startTimer();
  }

  startTimer() {
    if (this.timer > 0) {
      const interval = setInterval(() => {
        this.timer -= 1;
        this.minuteLabel = Math.trunc(this.timer / 60);
        this.secondLabel = this.timer % 60;
        if (this.timer === 0) {
          clearInterval(interval);
        }
      }, 1000);
    }
  }

  submitForm() {
    this.registerService.confirmEmail(this.confirmForm.value.token, RolesEnum.HOTELIER)
      .subscribe(() => {
        this.notificationService.successMessage('E-mail успешно подтверждён!');
        const data = new AuthDtoModel(this.registerService.emailOfReg, this.registerService.passOfReg, RolesEnum.HOTELIER);
        this.authService.login(data).pipe(tap(({access_token, token_type}) => this.authService.setUserToken(access_token, token_type)))
          .subscribe(() => {
            localStorage.setItem(KeysEnum.EMAIL, this.registerService.emailOfReg);
            localStorage.removeItem(KeysEnum.GET_HOTEL);
            this.router.navigate([`/${RoutesEnum.HOTEL}`, RoutesEnum.LIST]).finally();
          }, errorOnLogin => {
            this.router.navigate([RoutesEnum.AUTH, RoutesEnum.LOGIN]).finally();
          });
      }, error => {
        this.notificationService.errorMessage('Неверный код');
      });
  }

  resendConfirm() {
    if (this.timer !== 0) {
      return;
    }
    this.registerService.resendConfirmCode(RolesEnum.HOTELIER).subscribe(() => {
      this.notificationService.successMessage('Код повторно отправлен!');
      this.timer = 120;
      this.startTimer();
    }, error => this.notificationService.errorMessage(error));
  }

}
