import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Routing module
import {ConfirmEmailRoutingModule} from './confirm-email-routing.module';
// Components
import {ConfirmEmailComponent} from './confirm-email.component';
// Zorro modules
import {SharedModule} from '@app/shared/shared.module';

@NgModule({
  declarations: [ConfirmEmailComponent],
  imports: [
    CommonModule,
    ConfirmEmailRoutingModule,
    SharedModule
  ]
})
export class ConfirmEmailModule {
}
