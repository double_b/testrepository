import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Components
import {ChangePasswordComponent} from '@app/layouts/auth/change-password/change-password.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';


const routes: Routes = [
  {
    path: '',
    component: ChangePasswordComponent
  },
  {
    path: `:${RoutesEnum.SECONDS}`,
    component: ChangePasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangePasswordRoutingModule {
}
