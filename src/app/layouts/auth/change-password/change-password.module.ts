import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Routing module
import {ChangePasswordRoutingModule} from './change-password-routing.module';
// Components
import {ChangePasswordComponent} from './change-password.component';
// Zorro modules
import {SharedModule} from '@app/shared/shared.module';

@NgModule({
  declarations: [ChangePasswordComponent],
  imports: [
    CommonModule,
    ChangePasswordRoutingModule,
    SharedModule
  ]
})
export class ChangePasswordModule {
}
