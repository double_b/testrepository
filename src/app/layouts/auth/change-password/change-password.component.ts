import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location} from '@angular/common';

import {UtilsService} from '@app/core/services/utils/utils.service';
import {NzMessageService} from 'ng-zorro-antd';

// Services
import {RestoredService} from '@app/shared/services/auth/restored.service';
import {AuthService} from '@app/shared/services/auth/auth.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RolesEnum} from '@app/core/constants/roles.enum';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {tap} from 'rxjs/operators';
import {AuthDtoModel} from '@app/shared/services/auth/auth-dto.model';
import {Utils} from '@app/shared/helpers/utils';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {
  changePassForm: FormGroup;
  passwordVisible1 = false;
  passwordVisible2 = false;
  isLoading = false;

  timer = 120;
  minuteLabel = 0;
  secondLabel = 0;
  routSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private utils: UtilsService,
    private router: Router,
    private restoredService: RestoredService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService
  ) {
    this.changePassForm = fb.group({
      token: [null, [Validators.required]],
      password: [null, [Validators.required]],
      password_confirmation: [null, [Validators.required]]
    });

    // смотрим - отправил ли сервер время для таймера
    this.routSubscription = this.activatedRoute.paramMap.subscribe((params: Params) => {
      if (Utils.isExist(params.get(RoutesEnum.SECONDS))) {
        this.timer = params.get(RoutesEnum.SECONDS);
      }
    });

    this.startTimer();
  }

  startTimer() {
    if (this.timer > 0) {
      const interval = setInterval(() => {
        this.timer -= 1;
        this.minuteLabel = Math.trunc(this.timer / 60);
        this.secondLabel = this.timer % 60;
        if (this.timer === 0) {
          clearInterval(interval);
        }
      }, 1000);
    }
  }

  submitForm() {
    const {token, password, password_confirmation} = this.changePassForm.value;

    if (this.restoredService.isRestoreByEmail) {
      const email = localStorage.getItem(KeysEnum.EMAIL);
      this.restoredService.changePasswordByEmail({token, password, password_confirmation, email, role: RolesEnum.HOTELIER})
        .subscribe(res => {
            const data = new AuthDtoModel(email, password, RolesEnum.HOTELIER);
            this.authService.login(data).pipe(tap(({access_token, token_type}) => this.authService.setUserToken(access_token, token_type)))
              .subscribe(() => {
                localStorage.setItem(KeysEnum.EMAIL, email);
                localStorage.removeItem(KeysEnum.GET_HOTEL);
                this.notificationService.successMessage('Пароль успешно обновлён');
                this.router.navigate([`/${RoutesEnum.HOTEL}`, RoutesEnum.LIST]).finally();
              });
          },
          error => {
            this.notificationService.errorMessage(error);
            this.utils.presentErrorResponseMessage(error);
          });
    } else if (!this.restoredService.isRestoreByEmail) {
      const phone = localStorage.getItem(KeysEnum.PHONE);
      this.restoredService.changePasswordByPhone({
        token,
        password,
        password_confirmation,
        phone,
        role: RolesEnum.HOTELIER
      })
        .subscribe(res => {
            this.router.navigate([`/${RoutesEnum.HOTEL}`, RoutesEnum.LIST]).finally();
          },
          error => {
            this.notificationService.errorMessage(error);
            this.utils.presentErrorResponseMessage(error);
          });
    }
  }

  resendEmailCode() {
    if (this.timer !== 0) {
      return;
    }
    const email = localStorage.getItem(KeysEnum.EMAIL);
    this.restoredService.restoredPasswordByEmail({email, role: RolesEnum.HOTELIER}).subscribe(() => {
        this.notificationService.successMessage('Код повторно отправлен!');
        this.timer = 120;
        this.startTimer();
      },
      error => this.notificationService.errorMessage(error));
  }

  getLoginRoute(): string {
    return `/${RoutesEnum.AUTH}/${RoutesEnum.LOGIN}`;
  }

}
