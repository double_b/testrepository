import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {LoginComponent} from '@app/layouts/auth/login/login.component';


const routes: Routes = [
  {
      path: '',
      component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
