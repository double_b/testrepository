import {AfterViewInit, Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {retry, tap} from 'rxjs/operators';
import {KeysEnum} from '@app/core/constants/keys.enum';

// Models
import {AuthDtoModel} from '@app/shared/services/auth/auth-dto.model';

// Services
import {AuthService} from '@app/shared/services/auth/auth.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {RolesEnum} from '@app/core/constants/roles.enum';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {RegisterService} from '@app/shared/services/auth/register.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit, OnInit {

  loginForm: FormGroup;
  isLoading = false;
  status;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notificationService: NotificationService,
    private router: Router,
    private registerService: RegisterService,
    private zone: NgZone
  ) {
    this.loginForm = fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  ngOnInit() {
    this.loginForm.updateValueAndValidity();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.zone.run(() => {
        this.zone.run(() => {
          this.loginForm.updateValueAndValidity();
          this.loginForm.enable();
        });

      });
    }, 5000);


    // Обновляем токен приложения то получаю его
    this.authService.authApp()
      .pipe(retry(3))
      .subscribe(
        () => {},
        (error: any) => {
          this.notificationService.errorMessage('Ошибка при получении токена приложения');
        });
    if (this.registerService.existEmailOnRegistration != null) {
      this.loginForm.patchValue({
          username: this.registerService.existEmailOnRegistration
        }
      );
    }
  }

  submitForm() {
    this.loginForm.disable();
    const {username, password} = this.loginForm.value;
    const data: AuthDtoModel = {username, password, role: RolesEnum.HOTELIER};

    this.authService.login(data)
      .pipe(tap(({access_token, token_type}) => {
        this.loginForm.value.remember // Проверяю чекбокс сохранить пароль
          ? this.authService.setUserToken(access_token, token_type) // Если чекнут запомнить пароль записываю в локал стор
          : this.authService.setTemporaryUserToken(access_token, token_type); // Если не чекнут записываю в сессионый стор
      }))
      .subscribe(() => {
          this.loginForm.enable();
          localStorage.setItem(KeysEnum.EMAIL, username);
          localStorage.removeItem(KeysEnum.GET_HOTEL);
          this.router.navigate([RoutesEnum.HOTEL, RoutesEnum.LIST]).finally();
        },
        (error: any) => {
          if (error.code != null && error.code === 6) {
            this.notificationService.errorMessage('Вы не завершили регистрацию, пожалуйста, подтвердите Ваш Email');
            this.registerService.emailOfReg = username;
            this.registerService.passOfReg = password;

            setTimeout(() => {
              this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CONFIRM_EMAIL]).finally();
            }, 2000);
          } else {
            this.loginForm.enable();
            this.notificationService.errorMessage('Email и пароль не совпадают. Убедитесь что данные введены верно');
          }
        });
  }

  getRestoreRoute(): string {
    return `/${RoutesEnum.AUTH}/${RoutesEnum.RESTORE_PASS}`;
  }

  getRegistrationRoute(): string {
    return `/${RoutesEnum.AUTH}/${RoutesEnum.REGISTRATION}`;
  }
}
