import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

// Components
import {AuthComponent} from '@app/layouts/auth/auth.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: RoutesEnum.LOGIN,
        loadChildren: () => import('./login/login.module')
          .then(m => m.LoginModule)
      },
      {
        path: RoutesEnum.REGISTRATION,
        loadChildren: () => import('./registration/registration.module')
          .then(m => m.RegistrationModule)
      },
      {
        path: RoutesEnum.RESTORE_PASS,
        loadChildren: () => import('./restore-password/restore-password.module')
          .then(m => m.RestorePasswordModule)
      },
      {
        path: RoutesEnum.CHANGE_PASS,
        loadChildren: () => import('./change-password/change-password.module')
          .then(m => m.ChangePasswordModule)
      },
      {
        path: RoutesEnum.CONFIRM_EMAIL,
        loadChildren: () => import('./confirm-email/confirm-email.module')
          .then(m => m.ConfirmEmailModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
