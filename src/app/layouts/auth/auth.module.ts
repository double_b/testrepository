import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Rout
import {AuthRoutingModule} from './auth-routing.module';

// Components
import {AuthComponent} from './auth.component';

// Modules
import {SharedModule} from '@app/shared/shared.module';


@NgModule({
  declarations: [AuthComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ]
})
export class AuthModule {
}
