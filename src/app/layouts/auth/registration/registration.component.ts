import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {countries} from '@app/core/models/commun';

// Services
import {RegisterService} from '@app/shared/services/auth/register.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {RolesEnum} from '@app/core/constants/roles.enum';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  [x: string]: any;

  countries = countries;
  code;
  // isWantedLogin = false;
  registrationForm: FormGroup;
  isLoading = false;
  passwordVisible = false;

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private registerService: RegisterService,
    private router: Router
  ) {
    this.registrationForm = fb.group({
      first_name: [null, [Validators.required]],
      phone: [null, [Validators.required, Validators.maxLength(12)]],
      email: [null, [Validators.required, Validators.email]],
      property_name: [null, [Validators.required]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit(): void {
    this.code = this.countries[0].dial_code;
  }

  lower = text => text ? text.toLocaleLowerCase() : '';

  selectCountry(country) {
    this.code = country.dial_code;
  }

  // Отправляю регистрационные данные
  submitForm() {
    this.isLoading = true;
    const {
      email, phone, password,
      property_name, first_name,
      password_confirmation = password
    } = this.registrationForm.value;

    this.registerService.registerHotelier({
      email, phone, password, property_name,
      first_name, password_confirmation
    })
      .subscribe(() => {
          this.isLoading = false;
          this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CONFIRM_EMAIL]).finally();
        },
        error => {
          this.isLoading = false;
          if (error.code != null && error.code === 6) {
            this.notificationService.errorMessage('Вы не завершили регистрацию, пожалуйста, подтвердите Ваш Email');
            this.registerService.emailOfReg = email;
            this.registerService.passOfReg = password;
            this.registerService.resendConfirmCode(RolesEnum.HOTELIER).subscribe(() => {
              this.router.navigate([RoutesEnum.AUTH, RoutesEnum.CONFIRM_EMAIL]).finally();
            });
          } else if (error.status === 422) {
            this.registerService.existEmailOnRegistration = this.registrationForm.value.email;
            this.notificationService.errorMessage('E-mail уже зарегестрирован, попробуйте войти в аккаунт');
            this.router.navigate([RoutesEnum.AUTH, RoutesEnum.LOGIN]).finally();
          }
        });
  }

  getLoginRoute(): string {
    return `/${RoutesEnum.AUTH}/${RoutesEnum.LOGIN}`;
  }
}
