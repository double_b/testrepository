import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Routing module
import {RegistrationRoutingModule} from './registration-routing.module';

// Components
import {RegistrationComponent} from './registration.component';
import {SharedModule} from '@app/shared/shared.module';


@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    SharedModule
  ]
})
export class RegistrationModule {
}
