import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {RegistrationComponent} from '@app/layouts/auth/registration/registration.component';


const routes: Routes = [
  {
    path: '',
    component: RegistrationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
