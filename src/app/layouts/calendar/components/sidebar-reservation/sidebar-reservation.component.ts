import {Component, EventEmitter, Input, OnInit, Output,} from '@angular/core';
import uuidv4 from 'uuid/v4';
import {CalendarStore} from '@src/app/core/store/calendar.store';

@Component({
  selector: 'app-sidebar-reservation',
  templateUrl: './sidebar-reservation.component.html',
  styleUrls: ['./sidebar-reservation.component.scss']
})
export class SidebarReservationComponent implements OnInit {
  @Input() rooms: any[];
  @Input() room: any;
  @Input() booking: any;
  @Input() roomTypes: any[];
  @Input() roomType: any;
  @Output() toggle = new EventEmitter();

  id: string;
  _isOpened = false;
  _selectedRoom = null;
  selectedRoomType = null;

  constructor(private store: CalendarStore) {
  }

  ngOnInit() {
    this.id = uuidv4();

    this.selectedRoomType = this.roomType;
    this.room = this.roomType.rooms[0];
    this.selectedRoom = this.roomType.rooms[0];
  }

  set selectedRoom(value: string) {
    this._selectedRoom = value;
    this.store.setState({
      selectedBooking: {
        ...this.store.state.selectedBooking,
        room: value
      }
    });
  }

  get selectedRoom(): string {
    return this._selectedRoom;
  }

  set isOpened(value: boolean) {
    this._isOpened = value;

    if (value) {
      this.store.setState({
        selectedBooking: {
          room: this.room,
          roomType: this.roomType,
          booking: this.booking,
        }
      });
    }
  }

  get isOpened(): boolean {
    return this._isOpened;
  }

  selectRoomType(roomType) {
    this.selectedRoomType = roomType;
    this.rooms = roomType.rooms;
    this.selectedRoom = roomType.rooms[0];

    this.store.setState({
      selectedBooking: {
        ...this.store.state.selectedBooking,
        roomType,
      }
    });
  }

  toggleContent() {
    this.isOpened = !this.isOpened;
    this.toggle.emit({id: this.id, isOpened: this.isOpened});
  }

  closeContent() {
    this.isOpened = false;
    this.toggle.emit({id: this.id, isOpened: false});
  }
}
