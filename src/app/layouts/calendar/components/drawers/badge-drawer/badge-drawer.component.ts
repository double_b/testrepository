import {Component, Input} from '@angular/core';
import {NzDrawerRef, NzDrawerService} from 'ng-zorro-antd';
import {QuickEditComponent} from '../quick-edit/quick-edit.component';

@Component({
  selector: 'app-badge-drawer',
  templateUrl: './badge-drawer.component.html',
  styleUrls: ['./badge-drawer.component.scss', './../drawers.scss', './../../../calendar.component.scss']
})
export class BadgeDrawerComponent {

  @Input() data: any;

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private drawerService: NzDrawerService,
  ) {
  }

  close(): void {
    this.drawerRef.close();
  }

  openQuickEdit(): void {
    this.drawerService.create<QuickEditComponent, {}, string>({
      nzTitle: null,
      nzWidth: 750,
      nzClosable: false,
      nzBodyStyle: {padding: 0},
      nzPlacement: 'left',
      nzContent: QuickEditComponent,
      nzContentParams: {
        data: this.data,
      },
    });
  }
}
