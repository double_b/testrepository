import {Component} from '@angular/core';

@Component({
  selector: 'app-legend-drawer',
  templateUrl: './legend-drawer.component.html',
  styleUrls: ['./legend-drawer.component.scss', './../drawers.scss']
})
export class LegendDrawerComponent {

  colorsType = [
    {
      type: 'grey',
      name: 'Проверено'
    },
    {
      type: 'blue',
      name: 'Подтвердил'
    },
    {
      type: 'purple',
      name: 'Ожидание'
    },
    {
      type: 'green',
      name: 'Внутренний'
    }
  ];

  marks = [
    {
      type: 'red',
      name: 'Непогашенный остаток'
    },
    {
      type: 'yellow',
      name: 'Заметки'
    }
  ];
}
