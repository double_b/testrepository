import {Component, Input, OnInit, QueryList, ViewChildren,} from '@angular/core';
import {NzDrawerRef} from 'ng-zorro-antd';
import {SidebarReservationComponent} from '../../sidebar-reservation/sidebar-reservation.component';
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {CalendarService} from '../../../calendar.service';
import {Calendardata} from '@src/app/core/models/calendardata';

@Component({
  selector: 'app-sidebar-drawer',
  templateUrl: './sidebar-drawer.component.html',
  styleUrls: ['./sidebar-drawer.component.scss']
})
export class SidebarDrawerComponent implements OnInit {
  @Input() data: Calendardata;

  @Input() set currentDate(value: Date) {
    this._currentDate = value;
    this.store.setState({selectedDate: value});
  }

  get currentDate(): Date {
    return this._currentDate;
  }

  @ViewChildren('reserv') reservations: QueryList<SidebarReservationComponent>;

  isDatePickerOpen = false;
  expandedId: number;
  filteredData: any[];

  private _currentDate: Date;

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private store: CalendarStore,
    private calendarService: CalendarService,
  ) {
  }

  ngOnInit() {
    if (!this.currentDate) {
      this.currentDate = new Date();
    }

    this.drawerRef.afterClose.subscribe(() => {
      this.store.setState({selectedDate: null});
      this.store.setState({selectedBooking: null});
    });

    this.updateData();
  }

  close(): void {
    this.drawerRef.close();
  }

  updateData() {
    this.filteredData = this.data.room_types.map((roomtype) => {
      let reservations = [];
      if (roomtype.not_assigned) {
        reservations = roomtype.not_assigned
          .filter(reservation => this.calendarService.compareDates(reservation.checkin, this.currentDate));
      }
      return {...roomtype, reservations};
    });
  }

  closeAllReservs({id, isOpened}): void {
    this.reservations.toArray().forEach((reserv) => {
      if (isOpened && id !== reserv.id) {
        reserv.closeContent();
      }
    });

    let openedCount = 0;

    this.reservations.toArray().forEach((reserv) => {
      openedCount += Number(reserv.isOpened);
    });

    this.store.setState({openedReservCount: openedCount});
  }

  toggleDatePicker(): void {
    this.isDatePickerOpen = !this.isDatePickerOpen;
  }

  onDateChange(date): void {
    this.isDatePickerOpen = false;
    this.store.setState({searchRoomQuery: null, selectedBooking: null});
    this.calendarService.scrollToDate(date, this.data.dates[0]);
    this.updateData();
  }

  toggleRoomType(roomType): void {
    this.expandedId = this.expandedId ? null : roomType.room_type_id;
    this.store.setState({openedReservCount: 0, selectedRoom: null});

  }
}
