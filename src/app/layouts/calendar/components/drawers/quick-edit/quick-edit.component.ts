import {Component, Input, OnDestroy, Renderer2} from '@angular/core';
import {NzDrawerRef} from 'ng-zorro-antd';

@Component({
  selector: 'app-quick-edit',
  templateUrl: './quick-edit.component.html',
  styleUrls: ['./quick-edit.component.scss']
})
export class QuickEditComponent implements OnDestroy {
  @Input() data: any;

  // roomTypes: any[] = roomTypes;
  totalCost: number = 100;
  roomTypes = [
    {
      id: 1,
      name: 'SINGLE'
    },
    {
      id: 2,
      name: 'TWIN'
    },
  ];

  reservationData = {
    guest: 'Peter',
    periods: [
      {
        checkIn: '2019-11-20',
        checkout: '2019-11-23',
        room: {
          type: 1,
          count: 1,
          name: '101'
        }
      },
      {
        checkIn: '2019-11-20',
        checkout: '2019-11-26',
        room: {
          type: 2,
          count: 1,
          name: '103'
        }
      }
    ]
  };

  constructor(
    private renderer: Renderer2,
    private drawerRef: NzDrawerRef<string>
  ) {
    this.renderer.removeClass(document.body, 'hide_badge_backdrop');
  }

  close(): void {
    this.drawerRef.close();
  }

  ngOnDestroy(): void {
    this.renderer.addClass(document.body, 'hide_badge_backdrop');
  }
}
