import {
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ElementRef,
  Input, OnChanges,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2, SimpleChanges,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {CalendarService} from '../../calendar.service';
import {UtilsService} from '@src/app/core/services/utils/utils.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {ScrollStore} from '@src/app/core/store/scroll.store';
import {RoomRowComponent} from '../room-row/room-row.component';
import * as moment from 'moment';
import { NumbersEnum } from "@app/core/constants/numbers.enum";

import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import {skip} from "rxjs/operators";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-calendar-table',
  templateUrl: './calendar-table.component.html',
  styleUrls: ['./calendar-table.component.scss', './../../calendar.component.scss']
})
export class CalendarTableComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() data: any;

  @ViewChild(CdkVirtualScrollViewport, {static: true})
  viewport: CdkVirtualScrollViewport;
  @ViewChild('table', {read: ElementRef, static: true}) public table: ElementRef<any>;
  @ViewChildren('room-row') roomRows: QueryList<RoomRowComponent>;

  dataSubscription: Subscription;
  tableScrollSubscription: Subscription;
  tableStateSubscription: Subscription;
  isDragStarted: boolean;
  draggableClonedEl: any;
  leftEdgePos: number;
  topEdgePos: number;
  scrollState: boolean;
  dates: any[];
  isLoading: boolean;

  cellWidth = NumbersEnum.CALENDAR_CELL_WIDTH;
  batch = 14;
  datesForShow: any[] = [];
  currentScroll = 0;
  firstScroll = null;
  daysCount: any = 0;

  constructor(
    private renderer: Renderer2,
    private calendarService: CalendarService,
    private utilsService: UtilsService,
    private store: CalendarStore,
    private scrollStore: ScrollStore,
    private cdref: ChangeDetectorRef,
  ) {
    this.dataSubscription = this.calendarService.calendarData.subscribe(data => {
      this.isLoading = false;
      this.data = data;
      this.dates = data.dates;

      const prevDates = this.getPrevDates(this.dates[0]);
      const afterDates = this.getAfterDates(this.dates[this.dates.length - 1]);
      this.datesForShow = [...prevDates, ...this.dates, ...afterDates];
      const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
      this.firstScroll = (elCount-1)*this.cellWidth;
      this.currentScroll = this.firstScroll;
      if (this.table) {
        this.table.nativeElement.scrollTo({left: this.firstScroll});
        this.cdref.markForCheck();
      } else {
        const interval = setInterval(() => {
          if (this.table) {
            this.table.nativeElement.scrollTo({left: this.firstScroll});
            this.cdref.markForCheck();
            clearInterval(interval);
          }
        }, 2)
      }
      // console.log(this.dates);
    });
  }

  private getPrevDates(date): any {
    let dates: any[] = [];
    const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    for (let i = 1; i < elCount; i++) {
      dates.push(moment(date).subtract(i, 'day').format('YYYY-MM-DD'));
    }
    dates = dates.reverse();
    return dates;
  }

  private getAfterDates(date): any {
    let dates: any[] = [];
    const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    for (let i = 1; i < elCount + 4; i++) {
      dates.push(moment(date).add(i, 'day').format('YYYY-MM-DD'));
    }
    return dates;
  }

  ngOnInit() {
    this.daysCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    this.calendarService.tableToScroll = this.table;
    this.tableScrollSubscription = this.calendarService.onDragTable.pipe(skip(2)).subscribe((leftScroll) => {
      this.currentScroll = leftScroll;
      this.table.nativeElement.scrollTo({left: leftScroll});
    });

    this.tableStateSubscription = this.scrollStore.state$.subscribe((state) => {
      if (!state.canUpdate && !state.press) {
        this.setDatesAfterScroll()
        this.loadData();
      }
      if (!state.canScroll && state.press) {
        if (this.store.state.modal.reservation && this.store.state.modal.reservation.visible) {
          this.popoverClose(this.store.state.modal.reservation);
          this.store.state.count = 0;
        }
      }
      if (state.canScroll && state.press) {
        if (this.store.state.modal.reservation && this.store.state.modal.reservation.visible) {
          this.popoverClose(this.store.state.modal.reservation);
        }
        // this.renderer.addClass(document.body, 'scrolling');
        // this.renderer.removeClass(document.body, 'scrolling');
      }
      if (state.canUpdate && state.press) {
        this.store.setState({selectedCell: null});
        this.cleanReservationHandler();
        if (this.store.state.modal.reservation && this.store.state.modal.reservation.visible) {
          this.popoverClose(this.store.state.modal.reservation);
          this.store.state.count = 0;
        }
        this.renderer.removeClass(document.body, 'hide_badge_backdrop');
      }
    });

    document.addEventListener('mousemove', (e) => {
      if (this.isDragStarted) {
        this.setDraggablePos(e);
      }
    });

  }

  private setDatesAfterScroll() {
    if (this.currentScroll - this.firstScroll > 45) {
      const countAddDays = Math.ceil((this.currentScroll - this.firstScroll - 45) / this.cellWidth);
      const lastDate = moment(this.dates[this.dates.length - 1]);
      const tempStartDateArray: any[] = [];
      for (let i = 1; i <= countAddDays; i++) {
        const tempStartDate = moment(lastDate).add(i, 'day').format('YYYY-MM-DD');
        tempStartDateArray.push(tempStartDate);
      }
      this.dates = [...this.dates.slice(countAddDays, this.dates.length), ...tempStartDateArray];
      this.data.dates = []; // we will load new data
    }
    if (this.currentScroll - this.firstScroll < -45) {
      const countAddDays = Math.ceil(-(this.currentScroll - this.firstScroll + 45) / this.cellWidth);
      const firstDate = moment(this.dates[0]);
      let tempStartDateArray: any[] = [];
      for (let i = 1; i <= countAddDays; i++) {
        const tempStartDate = moment(firstDate).subtract(i, 'day').format('YYYY-MM-DD');
        tempStartDateArray.push(tempStartDate);
      }
      tempStartDateArray = tempStartDateArray.reverse();
      this.dates = [...tempStartDateArray, ...this.dates.slice(0, this.dates.length - countAddDays)];
      this.data.dates = []; // we will load new data
    }
  }

  ngAfterViewInit() {
    // move calendar to current date
    setTimeout(() => {
      this.calendarService.scrollToDate(new Date(), this.data.dates[0]);
    }, 1);
  }

  cleanReservationHandler() {
    this.store.setState({
      newReservationData: {
        from: null,
        to: null,
        room: null,
        visible: false,
        width: this.calendarService.cellWidth - 10,
      }
    });
  }

  popoverClose(data) {
    const roomTypeIndex = this.data.room_types.findIndex(roomType => {
      return roomType.room_type_id === data.roomType.room_type_id;
    });

    if (this.data.room_types[roomTypeIndex]
      .rooms[this.store.state.modal.roomIndex]) {
      this.data
        .room_types[roomTypeIndex]
        .rooms[this.store.state.modal.roomIndex]
        .reservations[this.store.state.modal.reservationIndex]
        .visible = false;
    }

    this.store.state.modal.reservation.visible = false;
    // trigger calendar data change
    this.calendarService.calendarData.next(this.data);
  }

  protected loadData() {
    if (this.dates.length > this.data.dates.length) {
      this.isLoading = true;
      this.calendarService.dateOffset.next({start: this.dates[0], end: this.dates[this.dates.length - 1]});
    }
  }


  protected nextBatch(e, offset) {
    const end = this.viewport.getRenderedRange().end;
    const total = this.viewport.getDataLength();
    if (end === total) {
      let tempDates: string[] = new Array();
      let i = 1;
      const lastDate = moment(offset);
      while (i <= this.batch) {
        tempDates = [...tempDates, lastDate.add(1, 'day').format('YYYY-MM-DD')];
        i++;
      }
      this.dates = [...this.dates, ...tempDates];
    }
  }


  dragStart({leftEdgePos, topEdgePos, draggableClonedEl, event}) {
    this.isDragStarted = true;
    this.leftEdgePos = leftEdgePos;
    this.topEdgePos = topEdgePos;
    this.draggableClonedEl = draggableClonedEl;
    this.setDraggablePos(event);
  }

  dragEnd() {
    this.isDragStarted = false;
    this.leftEdgePos = null;
    this.topEdgePos = null;
    this.draggableClonedEl = null;
  }

  setDraggablePos(e) {
    this.draggableClonedEl.style.left = `${e.clientX - this.leftEdgePos}px`;
    this.draggableClonedEl.style.top = `${e.clientY - this.topEdgePos}px`;
  }

  ngOnDestroy() {
    this.utilsService.unsubscriber(this.tableScrollSubscription);
    this.utilsService.unsubscriber(this.tableStateSubscription);
    this.utilsService.unsubscriber(this.dataSubscription);
  }

  trackByIdx(i) {
    return i;
  }

  trackByIdForRoomTypes(i) {
    return i;
  }

  getTableHeight(data: any): string {
    const typesCount = data.room_types.length;
    let roomsCount = 0;
    data.room_types.forEach(type => {
      roomsCount += type.rooms.length;
    });
    const typesHeight = typesCount * 54;
    const roomsHeight = roomsCount * 38;
    return `${typesHeight + roomsHeight}px`;
  }
}
