import {ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, ViewChild} from '@angular/core';
import _ from 'lodash';
import {SidebarDrawerComponent} from '../drawers/sidebar-drawer/sidebar-drawer.component';
import {NzDrawerService, NzModalService} from 'ng-zorro-antd';
import {LegendDrawerComponent} from '../drawers/legend-drawer/legend-drawer.component';
import {CalendarService} from '../../calendar.service';
import {RoomStorageComponent} from '../modals/room-storage/room-storage.component';
import {ReservActionsComponent} from '../modals/reserv-actions/reserv-actions.component';
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {Calendardata} from '@src/app/core/models/calendardata';
import {Calendarroomtype} from '@src/app/core/models/calendarroomtype';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ReplaySubject} from "rxjs";
import {debounceTime} from "rxjs/operators";

class SimpleChanges {
}

@Component({
  selector: 'app-calendar-sidebar',
  templateUrl: './calendar-sidebar.component.html',
  styleUrls: ['./calendar-sidebar.component.scss', './../../calendar.component.scss']
})
export class CalendarSidebarComponent implements OnInit, OnChanges, OnDestroy {
  @Input() data: Calendardata;
  @ViewChild('searchQuery', {static: true}) searchQuery: ElementRef;
  @ViewChild('roomsElm', {read: ElementRef, static: true}) public roomsElm: ElementRef<any>;

  presearchRoomTypes: any[] = [];
  allExpanded = true;
  isDaySelector = false;
  isPopoverVisible = false;
  navigateToDate = null;
  tableScrolled = 0;
  _roomTypes: Calendarroomtype[];
  inputValue;
  updateAfterChangeState = new ReplaySubject<any>(1);

  hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));

  state: any;

  constructor(
    private drawerService: NzDrawerService,
    private calendarService: CalendarService,
    private modalService: NzModalService,
    private store: CalendarStore,
    private cdref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.updateAfterChangeState.pipe(debounceTime(200)).subscribe((state) => {
      this.state = state;
      this.setIsSelected();
    });

    this.calendarService.onDragTable.subscribe(res => this.tableScrolled = res);
    this.calendarService.onScrollTable.subscribe(res => {
      this.roomsElm.nativeElement.scroll(
        {
          behavior: 'smooth',
          top: this.roomsElm.nativeElement.scrollTop + res
        });
    });

    this.store.setState({expandedRoomTypes: this.data.room_types.map(rt => rt.room_type_id)});
    this._roomTypes = this.data.room_types.map(rt => rt);

    this.store.state$.subscribe((state) => {
      this.updateAfterChangeState.next(state);
    });
    this.cdref.markForCheck();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.state) {
      this.setIsSelected();
    }
  }

  private setIsSelected() {
    this.data.room_types.map(roomType => {
      roomType.isExpanded = this.isExpanded(this.state.expandedRoomTypes, roomType.room_type_id);
      roomType.rooms.map(room => {
        room.isSelectedRoom = this.isSelectedRoom(this.state.selectedRoom, roomType.room_type_id, room.room_id)
      });
    });
    this.cdref.markForCheck();
  }

  openMenuDrawer(): void {
    const menuDrawer = this.drawerService.create<SidebarDrawerComponent, { data: Calendardata, currentDate: Date }, string>({
      nzTitle: null,
      nzWidth: 280,
      nzBodyStyle: {padding: '0px'},
      nzClosable: false,
      nzPlacement: 'left',
      nzContent: SidebarDrawerComponent,
      nzContentParams: {
        data: this.data,
        currentDate: this.navigateToDate,
      },
    });
  }

  openLegendDrawer(): void {
    const menuDrawer = this.drawerService.create<LegendDrawerComponent, {}, string>({
      nzTitle: null,
      nzWidth: 280,
      nzBodyStyle: {padding: '0px'},
      nzClosable: false,
      nzPlacement: 'left',
      nzContent: LegendDrawerComponent,
    });
  }

  openRoomStorageModal(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: RoomStorageComponent
    });

    this.isPopoverVisible = false;
  }

  openBlockDateModal(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Блокировка даты',
        blockDate: true,
      },
    });

    this.isPopoverVisible = false;
  }

  openOutOfServiceModal(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Номер временно не работает',
      },
    });

    this.isPopoverVisible = false;
  }

  selectRoomHandler(roomTypeId, roomId) {
    if (this.store.state.selectedRoom === `${roomTypeId}-${roomId}`) {
      this.store.setState({selectedRoom: null});
    } else {
      this.calendarService.scrollToRoom(roomId);
      this.store.setState({selectedRoom: `${roomTypeId}-${roomId}`});
    }
  }

  roomTypeCollapseHandler(roomTypeId, index) {
    this.store.setState({selectedRoom: null});
    this.data.room_types[index].isExpanded = !this.data.room_types[index].isExpanded;

    if (_.includes(this.store.state.expandedRoomTypes, roomTypeId)) {
      this.store.setState({
        expandedRoomTypes: this.store.state.expandedRoomTypes.filter(item => item !== roomTypeId),
      });
    } else {
      this.store.setState({
        expandedRoomTypes: this.store.state.expandedRoomTypes.concat(roomTypeId),
      });
    }
  }

  allCollapseHandler() {
    this.allExpanded = !this.allExpanded;
    if (this.store.state.expandedRoomTypes.length > 0) {
      this.store.setState({expandedRoomTypes: []});
    } else {
      this.store.setState({expandedRoomTypes: this.data.room_types.map(rt => rt.room_type_id)});
    }
  }

  isArray(arr) {
    return arr instanceof Array;
  }

  isSelectedRoom(...args: any[]): boolean | void {
    return this.calendarService.isSelectedRoom(...args);
  }

  isExpanded(...args: any[]): boolean | void {
    return this.calendarService.isExpanded(...args);
  }

  fastSearchRooms(roomTypeId, room) {
    this.store.setState({searchRoomQuery: room.name});
    this.selectRoomHandler(roomTypeId, room.room_id);
  }

  searchRooms(query) {
    if (query === '') {
      return;
    }
    const qry = query.toLowerCase();
    let result;

    this.store.setState({searchRoomQuery: qry});

    this._roomTypes.forEach((roomType) => {
      if (result) {
        return;
      }

      result = roomType.rooms.find((room) => {
        return room.name.toLowerCase().includes(qry);
      });

      result = {roomId: result.room_id, roomTypeId: roomType.room_type_id};
    });

    if (result) {
      this.selectRoomHandler(result.roomTypeId, result.roomId);
    }
  }

  presearchRooms(query) {
    // highlight the result
    if (query && query.roomTypeId) {
      this.fastSearchRooms(query.roomTypeId, query);
    } else {
      if (query === '') {
        return [];
      }
      const qry = query.toLowerCase();

      this.presearchRoomTypes = this._roomTypes.map((roomType) => {
        if (roomType.short_name.toLowerCase().includes(qry)) {
          return roomType.rooms.map(room => ({...room, roomTypeId: roomType.room_type_id}));
        } else {
          const filtered = roomType.rooms.filter((room) => {
            return room.name.toLowerCase().includes(qry);
          });
          return filtered.map(room => ({...room, roomTypeId: roomType.room_type_id}));
        }
      });
    }
  }

  dayNavigatorHandler() {
    this.isDaySelector = !this.isDaySelector;
  }

  dayNavigatorModalHandler(date) {
    this.navigateToDate = date;
    this.calendarService.scrollToDate(date, this.data.dates[0]);
    this.isDaySelector = false;
  }

  focusInput() {
    setTimeout(() => {
      this.searchQuery.nativeElement.focus();
    }, 400);
  }

  getReservationCreateRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.hotel.id}/${RoutesEnum.RESERVATION}/${RoutesEnum.CREATE}`;
  }

  ngOnDestroy() {
  }
}
