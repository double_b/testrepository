import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {CalendarService} from '../../calendar.service';

@Component({
  selector: 'app-reservation-split',
  templateUrl: './reservation-split.component.html',
  styleUrls: ['./reservation-split.component.scss']
})
export class ReservationSplitComponent implements OnInit {
  @Input() guest: any;
  @Input() period: any;
  @Input() roomTypes: any;

  datesSpan = [];
  totalCost = 0;
  roomInfo;

  // private _selectedRoomType;
  // private _init;

  // selectedRoom;
  // selectedCount;
  // checked = false;

  constructor(
    private calendarService: CalendarService,
  ) {
  }

  ngOnInit() {
    this.setDateSpan(this.period.checkin);
  }

  setDateSpan(start, end = null) {
    let startMoment;
    let endMoment;
    if (end === null) {
      startMoment = moment(start);
      endMoment = moment(start).add(6, 'days');
    } else {
      startMoment = moment(start);
      endMoment = moment(start);
    }

    this.datesSpan = [];

    const dates = this.calendarService.getAllDatesBetween(startMoment, endMoment);
    const prices = this.calendarService.getPrices(null, null, startMoment, endMoment, null, null);
    const availability = this.calendarService.getRoomsAvailability(null, null, startMoment, endMoment);
    const reservedDates = this.calendarService.getAllDatesBetween(moment(this.period.checkin), moment(this.period.checkout));
    for (let i = 0; i < dates.length; i++) {
      let status = 'available';
      if (reservedDates.includes(dates[i])) {
        status = 'self';
      } else if (availability[i].rooms <= (availability[i].booked + availability[i].closed)) {
        status = 'unavailable';
      }
      this.datesSpan.push({
        date: moment(dates[i]),
        price: prices[i].price,
        status,
      });
    }
  }
}
