import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-day-col',
  templateUrl: './day-col.component.html',
  styleUrls: ['./day-col.component.scss']
})
export class DayColComponent implements OnInit {

  @Input() day;

  constructor() { }

  ngOnInit() {
  }

}
