import {Component, ElementRef, ViewChild, OnInit, AfterViewInit, OnDestroy, Renderer2, ChangeDetectionStrategy} from '@angular/core';
import {CalendarService} from '../../calendar.service';
import {UtilsService} from '../../../../core/services/utils/utils.service';
import {ScrollStore} from '@src/app/core/store/scroll.store';
import {Subscription} from 'rxjs';
import _ from 'lodash';

@Component({
  selector: 'app-scroll-container',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './scroll-container.component.html',
})
export class ScrollContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('contentRef', { static: true }) contentRef: ElementRef;

  private onMouseMoveListener: Function;
  private onMouseUpListener: Function;
  private onMouseDownListener: Function;
  dragingSubscription: Subscription;

  constructor(
    private calendarService: CalendarService,
    private utilsService: UtilsService,
    public scrollStore: ScrollStore,
    private renderer: Renderer2,
  ) {
  }

  ngOnInit() {
    this.dragingSubscription = this.calendarService.dragingBooking.subscribe(res => {
      this.scrollStore.setState({ canScroll: res });
    });
  }

  ngAfterViewInit() {
    this.onMouseDownListener = this.renderer.listen(this.contentRef.nativeElement, 'mousedown', this.tablePressHandler.bind(this));

  }

  ngOnDestroy() {
    this.utilsService.unsubscriber(this.dragingSubscription);
    if (this.onMouseDownListener) {
      this.onMouseDownListener = this.onMouseDownListener();
    }
  }

  tableUnPressHandler(event) {
    if (this.scrollStore.state.press) {
      this.scrollStore.setState({  press: false,
        canUpdate: false});
      this._stopGlobalListening();
    }
  }

  tablePressHandler(event) {
    const isTouchEvent = event.type === 'touchstart';
    this._startGlobalListening(isTouchEvent);
    this.scrollStore.setState({ press: true,
      prevX: event.screenX,
      tableY: event.screenY,
      clientX: event.clientX});
  }

  tableScrollHandler(event) {
    if (this.scrollStore.state.press === true && !this.scrollStore.state.canScroll) {
        const leftScroll = this.calendarService.tableToScroll.nativeElement.scrollLeft - event.clientX + this.scrollStore.state.clientX;
        this.calendarService.onDragTable.next(leftScroll);
        this.scrollStore.setState({canUpdate: true, clientX: event.clientX});
    }
  }

  private _startGlobalListening(isTouchEvent: boolean) {
    if (!this.onMouseMoveListener) {
      const eventName = isTouchEvent ? 'touchmove' : 'mousemove';
      this.onMouseMoveListener = this.renderer.listen('document', eventName, this.tableScrollHandler.bind(this));
    }

    if (!this.onMouseUpListener) {
      const eventName = isTouchEvent ? 'touchend' : 'mouseup';
      this.onMouseUpListener = this.renderer.listen('document', eventName, this.tableUnPressHandler.bind(this));
    }
  }

  private _stopGlobalListening() {
    if (this.onMouseMoveListener) {
      this.onMouseMoveListener = this.onMouseMoveListener();
    }

    if (this.onMouseUpListener) {
      this.onMouseUpListener = this.onMouseUpListener();
    }
  }
}
