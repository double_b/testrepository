import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2, SimpleChanges,
  ViewChild,
} from '@angular/core';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {BehaviorSubject, Observable, ReplaySubject, Subscription} from 'rxjs';
import {utils} from 'sortablejs';
import * as _ from 'lodash';
import {RoomStorageComponent} from '../modals/room-storage/room-storage.component';
import {ReservActionsComponent} from '../modals/reserv-actions/reserv-actions.component';
import {ReservWarningComponent} from '../modals/reserv-warning/reserv-warning.component';
import {CalendarService} from '../../calendar.service';
import {CalendarStore} from '@src/app/core/store/calendar.store';

import * as moment from 'moment';
import {Calendarroomtype} from '@src/app/core/models/calendarroomtype';
import {Calendarroom} from '@src/app/core/models/calendarroom';
import {CalendarReservation} from '@src/app/core/models/calendarreservation';
import {debounceTime} from "rxjs/operators";
import {RoutesEnum} from "@app/core/constants/routes.enum";
import {KeysEnum} from "@app/core/constants/keys.enum";
import {BookingService} from "@app/layouts/reservation/booking/booking.service";

@Component({
  selector: 'app-room-row',
  templateUrl: './room-row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./room-row.component.scss', './../../calendar.component.scss']
})
export class RoomRowComponent implements OnInit, OnDestroy, OnChanges {
  @Input() data: Observable<any>;
  _data;
  @Input() days = [];
  @Input() day: string;
  @Input() expanded = false;
  @Input() isEmpty = false;
  // @Input() roomType: Calendarroomtype;
  @Output() onDragStart: EventEmitter<any> = new EventEmitter();
  @Output() onDragEnd: EventEmitter<any> = new EventEmitter();

  @ViewChild('daysElm', {read: ElementRef, static: true}) public daysElm: ElementRef<any>;
  @ViewChild('priceInput', {read: ElementRef}) public priceInput: ElementRef<any>;

  @Input() set roomTypeInput(value: Calendarroomtype) {
    this.roomType = value;
  }

  public get calendarServiceCellWidth(): number {
    return this.calendarService.cellWidth;
  }

  scrollIndex: any;
  hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));
  roomType: Calendarroomtype;
  tempPrice = <any> {};
  emptyBadge: any;
  selectedDate: Date;
  openedReservCount: number;
  isPopoverVisible = false;
  onNewReservationClickElm = false;
  outSideClickSubscription: Subscription;
  reservInfoModal: any;
  reservInfoModalVisible = false;
  reservWarningModal: any;
  reservWarningModalVisible = false;
  sourceCell: any;
  draggableStartLeft: number;
  draggableStartTop: number;
  draggableWidth: number;
  draggableHeight: number;
  draggableCenterX: number;
  draggableCenterY: number;
  draggableClonedEl: any;
  draggableEl: any;
  isDragStarted: boolean;
  fromOffset: any;
  selectedReserv: any;
  daysIndex = 0;
  newReservationData = {
    from: null,
    to: null,
    room: null,
    visible: false,
    width: (this.calendarService.cellWidth - 10),
  };
  targetCellData: any;
  preselectedData = {
    preSelectedStart: null,
    preSelectedEnd: null,
    room: null,
  };
  isToday = false;
  isSelectedDay = false;
  roomAvailability: any = '';
  roomPrice: any = '';
  isExpandedCheck: any = false;
  emptyBadgeCompareDates = false;
  subscriptionStartSetSelectedRoom = new ReplaySubject<any>(1);
  copyRoomType: any = {};
  copyRoomTypeForEmptyBadge: any = {};
  isNewReservation = false;
  previouslyState: any;
  data$ = new BehaviorSubject(null);

  formatterDollar = (value: number) => `$ ${value}`;
  parserDollar = (value: string) => value.replace('$ ', '');

  constructor(
    private renderer: Renderer2,
    public calendarService: CalendarService,
    private nzMessageService: NzMessageService,
    private modalService: NzModalService,
    public store: CalendarStore,
    private cdref: ChangeDetectorRef,
    private bookingService: BookingService
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setBookingByDate();
    this.isToday = this.compareDates(this.calendarService.todayString, this.day);
    if (this._data) {
      this.roomAvailability = this.getRoomAvailability(this.day);
      this.roomPrice = this.getRoomPrice(this.day);
    }
  }

  ngOnInit() {
    this.subscriptionStartSetSelectedRoom.subscribe((state) => {
      // здесь устанавливаются данные, которые раньше были прописаны в get function
      // так как все передается через 1 сервис я проверяю изменения состояний чтобы понять нужно ли фронтенду что-то перерисовать
      if (this.previouslyState) {
        if (this.previouslyState.selectedRoom !== state.selectedRoom) {
          this.setIsSelectedRoom(state);
        }

        if (this.previouslyState.selectedCell !== state.selectedCell) {
          this.setIsSelectedCell(state);
        }

        if (this.previouslyState.expandedRoomTypes !== state.expandedRoomTypes) {
          this.isExpandedCheck = this.isExpanded(state.expandedRoomTypes, this.roomType.room_type_id);
          this.cdref.markForCheck();
        }

        if (this.previouslyState.selectedDate !== state.selectedDate) {
          this.selectedDate = state.selectedDate;
          this.cdref.markForCheck();
        }

        if (this.previouslyState.selectedDate !== this.selectedDate) {
          this.isSelectedDay = this.compareDates(this.selectedDate, this.day);
        }
      } else {
        this.selectedDate = state.selectedDate;
        this.setIsSelectedCell(state);
        this.setIsSelectedRoom(state);
        this.isExpandedCheck = this.isExpanded(state.expandedRoomTypes, this.roomType.room_type_id);
        this.isSelectedDay = this.compareDates(this.selectedDate, this.day);
      }

      this.setEmptyBadgeCompareDates();
      this.previouslyState = Object.assign({}, state);
    });

    this.store.state$.subscribe((state) => {
      this.emptyBadge = state.selectedBooking;
      this.openedReservCount = state.openedReservCount;
      this.newReservationData = state.newReservationData;
      if (this.newReservationData.from && this.newReservationData.to) {
        this.setNewReservation();
      } else if (this.isNewReservation) {
        this.setNewReservation();
        this.isNewReservation = false;
      }

      this.subscriptionStartSetSelectedRoom.next(state);
    });

    this.calendarService.calendarData.subscribe(data => {
      this._data = data;
      this.cdref.markForCheck();
    });

    document.addEventListener('click', () => {
      if (this.newReservationData.room) {
        this.cleanReservationHandler();

        this.preselectedData = {
          ...this.preselectedData,
          preSelectedStart: null,
          room: null,
        };
      }
    });

    document.addEventListener('mouseup', (e) => {
      if (this.isDragStarted) {
        this.dragEnd(e);
      }
    });

    this.isToday = this.compareDates(this.calendarService.todayString, this.day);
    this.roomAvailability = this.getRoomAvailability(this.day);
    this.roomPrice = this.getRoomPrice(this.day);
  }

  private setNewReservation() {
    this.copyRoomTypeForEmptyBadge.rooms = this.roomType.rooms.map(room => {
      let newRoom = Object.assign({}, room);
      newRoom.condition = room === this.newReservationData.room && this.day === this.newReservationData.from && this.newReservationData.to;
      if (newRoom.condition) {
        this.isNewReservation = true;
      }
      return newRoom;
    });
    this.cdref.markForCheck();
  }

  private setIsSelectedRoom(state) {
    this.roomType.rooms.map(room => {
      room.isSelectedRoom = this.isSelectedRoom(state.selectedRoom, this.roomType.room_type_id, room.room_id) && !(this.newReservationData.from && this.newReservationData.to);
    });
    this.cdref.markForCheck();
  }

  private setIsSelectedCell(state) {
    // setTimeout костыль, иначе в переменную всегла записываются одни и те же данны, не понял почему
    setTimeout(() => {
      this.roomType.rooms.map(room => {
        room.isSelectedCell = this.isSelectedCell(state.selectedCell, this.roomType.room_type_id, room.room_id, this.day) && !(this.newReservationData.from && this.newReservationData.to);
      });
      this.cdref.markForCheck();
    }, 0)
  }

  private setBookingByDate() {
    this.copyRoomType.rooms = this.roomType.rooms.map(room => {
      let newRoom = Object.assign({}, room);
      const index = newRoom.reservations.findIndex(el => {
        return el.checkin == this.day;
      });
      newRoom.bookingByDate = newRoom.reservations[index];
      if (newRoom.bookingByDate) {
        newRoom.className = this.getClassName(newRoom.bookingByDate);
      }
      return newRoom;
    });
    this.cdref.markForCheck();
  }

  private setEmptyBadgeCompareDates() {
    this.emptyBadgeCompareDates = this.compareDates(this.day, this.emptyBadge && this.emptyBadge.booking && this.emptyBadge.booking.checkin) && this.emptyBadge && this.emptyBadge.booking;
  }

  dragStart(reserv, e) {
    if (reserv.booking.type == 'booking') {
      document.body.style.cursor = 'pointer';
      this.isDragStarted = true;
      this.selectedReserv = reserv;
      this.calendarService.dragingBooking.next(true);
      this.sourceCell = utils.closest(e.target, '.row__cell');
      this.draggableEl = utils.closest(e.target, '.badge');
      this.fromOffset = this.draggableEl.getBoundingClientRect();
      this.draggableWidth = parseInt(getComputedStyle(this.draggableEl).width, 10);
      this.draggableHeight = parseInt(getComputedStyle(this.draggableEl).height, 10);
      this.draggableStartLeft = e.clientX - this.fromOffset.left - 15;
      this.draggableStartTop = e.clientY - this.fromOffset.top;
      this.draggableCenterX = (e.clientX - this.fromOffset.left + this.fromOffset.right) / 2;
      this.draggableCenterY = (e.clientY - this.fromOffset.top + this.fromOffset.bottom) / 2;
      this.draggableClonedEl = this.draggableEl.cloneNode(true);
      this.draggableEl.classList.add('sortable-ghost');
      document.body.appendChild(this.draggableClonedEl);
      this.draggableClonedEl.classList.add('sortable-drag');
      this.draggableClonedEl.style.position = 'fixed';
      this.draggableClonedEl.style.zIndex = '10000';
      this.draggableClonedEl.style.pointerEvents = 'none';

      this.onDragStart.emit({
        event: e,
        draggableClonedEl: this.draggableClonedEl,
        leftEdgePos: e.clientX - this.fromOffset.left - 15,
        topEdgePos: e.clientY - this.fromOffset.top,
      });
    }
  }

  dragEnd(e) {
    const revert = () => {
      this.draggableEl.classList.remove('sortable-ghost');
      this.draggableClonedEl.classList.remove('sortable-drag');
      if (this.draggableClonedEl.parentNode) {
        this.draggableClonedEl.parentNode.removeChild(this.draggableClonedEl);
      }
      this.onDragEnd.emit();
    };
    const centerX = (e.clientX - this.fromOffset.left + this.fromOffset.right) / 2;
    const centerY = (e.clientY - this.fromOffset.top + this.fromOffset.bottom) / 2;
    document.body.style.cursor = 'auto';

    this.calendarService.dragingBooking.next(false);
    this.isDragStarted = false;

    const leftEdgePos = e.clientX - this.draggableStartLeft;
    const topEdgePos = e.clientY - this.draggableStartTop;
    const bottomEdgePos = topEdgePos + this.draggableHeight;
    const centerLineY = (topEdgePos + bottomEdgePos) / 2;
    const from = this.draggableEl.parentNode;

    const targetCell = this.getElementFromPoint({
      points: {
        x: leftEdgePos,
        y: centerLineY
      },
      selector: 'row__cell'
    });

    if (!targetCell) {
      revert();
      return;
    }

    this.targetCellData = {
      date: targetCell.getAttribute('data-date'),
      room: {
        room_id: targetCell.getAttribute('data-room-id'),
        name: targetCell.getAttribute('data-room-name')
      },
      roomType: {
        room_type_id: targetCell.getAttribute('data-room-type-id'),
        short_name: targetCell.getAttribute('data-room-type-name')
      },
    };

    const ocupiedCells = Math.floor(this.draggableWidth / this.calendarService.cellWidth) + 1;

    for (let i = 0; i < ocupiedCells; i++) {
      const xPoint = leftEdgePos + this.calendarService.cellWidth * i;
      const xPoints = i === 0 ? xPoint : xPoint + 10;
      const cell = utils.closest(document.elementFromPoint(xPoints, centerLineY), '.row__cell');

      if (cell && cell.children.length > 0 && cell.children[0] !== from) {
        revert();
        return;
      }
    }


    if (targetCell === utils.closest(from, '.row__cell')) {
      if (centerX === this.draggableCenterX && centerY === this.draggableCenterY && +this.selectedReserv.roomType.room_type_id
        === +this.roomType.room_type_id) {
        setTimeout(() => {
          const roomIndex = this.roomType.rooms.findIndex(room => {
            return room.room_id === this.selectedReserv.room.room_id;
          });
          const reservationIndex = this.roomType.rooms[roomIndex].reservations.findIndex(reservation => {
            return reservation.reservation_room_id === this.selectedReserv.booking.reservation_room_id;
          });
          this.store.state.modal.roomIndex = roomIndex;
          this.store.state.modal.reservationIndex = reservationIndex;
          if (this.store.state.modal.reservation) {
            if (this.selectedReserv.booking.reservation_room_id === this.store.state.modal.reservation.booking.reservation_room_id) {
              this.store.state.count = this.store.state.count + 1;
            } else {
              this.store.state.count = 0;
              this.store.state.count = this.store.state.count + 1;
            }
          } else {
            this.store.state.count = 0;
            this.store.state.count = this.store.state.count + 1;
          }
          if (this.store.state.count <= 1) {
            this.roomType.rooms[roomIndex].reservations[reservationIndex].visible = true;
          } else {
            this.store.state.count = 0;
          }
          // add class to hide backdrop for 1 click popover open #su-73
          this.renderer.addClass(document.body, 'hide_badge_backdrop');
          // to close reservation popover when info popover is opened
          this.store.setState({selectedCell: null});
          this.cleanReservationHandler();
          this.cdref.detectChanges();
        }, 1);
      }
    } else {
      const newReservData = this.selectedReserv.booking;
      // assign new dates to dragged badge
      // days difference for new assignment
      const days = moment(this.selectedReserv.booking.checkout, 'YYYY-MM-DD')
        .diff(moment(this.selectedReserv.booking.checkin, 'YYYY-MM-DD'), 'days');
      newReservData.newCheckin = this.targetCellData.date;
      newReservData.newCheckout = moment(newReservData.newCheckin, 'YYYY-MM-DD').add(days, 'days').format('YYYY-MM-DD');

      const values = {
        reservationRoomId: this.selectedReserv.booking.reservation_room_id, roomId: this.targetCellData.room.room_id,
        data: {checkin: newReservData.newCheckin, checkout: newReservData.newCheckout}
      };
      this.calendarService.moveReservation(values).subscribe(res => {
          if (res.code === 2001) {
            // old values
            res.old = {
              room_id: this.selectedReserv.room.room_id,
              checkin: this.selectedReserv.booking.checkin,
              checkout: this.selectedReserv.booking.checkout
            };

            // new values
            res.new = {
              room_id: this.targetCellData.room.room_id,
              checkin: newReservData.newCheckin,
              checkout: newReservData.newCheckout,
            };
            this.openReservWarning(res, values);
          } else {
            this.updateCalendarData(this.selectedReserv, this.targetCellData);
          }
        },
        error => {
          this.sourceCell.appendChild(this.draggableEl.parentNode);
          revert();
          this.nzMessageService.error('Ошибка');
        });
    }

    revert();
    targetCell.appendChild(from);
    this.calendarService.dragingBooking.next(false);
  }

  updateCalendarData(selectedReserv, targetCellData) {
    let newReservation;
    this._data.room_types.forEach(roomType => {
      // check dragged badge selectedRoomType
      if (+roomType.room_type_id === +selectedReserv.roomType.room_type_id) {
        roomType.rooms.forEach(room => {
          // check dragged badge room
          if (+room.room_id === +selectedReserv.room.room_id) {
            // get dragged badge reservation from selectedRoomType
            newReservation = room.reservations.find(reservation => {
              return reservation.reservation_room_id === selectedReserv.booking.reservation_room_id;
            });
            // remove dragged badge reservation from selectedRoomType
            room.reservations = room.reservations.filter(reservation => {
              return reservation.reservation_room_id !== selectedReserv.booking.reservation_room_id;
            });
          }
        });
      }
    });
    if (newReservation) {
      this._data.room_types.forEach(roomType => {
        // check dragged badge target selectedRoomType
        if (+roomType.room_type_id === +targetCellData.roomType.room_type_id) {
          roomType.rooms.forEach(room => {
            // check dragged badge target room
            if (+room.room_id === +targetCellData.room.room_id) {
              // assign new dates to dragged badge
              // days difference for new assignment
              const days = moment(this.selectedReserv.booking.checkout, 'YYYY-MM-DD')
                .diff(moment(this.selectedReserv.booking.checkin, 'YYYY-MM-DD'), 'days');
              newReservation.checkin = targetCellData.date;
              const nextDate = moment(newReservation.checkin, 'YYYY-MM-DD').add(days, 'days').format('YYYY-MM-DD');
              newReservation.checkout = nextDate;
              // push new reservation
              room.reservations = [...room.reservations, newReservation];
            }
          });
        }
      });
      // trigger calendar data change
      this.calendarService.calendarData.next(this._data);
    }
  }

  ngOnDestroy() {
    // this.outSideClickSubscription.unsubscribe();
    this.store.state.count = 0;
  }

  private getElementFromPoint({points: {x, y}, selector}): any {
    let targetCell: any;

    for (const possibleTarget of document.elementsFromPoint(x, y)) {
      if (possibleTarget.classList.contains(selector)) {
        targetCell = possibleTarget;
        break;
      }
    }

    return targetCell;
  }

  compareDates(a, b): boolean {
    return this.calendarService.compareDates(a, b);
  }

  isSelectedRoom(...args): any {
    return this.calendarService.isSelectedRoom(...args);
  }

  isExpanded(...args) {
    return this.calendarService.isExpanded(...args);
  }

  isSelectedCell(current, roomTypeId, roomId, day) {
    return current === `${roomTypeId}-${roomId}-${day}`;
  }

  selectPrice() {
    setTimeout(() => {
      this.priceInput.nativeElement.querySelector('input').select();
    }, 0);
  }

  swapPriceViewHandler(price) {
    if (price === null) {
      price = 0;
    }
    this.tempPrice.tepm = price;
    this.tempPrice.inputView = !this.tempPrice.inputView;
  }

  savePriceHandler(day) {
    this.daysIndex = this._data.dates.indexOf(day);
    this.roomType.prices[this.daysIndex] = this.tempPrice.tepm;
    this.tempPrice.inputView = !this.tempPrice.inputView;
  }

  openReservInfo(reservation) {
    /*
        if (!reservation.open) {
          reservation.open = true;
          reservation.drowerVisible = true;
          this.reservInfoModalVisible = true;
          this.reservInfoModal = this.modalService.create({
            nzWidth: 702,
            nzNoAnimation: true,
            nzBodyStyle: { padding: 0 },
            nzFooter: null,
            nzContent: ReservInfoComponent,
            nzComponentParams: {
              data: reservation
            }
          });

          this.reservInfoModal.afterClose.subscribe(() => {
            this.reservInfoModalVisible = false;
            reservation.drowerVisible = true;
            reservation.open = false;
          });
        }*/

    /*const openedAlready = this.rooms[reservation.index]['visible'];
    this.rooms.map(room => {
      room.visible = false;
    });
    if (!openedAlready) {
      this.rooms[reservation.index]['visible'] = !this.rooms[reservation.index]['visible']
    }*/
  }

  popoverClose(data) {
    const roomTypeIndex = this._data.room_types.findIndex(roomType => {
      return roomType.room_type_id === data.roomType.room_type_id;
    });
    const roomIndex = this._data.room_types[roomTypeIndex].rooms.findIndex(room => {
      return room.room_id === data.room.room_id;
    });
    const reservationIndex = this._data.room_types[roomTypeIndex].rooms[roomIndex].reservations.findIndex(reservation => {
      return reservation.reservation_room_id === data.booking.reservation_room_id;
    });
    this._data.room_types[roomTypeIndex].rooms[roomIndex].reservations[reservationIndex].visible = false;
    this.store.state.modal.reservation.visible = false;
    // trigger calendar data change
    this.calendarService.calendarData.next(this._data);
  }

  openReservWarning(reservation, values) {
    if (!reservation.open) {
      reservation.open = true;
      reservation.drowerVisible = true;
      this.reservWarningModalVisible = true;
      this.reservWarningModal = this.modalService.create({
        nzWidth: 600,
        nzNoAnimation: true,
        nzBodyStyle: {padding: 0},
        nzFooter: null,
        nzContent: ReservWarningComponent,
        nzOnCancel: () =>
          new Promise((resolve, reject) => {
            this.sourceCell.appendChild(this.draggableEl.parentNode);
            resolve();
          }).catch((err) => console.log(err)),

        nzOnOk: () =>
          new Promise((resolve, reject) => {
            this.updateCalendarData(this.selectedReserv, this.targetCellData);
            resolve();
          }).catch((err) => console.log(err)),
        nzComponentParams: {
          res: reservation,
          data: values,
        }
      });

      this.reservWarningModal.afterClose.subscribe((result) => {
        this.reservWarningModalVisible = false;
        reservation.drowerVisible = true;
        reservation.open = false;
      });
    }
  }

  openRoomStorageModal(data: any): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: RoomStorageComponent,
      nzComponentParams: {data},
    });

    this.isPopoverVisible = false;
  }

  openBlockDatesModal(data: any): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Блокировка даты',
        blockDate: true,
        data,
      },
    });

    this.isPopoverVisible = false;
  }

  openOutOfServiceModal(data: any): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Номер не работает',
        outOfService: true,
        data,
      },
    });

    this.isPopoverVisible = false;
  }

  cleanReservationHandler() {
    this.newReservationData = {
      from: null,
      to: null,
      room: null,
      visible: false,
      width: this.calendarService.cellWidth - 10,
    };
    this.store.setState({newReservationData: this.newReservationData});
  }

  onNewReservationClick(e) {
    e.stopPropagation();
  }

  formatDate(day) {
    return moment(day, 'YYYY-MM-DD').format('MMM DD, YYYY');
  }

  newReservationHandler(e, day, newRoom, roomTypeId) {
    e.stopPropagation();

    const clean = () => {
      this.store.setState({selectedCell: null});
      this.cleanReservationHandler();
      this.renderer.removeClass(document.body, 'hide_badge_backdrop');
    };

    if (!this.reservInfoModalVisible && !this.onNewReservationClickElm) {
      this.preselectedData = {
        ...this.preselectedData,
        preSelectedStart: day,
      };

      // check if end before start
      if (this.newReservationData.from && this.calendarService.isBigger(this.newReservationData.from, day)) {
        clean();
      }

      // Clean selected if change newReservationData
      if (
        this.newReservationData.room
        && this.newReservationData.room !== newRoom
        || this.newReservationData.from
        && this.newReservationData.to
      ) {
        clean();
      }

      // Verify if can set toDate
      if (this.newReservationData.from) {
        let canSetTo = true;
        const reservationsDates = this.calendarService.getAllDatesBetween(this.newReservationData.from, day);

        this.newReservationData.room.reservations.forEach(booking => {
          if (booking.checkin === this.preselectedData.preSelectedStart && !this.newReservationData.from) {
            canSetTo = false;
          }

          if (this.newReservationData.from && _.includes(reservationsDates, booking.checkin)) {
            canSetTo = false;
          }
        });

        if (canSetTo) {
          if (!this.newReservationData.to) {
            this.preselectedData = {
              ...this.preselectedData,
              room: null,
              preSelectedStart: null,
            };
            // add class to hide backdrop for correct cell selection #su-35
            this.renderer.addClass(document.body, 'hide_badge_backdrop');
            this.newReservationData = {
              ...this.newReservationData,
              to: day,
              visible: true,
              width: (this.calendarService.getDaysBetween(this.newReservationData.from, day) * this.calendarService.cellWidth) - 10,
            };
            this.store.setState({newReservationData: this.newReservationData});
          } else {
            clean();
          }
        } else {
          this.nzMessageService.error('reservation already present');
          clean();
        }

      } else {
        if (!this.calendarService.getBookingByDate(day, newRoom)) {
          this.store.setState({selectedCell: `${roomTypeId}-${newRoom.room_id}-${day}`});
          this.newReservationData = {
            ...this.newReservationData,
            from: day,
            room: newRoom,
          };
          this.store.setState({newReservationData: this.newReservationData});
        }
      }
    }

    this.onNewReservationClickElm = false;
  }

  getDaysBetween(data: any): number {
    return this.calendarService.getDaysBetween(data.from, data.to);
  }

  getNightsPlural(newReservationData: any): string {
    const daysBetween = this.getDaysBetween(newReservationData);
    return daysBetween < 5 ? 'Ночи' : 'Ночей';
  }

  getBookingByDate(date: any, room: Calendarroom): CalendarReservation | undefined {
    return this.calendarService.getBookingByDate(date, room);
  }

  /**
   * Get room availability from data based on date
   * @param date
   */
  getRoomAvailability(date: string) {
    this.daysIndex = this._data.dates.indexOf(date);
    return this.roomType.availability[this.daysIndex];
  }

  /**
   * Get room price from data based on date
   * @param date
   */
  getRoomPrice(date: string) {
    this.daysIndex = this._data.dates.indexOf(date);
    return this.roomType.prices[this.daysIndex];
  }

  /**
   * Get reservation popover class name
   * @param booking
   * - Когда гость еще не заехал  - цвет должно быть синим guest_arrived_at === null
   * - Когда гость проживает (заехал но еще не выехал) - цвет должно быть зеленым guest_arrived_at != null, guest_left_at === null
   * - Когда  выехал  - цвет должно быть серым guest_left_at != null
   */

  getClassName(booking) {
    let className: string;
    className = 'resides';

    if (!booking.guest_arrived_at) {
      className = 'awaited';
    }
    if (booking.guest_left_at) {
      className = 'left';
    }
    return `${booking.type}${booking.type === 'booking' ? `__${className}` : ''}`;
  }

  identify(index, item){
    return item.room_id;
  }

  get runCHangeDetector() {
    console.log('checking');
    return;
  }

  public get getNewReservationLink() {
    return `/${RoutesEnum.HOTEL}/${this.hotel.id}/${RoutesEnum.RESERVATION}/${RoutesEnum.CREATE}`;
  }

  setFilterForReservation(): void {
    const searchQuery = {
      checkIn: new Date(this.newReservationData.from),
      checkOut: new Date(this.newReservationData.to),
      rate: null,
      roomsType: null,
      source: {id: 1, children: []}
    }
    this.bookingService.searchQuery.next(searchQuery);
  }

  // startViewScroll(i) {
  //   document.addEventListener('scroll', this.scroll, true);
  //   this.scrollIndex = i;
  // }
  //
  // scroll = (event): void => {
  //   console.log(this.element);
  //   const {x, y} = this.element.nativeElement.getBoundingClientRect();
  //   console.log(x, y);
  //   console.log(this.element.nativeElement.offsetHeight);
  //   if (y < 100) {
  //     console.log(this.copyRoomType.rooms[this.scrollIndex].bookingByDate);
  //     this.popoverClose(this.copyRoomType.rooms[this.scrollIndex].bookingByDate);
  //   }
  // };
}
