import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnChanges, SimpleChanges,
} from '@angular/core';
import { CalendarService } from '../../calendar.service';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { SidebarDrawerComponent } from '../drawers/sidebar-drawer/sidebar-drawer.component';
import { NzDrawerService } from 'ng-zorro-antd';
import { CalendarStore } from '@src/app/core/store/calendar.store';
import { NumbersEnum } from "@app/core/constants/numbers.enum";

@Component({
  selector: 'app-days-bar',
  templateUrl: './days-bar.component.html',
  styleUrls: ['./days-bar.component.scss', './../../calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaysBarComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() dates: any[] = [];
  @Input() position = 'top';

  @ViewChild('topBar', { read: ElementRef, static: true }) public topBar: ElementRef<any>;
  @ViewChild('monthBar', { read: ElementRef }) public monthBar: ElementRef<any>;
  days_index = 0;
  dataSubscription: Subscription;
  tableScrollSubscription: Subscription;
  months: any[] = [];
  monthMargin = 0;
  daysCount: any = 0;
  notAssigned: any[] = [];
  roomsBooked: any[] = [];
  cellWidth = NumbersEnum.CALENDAR_CELL_WIDTH;

  constructor(
    private calendarService: CalendarService,
    private drawerService: NzDrawerService,
    private store: CalendarStore,
    private cdref: ChangeDetectorRef,
  ) {
    this.calendarService.calendarData.subscribe(data => {
      this.data = data;
      const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
      if (this.topBar) {
        this.topBar.nativeElement.scrollTo({left: (elCount-1)*this.cellWidth});
        this.cdref.markForCheck();
      } else {
        const interval = setInterval(() => {
          if (this.topBar) {
            this.topBar.nativeElement.scrollTo({left: (elCount-1)*this.cellWidth});
            this.cdref.markForCheck();
            clearInterval(interval);
          }
        }, 2)
      }
    });

   }

  ngOnInit() {
    this.daysCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    setTimeout(() => {
      this.topBar.nativeElement.scrollTo({left: (elCount-1)*this.cellWidth});
      this.cdref.markForCheck();
    }, 50)
    this.tableScrollSubscription = this.calendarService.onDragTable.subscribe((leftScroll) => {
      this.monthMargin = leftScroll;
      this.topBar.nativeElement.scrollTo({ left: leftScroll });
      if (this.monthBar) {
        this.monthBar.nativeElement.scrollTo({ left: leftScroll });
      }
    });
  }
  ngOnChanges(changes: SimpleChanges) {
    if (this.dates) {
      this.months = this.calendarService.getAllMonthData(this.dates);
      this.months.forEach(month => {
        month.width = this.getMonthWidth(month.days);
      });
      this.roomsBooked = [];
      this.notAssigned = [];
      this.setNotAssigned();
    }
  }

  /**
   * Get availability from data based on date
   * @param date
   */
  getAvailability(date: string) {
    this.days_index = this.data.dates.indexOf(date);
    return this.data.total_availability[this.days_index];
  }

  private setNotAssigned() {
    this.dates.forEach(date => {
      this.notAssigned.push(this.getNotAssigned(date));
      this.roomsBooked.push(this.getBooked(date))
    });
  }

  /**
   * Get booked from data based on date
   * @param date
   */
  getBooked(date: string) {
    this.days_index = this.data.dates.indexOf(date);
    return this.data.rooms_booked[this.days_index];
  }

  /**
   * Get not assigned from data based on date
   * @param date
   */
  getNotAssigned(date: string) {
    this.days_index = this.data.dates.indexOf(date);
    return this.data.not_assigned[this.days_index];
  }

  selectDay(day) {
    if (this.compareDates(this.store.state.selectedDate, day)) {
      this.store.setState({ selectedDate: null });
    } else {
      this.store.setState({ selectedDate: day });
    }
  }


  formatDay(day) {
    return moment(day).format('ddd D').split(' ');
  }
  getMonthWidth(days) {
    return days * this.calendarService.cellWidth;
  }
  /*
  getAvailabilityByDate(date): number {
    let totalAmount = 0;
    this.unassignedBookings.forEach((selectedRoomType) => {
      const item = selectedRoomType.availability.find((item: any) => item.date === date);

      if (item) {
        totalAmount += parseInt(item.amount, 10);
      }
    })

    return totalAmount;
  }*/


  compareDates(a, b): boolean {
    return this.calendarService.compareDates(a, b);
  }

  openSidebarDrawer(e, date: Date, available: number): void {
    //e.stopPropagation();

    if (available > 0) {
      this.drawerService.create<SidebarDrawerComponent, { data: any[], currentDate: Date }, string>({
        nzTitle: null,
        nzWidth: 280,
        nzBodyStyle: { padding: 0 },
        nzClosable: false,
        nzPlacement: 'left',
        nzContent: SidebarDrawerComponent,
        nzContentParams: {
          data: this.data,
          currentDate: date,
        }
      });
    }
  }
}
