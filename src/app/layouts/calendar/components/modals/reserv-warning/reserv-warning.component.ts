import {Component, Input, OnInit} from '@angular/core';
import {NzModalRef} from 'ng-zorro-antd';
import {CalendarService} from '../../../calendar.service';
import {NotificationService} from '@src/app/shared/services/notification/notification.service';

@Component({
  selector: 'app-reserv-warning',
  templateUrl: './reserv-warning.component.html',
  styleUrls: ['./reserv-warning.component.scss']
})
export class ReservWarningComponent implements OnInit {
  @Input() data: any;
  @Input() res: any;

  constructor(private calendarService: CalendarService,
              private notificationService: NotificationService,
              private modal: NzModalRef) {
  }

  ngOnInit() {
  }

  submit(changeType: string): void {
    this.data.data.change_type = changeType;
    this.calendarService.moveReservation(this.data).subscribe(res => {
        this.res = res;
        this.modal.triggerOk();
      },
      error => {
        this.notificationService.errorMessage('Ошибка');
        this.close();
      });
  }

  close(): void {
    this.modal.triggerCancel();
  }
}
