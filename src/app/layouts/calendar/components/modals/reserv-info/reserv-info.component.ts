import {NzDrawerService, NzModalService} from 'ng-zorro-antd';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {QuickEditComponent} from '../../drawers/quick-edit/quick-edit.component';
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {CalendarService} from '../../../calendar.service';
import {CalendarReservationInfoData} from '@src/app/core/models/calendarreservationinfodata';
import {NotificationService} from '@src/app/shared/services/notification/notification.service';
import {RoomStorageComponent} from '../../modals/room-storage/room-storage.component';
import {RoutesEnum} from "@app/core/constants/routes.enum";
import {Router} from "@angular/router";
import {KeysEnum} from "@app/core/constants/keys.enum";
import {ReservationListService} from "@app/layouts/reservation/reservation-list/reservation-list.service";
import {BookingService} from "@app/layouts/reservation/booking/booking.service";
import {ReservActionsComponent} from "@app/layouts/calendar/components/modals/reserv-actions/reserv-actions.component";
import { CalendarApiService } from "@app/shared/services/calendar-api/calendar-api.service";
import {IGuestData, IGuestFlowData} from "@app/core/models/booking-guest-data";


@Component({
  selector: 'app-reserv-info',
  templateUrl: './reserv-info.component.html',
  styleUrls: ['./reserv-info.component.scss']
})
export class ReservInfoComponent implements OnInit, OnDestroy {
  @Input() data: CalendarReservationInfoData;

  @Output() sendCloseToParent = new EventEmitter();
  isOpen = false;
  expectedArrival: Date;
  tempData: any;
  hotelID = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
  notes: any[] = [];
  isVisibleCancelReservationHold = false;

  constructor(
    private drawerService: NzDrawerService,
    public store: CalendarStore,
    private calendarService: CalendarService,
    private notificationService: NotificationService,
    private modalService: NzModalService,
    private router: Router,
    private reservationListService: ReservationListService,
    private calendarApiService: CalendarApiService,
    private bookingService: BookingService,
    private renderer: Renderer2) {
  }

  ngOnInit() {
    console.log(this.data);
    this.getNotes();
    if (this.store.state.modal.reservation && this.store.state.modal.reservation.visible) {
      this.sendCloseToParent.emit(this.store.state.modal.reservation);
    }
    this.store.state.modal.reservation = this.data;
    this.store.state.modal.reservation.visible = true;

    /** booking:
     * - Когда гость еще не заехал  - цвет должно быть синим guest_arrived_at === null
     - Когда гость проживает (заехал но еще не выехал) - цвет должно быть зеленым guest_arrived_at != null, guest_left_at === null
     - Когда  выехал  - цвет должно быть серым guest_left_at != null
     */

    if (this.data.booking.type === 'booking') {
      this.data.guestStatus = 'booking__resides';
      this.data.statusForShow = 'Проживает';

      if (!this.data.booking.guest_arrived_at) {
        this.data.guestStatus = 'booking__awaited';
        this.data.statusForShow = 'Подтверждено';
      }
      if (this.data.booking.guest_left_at) {
        this.data.guestStatus = 'booking__left';
        this.data.statusForShow = 'Выехал';
      }
    }
  }

  private getNotes() {
    this.calendarApiService.getNotes(this.hotelID, this.data.booking.reservation_id).subscribe(res => {
      this.notes = res;
      this.notes.reverse();
    });
  }

  openQuickEdit(): void {
    this.drawerService.create<QuickEditComponent, {}, string>({
      nzTitle: null,
      nzWidth: 750,
      nzClosable: false,
      nzBodyStyle: {padding: 0},
      nzPlacement: 'left',
      nzContent: QuickEditComponent,
      nzContentParams: {
        data: this.data,
      },
    });
  }

  openReservation(): void {
    this.reservationListService.bookingId.next(this.data.booking.reservation_id);
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.hotelID, RoutesEnum.RESERVATION, RoutesEnum.DETAILS]);
  }

  closePopover(): void {
    this.sendCloseToParent.emit(this.data);
    this.store.state.count = 0;
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  confirmBooking(): void {
    const searchQuery = {
      checkIn: new Date(this.data.booking.checkin),
      checkOut: new Date(this.data.booking.checkout),
      rate: null,
      roomsType: null,
      source: {id: 1, children: []}
    }
    this.bookingService.searchQuery.next(searchQuery);
    this.bookingService.addedItems.next([]);
    this.bookingService.mainGuestInfo.next(this.getGuestInfoModel())
  }

  private getGuestInfoModel(): IGuestFlowData {
    return {
      guestFormData: {
        first_name: this.data.booking.guest.first_name,
        last_name: this.data.booking.guest.last_name,
        email: this.data.booking.hold.guest_email,
        phone: this.data.booking.hold.guest_phone,
        country_id: null,

        address: null,
        address2: null,
        birthday: null,
        checkin_hours: null,
        checkin_minutes: null,
        city_id: null,
        company_name: null,
        company_number: null,
        document_country_id: null,
        document_expired_at: null,
        document_issued_at: null,
        document_number: null,
        document_type_id: null,
        gender: null,
        guest_country_id: null,
        inn: null,
        region: null,
        room_number: null,
        zip_code: null
      }
    }
  }

  get getConfirmBookingUrl(): string {
    return `/${RoutesEnum.HOTEL}/${this.hotelID}/${RoutesEnum.RESERVATION}/${RoutesEnum.CREATE}`;
  }

  cancelReservationHold(): void {
    if (confirm('Отменить удерживание номера?')) {
      this.calendarService.cancelReservationHold(this.data.booking.hold.id)
        .subscribe(res => {
            this.notificationService.successMessage('Удерживание номера отменено');
            this.sendCloseToParent.emit(this.data);
            // load calendar data
            this.calendarService.dateOffset.next('');
          },
          error => {
            this.notificationService.errorMessage('Ошибка');
            this.sendCloseToParent.emit(this.data);
          }
        );

    } else {
      this.sendCloseToParent.emit(this.data);
    }
  }

  // cancelReservationHold(): void {
  //   this.isVisibleCancelReservationHold = true;
  // }
  //
  // handleCancelReservationHoldOk(): void {
  //   this.calendarService.cancelReservationHold(this.data.booking.hold.id)
  //     .subscribe(res => {
  //         this.notificationService.succesMessage('Удерживание номера отменено');
  //         this.sendCloseToParent.emit(this.data);
  //         // load calendar data
  //         this.calendarService.dateOffset.next('');
  //       },
  //       error => {
  //         this.notificationService.errorMessage('Ошибка');
  //         this.sendCloseToParent.emit(this.data);
  //       }
  //     );
  // }
  //
  // handleCancelReservationHoldCancel(): void {
  //   this.sendCloseToParent.emit(this.data);
  //   this.isVisibleCancelReservationHold = false;
  // }

  openRoomHoldModal(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: RoomStorageComponent,
      nzComponentParams: {
        data: this.data.booking,
        isEdit: true
      },
    });
    this.sendCloseToParent.emit(this.data);
  }

  clearBlockDates(): void {
    if (confirm('Очистить даты?')) {
      this.calendarService.clearBlockDates(this.data.booking.reservation_id)
        .subscribe(res => {
            this.notificationService.successMessage('Блокировка дат удалена');
            this.sendCloseToParent.emit(this.data);
            // load calendar data
            this.calendarService.dateOffset.next('');
          },
          error => {
            this.notificationService.errorMessage('Ошибка');
            this.sendCloseToParent.emit(this.data);
          }
        );

    } else {
      this.sendCloseToParent.emit(this.data);
    }
  }

  editBlockDates(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Блокировка даты',
        blockDate: true,
        data: this.data,
        isEdit: true
      },
    });
  }

  clearCloseRoom(): void {
    if (confirm('Убрать закрытие номера?')) {
      this.calendarService.clearCloseRoom(this.data.booking.reservation_id)
        .subscribe(res => {
            this.notificationService.successMessage('Закрытие номера удалено');
            this.sendCloseToParent.emit(this.data);
            // load calendar data
            this.calendarService.dateOffset.next('');
          },
          error => {
            this.notificationService.errorMessage('Ошибка');
            this.sendCloseToParent.emit(this.data);
          }
        );

    } else {
      this.sendCloseToParent.emit(this.data);
    }
  }

  editCloseRoom(): void {
    this.modalService.create({
      nzWidth: 702,
      nzBodyStyle: {padding: 0},
      nzFooter: null,
      nzNoAnimation: true,
      nzContent: ReservActionsComponent,
      nzComponentParams: {
        title: 'Номер не работает',
        outOfService: true,
        data: this.data,
        isEdit: true
      },
    });
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, this.data.guestStatus);
  }

}
