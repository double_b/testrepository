import {Component, Input, OnInit} from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {NotificationService} from '@src/app/shared/services/notification/notification.service';
import {CalendarReservation} from '@src/app/core/models/calendarreservation';
import {CalendarService} from '../../../calendar.service';
import {CalendarApiService} from '@app/shared/services/calendar-api/calendar-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Component({
  selector: 'app-reserv-actions',
  templateUrl: './reserv-actions.component.html',
  styleUrls: ['./reserv-actions.component.scss']
})
export class ReservActionsComponent implements OnInit {

  @Input() data: CalendarReservation | any; // { range: newReservationData, selectedRoomType: selectedRoomType }
  @Input() title: string;
  @Input() blockDate = false;
  @Input() outOfService = false;
  @Input() isEdit = false;

  validateForm: FormGroup;
  endOpen = false;
  rooms: any[] = [];

  constructor(
    private modal: NzModalService,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private calendarService: CalendarService,
    private calendarApiService: CalendarApiService
  ) {
  }

  ngOnInit() {

    let data;

    // check if modal called from add storage at sidebar, edit storage or add storage at calendar
    if (!this.isEdit) {
      if (this.data && this.data.hold) {
        data = this.data.hold;
      } else if (this.data && this.data.range) {

        data = {
          checkin: this.data.range.from,
          checkout: this.data.range.to
        };
      }

      this.validateForm = this.fb.group({
        reason: [null, [Validators.required]],
        // reason: [data ? data.reason : null, [Validators.required]], // is 'reason' field required here?
        room_id: [this.data.range.room.room_id.toString(), [Validators.required]],
        checkin: [null, [Validators.required]],
        checkout: [null, [Validators.required]],
      });

      if (this.data) {
        this.validateForm.get('checkin').setValue(new Date(data.checkin));
        this.validateForm.get('checkout').setValue(new Date(data.checkout));
        // is 'room_id' required here?
        // this.validateForm.get('room_id').setValue(this.data.room_id);
      }
    } else {
      this.validateForm = this.fb.group({
        reason: [this.data.booking.reason, [Validators.required]],
        room_id: [this.data.room.room_id.toString(), [Validators.required]],
        checkin: [new Date(this.data.booking.checkin), [Validators.required]],
        checkout: [new Date(this.data.booking.checkout), [Validators.required]],
      });
    }


    if (this.blockDate || this.outOfService) {
      this.getRoomsAvailableForBooking();
    }
  }

  getRoomsAvailableForBooking = (reservationRoomId?: number) => {

    if (!this.validateForm.get('checkin').value || !this.validateForm.get('checkout').value) {
      return false;
    }

    const hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);
    const startDate = moment(this.validateForm.get('checkin').value).format('YYYY-MM-DD');
    const endDate = moment(this.validateForm.get('checkout').value).format('YYYY-MM-DD');

    this.calendarApiService
      .getRoomsAvailableByGroupsForBooking(hotelId, startDate, endDate, reservationRoomId, this.isEdit ? this.data.booking.reservation_id : '')
      .subscribe(types => {

        types.forEach(type => {
          const typeRooms = type.rooms;
          typeRooms.forEach(room => {
            this.rooms.push({id: room.id, name: `${type.short_name} ${room.name}`});
          });
        });
      });
  };

  disabledStartDate = (startDate: Date): boolean => {

    if (startDate.getTime() < moment().startOf('day').valueOf()) {
      return true;
    }

    if (!startDate || !this.validateForm.get('checkout').value) {
      return false;
    }

    return startDate.getTime() > this.validateForm.get('checkout').value.getTime();
  };

  disabledEndDate = (endDate: Date): boolean => {

    if (!endDate || !this.validateForm.get('checkin').value) {
      return false;
    }

    return endDate.getTime() <= this.validateForm.get('checkin').value.getTime();
  };

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      this.endOpen = true;
    }
    // load available rooms for these dates
    this.getRoomsAvailableForBooking();
  }

  handleEndOpenChange(open: boolean): void {
    this.endOpen = open;

    // load available rooms for these dates
    this.getRoomsAvailableForBooking();
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    // check if data are valid
    if (!this.validateForm.valid) {
      return;
    }

    const hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);

    const roomId = this.validateForm.get('room_id').value;

    const startDate = moment(this.validateForm.get('checkin').value).format('YYYY-MM-DD');
    const endDate = moment(this.validateForm.get('checkout').value).format('YYYY-MM-DD');

    const postData = {
      checkin: startDate,
      checkout: endDate,
      room_ids: [Number(roomId)],
      reason: this.validateForm.get('reason').value
    };

    if (this.blockDate) {
      if (!this.isEdit) {
        this.calendarApiService
          .blockDate(postData, hotelId)
          .subscribe(res => {
              // load calendar data
              this.calendarService.dateOffset.next('');
              this.modal.closeAll();
            },
            error => {
            }
          );
      } else {
        this.calendarApiService
          .updateBlockDate(postData, hotelId, this.data.booking.reservation_id)
          .subscribe(res => {
              // load calendar data
              this.calendarService.dateOffset.next('');
              this.modal.closeAll();
            },
            error => {
            }
          );
      }
    } else if (this.outOfService) {
      if (!this.isEdit) {
        this.calendarApiService
          .outOfService(postData, hotelId)
          .subscribe(res => {
              // load calendar data
              this.calendarService.dateOffset.next('');
              this.modal.closeAll();
            },
            error => {
            }
          );
      } else {
        this.calendarApiService
          .updateOutOfService(postData, hotelId, this.data.booking.reservation_id)
          .subscribe(res => {
              // load calendar data
              this.calendarService.dateOffset.next('');
              this.modal.closeAll();
            },
            error => {
            }
          );
      }
    } else {
      this.calendarApiService
        .saveRoomServiceInfo(postData, hotelId, roomId)
        .subscribe(res => {
            this.notificationService.successMessage('Номера поставлен на обслуживание');

            // load calendar data
            this.calendarService.dateOffset.next('');
            this.modal.closeAll();
          },
          error => {
            this.notificationService.errorMessage('нельзя добавить временное хранение номера" /  "нельзя добавить метку');
          }
        );
    }
  }

  close(): void {
    this.modal.closeAll();
  }
}
