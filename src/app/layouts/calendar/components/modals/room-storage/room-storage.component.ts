import {Component, Input, OnInit} from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CalendarService} from '../../../calendar.service';
import * as moment from 'moment';
import {NotificationService} from '@src/app/shared/services/notification/notification.service';
import {CalendarReservation} from '@src/app/core/models/calendarreservation';
import {CalendarApiService} from '@app/shared/services/calendar-api/calendar-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Component({
  selector: 'app-room-storage',
  templateUrl: './room-storage.component.html',
  styleUrls: ['./room-storage.component.scss']
})
export class RoomStorageComponent implements OnInit {
  @Input() data: CalendarReservation | any; // range: newReservationData, selectedRoomType: selectedRoomType
  @Input() isEdit: any = false;

  validateForm: FormGroup;
  endOpen = false;

  rooms: any[] = [];

  constructor(
    private modal: NzModalService,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private calendarService: CalendarService,
    private calendarApiService: CalendarApiService
  ) {
  }

  ngOnInit() {
    let data;
    let startDate;
    let endDate;

    // check if modal called from add storage at sidebar, edit storage or add storage at calendar
    if (this.data && this.data.hold) {
      data = this.data.hold;

      startDate = this.data.checkin;
      endDate = this.data.checkout;
      data.guest = this.data.guest;

    } else if (this.data && this.data.range) {

      startDate = this.data.range.from;
      endDate = this.data.range.to;
    }

    let rooms = null;
    if (this.data && this.data.rooms_ids) {
      rooms = this.data.rooms_ids.map(id => {
        return id.toString();
      });
    } else {
      rooms = [this.data.range.room.room_id.toString()];
    }

    this.validateForm = this.fb.group({
      guest_first_name: [data && data.guest ? data.guest.first_name : null, [Validators.required]],
      guest_last_name: [data && data.guest ? data.guest.last_name : null, [Validators.required]],
      guest_phone: [data ? data.guest_phone : null, [Validators.required]],
      guest_email: [data ? data.guest_email : null, [Validators.required]],
      duration_format: [data ? data.duration_format : null, [Validators.required]],
      hold_duration: [data ? data.hold_duration : null, [Validators.required]],

      room_ids: [rooms, [Validators.required]],
      checkin: [null, [Validators.required]],
      checkout: [null, [Validators.required]],
    });

    if (startDate && endDate) {
      this.validateForm.get('checkin').setValue(new Date(startDate));
      this.validateForm.get('checkout').setValue(new Date(endDate));
      // // this.validateForm.get('selectedRoomType').setValue(this.data.room_ids);

      // load room ids
      this.getRoomsAvailableForBooking();
    }

  }

  getRoomsAvailableForBooking = (reservationRoomId?: number) => {

    if (!this.validateForm.get('checkin').value || !this.validateForm.get('checkout').value) {
      return false;
    }

    const hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);
    const startDate = moment(this.validateForm.get('checkin').value).format('YYYY-MM-DD');
    const endDate = moment(this.validateForm.get('checkout').value).format('YYYY-MM-DD');

    this.calendarApiService
      .getRoomsAvailableByGroupsForBooking(hotelId, startDate, endDate, reservationRoomId, this.isEdit ? this.data.reservation_id : '')
      .subscribe(types => {
        types.forEach(type => {

          const typeRooms = type.rooms;

          typeRooms.forEach(room => {
            this.rooms.push({id: room.id, name: `${type.short_name} ${room.name}`});
          });
        });

      });
  };

  disabledStartDate = (startDate: Date): boolean => {

    if (startDate.getTime() < moment().startOf('day').valueOf()) {
      return true;
    }

    if (!startDate || !this.validateForm.get('checkout').value) {
      return false;
    }

    return startDate.getTime() > this.validateForm.get('checkout').value.getTime();
  };

  disabledEndDate = (endDate: Date): boolean => {

    if (!endDate || !this.validateForm.get('checkin').value) {
      return false;
    }

    return endDate.getTime() <= this.validateForm.get('checkin').value.getTime();
  };

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      this.endOpen = true;
    }
    // load available rooms for these dates
    this.getRoomsAvailableForBooking();
  }

  handleEndOpenChange(open: boolean): void {
    this.endOpen = open;

    // load available rooms for these dates
    this.getRoomsAvailableForBooking();
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    // check if data are valid
    if (!this.validateForm.valid) {
      return;
    }

    const hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);

    const startDate = moment(this.validateForm.get('checkin').value).format('YYYY-MM-DD');
    const endDate = moment(this.validateForm.get('checkout').value).format('YYYY-MM-DD');

    let expire_date: any;
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    if (this.validateForm.value.duration_format === 'hours') {
      expire_date = moment(now).add(Number(this.validateForm.value.hold_duration), 'hour').format('YYYY-MM-DD HH:mm:ss');
    } else {
      expire_date = moment(now).add(Number(this.validateForm.value.hold_duration), 'day').format('YYYY-MM-DD HH:mm:ss');
    }

    const ids = this.validateForm.value.room_ids.map(el => {
      return Number(el);
    });

    const postData = {
      checkin: startDate,
      checkout: endDate,
      room_ids: ids,
      arrival_time: '13:00',
      hold: {
        guest_first_name: this.validateForm.value.guest_first_name,
        guest_last_name: this.validateForm.value.guest_last_name,
        guest_email: this.validateForm.value.guest_email,
        guest_phone: this.validateForm.value.guest_phone,
        hold_duration: Number(this.validateForm.value.hold_duration),
        duration_format: this.validateForm.value.duration_format,
        expire_date: expire_date
      }
    };

    console.log();
    this.calendarApiService
      .saveReservationHoldInfo(postData, hotelId, this.data && this.data.hold ? this.data.hold.id : null)
      .subscribe(reservation => {

          this.notificationService.successMessage('Хранение номера добавлено');

          // load calendar data
          this.calendarService.dateOffset.next('');
          this.modal.closeAll();
        },
        error => {
          this.notificationService.errorMessage('нельзя добавить временное хранение номера" /  "нельзя добавить метку');
        }
      );

  }

  close(): void {
    this.modal.closeAll();
  }
}
