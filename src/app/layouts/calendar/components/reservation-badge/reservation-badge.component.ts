import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { CalendarService } from '../../calendar.service';

@Component({
  selector: 'app-reservation-badge',
  templateUrl: './reservation-badge.component.html',
  styleUrls: ['./reservation-badge.component.scss']
})
export class ReservationBadgeComponent implements OnInit, OnChanges {
  @Input() data = null; // reservarionData
  @ViewChild('element') element: ElementRef;
  @Output() sendCloseToParent = new EventEmitter();

  width = this.calendarService.cellWidth;
  title;

  private today = new Date().getTime();
  class: string;

  constructor(
    private calendarService: CalendarService
  ) {
  }

  ngOnInit() {
    console.log(this.data.reservation_id == 1623845510);
    document.addEventListener('scroll', this.scroll, true);
  }

  scroll = (event): void => {
    const {x, y} = this.element.nativeElement.getBoundingClientRect();
    if (y < 150) {
      this.sendCloseToParent.emit(this.data);
    }
  };

  ngOnChanges(changes: SimpleChanges) {
    this.width =
      (this.calendarService.getDaysBetween
      (this.data.checkin, this.data.checkout) * this.calendarService.cellWidth) - 25;
    if (this.width > 30000) {
      this.width = 50;
    }
  }

  getClassName(data) {

    if (data.type !== 'booking') {
      return this.class;
    }

    /**
     * - Когда гость еще не заехал  - цвет должно быть синим guest_arrived_at === null
     - Когда гость проживает (заехал но еще не выехал) - цвет должно быть зеленым guest_arrived_at != null, guest_left_at === null
     - Когда  выехал  - цвет должно быть серым guest_left_at != null
     */
    this.class = 'resides';

    if (!data.guest_arrived_at) {
      this.class = 'awaited';
    }
    if (data.guest_left_at) {
      this.class = 'left';
    }

    if (data.moved_to) {
      this.class += ' moved_to';
    }
    if (data.moved_from) {
      this.class += ' moved_from';
    }
    return this.class;
  }
}
