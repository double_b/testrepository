import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ScrollStore} from '@src/app/core/store/scroll.store';
import {CalendarService} from './calendar.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CalendarStore} from "@app/core/store/calendar.store";

// import {Calendardata} from '@src/app/core/models/calendardata';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CalendarComponent implements OnInit, OnDestroy {
  dataSubscription: Subscription;
  calendarData: any;
  isLoading = true;
  isError = false;

  constructor(
    private scrollStore: ScrollStore,
    private calendarService: CalendarService,
    private cdr: ChangeDetectorRef,
    private store: CalendarStore
  ) {
    this.dataSubscription = this.calendarService.calendarData.subscribe(data => {
      if (data) {
        this.isLoading = false;
        this.calendarData = data;
        this.cdr.markForCheck();
      }
    }, error => error);

  }

  ngOnInit() {
    this.calendarService.getOffset();
  }

  ngOnDestroy() {
    this.calendarService.calendarData.next(null);
  }

  tableUnPressHandler(event) {
    this.scrollStore.setState({press: false, canUpdate: false});
  }


}
