import {Injectable} from '@angular/core';
import * as moment from 'moment';
import _ from 'lodash';
import {BehaviorSubject, Observable} from 'rxjs';
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {Calendarroom} from '@src/app/core/models/calendarroom';
import {CalendarReservation} from '@src/app/core/models/calendarreservation';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {NumbersEnum} from '@app/core/constants/numbers.enum';
import {CalendarApiService} from '@app/shared/services/calendar-api/calendar-api.service';

@Injectable()
export class CalendarService {

  todayString: string;
  cellWidth = NumbersEnum.CALENDAR_CELL_WIDTH;
  windowHeight = window.innerHeight;
  outOfCalendarData = new BehaviorSubject<boolean>(false as boolean);
  calendarData = new BehaviorSubject<any>(null as any);
  tableToScroll: any;
  dragingBooking = new BehaviorSubject<boolean>(false as boolean);
  onDragTable = new BehaviorSubject<number>(0 as number);
  onScrollTable = new BehaviorSubject<number>(0 as number);
  dateOffset = new BehaviorSubject<any>({start: '', end: ''});
  calendarDataSubscription = null;

  startDate: any;
  endDate: any;
  rawData: any;

  constructor(private store: CalendarStore,
              private calendarApiService: CalendarApiService
  ) {
    moment.locale('ru');
    this.todayString = moment().format('YYYY-MM-DD');

    const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    this.startDate = moment().subtract(2, 'day').format('YYYY-MM-DD');
    this.endDate = moment().add(elCount - 2, 'day').format('YYYY-MM-DD');
  }

  public getOffset() {
    this.dateOffset.subscribe(offset => {
      if (offset.start) {
        this.startDate = moment(offset.start).format('YYYY-MM-DD');
      }
      if (offset.end) {
        this.endDate = moment(offset.end).format('YYYY-MM-DD');
      }
      if (this.getPropertyId()) {

        // TESTING: load mockup data
        // this.calendarData.next(calendar.data);

        // load live data
        if (this.calendarDataSubscription === null) {
          this.calendarDataSubscription = this.getCalendar(this.startDate, this.endDate).subscribe(res => {
            this.calendarDataSubscription = null;
            this.calendarData.next(res);
          });
        }
      } else {
        // TODO: throw error
        this.calendarData.next(null);
      }
    });
  }

  destroy() {
    if (this.calendarDataSubscription) {
      this.calendarDataSubscription.unsubscribe();
    }
  }

  cancelReservationHold(holdId: number): Observable<any> {
    return this.calendarApiService.cancelReservationHold(holdId, this.getPropertyId());
  }

  clearBlockDates(reservationId: number): Observable<any> {
    return this.calendarApiService.clearBlockDates(reservationId, this.getPropertyId());
  }

  clearCloseRoom(reservationId: number): Observable<any> {
    return this.calendarApiService.clearCloseRoom(reservationId, this.getPropertyId());
  }

  moveReservation(data: object): Observable<any> {
    return this.calendarApiService.moveReservation(this.getPropertyId(), data);
  }

  getCalendar(startDate, endDate): Observable<any> {
    return this.calendarApiService.getCalendar(this.getPropertyId(), startDate, endDate);
  }

  getDaysInMonth({day, month, year}) {
    const date = new Date(year, month, day);
    const days = [];
    while (date.getMonth() === month) {
      days.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return days;
  }

  getDaysBetween(from: string, to: string) {
    return moment(to).diff(moment(from), 'days');
  }

  addDays(date, days) {
    return moment(date, 'YYYY-MM-DD').add(days, 'days').format('YYYY-MM-DD');
  }

  subtractDays(date, days) {
    return moment(date, 'YYYY-MM-DD').subtract(days, 'days').format('YYYY-MM-DD');
  }

  isBetween(startDate, endDate, compare) {
    return moment(compare, 'YYYY-MM-DD').isBetween(startDate, endDate);
  }

  isBigger(start, end) {
    return moment(start, 'YYYY-MM-DD').isSameOrAfter(moment(end, 'YYYY-MM-DD'));
  }

  getAllDatesBetween(start, end) {
    const datesBetween = [];

    const dayRange = moment(end).diff(moment(start), 'days');

    for (let i = 0; i < dayRange; i++) {
      datesBetween.push(moment(start, 'YYYY-MM-DD').add(i, 'days').format('YYYY-MM-DD'));
    }

    return datesBetween;
  }

  getDayNumberInYear(day, currentCalendarDay) {
    const currentDay = moment(currentCalendarDay, 'YYYY-MM-DD');
    return this.getAllDatesBetween(currentDay, day).length;
  }

  getAllMonthData(days) {
    const months = [];

    days.forEach(day => {
      const currentMoment = moment(day, 'YYYY-MM-DD');
      const currentMonthName = currentMoment.format('MMMM');

      if (
        months.length > 0
        && months[months.length - 1].name === currentMonthName
      ) {
        months[months.length - 1].days++;
      } else {
        months.push({
          name: currentMonthName,
          days: 1,
          year: currentMoment.format('YYYY')
        });
      }
    });

    return months;
  }


  getPrices(propertyId, roomTypeId, start, end, rateId, occupancy) {
    // Когда заработает API 🤷‍♂️
    // return this.http.get(`${baseUrl}/hotelier/${propertyId}/room-types/${roomTypeId}/prices`, {
    //   params: {
    //     checkin: moment(start).format('Y-m-d'),
    //     checkout: moment(end).format('Y-m-d'),
    //     'rate-id': rateId,
    //     occupancy,
    //   }
    // });

    return this.getAllDatesBetween(start, end).map(date => {
      return {
        date,
        price: 35,
      };
    });
  }

  getRoomsAvailability(propertyId, roomTypeId, start, end) {
    // Когда заработает API 🤷‍♂️
    // return this.http.get(`${baseUrl}/hotelier/${propertyId}/room-types/${roomTypeId}/inventory`, {
    //   params: {
    //     start: moment(start).format('Y-m-d'),
    //     end: moment(end).format('Y-m-d'),
    //   }
    // });

    return this.getAllDatesBetween(start, end).map(date => {
      return {
        date,
        rooms: 3,
        booked: 1,
        closed: 1,
      };
    });
  }

  /**
   * scroll calendar to given date
   * @param date
   * @param currentCalendarDay
   * @param daysMargin
   */
  scrollToDate(date: Date, currentCalendarDay, daysMargin = 3) {
    const elCount = Math.ceil((document.documentElement.clientWidth - 280) / this.cellWidth);
    const startDate = moment(date).subtract(2, 'day').format('YYYY-MM-DD');
    const endDate = moment(date).add(elCount - 2, 'day').format('YYYY-MM-DD');

    this.getCalendar(startDate, endDate).subscribe(res => {
      this.calendarData.next(res);
    });
  }

  scrollToRoom(roomId) {
    const roomBar = document.querySelector(`.rscroll${roomId}`) as HTMLElement;
    roomBar.scrollIntoView({behavior: 'smooth', block: 'center'});
  }

  getBookingByDate(date: any, room: Calendarroom, start: boolean = true): CalendarReservation | undefined {
    return room.reservations.find((reservation: any) => {
      if (start) {
        return new Date(reservation.checkin).getTime() === new Date(date).getTime();
      } else {
        return new Date(reservation.checkout).getTime() === new Date(date).getTime();
      }
    });
  }

  /*
    Compare dates
    @param {Date|String} a
    @param {Date|String} b
   */
  compareDates(a: any, b: any): boolean {

    // false if one of dates is undefined or null
    if (!(a && b)) {
      return false;
    }

    // faster compare if both are strings
    if (typeof a === 'string' && typeof b === 'string') {
      return a === b;
    }

    return new Date(a).getTime() === new Date(b).getTime();
  }

  isSelectedRoom(...args: string[]): void
  isSelectedRoom(current: string, roomTypeId: string, roomId: string): boolean {
    return current === `${roomTypeId}-${roomId}`;
  }

  isExpanded(...args: any[]): void
  isExpanded(current: string[], roomTypeId: string): boolean {
    return _.includes(current, roomTypeId);
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }
}
