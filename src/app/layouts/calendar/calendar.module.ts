import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Rote
import {RouterModule} from '@angular/router';
import {CalendarRoutingModule} from './calendar-routing.module';

// Components
import {CalendarComponent} from './calendar.component';
import {DaysBarComponent} from './components/days-bar/days-bar.component';
import {CalendarTableComponent} from './components/calendar-table/calendar-table.component';
import {CalendarSidebarComponent} from './components/calendar-sidebar/calendar-sidebar.component';
import {ReservationBadgeComponent} from './components/reservation-badge/reservation-badge.component';
import {SidebarReservationComponent} from './components/sidebar-reservation/sidebar-reservation.component';
import {ReservationSplitComponent} from './components/reservation-split/reservation-split.component';
import {QuickEditComponent} from './components/drawers/quick-edit/quick-edit.component';
import {DayColComponent} from './components/day-col/day-col.component';
import {RoomRowComponent} from './components/room-row/room-row.component';
import {RoomStorageComponent} from './components/modals/room-storage/room-storage.component';
import {ReservActionsComponent} from './components/modals/reserv-actions/reserv-actions.component';
import {SidebarDrawerComponent} from './components/drawers/sidebar-drawer/sidebar-drawer.component';
import {LegendDrawerComponent} from './components/drawers/legend-drawer/legend-drawer.component';
import {BadgeDrawerComponent} from './components/drawers/badge-drawer/badge-drawer.component';
import {ReservInfoComponent} from './components/modals/reserv-info/reserv-info.component';
import {ReservWarningComponent} from './components/modals/reserv-warning/reserv-warning.component';
import {BaseModalComponent} from '@src/app/core/shared/base-modal/base-modal.component';
import {ScrollContainerComponent} from './components/scroll-container/scroll-container.component';

import {ScrollingModule} from '@angular/cdk/scrolling';

// Zorro modules
import {
  NzAlertModule,
  NzAutocompleteModule,
  NzButtonModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzDrawerModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzInputNumberModule,
  NzMenuModule,
  NzModalModule,
  NzPopoverModule,
  NzSelectModule,
  NzSpinModule,
  NzTabsModule,
  NzTimePickerModule,
} from 'ng-zorro-antd';
import {NzTableModule} from 'ng-zorro-antd/table';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';

// Modules
import {DragScrollModule} from 'ngx-drag-scroll';
import {DragulaModule} from 'ng2-dragula';
import {ClickOutsideModule} from 'ng4-click-outside';
import {SharedModule} from '@app/core/shared/shared.module';
import {CalendarService} from '@app/layouts/calendar/calendar.service';

// import {CalendarStore} from '@src/app/core/store/calendar.store';
// import {ScrollStore} from '@src/app/core/store/scroll.store';


@NgModule({
  declarations: [
    CalendarComponent,
    DaysBarComponent,
    CalendarTableComponent,
    CalendarSidebarComponent,
    SidebarReservationComponent,
    ReservationSplitComponent,
    ReservationBadgeComponent,
    ReservInfoComponent,
    ReservWarningComponent,
    BaseModalComponent,
    DayColComponent,
    RoomRowComponent,
    RoomStorageComponent,
    ReservActionsComponent,
    SidebarDrawerComponent,
    LegendDrawerComponent,
    BadgeDrawerComponent,
    QuickEditComponent,
    ScrollContainerComponent,
  ],
  imports: [
    NzAlertModule,
    CommonModule,
    NzGridModule,
    DragScrollModule,
    NzButtonModule,
    ScrollingModule,
    NzIconModule,
    DragulaModule,
    NzFormModule,
    NzTimePickerModule,
    NzPopoverModule,
    NzMenuModule,
    NzModalModule,
    NzInputModule,
    NzDropDownModule,
    NzDrawerModule,
    NzSelectModule,
    NzCheckboxModule,
    NzDatePickerModule,
    NzAutocompleteModule,
    NzTableModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    NzSpinModule,
    NzTabsModule,
    SharedModule,
    NzInputNumberModule,
    CalendarRoutingModule,
    RouterModule
  ],
  exports: [
    CalendarComponent,
  ],
  entryComponents: [
    ReservInfoComponent,
    ReservWarningComponent,
    ReservActionsComponent,
    RoomStorageComponent,
    SidebarDrawerComponent,
    LegendDrawerComponent,
    BadgeDrawerComponent,
    QuickEditComponent,
  ],
  providers: [CalendarService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarModule {
}
