import {Component, OnDestroy, OnInit} from '@angular/core';
import {SmartBookingChartTheme} from '@app/layouts/statistic/helper/smart-booking-chart.theme';
import {ChartModel, ConvertedChartDataModel} from '@app/layouts/statistic/helper/chart.model';
import {StatisticService} from '@app/layouts/statistic/helper/statistic.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-statistic-chart',
  templateUrl: './statistic-chart.component.html',
  styleUrls: ['./statistic-chart.component.scss']
})
export class StatisticChartComponent implements OnInit, OnDestroy {

  // для кэширование данных - нужен для пагинации
  convertedChartData: ConvertedChartDataModel = null;
  // кол-во строк на 1-ой странице
  rowsInOnePage = 50;
  // общее кол-во строк
  totalRows = 0;
  // для отображения состояния пагинации (номера отображаемых строк)
  showedRows: number[] = [];
  currentPageInx = 0;
  // дефолтные значения чарта
  data: ChartModel = {
    chart: {
      xAxisname: 'Даты',
      sNumberSuffix: ' UZS',
      theme: SmartBookingChartTheme.themeName
    },
    categories: [
      {
        category: []
      }
    ],
    dataset: [
      {
        seriesName: 'Номера',
        data: []
      },
      {
        seriesName: 'Доход UZS',
        parentYAxis: 'S',
        data: []
      }
    ]
  };
  // подписка на данных с сервиса - при дестрое отписываемся
  chartDataSub: Subscription = null;

  constructor(public statisticService: StatisticService) {
  }

  ngOnInit() {
    this.chartDataSub = this.statisticService.chartData$.subscribe(data => {
      if (data != null) {
        this.convertedChartData = data;
        this.totalRows = data.categories.length;
        this.onSwitchPage(1);
      }
    });
  }

  ngOnDestroy() {
    if (this.chartDataSub != null) {
      this.chartDataSub.unsubscribe();
    }
  }

  onSwitchPage(inx: number) {
    this.currentPageInx = inx;
    const startInx = this.rowsInOnePage * inx - (this.rowsInOnePage - 1);
    let endInx = this.rowsInOnePage * inx;
    if (endInx > this.totalRows) {
      endInx = this.totalRows;
    }
    this.showedRows[0] = startInx;
    this.showedRows[1] = endInx;
    this.updateChart(startInx, endInx);
  }

  // заполняем чарт относительно пагинации
  updateChart(startInx: number, endInx: number) {
    this.data.categories[0].category = [];
    this.data.dataset[0].data = [];
    this.data.dataset[1].data = [];
    this.data.categories[0].category.push(...this.convertedChartData.categories.slice((startInx - 1), (endInx)));
    this.data.dataset[0].data.push(...this.convertedChartData.firstSeriesData.slice((startInx - 1), (endInx)));
    this.data.dataset[1].data.push(...this.convertedChartData.secondSeriesData.slice((startInx - 1), (endInx)));
  }

}
