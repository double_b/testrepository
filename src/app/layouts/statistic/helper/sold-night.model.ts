export class SoldNightModel {
  inx: number;
  startDate: Date;
  endDate: Date;
  percent: number;
  reserved: number;
  profit: number;
  currency: string;

  constructor(inx: number, startDate: Date, percent: number, reserved: number, profit: number, currency: string, endDate?: Date) {
    this.inx = inx;
    this.startDate = startDate;
    this.endDate = endDate;
    this.percent = percent;
    this.reserved = reserved;
    this.profit = profit;
    this.currency = currency;
  }
}
