export class SourceShortModel {
  active: boolean|number;
  id: number;
  name: string;

  constructor(id: number, active: boolean|number, name: string) {
    this.active = active;
    this.id = id;
    this.name = name;
  }
}
