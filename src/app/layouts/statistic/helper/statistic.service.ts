import {Injectable, OnDestroy} from '@angular/core';
import {SoldNightModel} from '@app/layouts/statistic/helper/sold-night.model';
import {BehaviorSubject, Subscription} from 'rxjs';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomTypeShortModel} from '@app/layouts/statistic/helper/room-type-short.model';
import {SourceShortModel} from '@app/layouts/statistic/helper/source-short.model';
import {StatisticApiService} from '@app/shared/services/statistic-api/statistic-api.service';
import {Utils} from '@app/shared/helpers/utils';
import {ConfirmedReservationsModel, Reservation} from '@app/shared/services/statistic-api/statistic-response.model';
import {CollectedDataModel, CollectedReservationModel} from '@app/layouts/statistic/helper/collected-data.model';
import {ConvertedChartDataModel} from '@app/layouts/statistic/helper/chart.model';
import {SourcesApiService} from '@app/shared/services/sources-api/sources-api.service';

@Injectable({
  providedIn: 'root'
})
export class StatisticService implements OnDestroy {

  // константы для radioButtons (день/неделя/месяц)
  static SHOW_BY_DAY = 'day';
  static SHOW_BY_WEEK = 'week';
  static SHOW_BY_MONTH = 'month';

  // раздает источников бронирования для фильтра
  public readonly _reservationSources = new BehaviorSubject<SourceShortModel[]>([]);
  readonly reservationSources$ = this._reservationSources.asObservable();

  // раздает типы номеров для фильтра
  public readonly _roomTypes = new BehaviorSubject<RoomTypeShortModel[]>([]);
  readonly roomTypes$ = this._roomTypes.asObservable();

  // раздает данные для таблицы
  public readonly _tableData = new BehaviorSubject<SoldNightModel[]>([]);
  readonly tableData$ = this._tableData.asObservable();

  // раздает данные для чарта
  public readonly _chartData = new BehaviorSubject<ConvertedChartDataModel>(null);
  readonly chartData$ = this._chartData.asObservable();

  // true/false - таблица слушает его для отображение лоадера при загрузке данных
  public readonly _loader = new BehaviorSubject<boolean>(false);
  readonly loader$ = this._loader.asObservable();

  // основная подписка для получения данных с сервера
  // выведена отдельно чтоб при дестрое отписываться - других ф-й у подписки нет
  reservationsSub: Subscription = null;
  // подписки для получения типов номеров и источников брони с сервера
  // выведены отдельно чтоб при дестрое отписываться - других ф-й у подписок нет
  sourcesSub: Subscription = null;
  roomTypeSub: Subscription = null;

  // подготовленные данные для отображения
  // держатся в runtime чтоб при изменении формата отображения (день/неделя/месяц/пустые даты) не бегать на сервер лишний раз
  // при дестрое очищается
  collectedDataModel: CollectedDataModel[] = [];
  // кол-во максимальной заполняемости гостиницы на 1 день
  // нужен для вычета процента заполняемости
  roomNights = 0;

  constructor(private roomsService: RoomsService,
              private sourcesService: SourcesApiService,
              private statisticApiService: StatisticApiService) {
  }

  // получаем источники брони - больше другой логики тут нет
  getReservationSources() {
    this.sourcesSub = this.sourcesService.getSources(this.getHotelId()).subscribe(res => {
      const reservationSources: SourceShortModel[] = [];
      res.forEach(source => {
        reservationSources.push(new SourceShortModel(source.id, source.active, source.name));
      });
      this._reservationSources.next([]);
      this._reservationSources.next(reservationSources);
    });
  }

  // получаем типы номеров - больше другой логики тут нет
  getRoomTypes() {
    this.roomTypeSub = this.roomsService.getRoomsTypeList(this.getHotelId()).subscribe(res => {
      const roomTypes: RoomTypeShortModel[] = [];
      res.forEach(type => {
        roomTypes.push(new RoomTypeShortModel(type.id, type.name));
      });
      this._roomTypes.next([]);
      this._roomTypes.next(roomTypes);
    });
  }

  /**
   * получаем данные с сервера
   * @param dates - период дат
   * @param addEmptyDates - отоброжать пустые даты или нет (true - отображать)
   * @param showFormat - формат отображения (день/неделя/месяц)
   * @param sourceIds - источники брони (id источников)
   * @param roomTypeIds - типы номеров (id типов)
   */
  getTableData(dates: Date[], addEmptyDates: boolean, showFormat: string, sourceIds: number[] = [], roomTypeIds: number[] = []) {
    // говорим показать лоадер
    this._loader.next(true);
    this.reservationsSub = this.statisticApiService.getConfirmedReservations(dates[0], dates[1], this.getHotelId())
      .subscribe(data => {
        // отправляем данные на конвертацию в читабельный вид
        this.prepareTableData(dates[0], dates[1], data, addEmptyDates, showFormat, sourceIds, roomTypeIds);
      });
  }

  /**
   * конвертируем данные с сервера в нужный формат
   * @param fromDate - начало периода
   * @param byDate - конец периода
   * @param data - данные с сервера
   * @param addEmptyDates - отоброжать пустые даты или нет (true - отображать)
   * @param showFormat - формат отображения (день/неделя/месяц)
   * @param sourceIds - источники брони (id источников)
   * @param roomTypeIds - типы номеров (id типов)
   */
  prepareTableData(fromDate: Date, byDate: Date, data: ConfirmedReservationsModel,
                   addEmptyDates: boolean, showFormat: string, sourceIds: number[] = [], roomTypeIds: number[] = []) {
    // для временного хранения данных
    const list: CollectedDataModel[] = [];
    // получаем количество дней от и до указанных в фильтре
    const dateDiff = Utils.getDateDifference(fromDate, byDate);
    // собираем данные брони для каждого дня указанного периода
    for (let i = 0; i < dateDiff; i++) {
      // получаем тек дату по циклу
      const currentDate = new Date(fromDate);
      currentDate.setDate(fromDate.getDate() + i);
      // временный объект для хранения данных тек дня по циклу
      const tempData: CollectedDataModel = new CollectedDataModel();
      tempData.date = currentDate;
      tempData.reservations = [];
      // проходим по всем броням и отбираем совподающие даты
      for (const reservation of data.reservations) {
        // сначала смотрим попадает ли броня под фильтр источников брони
        if (sourceIds.length > 0) {
          if (!sourceIds.includes(reservation.source_id)) {
            // если не попадает под фильтр - переходим к след итерации
            continue;
          }
        }
        // смотрим попадает ли броня под фильтр типов номеров
        if (roomTypeIds.length > 0) {
          let roomTypeFilterResult = false;
          const tempRoomTypes: number[] = [];
          if (reservation.reservation_rooms.length > 0) {
            for (const res of reservation.reservation_rooms) {
              if (Utils.isExist(res.room)) {
                tempRoomTypes.push(res.room.room_type_id);
              }
            }
          }
          if (tempRoomTypes.length > 0) {
            for (const tempType of tempRoomTypes) {
              if (roomTypeIds.includes(tempType)) {
                roomTypeFilterResult = true;
                break;
              }
            }
          }
          if (!roomTypeFilterResult) {
            // если в типах номеров брони не найден ни один попадающий под фильтр
            // то переходит к след. итерации - к след. брони
            continue;
          }
        }
        // проверяем - подходит ли бронь под тек день цикла
        if (this.compareDates(reservation, currentDate)) {
          const tempReservation: CollectedReservationModel = new CollectedReservationModel();
          tempReservation.id = reservation.id;
          tempReservation.channel = reservation.channel;
          tempReservation.channelId = reservation.channel_booking_id;
          tempReservation.daySum = reservation.day_sum;
          tempReservation.status = reservation.status;
          tempReservation.currency = reservation.currency;
          tempReservation.type = reservation.type;
          tempReservation.nights = reservation.nights;
          tempReservation.totalSum = reservation.total_sum;
          tempReservation.totalRooms = reservation.total_rooms;
          tempData.reservations.push(tempReservation);
        }
      }
      list.push(tempData);
    }
    // сначала очищаем кэшированные данные - потом заполняем
    this.collectedDataModel = [];
    this.collectedDataModel.push(...list);
    this.roomNights = data.room_nights;
    this.onFilterSwitch(addEmptyDates, showFormat);
  }

  /**
   * при переключении формата отображения
   * @param addEmptyDates - отоброжать пустые даты или нет (true - отображать)
   * @param showFormat - формат отображения (день/неделя/месяц)
   */
  onFilterSwitch(addEmptyDates: boolean, showFormat: string) {
    // говорим показать лоадер
    this._loader.next(true);
    switch (showFormat) {
      case StatisticService.SHOW_BY_DAY:
        this.fillTableByDate(this.collectedDataModel, this.roomNights, addEmptyDates);
        break;
      case StatisticService.SHOW_BY_WEEK:
        this.fillTableByWeek(this.collectedDataModel, this.roomNights, addEmptyDates);
        break;
      case StatisticService.SHOW_BY_MONTH:
        this.fillTableByMonth(this.collectedDataModel, this.roomNights, addEmptyDates);
        break;
    }
  }

  /**
   * конвертируем к отображению по дням
   * @param list - массив рассортированных по датам броней
   * @param roomNights - кол-во максимальной заполняемости гостиницы на 1 день
   * @param addEmpty - отоброжать пустые даты или нет (true - отображать)
   */
  fillTableByDate(list: CollectedDataModel[], roomNights: number, addEmpty: boolean = true) {
    const tableData: SoldNightModel[] = [];
    const chartData = new ConvertedChartDataModel();
    chartData.categories = [];
    chartData.firstSeriesData = [];
    chartData.secondSeriesData = [];
    let count = 0;
    // перебираем каждый день
    for (const item of list) {
      // если у тек дня нет ни одной брони и пустые даты выключены - то пропускаем итерацию
      if (item.reservations.length === 0 && !addEmpty) {
        continue;
      }
      // суммируем данные всех броней за тек по циклу день
      const startDate = item.date;
      let percent = 0;
      let reserved = 0;
      let profit = 0;
      let currency = '';
      if (item.reservations.length > 0) {
        let tempNights = 1;
        for (const res of item.reservations) {
          tempNights += res.nights;
        }
        percent = this.calculatePercent(item, tempNights);
        reserved = this.calculateReserved(item);
        profit = this.calculateProfit(item);
        currency = this.detectCurrency(item);
      }
      const tempTableData = new SoldNightModel(count++, startDate, percent, reserved, profit, currency, null);
      tableData.push(tempTableData);
      chartData.pushData(reserved, profit, startDate);
    }
    this._chartData.next(null);
    this._chartData.next(chartData);
    // очищаем кэш - чтоб не было дублирования
    this._tableData.next([]);
    // раздаем новые данные
    this._tableData.next(tableData);
    // вырубаем лоадер
    this._loader.next(false);
  }

  /**
   * конвертируем к отображению по неделям
   * @param list - массив рассортированных по датам броней
   * @param roomNights - кол-во максимальной заполняемости гостиницы на 1 день
   * @param addEmpty - отоброжать пустые даты или нет (true - отображать)
   */
  fillTableByWeek(list: CollectedDataModel[], roomNights: number, addEmpty: boolean = true) {
    const tableData: SoldNightModel[] = [];
    const chartData = new ConvertedChartDataModel();
    chartData.categories = [];
    chartData.firstSeriesData = [];
    chartData.secondSeriesData = [];
    // вычисляем кол-во недель
    let weekCount = (list.length / 7);
    // если есть неполная неделя в конце
    if (list.length % 7 > 0) {
      weekCount++;
    }
    let count = 0;
    for (let i = 0; i < weekCount; i++) {
      // порядковый номер дня в тек неделе
      let dayCounter = 0;
      let percent = 0;
      let reserved = 0;
      let profit = 0;
      let currency = '';
      let startDate = null;
      let endDate = null;
      while (dayCounter < 7) {
        // если счетчик равен кол-ву дней - значит достигли конца массива - и завершаем цикл, даже если неделя не закончилась
        if (list.length === count) {
          break;
        }
        const item = list[count];
        // начальную дату берем из 1-го дня
        if (dayCounter === 0) {
          startDate = new Date(item.date);
        }
        // записываем дату последнего дня недели
        // если цикл завершается не доходя до конца недели - то остается нулл
        if (dayCounter === 6) {
          endDate = new Date(item.date);
        }
        if (item.reservations.length > 0) {
          let tempNights = 1;
          for (const res of item.reservations) {
            tempNights += res.nights;
          }
          percent = this.calculatePercent(item, tempNights);
          reserved += this.calculateReserved(item);
          profit += this.calculateProfit(item);
          currency = this.detectCurrency(item);
        }
        dayCounter++;
        count++;
      }
      // значит достигли последнего дня - выходим из цикла
      if (startDate === null) {
        break;
      }
      const tempData = new SoldNightModel(i, startDate, percent, reserved, profit, currency, endDate);
      // проверка на отображение пустых дат
      if (!addEmpty) {
        if (tempData.reserved > 0) {
          tableData.push(tempData);
          chartData.pushData(reserved, profit, startDate, endDate);
        }
      } else {
        tableData.push(tempData);
        chartData.pushData(reserved, profit, startDate, endDate);
      }
    }
    this._chartData.next(null);
    this._chartData.next(chartData);
    // очищаем кэш - чтоб не было дублирования
    this._tableData.next([]);
    // обновляем данные
    this._tableData.next(tableData);
    // вырубаем лоадер
    this._loader.next(false);
  }

  /**
   * конвертируем к отображению по месяцам
   * @param list - массив рассортированных по датам броней
   * @param roomNights - кол-во максимальной заполняемости гостиницы на 1 день
   * @param addEmpty - отоброжать пустые даты или нет (true - отображать)
   */
  fillTableByMonth(list: CollectedDataModel[], roomNights: number, addEmpty: boolean = true) {
    const tableData: SoldNightModel[] = [];
    const chartData = new ConvertedChartDataModel();
    chartData.categories = [];
    chartData.firstSeriesData = [];
    chartData.secondSeriesData = [];
    // мап нужен для записи индексов массива с ключом порядкового номера месяца к которому относится бронь
    const datesMap: Map<number, number[]> = new Map<number, number[]>();
    // перебираем все брони и записываем индекс массива с ключом соответствующего месяца
    list.forEach((item, inx) => {
      const currentMonth = item.date.getMonth();
      if (datesMap.has(currentMonth)) {
        datesMap.get(currentMonth).push(inx);
      } else {
        const array: number[] = [];
        array.push(inx);
        datesMap.set(currentMonth, array);
      }
    });
    // далее проходим по каждому месяцу
    datesMap.forEach((value, key) => {
      const startDate = list[value[0]].date;
      const endDate = list[value[value.length - 1]].date;
      let percent = 0;
      let reserved = 0;
      let profit = 0;
      let currency = '';
      value.forEach(i => {
        if (list[i].reservations.length > 0) {
          let tempNights = 1;
          for (const res of list[i].reservations) {
            tempNights += res.nights;
          }
          percent = this.calculatePercent(list[i], tempNights);
          reserved += this.calculateReserved(list[i]);
          profit += this.calculateProfit(list[i]);
          currency = this.detectCurrency(list[i]);
        }
      });
      const tempData = new SoldNightModel(key, startDate, percent, reserved, profit, currency, endDate);
      // проверка на отображение пустых дат
      if (!addEmpty) {
        if (reserved > 0) {
          tableData.push(tempData);
          chartData.pushData(reserved, profit, startDate, endDate, true);
        }
      } else {
        tableData.push(tempData);
        chartData.pushData(reserved, profit, startDate, endDate, true);
      }
    });
    this._chartData.next(null);
    this._chartData.next(chartData);
    // очищаем кэш - чтоб не было дублирования
    this._tableData.next([]);
    // обновляем данные
    this._tableData.next(tableData);
    // вырубаем лоадер
    this._loader.next(false);
  }

  // считаем процент заполнения
  calculatePercent(item: CollectedDataModel, roomNights: number): number {
    return Utils.round((item.reservations.length * 100) / roomNights);
  }

  // считаем кол-во проданных номеров
  calculateReserved(item: CollectedDataModel): number {
    let result = 0;
    item.reservations.forEach(reservation => result += reservation.totalRooms);
    return result;
  }

  // считаем доход
  calculateProfit(item: CollectedDataModel): number {
    let result = 0;
    item.reservations.forEach(reservation => result += reservation.daySum);
    // округляем до 2х чисел после запятой
    return Utils.round(result);
  }

  // вытаскиваем валюту
  detectCurrency(item: CollectedDataModel): string {
    return item.reservations[0].currency;
  }

  // проверяем - указанная дата находится внутри периода брони или нет
  compareDates(reservation: Reservation, currentDate: Date): boolean {
    let result = false;
    const inDate = new Date(reservation.checkin);
    const outDate = new Date(reservation.checkout);
    // сбрасываем время - чтоб сравнивать только даты
    inDate.setHours(0, 0, 0, 0);
    outDate.setHours(0, 0, 0, 0);
    currentDate.setHours(0, 0, 0, 0);
    if (currentDate >= inDate) {
      if (currentDate < outDate) {
        result = true;
      }
    }
    return result;
  }

  ngOnDestroy() {
    if (this.reservationsSub != null) {
      this.reservationsSub.unsubscribe();
    }
    if (this.roomTypeSub != null) {
      this.roomTypeSub.unsubscribe();
    }
    if (this.sourcesSub != null) {
      this.sourcesSub.unsubscribe();
    }
    this.collectedDataModel = [];
  }

  getHotelId(): number {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return Number(hotel);
  }
}
