export class CollectedDataModel {
  date: Date;
  reservations: CollectedReservationModel[];
}

export class CollectedReservationModel {
  id: number;
  channel: string;
  channelId: number;
  daySum: number;
  status: string;
  currency: string;
  type: string;
  nights: number;
  totalSum: number;
  totalRooms: number;
}
