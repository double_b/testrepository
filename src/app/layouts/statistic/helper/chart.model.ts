import {Utils} from '@app/shared/helpers/utils';

export class ConvertedChartDataModel {
  categories: ChartCategoryContentModel[] = [];
  firstSeriesData: ChartDataModel[] = [];
  secondSeriesData: ChartDataModel[] = [];

  pushData(reserved: number, profit: number, startDate: Date, endDate: Date = null, isMonthMode: boolean = false) {
    this.categories.push(new ChartCategoryContentModel(Utils.formatDateChart(startDate, endDate, isMonthMode)));
    this.firstSeriesData.push(new ChartDataModel(reserved.toString()));
    this.secondSeriesData.push(new ChartDataModel(profit.toString()));
  }
}

export class ChartModel {
  chart: ChartConfigModel;
  categories: ChartCategoryModel[] = [];
  dataset: ChartSeriesModel[] = [];
}

class ChartConfigModel {
  xAxisname: string;
  pYAxisName?: string;
  sYAxisName?: string;
  sNumberSuffix: string;
  theme: string;
}

class ChartCategoryModel {
  category: ChartCategoryContentModel[] = [];
}

export class ChartCategoryContentModel {
  label: string;

  constructor(label: string) {
    this.label = label;
  }
}

class ChartSeriesModel {
  seriesName: string;
  parentYAxis?: string;
  data: ChartDataModel[] = [];
}

export class ChartDataModel {
  value: string;

  constructor(value: string) {
    this.value = value;
  }
}
