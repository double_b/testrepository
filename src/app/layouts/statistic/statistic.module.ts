import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FusionChartsModule} from 'angular-fusioncharts';

import {StatisticRoutingModule} from './statistic-routing.module';
import {StatisticComponent} from './statistic.component';
import {SharedModule} from '@app/shared/shared.module';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';
import {StatisticChartComponent} from './statistic-chart/statistic-chart.component';
import {StatisticTableComponent} from './statistic-table/statistic-table.component';

// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import {SmartBookingChartTheme} from '@app/layouts/statistic/helper/smart-booking-chart.theme';
import {CustomPipesModule} from '@app/core/pipes/custom-pipes.module';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme);

// register custom theme for chart
// @ts-ignore
FusionCharts.register('theme', SmartBookingChartTheme.theme);

@NgModule({
  declarations: [StatisticComponent, StatisticChartComponent, StatisticTableComponent],
  imports: [
    CommonModule,
    StatisticRoutingModule,
    FusionChartsModule,
    SharedModule,
    OldShared,
    CustomPipesModule
  ]
})
export class StatisticModule {
}
