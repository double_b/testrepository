import {Component, OnDestroy, OnInit} from '@angular/core';
import {StatisticService} from '@app/layouts/statistic/helper/statistic.service';
import {Subscription} from 'rxjs';
import {SoldNightModel} from '@app/layouts/statistic/helper/sold-night.model';

@Component({
  selector: 'app-statistic-table',
  templateUrl: './statistic-table.component.html',
  styleUrls: ['./statistic-table.component.scss']
})
export class StatisticTableComponent implements OnInit, OnDestroy {

  currentPageInx = 1;
  // кол-во строк на 1-ой странице
  rowsInOnePage = 50;
  // данные для отображения таблицы
  tableData: SoldNightModel[] = [];
  // подписка для данных таблицы
  // выведена отдельно чтоб отписаться при дестрое
  tableDataSub: Subscription = null;
  // для отображения состояния пагинации (номера отображаемых строк)
  showedRows: number[] = [];
  // общее кол-во строк
  totalRows = 0;

  constructor(public statisticService: StatisticService) {
  }

  ngOnInit() {
    // подписываемся на получение данных
    this.tableDataSub = this.statisticService.tableData$.subscribe(data => {
      this.tableData = [];
      this.totalRows = data.length;
      this.tableData.push(...data);
      this.onSwitchPage(1);
    });
  }

  ngOnDestroy() {
    if (this.tableDataSub != null) {
      this.tableDataSub.unsubscribe();
    }
  }

  onSwitchPage(inx: number) {
    this.currentPageInx = inx;
    this.showedRows[0] = this.rowsInOnePage * inx - (this.rowsInOnePage - 1);
    this.showedRows[1] = this.rowsInOnePage * inx;
    if (this.showedRows[1] > this.totalRows) {
      this.showedRows[1] = this.totalRows;
    }
  }
}
