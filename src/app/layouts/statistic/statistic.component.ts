import {Component, OnInit} from '@angular/core';
import {StatisticService} from '@app/layouts/statistic/helper/statistic.service';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {
  // Формат отображения для radioButton
  periodFormats = [
    {
      title: 'DAY',
      value: StatisticService.SHOW_BY_DAY
    },
    {
      title: 'WEEK',
      value: StatisticService.SHOW_BY_WEEK
    },
    {
      title: 'MONTH',
      value: StatisticService.SHOW_BY_MONTH
    }];
  // по дефолту отображается по дням
  selectedPeriodFormat = this.periodFormats[2].value;
  // данные фильтров по типу номеров и источников бронирования
  selectedRoomTypeIds: number[] = [];
  selectedSourceIds: number[] = [];
  // checkbox - отображать пустые даты (без броней) или нет
  // по дефолту пустые даты отображаются
  emptyDates = true;

  // выбранный год
  selectedYear = new Date().getFullYear();
  // массив для селекта
  years: number[] = [];

  constructor(public statisticService: StatisticService) {
    // для фильтра дат уставливаем текущий год
    this.setDefaultDate();
  }

  ngOnInit() {
    // получаем с сервера типы номеров и источники брони
    this.statisticService.getRoomTypes();
    this.statisticService.getReservationSources();
    // получаем с сервера данные по дефолтным фильтрам
    this.refreshDataByFilter();
  }

  // при изменении формата отображения - день/неделя/месяц
  // при вкл/выкл отображать пустые даты
  // в сервис передаем параметры фильтра - сервис обновляет данные - а таблица подписана на сервис
  onSwitchShowFormat() {
    this.statisticService.onFilterSwitch(this.emptyDates, this.selectedPeriodFormat);
  }

  // обновление данных из сервера
  // вызывается при ините/изменении дат/типа номера/источников брони
  // в сервис передаем параметры фильтра - сервис обновляет данные - а таблица подписана на сервис
  refreshDataByFilter() {
    const start = new Date();
    start.setFullYear(this.selectedYear, 0, 1);
    const end = new Date(this.selectedYear);
    end.setFullYear(this.selectedYear, 11, 31);
    this.statisticService.getTableData([start, end], this.emptyDates,
      this.selectedPeriodFormat, this.selectedSourceIds, this.selectedRoomTypeIds);
  }

  setDefaultDate() {
    // список лет заполняем в след порядке
    const current = new Date().getFullYear();
    this.years.push(current - 2);  // минус 2 года
    this.years.push(current - 1);  // минус 1 год
    this.years.push(current);      // тек год
    this.years.push(current + 1);  // плюс 1 год
  }

}
