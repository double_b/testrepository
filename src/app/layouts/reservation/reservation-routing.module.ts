import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';


const routes: Routes = [
  {
    path: '',
    redirectTo: RoutesEnum.LIST
  },
  {
    path: RoutesEnum.CREATE,
    loadChildren: () => import('./booking/booking.module')
      .then(m => m.BookingModule)
  },
  {
    path: RoutesEnum.LIST,
    loadChildren: () => import('./reservation-list/reservation-list.module')
      .then(m => m.ReservationListModule)
  },
  {
    path: RoutesEnum.DETAILS,
    loadChildren: () => import('./reservation-details/reservation-details.module')
      .then(m => m.ReservationDetailsModule)
  },
  {
    path: RoutesEnum.IMPORT,
    loadChildren: () => import('./reservation-import/reservation-import.module')
      .then(m => m.ReservationImportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule {
}
