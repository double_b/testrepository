import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

// Components
import {BookingComponent} from './booking.component';
import {BookingDetailsComponent} from './components/booking-details/booking-details.component';
import {NewBookingComponent} from './components/new-booking/new-booking.component';
import {AddGuestsComponent} from './components/add-guests/add-guests.component';
import {ApplyBookingComponent} from './components/apply-booking/apply-booking.component';

// Modules
import {CustomPipesModule} from '@app/core/pipes/custom-pipes.module';
import {BookingRoutingModule} from './booking-routing.module';

import {DigitOnlyModule} from '@uiowa/digit-only';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '@app/core/shared/shared.module';
import { BookingSearchComponent } from './components/new-booking/booking-search/booking-search.component';
import { TotalCounterComponent } from './components/total-counter/total-counter.component';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import { DeleteRoomsModalComponent} from "@app/layouts/reservation/booking/components/delete-rooms-modal/delete-rooms-modal.component";

@NgModule({
  declarations: [
    BookingComponent,
    BookingDetailsComponent,
    NewBookingComponent,
    AddGuestsComponent,
    ApplyBookingComponent,
    BookingSearchComponent,
    TotalCounterComponent,
    DeleteModalComponent,
    DeleteRoomsModalComponent
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    CustomPipesModule,
    ReactiveFormsModule,
    FormsModule,
    BookingRoutingModule,
    DigitOnlyModule,
    TranslateModule,
    SharedModule,
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BookingModule {
}
