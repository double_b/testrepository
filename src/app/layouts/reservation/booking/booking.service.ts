import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {IGuestFlowData} from '@app/core/models/booking-guest-data';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {SourcesApiService} from '@app/shared/services/sources-api/sources-api.service';
import {SourceModel} from '@app/shared/models/source.model';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {TariffPlansApiService} from '@app/shared/services/tariff-plans-api/tariff-plans-api.service';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {GuestApiService} from '@app/shared/services/guest-api/guest-api.service';
import {HttpParams} from '@angular/common/http';
import {ReservationApiService} from '@app/shared/services/reservation-api/reservation-api.service';
import {PriceApiService} from '@app/shared/services/price-api/price-api.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private readonly addedItems$: BehaviorSubject<any[]>;
  private readonly foundRooms$: BehaviorSubject<any[]>;
  private readonly searchQuery$: BehaviorSubject<any[]>;
  private readonly reservedRoomSelectionList$: BehaviorSubject<any[]>;
  private readonly mainGuestInfo$: BehaviorSubject<IGuestFlowData>;

  public confirmReservation: Subject<boolean> = new Subject<boolean>();
  public checkDetailsValid: Subject<boolean> = new Subject<boolean>();

  public get addedItems(): BehaviorSubject<any[]> {
    return this.addedItems$;
  }

  public get foundRooms(): BehaviorSubject<any[]> {
    return this.foundRooms$;
  }

  public get searchQuery(): BehaviorSubject<any> {
    return this.searchQuery$;
  }

  public get reservedRoomSelectionList(): BehaviorSubject<any> {
    return this.reservedRoomSelectionList$;
  }

  public get mainGuestInfo(): BehaviorSubject<IGuestFlowData> {
    return this.mainGuestInfo$;
  }

  constructor(private sourcesApiService: SourcesApiService,
              private priceApiService: PriceApiService,
              private tariffPlansApiService: TariffPlansApiService,
              private guestApiService: GuestApiService,
              private roomsService: RoomsService,
              private reservationApiService: ReservationApiService) {
    this.addedItems$ = new BehaviorSubject([]);
    this.foundRooms$ = new BehaviorSubject([]);
    this.reservedRoomSelectionList$ = new BehaviorSubject([]);
    this.searchQuery$ = new BehaviorSubject(null);
    this.mainGuestInfo$ = new BehaviorSubject(null);
  }

  async getPriceAsPromise(roomTypeId: number,
                          checkinDate: Date, checkoutDate: Date,
                          rateId: number, occupancy: number, local?: number): Promise<any[]> {
    const res = await this.priceApiService.getPrice(this.getPropertyId(), roomTypeId, checkinDate, checkoutDate, rateId, occupancy, local);
    return res.toPromise();
  }

  getPriceByRoomType(roomTypeId: number, endPoint: string, params?: HttpParams): Observable<any[]> {
    return this.priceApiService.getPriceByRoomType(this.getPropertyId(), roomTypeId, endPoint, params);
  }

  getSearchPrice(params?: HttpParams): Observable<any> {
    return this.priceApiService.getSearchPrice(this.getPropertyId(), params);
  }

  getSearchPriceForCompany(endPoint: string, params?: HttpParams): Observable<any> {
    return this.priceApiService.getSearchPriceForCompany(this.getPropertyId(), endPoint, params);
  }

  addReservation(body: any): Observable<any> {
    return this.reservationApiService.addReservation(this.getPropertyId(), body);
  }

  getRoomsAvailableForBooking(roomType: number, checkinDate: Date, checkoutDate: Date): Observable<any[]> {
    return this.roomsService.getRoomsAvailableForBooking(this.getPropertyId(), roomType, checkinDate, checkoutDate);
  }

  getRoomTypes(): Observable<RoomsTypesListModel[]> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId());
  }

  getTariffPlans(): Observable<TariffPlanModel[]> {
    return this.tariffPlansApiService.getTariffPlans(this.getPropertyId());
  }

  getSources(): Observable<SourceModel[]> {
    return this.sourcesApiService.getSources(this.getPropertyId());
  }

  getGuests(params?: HttpParams): Observable<any> {
    return this.guestApiService.getGuests(this.getPropertyId(), params);
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }

  clearSavedData() {
    this.addedItems$.next([]);
    this.foundRooms$.next([]);
    this.reservedRoomSelectionList$.next([]);
    this.searchQuery$.next(null);
    this.mainGuestInfo$.next(null);
  }
}
