import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {BookingService} from '@app/layouts/reservation/booking/booking.service';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, OnDestroy {
  public stepIndex = 0;
  public isStepsHeader = true;
  public areGuestsPageActivated = false;
  public addedItems: any[] = [];
  public isValidFrom: any = false;

  stepItems = [
    {
      index: 0
    },
    {
      index: 1
    },
    {
      index: 2
    }
  ];

  constructor(private router: Router, private bookingService: BookingService) {
  }

  ngOnInit() {
  }

  public isNewBookingVisible(): boolean {
    return this.stepIndex === 0;
  }

  public isBookingDetailsVisible(): boolean {
    return this.stepIndex === 1;
  }

  public isApplyBookingVisible(): boolean {
    return this.stepIndex === 2 && this.isStepsHeader;
  }

  public onIndexChange(index: number): void {
    this.stepIndex = index;
  }

  // @FIXME: temp solution to go back from the 4th page
  // private areGuestsPageActivated = false;

  public handleStep(stepMark: string): void {
    if (stepMark === 'first-page') {
      // go to search-page from additional info
      this.stepIndex = 0;
    }
    if (stepMark === 'second-page') {
      // go to step with main & additional info of a person
      this.stepIndex = 1;
    }
    // if (stepMark === 'third-page') {
    //     // go to add-guests page from additional info
    //     this.stepIndex = 2;
    //     this.areGuestsPageActivated = true;
    // }
    if (stepMark === 'forth-page') {
      // got to apply-page from additional info or adding-guests
      this.stepIndex = 2;
    }
    if (stepMark === 'forth-go-back') {
      // go back to guests or details from the apply-booking page
      this.stepIndex = this.areGuestsPageActivated ? 2 : 1;
    }
    if (stepMark === 'finish-booking') {
      // the booking data is confirmed, go to page with all reservations
      this.isStepsHeader = false;
      // tslint:disable-next-line:max-line-length
      this.router.navigate([`/${RoutesEnum.HOTEL}`, this.bookingService.getPropertyId(), RoutesEnum.RESERVATION, RoutesEnum.LIST]).finally();
    }
  }

  public nextStepFromModal(): void {
    if (this.stepIndex === 1 && !this.isValidFrom) {
      this.bookingService.checkDetailsValid.next();
      return;
    }
    this.stepIndex++;
  }

  public prevStepFromModal(): void {
    this.stepIndex--;
  }

  public setAddedItems(event) {
    this.addedItems = event;
  }

  public setValidForm(event) {
    this.isValidFrom = event;
  }

  public confirm() {
    this.bookingService.confirmReservation.next();
  }

  public get totalValue(): TotalValueModel {
    const result = new TotalValueModel();
    result.totalPriceWithoutTax = this.totalAddedRoomPriceWithoutTax;
    result.totalPriceWithTax = this.totalAddedRoomPriceWithTax;
    result.proposedDeposit = this.totalProposedDeposit;
    result.total = this.totalPrice;
    return result;
  }

  public get totalAddedRoomPriceWithoutTax(): number {
    if (this.addedItems.length > 0) {
      const priceArray = this.addedItems.map(item => Number(item.totalPrice));
      return priceArray.length === 0 ? 0 : priceArray.reduce((prev, curr) => Number(prev) + Number(curr));
    } else {
      return 0;
    }
  }

  public get totalAddedRoomPriceWithTax(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  public get totalProposedDeposit(): number {
    return 0;
  }

  public get totalPrice(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  ngOnDestroy() {
    this.bookingService.clearSavedData();
  }
}
