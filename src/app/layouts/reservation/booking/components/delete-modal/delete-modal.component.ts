import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent {

  @Input() isVisible = false;
  @Output() onCancel = new EventEmitter();
  @Output() onOk = new EventEmitter();

  handleCancel() {
    this.onCancel.emit();
  }

  handleOk() {
    this.onOk.emit();
  }
}
