import {Component, Input} from '@angular/core';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';

@Component({
  selector: 'app-total-counter',
  templateUrl: './total-counter.component.html',
  styleUrls: ['./total-counter.component.scss']
})
export class TotalCounterComponent {

  @Input() totalValue: TotalValueModel = new TotalValueModel();
}
