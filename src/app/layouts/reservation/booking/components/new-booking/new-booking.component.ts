import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {forkJoin, Observable, of, Subscription} from 'rxjs';
import {map, mergeMap} from 'rxjs/operators';
import {BookingService} from '../../booking.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import {SearchQueryModel} from '@app/layouts/reservation/booking/components/new-booking/booking-search/search-query.model';
import {Utils} from '@app/shared/helpers/utils';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';
import {NzMessageService} from 'ng-zorro-antd';


type LoadingDataStatus = 'initial' | 'loading' | 'done';

@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.scss']
})
export class NewBookingComponent implements OnInit, OnDestroy {
  private loadDataSubscription: Subscription;

  @Output() stepMark = new EventEmitter<string>();
  @Output() changedAddedItems = new EventEmitter<any>();

  searchQuery: SearchQueryModel = null;

  searchResult = [];

  public active = false;
  public roomsActive = false;
  public addedItems = [];
  public foundItemsModalIndexes = [];
  public reservedRoomSelectionList = [];

  public isDeleteRoomModalVisible = false;
  public isDeleteRoomsModalVisible = false;
  public roomIndexToDelete = null;
  public dataIndexToUpdate = null;

  public isDataLoading = false;

  private today = new Date('2021-09-10');

  public loadingDataStatus: LoadingDataStatus = 'initial';

  public get yesterday(): Date {
    return new Date(this.today.getTime() - 24 * 60 * 60 * 1000);
  }

  public getNextDay(currentDate): Date {
    return new Date(currentDate + 24 * 60 * 60 * 1000);
  }

  public get tableNote(): string {
    return this.loadingDataStatus === 'done' && this.searchResult.length === 0
      ? `На выбранные даты нет номеров`
      : `Добавьте даты выезда-заезда и выберите источник <br> чтобы начать бронирование`;
  }

  public updatePriceValue(newPriceStr: string, priceObj: any): void {
    const newPriceNum = Number(newPriceStr.split(' ').join(''));
    priceObj.price = isNaN(newPriceNum) ? priceObj.price : newPriceNum;
  }

  constructor(private bookingService: BookingService, private message: NzMessageService) {
  }

  ngOnInit() {
    this.bookingService.addedItems.subscribe(res => {
      this.addedItems = res;
      console.log(this.addedItems);
      this.changedAddedItems.emit(this.addedItems);
    });
    this.bookingService.foundRooms.subscribe(res => {
      this.searchResult = res;
    });
    this.bookingService.searchQuery.subscribe(res => {
      this.searchQuery = res;
    });
    this.bookingService.reservedRoomSelectionList.subscribe(res => {
      this.reservedRoomSelectionList = res;
    });
  }

  public ngOnDestroy(): void {
    this.sendRoomsToStream();
    this.sendFoundRoomsToStream();
    this.sendSearchQueryToStream();
    this.sendReservedRoomSelectionListStream();
    if (this.loadDataSubscription) {
      this.loadDataSubscription.unsubscribe();
    }
  }

  public search(searchQuery: SearchQueryModel): void {
    this.isDataLoading = true;
    this.searchQuery = searchQuery;
    if (searchQuery) {
      this.getSearchPrice(Utils.parseDate(searchQuery.checkIn),
        Utils.parseDate(searchQuery.checkOut),
        searchQuery.roomsType, searchQuery.rate, searchQuery.source);
    }
  }

  private sendRoomsToStream(): void {
    this.bookingService.addedItems.next(this.addedItems);
  }

  private sendFoundRoomsToStream(): void {
    this.bookingService.foundRooms.next(this.searchResult);
  }

  private sendSearchQueryToStream(): void {
    this.bookingService.searchQuery.next(this.searchQuery);
  }

  private sendReservedRoomSelectionListStream(): void {
    this.bookingService.reservedRoomSelectionList.next(this.reservedRoomSelectionList);
  }

  public nextStep(): void {
    this.sendRoomsToStream();
    this.sendFoundRoomsToStream();
    this.sendSearchQueryToStream();
    this.sendReservedRoomSelectionListStream();
    this.stepMark.next('second-page');
  }

  public isDisabled(data: any, index: number): boolean {
    const chosen = data.roomsArray[index];
    return chosen.selected === 0;
  }

  public addRoom(room: any, parenIndex: number): void {
    room.nightsCount = this.searchResult[parenIndex].nights;
    room.checkinDate = this.searchQuery.checkIn;
    room.checkoutDate = this.searchQuery.checkOut;

    this.reservedRoomSelectionList.push({
      room,
      selected: room.selected,
      id: this.searchResult[parenIndex].id
    });
    let selected = room.selected;

    const comparedCond = (roomPrevSelection, r = room) => {
      return roomPrevSelection.room.roomTypeId === r.roomTypeId;
    };

    const totalSelectedCount = this.reservedRoomSelectionList.length > 0
      ? this.reservedRoomSelectionList
        .filter(item => item.id === this.searchResult[parenIndex].id)
        .filter(item => comparedCond(item, room))
        .map(item => item.selected)
        .reduce((prev, curr) => prev + curr)
      : 0;

    this.searchResult[parenIndex].availableSelection = this.searchResult[parenIndex].available - totalSelectedCount;
    room.selected = 0;

    while (selected > 0) {
      this.addedItems.push({...room});
      this.changedAddedItems.emit(this.addedItems);
      selected -= 1;
    }
    this.message.create('success', `Номер добавлен!`);
    this.search(this.searchQuery);
  }

  genArray(count: number, extraParams = ''): number[] {
    const array = [];
    for (let i = 0; i <= count; i++) {
      if (extraParams === '' || extraParams === 'no-zeros' && i !== 0) {
        array.push(i);
      }
    }

    return array;
  }

  public genArrayOnSelection(available: number, availableSelection: number, selected: number): number[] {
    const secondParam = available - selected > availableSelection ? availableSelection + selected : selected;
    const array = [];
    const total = availableSelection > secondParam ? availableSelection : secondParam;
    for (let i = 0; i <= total; i++) {
      array.push(i);
    }
    return array;
  }

  // work with observables flow to loadd all data & then process it
  processingData(data: any[], url: string, childId?: number): Observable<any> {
    if (data.length === 0) {
      return of([]);
    }

    const postProcessingData$: Observable<any>[] = [];

    data.forEach(item => {
      const rooms$: Observable<any>[] = [];
      item.rates.forEach(rate => {
        const priceRetrieving$: Observable<any>[] = [];
        // tslint:disable-next-line:max-line-length
        const price$ = this.getPriceNonAsync(item.roomType.id, item.checkin, item.checkout, rate.id, item.roomType.max_persons, 1, url, childId)
          .pipe(map(price => ({price, totalPrice: this.getTotalPrice(price), resident: 1})));
        priceRetrieving$.push(price$);

        if (rate.occupancies != null && (rate.occupancies > 0 || rate.occupancies.length > 0)) {
          // tslint:disable-next-line:max-line-length no-shadowed-variable
          const price$ = this.getPriceNonAsync(item.roomType.id, item.checkin, item.checkout, rate.id, item.roomType.max_persons, 0, url, childId)
            .pipe(map(price => ({price, totalPrice: this.getTotalPrice(price), resident: 0})));
          priceRetrieving$.push(price$);
        }
        const roomData$ = forkJoin(priceRetrieving$)
          .pipe(
            map((priceResponse: any[]) => {
              let rooms = priceResponse.map(resp => {
                return {
                  isExpand: false,
                  totalPrice: resp.totalPrice,
                  price: resp.price,
                  selected: 0,
                  roomTypeId: item.roomType.id,
                  rateId: rate.id,
                  name: item.roomType.name,
                  rate_name: rate.name,
                  resident: resp.resident,
                  max_children: item.roomType.max_children,
                  max_persons: item.roomType.max_persons,
                  selectedPerson: item.roomType.max_persons,
                  selectedChildren: 0,
                  occupancy: item.roomType.max_persons, // @FIXME: the field 'occupancy' from rate.occupancies ??
                };
              });
              rooms = rooms.filter(el => {
                return el.price.length > 0;
              });
              return rooms;
            }),
          );

        rooms$.push(roomData$);
      });

      const postProcessing$ = forkJoin(rooms$)
        .pipe(
          map((rooms: Array<Array<any>>) => {
            return [].concat.apply([], rooms);
          }),
          map((rooms: any[]) => {
            return {
              available: item.available,
              availableSelection: item.available,
              totalAvailableForDeleteChecking: item.available,
              checkin: item.checkin,
              checkout: item.checkout,
              nights: item.nights,
              roomsArray: rooms,
              roomTypeIdForFilter: item.roomType.id,
              id: Date.now()
            };
          })
        );
      postProcessingData$.push(postProcessing$);
    });

    return forkJoin(postProcessingData$);
  }

  private getSearchPrice(checkIn: string, checkOut: string, roomType?: number, rate?: number, source?: any): void {
    this.loadingDataStatus = 'loading';

    let params = new HttpParams();
    if (checkIn != null && checkIn !== '') {
      params = params.set('checkin', checkIn);
    }
    if (checkOut != null && checkOut !== '') {
      params = params.set('checkout', checkOut);
    }
    if (roomType != null) {
      params = params.set('room_types[]', `${roomType}`);
    }
    if (rate != null) {
      params = params.set('rates[]', `${rate}`);
    }

    if (Number.isInteger(source.id)) {
      const searchPrice$ = this.bookingService.getSearchPrice(params);
      const url = 'prices';
      searchPrice$
        .pipe(
          mergeMap(res => this.processingData(res, url)),
          mergeMap(res => this.processPrevRoomSelecion(res)),
        )
        .subscribe(postProcessingData => {
          this.setSearchResult(postProcessingData);
        });
    } else {
      const childId = source.id.toString().split('.')[1];
      let url = '';
      url = 'company-prices';
      params = params.set('booking', 'true');
      params = params.set('company_id', childId);
      const searchPrice$ = this.bookingService.getSearchPriceForCompany(url, params);
      searchPrice$
        .pipe(
          mergeMap(res => this.processingData(res, url, childId)),
          mergeMap(res => this.processPrevRoomSelecion(res)),
        )
        .subscribe(postProcessingData => {
          this.setSearchResult(postProcessingData);
        });
    }
  }

  private setSearchResult(postProcessingData) {
    this.searchResult = postProcessingData;
    this.searchResult.forEach((item, index) => {
      this.foundItemsModalIndexes.push({id: index, visible: false});
    });
    this.searchResult.map(el => {
      el.roomsArray.map(room => {
        if (el.available > 0) {
          room.selected = 1;
        }
      });
    });
    this.searchResult = this.searchResult.filter(el => {
      return el.roomsArray.length > 0;
    });
    this.isDataLoading = false;
    this.loadingDataStatus = 'done';
  }

  areRoomsIntersectedWithDateRange(
    roomData,
    roomPrevSelection,
    options: 'no-pattern' | 'same-props' | 'no-min-max' = null
  ): boolean {

    let roomDataDates;
    let roomPrevSelectionDates;

    if (options === 'no-pattern') {
      roomDataDates = {
        checkin: moment(roomData.checkin).startOf('hour').toDate(),
        checkout: moment(roomData.checkout).startOf('hour').toDate(),
        delta: moment(roomData.checkout).diff(moment(roomData.checkin), 'days'),
      };

      roomPrevSelectionDates = {
        checkin: moment(roomPrevSelection.checkinDate).startOf('hour').toDate(),
        checkout: moment(roomPrevSelection.checkoutDate).startOf('hour').toDate(),
        delta: moment(roomPrevSelection.checkoutDate).diff(moment(roomPrevSelection.checkinDate), 'days'),
      };
    } else if (options === 'same-props') {
      roomDataDates = {
        checkin: moment(roomData.checkinDate).startOf('hour').toDate(),
        checkout: moment(roomData.checkoutDate).startOf('hour').toDate(),
        delta: moment(roomData.checkoutDate).diff(moment(roomData.checkinDate, 'YYYY-MM-DD'), 'days'),
      };

      roomPrevSelectionDates = {
        checkin: moment(roomPrevSelection.checkinDate).startOf('hour').toDate(),
        checkout: moment(roomPrevSelection.checkoutDate).startOf('hour').toDate(),
        delta: moment(roomPrevSelection.checkoutDate).diff(moment(roomPrevSelection.checkinDate), 'days'),
      };
    } else {
      roomDataDates = {
        checkin: moment(roomData.checkin, 'YYYY-MM-DD').startOf('hour').toDate(),
        checkout: moment(roomData.checkout, 'YYYY-MM-DD').startOf('hour').toDate(),
        delta: moment(roomData.checkout, 'YYYY-MM-DD').diff(moment(roomData.checkin, 'YYYY-MM-DD'), 'days'),
      };

      roomPrevSelectionDates = {
        checkin: moment(roomPrevSelection.checkinDate).startOf('hour').toDate(),
        checkout: moment(roomPrevSelection.checkoutDate).startOf('hour').toDate(),
        delta: moment(roomPrevSelection.checkoutDate).diff(moment(roomPrevSelection.checkinDate), 'days'),
      };
    }

    // checki if one range is included in another
    let max = null;
    let min = null;
    if (roomDataDates.delta > roomPrevSelectionDates.delta) {
      max = roomDataDates;
      min = roomPrevSelectionDates;
    } else if (roomDataDates.delta < roomPrevSelectionDates.delta) {
      max = roomPrevSelectionDates;
      min = roomDataDates;
    }

    // make comparison to check date ranges are intersected
    let leftLineSegment = null;
    let rightLineSegment = null;

    if (min != null && max != null && max.checkin <= min.checkin && max.checkout >= min.checkout
      || roomDataDates.checkin === roomPrevSelectionDates.checkin && roomDataDates.checkout === roomPrevSelectionDates.checkout) {
      return true;
    } else if (roomDataDates.checkin < roomPrevSelectionDates.checkin) {
      leftLineSegment = roomDataDates;
      rightLineSegment = roomPrevSelectionDates;
    } else if (roomDataDates.checkin > roomPrevSelectionDates.checkin) {
      leftLineSegment = roomPrevSelectionDates;
      rightLineSegment = roomDataDates;
    } else {
      leftLineSegment = roomPrevSelectionDates;
      rightLineSegment = roomDataDates;
    }
    return leftLineSegment.checkout >= rightLineSegment.checkin;
  }

  private processPrevRoomSelecion(postProcessingData: any[]): Observable<any> {
    if (this.reservedRoomSelectionList.length > 0 && postProcessingData.length > 0) {
      this.reservedRoomSelectionList.forEach(roomPrevSelection => {
        const prevSelected = roomPrevSelection.selected != null ? roomPrevSelection.selected : 0;
        postProcessingData.forEach((item, i) => {
          if (this.areRoomsIntersectedWithDateRange(postProcessingData[i], roomPrevSelection.room)) {
            if (postProcessingData[i].roomTypeIdForFilter === roomPrevSelection.room.roomTypeId) {
              const delta = postProcessingData[i].available - prevSelected;
              // the room count for another date may be less than for first-search date
              postProcessingData[i].available = (postProcessingData[i].available === 0 || delta < 0) ? 0 : delta;
              postProcessingData[i].availableSelection = postProcessingData[i].available;
            }
          }
        });
      });
    }

    return of(postProcessingData);
  }

  public getProcessedDate(dateStr, options = null): string {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }

  // tslint:disable-next-line:max-line-length
  getPriceNonAsync(roomTypeId: number, checkIn: string, checkOut: string, rateId: number, occupancy: number, local: number, url: string, childId?: number): Observable<any> {
    let params = new HttpParams();
    if (checkIn != null && checkIn !== '') {
      params = params.set('checkin', checkIn);
    }
    if (checkOut != null && checkOut !== '') {
      params = params.set('checkout', checkOut);
    }
    if (rateId != null) {
      params = params.set('rate_id', `${rateId}`);
    }
    if (occupancy != null) {
      params = params.set('occupancy', `${occupancy}`);
    }
    if (local != null) {
      params = params.set('local', `${local}`);
    }
    if (childId != null) {
      params = params.set('company_id', `${childId}`);
    }

    return this.bookingService.getPriceByRoomType(roomTypeId, url, params);
  }

  async getPrice(roomTypeId: number, checkIn: Date, checkOut: Date,
                 rateId: number, occupancy: number, local: number) {
    return await this.bookingService.getPriceAsPromise(roomTypeId, checkIn, checkOut, rateId, occupancy, local);
  }

  getTotalPrice(prices): number {
    let totalPrice = 0;
    prices.forEach(price => {
      totalPrice = Number(totalPrice) + Number(price.price);
    });
    return totalPrice;
  }

  changeRoomCount(count, i, room) {
    room.selected = count;
  }

  async changePerson(selectedPeople, room, data) {
    if (selectedPeople != null) {
      room.selectedPerson = selectedPeople;
    }
    const checkinDate = new Date(data.checkin);
    const checkOutDate = new Date(data.checkout);
    const price = await this.getPrice(room.roomTypeId, checkinDate, checkOutDate, room.rateId, selectedPeople, room.resident);
    room.price = price;
    room.totalPrice = this.getTotalPrice(price);
  }

  public get totalAddedRoomPriceWithoutTax(): number {
    if (this.addedItems) {
      const priceArray = this.addedItems.map(item => Number(item.totalPrice));
      return priceArray.length === 0 ? 0 : priceArray.reduce((prev, curr) => Number(prev) + Number(curr));
    }
  }

  // TODO посчитать цену с налогами - логика пока неизвестна
  public get totalAddedRoomPriceWithTax(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  // TODO посчитать предложенный депозит - логика пока неизвестна
  public get totalProposedDeposit(): number {
    return 0;
  }

  // TODO посчитать итоговую сумму оплаты - логика пока неизвестна
  public get totalPrice(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  public get totalValue(): TotalValueModel {
    const result = new TotalValueModel();
    result.totalPriceWithoutTax = this.totalAddedRoomPriceWithoutTax;
    result.totalPriceWithTax = this.totalAddedRoomPriceWithTax;
    result.proposedDeposit = this.totalProposedDeposit;
    result.total = this.totalPrice;
    return result;
  }

  openPrices(room) {
    if (!room.isExpand) {
      room.isExpand = true;
    }
  }

  closePaymentModalPrices(room) {
    if (room.isExpand) {
      room.isExpand = false;
    }
  }

  public savePaymentModalPrices(room: any): void {
    room.price = room.price.map(priceObj => {
      priceObj.price = Number(String(priceObj.price).split(' ').join(''));
      return priceObj;
    });
    room.isExpand = false;
  }

  getDate(date: string, index: number): string {
    const month = {
      ['01']: 'Январь',
      ['02']: 'Февраль',
      ['03']: 'Март',
      ['04']: 'Апрель',
      ['05']: 'Май',
      ['06']: 'Июнь',
      ['07']: 'Июль',
      ['08']: 'Август',
      ['09']: 'Сентябрь',
      ['10']: 'Октябрь',
      ['11']: 'Ноябрь',
      ['12']: 'Декабрь',
    };
    const parseDate = date.split('-')[index];
    if (index === 1) {
      return month[parseDate];
    }
    return parseDate;
  }

  getDayName(date): string {
    const dayNames = {
      ['Monday']: 'Пн',
      ['Tuesday']: 'Вт',
      ['Wednesday']: 'Ср',
      ['Thursday']: 'Чт',
      ['Friday']: 'Пт',
      ['Saturday']: 'Сб',
      ['Sunday']: 'Вс',
    };

    const dt = moment(date, 'YYYY-MM-DD HH:mm:ss');
    const dtFormat = dt.format('dddd');
    const dayOfWeek = dayNames[dtFormat];
    return dayOfWeek != null
      ? dayOfWeek + ' ' + this.getDate(date, 2)
      : date;
  }

  getEmptyRowsForDaysOfWeek(room): string[] {
    const totalRowsInTable = 9;
    const result = [];
    let lastApiDate = room.price.slice(-1).pop().date;

    if (lastApiDate == null) {
      lastApiDate = moment(this.yesterday).format('YYYY-MM-DD');
    }

    const displayDayCount = room.price.length;
    const leftToDisplay = totalRowsInTable - displayDayCount;

    let iterableDate = this.getNextDay(moment(lastApiDate, 'YYYY-MM-DD'));

    if (leftToDisplay > 0) {
      for (let i = 0; i < leftToDisplay; i++) {
        const dayName = this.getDayName(moment(iterableDate).format('YYYY-MM-DD'));
        result.push(dayName);
        iterableDate = this.getNextDay(moment(iterableDate, 'YYYY-MM-DD'));
      }
    }
    return result;
  }

  public getTotalPaymentPrice(prices: any[]): number {
    const priceArr = prices.map(priceObj => priceObj.price);
    return priceArr.length === 0 ? 0 : priceArr.reduce((prev, curr) => Number(prev) + Number(curr));
  }

  public deleteItem(roomIndex: number): void {
    this.roomIndexToDelete = roomIndex;
    this.isDeleteRoomModalVisible = true;
  }

  public handleCancel(): void {
    this.isDeleteRoomModalVisible = false;
  }

  public handleRoomsCancel(): void {
    this.isDeleteRoomsModalVisible = false;
  }

  private decreasePrevRoomSelection(prevRoom: any): void {
    // there is check for full equality (including adults & children count) between item and prevRoom
    // because prevRoom is an item of prevRoomComparedArr that is based on this.reservedRoomSelectionList
    const prevRoomIndex = this.reservedRoomSelectionList.findIndex(item => _.isEqual(item, prevRoom));
    this.reservedRoomSelectionList[prevRoomIndex].selected -= 1;
    if (this.reservedRoomSelectionList[prevRoomIndex].selected === 0) {
      if (prevRoomIndex === 0) {
        this.reservedRoomSelectionList.shift();
      } else {
        this.reservedRoomSelectionList = this.reservedRoomSelectionList.splice(prevRoomIndex - 1, 1);
      }
    }
  }

  public handleOk(): void {
    if (this.roomIndexToDelete != null) {
      const roomToDelete = this.addedItems[this.roomIndexToDelete];
      const prevRoomComparedArr = this.reservedRoomSelectionList
        .filter(prevRoomData => {
          const areDateRangesIntersected = this.areRoomsIntersectedWithDateRange(roomToDelete, prevRoomData.room, 'same-props');
          if (areDateRangesIntersected) {
            return prevRoomData;
          }
        });

      const searchResultRoomDataIndex = this.searchResult
        .findIndex(item => item.roomTypeIdForFilter === roomToDelete.roomTypeId);

      if (prevRoomComparedArr.length > 0) {
        if (prevRoomComparedArr.length === 1) {
          const prevRoom = prevRoomComparedArr[0];
          this.decreasePrevRoomSelection(prevRoom);
        } else if (prevRoomComparedArr.length > 1) {
          const sameRoomArr = prevRoomComparedArr.filter(item => {
            // selected adults or children count cannot be always equal!
            const isRoomEqual = item.room.max_children === roomToDelete.max_children
              && item.room.max_persons === roomToDelete.max_persons
              && item.room.name === roomToDelete.name
              && item.room.nightsCount === roomToDelete.nightsCount
              && item.room.occupancy === roomToDelete.occupancy
              && item.room.rateId === roomToDelete.rateId
              && item.room.resident === roomToDelete.resident
              && item.room.roomTypeId === roomToDelete.roomTypeId;
            if (isRoomEqual) {
              return item;
            }
          });
          const prevRoom = _.maxBy(sameRoomArr, 'selected');
          this.decreasePrevRoomSelection(prevRoom);
        }
      }

      const newValue = this.searchResult[searchResultRoomDataIndex].availableSelection + 1;
      // tslint:disable-next-line:max-line-length
      const overflow = newValue > this.searchResult[searchResultRoomDataIndex].totalAvailableForDeleteChecking;
      if (!overflow && this.areRoomsIntersectedWithDateRange(this.searchResult[searchResultRoomDataIndex], roomToDelete)) {
        this.searchResult[searchResultRoomDataIndex].availableSelection = newValue;
        // tslint:disable-next-line:max-line-length
        this.searchResult[searchResultRoomDataIndex].available = this.searchResult[searchResultRoomDataIndex].availableSelection;
      }

      this.addedItems = this.addedItems.filter((value, i, arr) => i !== this.roomIndexToDelete);
      this.changedAddedItems.emit(this.addedItems);
      this.isDeleteRoomModalVisible = false;
      this.sendRoomsToStream();
    }
  }

  public handleRoomsOk() {
    this.isDeleteRoomsModalVisible = false;
    this.addedItems = [];
    this.changedAddedItems.emit(this.addedItems);
    this.reservedRoomSelectionList = [];
    this.search(this.searchQuery);
  }

  public deleteAllRooms(fromSearch = false) {
    if (fromSearch) {
      this.handleRoomsOk();
    } else {
      this.isDeleteRoomsModalVisible = true;
    }
  }
}
