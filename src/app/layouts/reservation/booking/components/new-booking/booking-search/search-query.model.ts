export class SearchQueryModel {
  checkIn: any;
  checkOut: any;
  roomsType?: number;
  rate?: number;
  source: any;

  constructor(checkIn: any, checkOut: any, roomsType?: number, rate?: number, source?: any) {
    this.checkIn = checkIn;
    this.checkOut = checkOut;
    this.roomsType = roomsType;
    this.rate = rate;
    this.source = source;
  }
}
