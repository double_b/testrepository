import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {SourceModel} from '@app/shared/models/source.model';
import {differenceInCalendarDays} from 'date-fns';
import {forkJoin, Subscription} from 'rxjs';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {BookingService} from '@app/layouts/reservation/booking/booking.service';
import {SearchQueryModel} from '@app/layouts/reservation/booking/components/new-booking/booking-search/search-query.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-booking-search',
  templateUrl: './booking-search.component.html',
  styleUrls: ['./booking-search.component.scss']
})
export class BookingSearchComponent implements OnInit, OnDestroy {

  @Output() onSearchClick = new EventEmitter();
  @Output() deleteAllRooms = new EventEmitter();

  @Input() set searchQuery(searchQuery: any) {
    if (searchQuery) {
      this.searchQueryInput = searchQuery;
      this.sourceFromSearchQuery = searchQuery.source;
      this.checkinControl.setValue(searchQuery.checkIn);
      this.checkoutControl.setValue(searchQuery.checkOut);
      this.roomTypesControl.setValue(searchQuery.roomsType);
      this.rateControl.setValue(searchQuery.rate);
    }
  }

  public searchQueryInput: any;
  public previouslySourceValue: any;

  private checkinControlSubscription: Subscription;
  sourceFromSearchQuery: any;
  sourceControl = new FormControl(null);
  checkinControl = new FormControl(null);
  checkoutControl = new FormControl(null);
  rateControl = new FormControl(null);
  roomTypesControl = new FormControl(null);

  nodes = [];
  sources: SourceModel[];
  rates: TariffPlanModel[];
  roomTypes: RoomsTypesListModel[];
  private today = new Date();

  public get tomorrow(): Date {
    return new Date(this.today.getTime() + 24 * 60 * 60 * 1000);
  }

  constructor(private bookingService: BookingService) {
  }

  ngOnInit() {
    this.loadData();
    if (!this.checkoutControl.value && !this.checkinControl.value) {
      this.setInitialCheckInOutDates();
    }
    this.checkinControlSubscription = this.checkinControl.valueChanges.subscribe(value => {
      if (value != null && value !== '') {
        const checkout = this.checkoutControl.value;
        if (checkout != null && checkout >= value) {
          this.checkoutControl.setValue(checkout);
        } else {
          this.checkoutControl.setValue(this.getNextControlDay());
        }
      } else {
        this.checkoutControl.reset();
      }
    });
  }

  public ngOnDestroy() {
    if (this.checkinControlSubscription) {
      this.checkinControlSubscription.unsubscribe();
    }
  }

  private loadData(): void {
    forkJoin([
      this.bookingService.getSources(),
      this.bookingService.getTariffPlans(),
      this.bookingService.getRoomTypes()
    ]).subscribe((res: [SourceModel[], TariffPlanModel[], RoomsTypesListModel[]]) => {
      this.sources = res[0];
      this.rates = res[1];
      this.roomTypes = res[2];

      this.processTree();
    });
  }

  private processTree() {
    this.nodes = [];
    this.sources.forEach((el, i) => {
      let item: any = {
        title: el.name,
        id: el.id,
        children: [],
        isLeaf: true,
        key: {id: i + 1, children: el.companies}
      };

      if (el.id === 5 || el.id === 4) {
        item.disabled = true;
      }

      if (el.companies.length > 0) {
        item.isLeaf = false;
        item.disabled = true;
        el.companies.forEach(childEl => {
          item.children.push({
            title: childEl.name,
            id: childEl.id,
            isLeaf: true,
            parentId: el.id,
            key: {id: (i + 1 + '.' + childEl.id).toString()}
          });
        });
      } else {
        delete item.children;
      }
      this.nodes.push(item);
    });

    if (this.sourceFromSearchQuery) {
      this.setSelectedSource();
    }

  }

  private setSelectedSource() {
    const foundIndex = this.nodes.findIndex(el => {
      return el.key.id == this.sourceFromSearchQuery.id;
    });
    if (foundIndex > -1) {
      this.sourceControl.setValue(this.nodes[foundIndex].key);
    } else {
      const childId = this.sourceFromSearchQuery.id.toString().split('.')[1];
      const parentId = Math.trunc(this.sourceFromSearchQuery.id);
      const foundChildIndex = this.nodes[parentId].children.findIndex(el => {
        return el.id == childId;
      });
      this.sourceControl.setValue(this.nodes[parentId].children[foundChildIndex].key);
    }
  }

  public search(): void {
    const searchQuery: SearchQueryModel = {
      checkIn: this.checkinControl.value,
      checkOut: this.checkoutControl.value,
      roomsType: this.roomTypesControl.value,
      rate: this.rateControl.value,
      source: this.sourceControl.value
    };
    this.onSearchClick.emit(searchQuery);
  }

  isValidDate(): boolean {
    return this.checkinControl.value &&
      this.checkoutControl.value &&
      (new Date(this.checkinControl.value) < new Date(this.checkoutControl.value))
      && this.sourceControl.value;
  }

  public setInitialCheckInOutDates(): void {
    this.checkinControl.setValue(this.today);
    this.checkoutControl.setValue(this.tomorrow);
  }

  disabledCheckinDate = (current: Date): boolean => {
    // Can not select days before today
    return differenceInCalendarDays(this.today, current) > 0;
  };

  disabledCheckoutDate = (current: Date): boolean => {
    // date should be greater than today && the checkin date
    const checkinDateNext = this.getNextControlDay();
    if (checkinDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkinDateNext, current) > 0;
  };

  public getNextControlDay(): Date | null {
    if (this.checkinControl.value) {
      const date = new Date(this.checkinControl.value);
      return new Date(date.getTime() + 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  public setDayForCheckout() {
    const checkin = new Date(this.checkinControl.value).getTime();
    const checkout = new Date(this.checkoutControl.value).getTime();
    if (checkin == checkout) {
      let tomorrow = new Date(this.checkinControl.value);
      tomorrow.setDate(tomorrow.getDate() + 1);
      this.checkoutControl.setValue(tomorrow);
    }
  }

  public startDeleteAllRooms() {
    if (!this.previouslySourceValue) {
      this.previouslySourceValue = this.sourceControl.value;
    } else {
      this.search();
    }

    if (this.searchQueryInput && this.sourceControl.value.id !== this.searchQueryInput.source.id) {
      this.deleteAllRooms.emit();
    }
  }
}
