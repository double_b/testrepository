import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BookingService} from '../../booking.service';
import * as moment from 'moment';
import {Patterns} from '@app/core/constants/validation-patterns';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {IGuestFlowData} from '@app/core/models/booking-guest-data';
import {Subscription} from 'rxjs';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';
import {isNumber} from 'util';
import {ExtrasService} from '@app/shared/services/extras/extras.service';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss']
})
export class BookingDetailsComponent implements OnInit, OnDestroy {
  @Output() stepMark = new EventEmitter<string>();
  @Output() changedValidForm = new EventEmitter<boolean>();

  private mainGuestInfoSubscription: Subscription;

  public countriesList;
  public docTypes;

  public isDeleteRoomModalVisible = false;
  public roomIndexToDelete = null;

  private items = [];

  public isExtraInfoPanelShown = false;

  public mainGuestForm: FormGroup;

  public availableRooms = [];

  public genderSelection = [
    {value: 0, label: 'Женский'},
    {value: 1, label: 'Мужской'},
  ];
  public popupGuestCities = [];
  public guestSuggestions = [];

  private existingGuestId: number;

  private isJumpToAnotherNavbarTab = true;

  public get autocompleteCities(): any[] {
    return this.popupGuestCities;
  }

  public get addedItems(): any[] {
    return this.items;
  }

  public get totalAddedRoomPriceWithoutTax(): number {
    const priceArray = this.addedItems.map(item => item.totalPrice);
    return priceArray.length === 0 ? 0 : priceArray.reduce((prev, curr) => prev + curr);
  }

  // TODO посчитать цену с налогами - логика пока неизвестна
  public get totalAddedRoomPriceWithTax(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  // TODO посчитать предложенный депозит - логика пока неизвестна
  public get totalProposedDeposit(): number {
    return 0;
  }

  // TODO посчитать итоговую сумму оплаты - логика пока неизвестна
  public get totalPrice(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  public get totalValue(): TotalValueModel {
    const result = new TotalValueModel();
    result.totalPriceWithoutTax = this.totalAddedRoomPriceWithoutTax;
    result.totalPriceWithTax = this.totalAddedRoomPriceWithTax;
    result.proposedDeposit = this.totalProposedDeposit;
    result.total = this.totalPrice;
    return result;
  }

  private addedItemsSubscription: Subscription = null;
  private guestCountryIdSubscription: Subscription = null;
  private getAllCountriesSubscription: Subscription = null;
  private getAllDocTypesSubscription: Subscription = null;
  private checkDetailsValidSubscription: Subscription = null;
  private roomsAvailableForBookingSubscription: Subscription[] = [];
  private formControlsSubscription: Subscription[] = [];
  private apiGuestsSubscription: Subscription[] = [];

  guest_autocomplete;

  constructor(private bookingService: BookingService,
              private extrasService: ExtrasService) {
    this.addedItemsSubscription = this.bookingService.addedItems.subscribe(items => {
      this.items = items;

      if (items != null) {
        this.items.forEach(room => {
          const roomType = room.roomTypeId;
          const availableRoomSubscription = this.bookingService
            .getRoomsAvailableForBooking(roomType, room.checkinDate, room.checkoutDate)
            .subscribe(data => {
              room.availableForBooking = data;
              this.availableRooms = this.availableRooms.concat(data);
            });
          this.roomsAvailableForBookingSubscription.push(availableRoomSubscription);
        });
      }
    });

    this.extrasService.countries().subscribe(data => {
      this.countriesList = data;
    });

    this.extrasService.docTypes().subscribe(data => {
      this.docTypes = data;
    });

    this.mainGuestForm = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.pattern(Patterns.emailPattern)]),
      phone: new FormControl(null),
      country_id: new FormControl(null, Validators.required),
      checkin_hours: new FormControl(null),
      checkin_minutes: new FormControl(null),
      room_number: new FormControl(null),
      birthday: new FormControl(null),
      gender: new FormControl(null),
      inn: new FormControl(null),
      company_name: new FormControl(null),
      company_number: new FormControl(null),
      city_id: new FormControl(null),
      guest_country_id: new FormControl(null),
      region: new FormControl(null),
      address: new FormControl(null),
      address2: new FormControl(null),
      zip_code: new FormControl(null),
      document_type_id: new FormControl(null),
      document_number: new FormControl(null),
      document_issued_at: new FormControl(null),
      document_country_id: new FormControl(null),
      document_expired_at: new FormControl(null),
    });
  }

  private setSubscribtionFormForAutocomplete(): void {
    const ctrlsSearchBy = ['first_name', 'last_name', 'email', 'phone'];
    ctrlsSearchBy.forEach(key => {
      const controlSubscription = this.mainGuestForm
        .get(key)
        .valueChanges
        .pipe(
          distinctUntilChanged(),
          debounceTime(500)
        )
        .subscribe(data => {
          if (data != null && data !== '') {
            const params = new HttpParams().set(key, data);
            const guestSubscription = this.bookingService.getGuests(params)
              .subscribe((resp: any[]) => {
                this.guestSuggestions = resp;
              });
            this.apiGuestsSubscription.push(guestSubscription);
          }
        });
      this.formControlsSubscription.push(controlSubscription);
    });

    this.guestCountryIdSubscription = this.mainGuestForm.get('guest_country_id')
      .valueChanges
      .subscribe(countryId => {
        if (this.countriesList != null && countryId != null) {
          const country = this.countriesList.find(item => item.id === countryId);
          if (country != null) {
            this.popupGuestCities = country.cities;
          }
        }
      });
  }

  public ngOnInit(): void {
    this.mainGuestInfoSubscription = this.bookingService.mainGuestInfo
      .subscribe(data => {
        if (data != null) {
          console.log(data);
          this.mainGuestForm.setValue(data.guestFormData);
          this.changedValidForm.emit(!this.mainGuestForm.invalid);
        }
      });

    this.setSubscribtionFormForAutocomplete();

    this.checkDetailsValidSubscription = this.bookingService.checkDetailsValid.subscribe(() => {
      this.setControlsAsDirty();
    });
  }

  private setControlsAsDirty() {
    Object.values(this.mainGuestForm.controls).forEach(control => control.markAsDirty());
  }

  public ngOnDestroy(): void {
    // if we click on a navbar tab, the streams should be cleared for a new reservation
    // if (this.isJumpToAnotherNavbarTab) {
    //   this.bookingService.mainGuestInfo.next(null);
    //   this.bookingService.addedItems.next([]);
    // }

    this.sendDataToStream();

    this.roomsAvailableForBookingSubscription.forEach(subscr => subscr.unsubscribe());
    this.formControlsSubscription.forEach(subscr => subscr.unsubscribe());
    this.apiGuestsSubscription.forEach(subscr => subscr.unsubscribe());

    this.roomsAvailableForBookingSubscription = [];
    this.formControlsSubscription = [];
    this.apiGuestsSubscription = [];

    this.addedItemsSubscription.unsubscribe();
    this.guestCountryIdSubscription.unsubscribe();
    this.mainGuestInfoSubscription.unsubscribe();
    if (this.getAllCountriesSubscription) {
      this.getAllCountriesSubscription.unsubscribe();
    }
    if (this.getAllDocTypesSubscription) {
      this.getAllDocTypesSubscription.unsubscribe();
    }
    if (this.checkDetailsValidSubscription) {
      this.checkDetailsValidSubscription.unsubscribe();
    }
  }

  public formatTime(control: 'checkin_hours' | 'checkin_minutes', param: 'hours' | 'minutes'): void {
    let value = this.mainGuestForm.get(control).value;

    const preProcessedValue = String(value).split('');

    if (preProcessedValue[0] === '0') {
      value = preProcessedValue[1];
    }

    const numeric = Number(value);
    if (value != null && value !== '' && !isNaN(numeric)) {
      if (numeric < 0) {
        value = '0';
      }
      if (numeric > 0 && numeric < 9) {
        value = `0${value}`;
      }
      if (param === 'hours' && numeric > 24) {
        value = '23';
      }
      if (param === 'minutes' && numeric > 60) {
        value = '59';
      }
    }
    this.mainGuestForm.get(control).setValue(value);
  }

  private setDataToForm(guest: any): void {
    console.log(guest);
    this.mainGuestForm.get('first_name').setValue(guest.first_name);
    this.mainGuestForm.get('last_name').setValue(guest.last_name);
    this.mainGuestForm.get('email').setValue(guest.email);
    this.mainGuestForm.get('phone').setValue(guest.phone);
    this.mainGuestForm.get('country_id').setValue(guest.country.id);
    this.mainGuestForm.get('birthday').setValue(this.getProcessedDate(guest.birthday, true));
    this.mainGuestForm.get('gender').setValue(guest.gender);
    this.mainGuestForm.get('inn').setValue(guest.inn);
    this.mainGuestForm.get('company_name').setValue(guest.company_name);
    this.mainGuestForm.get('company_number').setValue(guest.company_number);
    this.mainGuestForm.get('city_id').setValue(guest.city_id);
    this.mainGuestForm.get('region').setValue(guest.region);
    this.mainGuestForm.get('address').setValue(guest.address);
    this.mainGuestForm.get('address2').setValue(guest.address2);
    this.mainGuestForm.get('zip_code').setValue(guest.zip_code);
    this.mainGuestForm.get('document_type_id').setValue(guest.document_type_id);
    this.mainGuestForm.get('document_number').setValue(guest.document_number);
    this.mainGuestForm.get('document_issued_at').setValue(this.getProcessedDate(guest.document_issued_at, true));
    this.mainGuestForm.get('document_country_id').setValue(guest.document_country_id);
    this.mainGuestForm.get('document_expired_at').setValue(this.getProcessedDate(guest.document_expired_at, true));
  }

  public handleOptionClicking(guest): void {
    if (isNumber(guest.id)) {
      const guestfilter = this.guestSuggestions.find(g => g.id === guest.id);
      if (guestfilter) {
        this.setDataToForm(guestfilter);
        this.existingGuestId = guestfilter.id;
      }
    }
    this.changedValidForm.emit(!this.mainGuestForm.invalid);
  }

  public getProcessedDate(dateStr: string, asString = false, config = null): string | Date | null {
    const dateProcessed = config === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    if (asString) {
      return dateProcessed.isValid() ? dateProcessed.toDate() : null;
    } else {
      return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
    }
  }

  public toggleExtraInfoPanel(): void {
    this.isExtraInfoPanelShown = !this.isExtraInfoPanelShown;
  }

  public submitForm(): void {
    for (const i in this.mainGuestForm.controls) {
      if (i) {
        this.mainGuestForm.controls[i].markAsDirty();
        this.mainGuestForm.controls[i].updateValueAndValidity();
      }
    }

    if (this.mainGuestForm.invalid) {
      return;
    }
  }

  private sendRoomsToStream(): void {
    this.bookingService.addedItems.next(this.addedItems);
  }

  private sendMainGuestDataToStream(): void {
    // @FIXME: the cities are absent in DB for 12.09.2019
    // so the city_id field should be null
    const data = this.mainGuestForm.value;
    data.city_id = null;
    const streamData: IGuestFlowData = {
      guestFormData: data,
      existingGuestId: this.existingGuestId
    };

    this.bookingService.mainGuestInfo.next(streamData);
  }

  private sendDataToStream(): void {
    this.sendMainGuestDataToStream();
    this.sendRoomsToStream();
  }

  public prevStep(): void {
    this.isJumpToAnotherNavbarTab = false;

    // this.mainGuestForm.reset();
    // this.bookingService.mainGuestInfo.next(null);
    // this.bookingService.addedItems.next(null);
    this.stepMark.emit('first-page');
  }

  public addGuests(): void {
    this.isJumpToAnotherNavbarTab = false;

    this.sendDataToStream();
    this.stepMark.emit('third-page');
  }

  public nextStep(): void {
    this.isJumpToAnotherNavbarTab = false;

    this.sendDataToStream();
    this.stepMark.next('forth-page');
  }

  public deleteItem(index: number): void {
    this.roomIndexToDelete = index;
    this.isDeleteRoomModalVisible = true;
  }

  public handleCancel(): void {
    this.isDeleteRoomModalVisible = false;
  }

  public handleOk(): void {
    if (this.roomIndexToDelete != null) {
      this.items = this.items.filter((value, i, arr) => i !== this.roomIndexToDelete);
      this.bookingService.addedItems.next(this.items);
      this.isDeleteRoomModalVisible = false;
      if (this.items.length === 0) {
        this.bookingService.mainGuestInfo.next(null);
        this.bookingService.addedItems.next([]);
        this.stepMark.emit('first-page');
      }
    }
  }
}
