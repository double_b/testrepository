import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BookingService} from '../../booking.service';
import * as moment from 'moment';
import {IGuestFlowData} from '@app/core/models/booking-guest-data';
import {Subscription} from 'rxjs';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {ExtrasService} from '@app/shared/services/extras/extras.service';


@Component({
  selector: 'app-apply-booking',
  templateUrl: './apply-booking.component.html',
  styleUrls: ['./apply-booking.component.scss']
})
export class ApplyBookingComponent implements OnDestroy, OnInit {
  @Output() stepMark = new EventEmitter<string>();

  public items = [];
  private guest;
  private mainGuestFullData;

  sendWithEmail = false;
  sendWithoutEmail = false;

  public isExtraInfoPanelShown = false;

  public bookingDetailsForm: FormGroup;

  private isJumpToAnotherNavbarTab = true;
  public confirmReservationSubscription = null;

  public arrivalDate = null;
  public dateOfDeparture = null;
  public nightsCount = null;
  public bookingDate = null;
  public source = null;
  public guestName = null;
  public gestEmail = null;
  public phone = null;
  public country = null;
  public city = null;
  public region = null;
  public firstAddress = null;
  public secondAddress = null;
  public zipIndex = null;
  public expectedArrivalTime = null;
  public sourceActive = null;

  public isModalVisible: boolean = false;

  private sources;
  private bookingSource;
  private chosenCountry;

  public isDataLoaded = false;

  private requestData: any = {};
  private existingGuestId: number;

  public get sourceOfBooking(): any {
    return this.bookingSource;
  }

  public get totalAddedRoomPriceWithoutTax(): number {
    const priceArray = this.items.map(item => item.totalPrice);
    return priceArray.length === 0 ? 0 : priceArray.reduce((prev, curr) => prev + curr);
  }

  // TODO посчитать цену с налогами - логика пока неизвестна
  public get totalAddedRoomPriceWithTax(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  // TODO посчитать предложенный депозит - логика пока неизвестна
  public get totalProposedDeposit(): number {
    return 0;
  }

  // TODO посчитать итоговую сумму оплаты - логика пока неизвестна
  public get totalPrice(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  public get totalValue(): TotalValueModel {
    const result = new TotalValueModel();
    result.totalPriceWithoutTax = this.totalAddedRoomPriceWithoutTax;
    result.totalPriceWithTax = this.totalAddedRoomPriceWithTax;
    result.proposedDeposit = this.totalProposedDeposit;
    result.total = this.totalPrice;
    return result;
  }

  private getCheckiOutDates(items): any {
    let earliestDate;
    let latestDate;

    items.forEach((room, index) => {
      if (index === 0) {
        earliestDate = room.checkinDate;
        latestDate = room.checkoutDate;
      } else {
        if (room.checkinDate < earliestDate) {
          earliestDate = room.checkinDate;
        }
        if (room.checkoutDate < latestDate) {
          latestDate = room.checkoutDate;
        }
      }
    });

    const diffTime = Math.abs(latestDate.getTime() - earliestDate.getTime());
    const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));

    const result = {
      start: moment(earliestDate).format('DD.MM.YYYY'),
      end: moment(latestDate).format('DD.MM.YYYY'),
      nights: diffDays,
    };

    return result;
  }

  public get addedItems(): any[] {
    return this.items;
  }

  private sendRoomsToStream(): void {
    this.bookingService.addedItems.next(this.addedItems);
  }

  private sendMainGuestDataToStream(): void {
    this.guest.city_id = null;
    const streamData: IGuestFlowData = {
      guestFormData: this.guest,
      existingGuestId: this.existingGuestId
    };

    this.bookingService.mainGuestInfo.next(streamData);
  }

  private sendDataToStream(): void {
    this.sendMainGuestDataToStream();
    this.sendRoomsToStream();
  }

  private setMainGuestData(guest: any): void {
    const currentTime = moment(new Date()).format('DD.MM.YYYY');

    this.bookingDate = currentTime;
    this.source =
      this.sourceOfBooking != null && this.sourceOfBooking !== ''
        ? this.sourceOfBooking.name : null;
    this.guestName =
      guest.first_name != null && guest.last_name != null &&
      guest.first_name !== '' && guest.last_name !== ''
        ? (guest.first_name + ' ' + guest.last_name) : null;
    this.gestEmail = guest.email != null && guest.email !== ''
      ? guest.email : null;
    this.phone = guest.phone != null && guest.phone !== ''
      ? guest.phone : null;
    this.country = this.chosenCountry != null && this.chosenCountry !== ''
      ? this.chosenCountry.name : null;
    this.city = guest.city_id != null && guest.city_id !== ''
      ? guest.city_id : null;
    this.region = guest.region != null && guest.region !== ''
      ? guest.region : null;
    this.firstAddress = guest.address != null && guest.address !== ''
      ? guest.address : null;
    this.secondAddress = guest.address2 != null && guest.address2 !== ''
      ? guest.address2 : null;
    this.zipIndex = guest.zip_code != null && guest.zip_code !== ''
      ? guest.zip_code : null;
    this.expectedArrivalTime =
      guest.checkin_hours != null && guest.checkin_minutes != null &&
      guest.checkin_hours !== '' && guest.checkin_minutes !== ''
        ? (guest.checkin_hours + ':' + guest.checkin_minutes) : null;
  }

  private mainGuestInfoSubscription: Subscription;
  private getApiSourcesSubscription: Subscription;
  private getAllCountriesWithoutCitiesSubscription: Subscription;
  private addedBookingItemsSubscription: Subscription;

  constructor(private bookingService: BookingService,
              private notificationService: NotificationService,
              private extrasService: ExtrasService) {
    this.mainGuestInfoSubscription = this.bookingService.mainGuestInfo.subscribe(guest => {
      this.mainGuestFullData = guest;
      if (guest != null) {
        this.guest = guest.guestFormData;
        this.existingGuestId = guest.existingGuestId;
      }

      // @TODO: later try to rewrite with rxjs operators (see new-booking component)
      this.getApiSourcesSubscription = this.bookingService.getSources().subscribe(sources => {
        this.sources = (<any> sources);
        this.sourceActive = this.bookingService.searchQuery.getValue().source;
        this.bookingSource = this.sources.find(item => item.id === 1); // booking with reseption

        this.getAllCountriesWithoutCitiesSubscription = this.extrasService.countries()
          .subscribe(countries => {
            this.chosenCountry = countries.find(ctr => ctr.id === this.guest.country_id);

            this.addedBookingItemsSubscription = this.bookingService.addedItems.asObservable().subscribe(rooms => {
              this.items = rooms;

              if (rooms != null && rooms.length > 0) {
                const {start, end, nights} = this.getCheckiOutDates(rooms);
                this.arrivalDate = start;
                this.dateOfDeparture = end;
                this.nightsCount = `${nights}`;
              }

              this.setMainGuestData(this.guest);
              this.isDataLoaded = true;
            });
          });

      });
    });
  }

  ngOnInit() {
    this.confirmReservationSubscription = this.bookingService.confirmReservation.subscribe(() => {
      this.confirm();
    });
  }

  public ngOnDestroy(): void {
    // if we click on a navbar tab, the streams should be cleared for a new reservation
    // if (this.isJumpToAnotherNavbarTab) {
    //   this.bookingService.mainGuestInfo.next(null);
    //   this.bookingService.addedItems.next([]);
    // }

    if (this.mainGuestInfoSubscription != null) {
      this.mainGuestInfoSubscription.unsubscribe();
    }
    if (this.getApiSourcesSubscription != null) {
      this.getApiSourcesSubscription.unsubscribe();
    }
    if (this.getAllCountriesWithoutCitiesSubscription != null) {
      this.getAllCountriesWithoutCitiesSubscription.unsubscribe();
    }
    if (this.addedBookingItemsSubscription != null) {
      this.addedBookingItemsSubscription.unsubscribe();
    }

    if (this.confirmReservationSubscription != null) {
      this.confirmReservationSubscription.unsubscribe();
    }
  }

  private getGuestPartForRequest(mainGuestFullData: IGuestFlowData): any {
    const requestData: any = {};

    if (mainGuestFullData.existingGuestId) { // existing_guest
      requestData.existing_guest = this.existingGuestId;
    } else { // new guest params
      if (mainGuestFullData.guestFormData != null) {
        requestData.new_guest = {};
        // required
        if (mainGuestFullData.guestFormData.first_name != null && mainGuestFullData.guestFormData.first_name !== '') {
          requestData.new_guest.first_name = mainGuestFullData.guestFormData.first_name;
        }
        if (mainGuestFullData.guestFormData.last_name != null && mainGuestFullData.guestFormData.last_name !== '') {
          requestData.new_guest.last_name = mainGuestFullData.guestFormData.last_name;
        }
        if (mainGuestFullData.guestFormData.email != null && mainGuestFullData.guestFormData.email !== '') {
          requestData.new_guest.email = mainGuestFullData.guestFormData.email;
        }
        if (mainGuestFullData.guestFormData.phone != null && mainGuestFullData.guestFormData.phone !== '') {
          requestData.new_guest.phone = mainGuestFullData.guestFormData.phone;
        }
        if (mainGuestFullData.guestFormData.country_id != null) {
          requestData.new_guest.country_id = mainGuestFullData.guestFormData.country_id;
        }

        // extra
        if (mainGuestFullData.guestFormData.birthday != null && mainGuestFullData.guestFormData.birthday !== '') {
          const birthday = moment(mainGuestFullData.guestFormData.birthday, 'DD.MM.YYYY').format('YYYY-MM-DD');
          requestData.new_guest.birthday = birthday;
        }
        if (mainGuestFullData.guestFormData.gender != null) {
          requestData.new_guest.gender = mainGuestFullData.guestFormData.gender;
        }
        if (mainGuestFullData.guestFormData.inn != null && mainGuestFullData.guestFormData.inn !== '') {
          requestData.new_guest.inn = mainGuestFullData.guestFormData.inn;
        }
        if (mainGuestFullData.guestFormData.company_name != null && mainGuestFullData.guestFormData.company_name !== '') {
          requestData.new_guest.company_name = mainGuestFullData.guestFormData.company_name;
        }
        if (mainGuestFullData.guestFormData.company_number != null && mainGuestFullData.guestFormData.company_number !== '') {
          requestData.new_guest.company_number = mainGuestFullData.guestFormData.company_number;
        }
        if (mainGuestFullData.guestFormData.city_id != null) {
          requestData.new_guest.city_id = mainGuestFullData.guestFormData.city_id;
        }
        if (mainGuestFullData.guestFormData.region != null && mainGuestFullData.guestFormData.region !== '') {
          requestData.new_guest.region = mainGuestFullData.guestFormData.region;
        }
        if (mainGuestFullData.guestFormData.address != null && mainGuestFullData.guestFormData.address !== '') {
          requestData.new_guest.address = mainGuestFullData.guestFormData.address;
        }
        if (mainGuestFullData.guestFormData.address2 != null && mainGuestFullData.guestFormData.address2 !== '') {
          requestData.new_guest.address2 = mainGuestFullData.guestFormData.address2;
        }
        if (mainGuestFullData.guestFormData.zip_code != null && mainGuestFullData.guestFormData.zip_code !== '') {
          requestData.new_guest.zip_code = mainGuestFullData.guestFormData.zip_code;
        }
        if (mainGuestFullData.guestFormData.document_type_id != null && mainGuestFullData.guestFormData.document_type_id !== '') {
          requestData.new_guest.document_type_id = mainGuestFullData.guestFormData.document_type_id;
        }
        if (mainGuestFullData.guestFormData.document_number != null && mainGuestFullData.guestFormData.document_number !== '') {
          requestData.new_guest.document_number = mainGuestFullData.guestFormData.document_number;
        }
        if (mainGuestFullData.guestFormData.document_country_id != null) {
          requestData.new_guest.document_country_id = mainGuestFullData.guestFormData.document_country_id;
        }

        if (mainGuestFullData.guestFormData.document_issued_at != null && mainGuestFullData.guestFormData.document_issued_at !== '') {
          const date = moment(mainGuestFullData.guestFormData.document_issued_at, 'DD.MM.YYYY').format('YYYY-MM-DD');
          requestData.new_guest.document_issued_at = date;
        }
        if (mainGuestFullData.guestFormData.document_expired_at != null && mainGuestFullData.guestFormData.document_expired_at !== '') {
          const date = moment(mainGuestFullData.guestFormData.document_expired_at, 'DD.MM.YYYY').format('YYYY-MM-DD');
          requestData.new_guest.document_expired_at = date;
        }
      }
    }

    return requestData;
  }

  private prepareDataToSend(): void {
    if (Number.isInteger(this.sourceActive.id)) {
      this.requestData.source_id = this.sourceActive.id;
    } else {
      const childId = this.sourceActive.id.toString().split('.')[1];
      this.requestData.source_id = Math.trunc(this.sourceActive.id);
      this.requestData.company_id = Number(childId);
    }

    if (this.expectedArrivalTime != null && this.expectedArrivalTime !== '') {
      this.requestData.arrival_time = this.expectedArrivalTime; // hh:mm
    }
    const mainGuest = this.getGuestPartForRequest(this.mainGuestFullData);
    this.requestData = {...(this.requestData), ...mainGuest};

    // rooms
    if (this.items != null && this.items.length > 0) {
      const processedRoomList = [];
      this.items.forEach(room => {
        const processedRoom: any = {
          checkin: moment(room.checkinDate).format('YYYY-MM-DD'),
          checkout: moment(room.checkoutDate).format('YYYY-MM-DD'),
          rate_id: room.rateId,
          room_type_id: room.roomTypeId,
          room_id: null, // физические номера пока не доступны
          occupancy: room.occupancy,
          local: room.resident,
          adults: room.selectedPerson,
          children: room.selectedChildren,
          prices: room.price,
        };

        const existingGuests = [];
        const newGuests = [];

        if (room.guestData != null) {
          room.guestData.forEach(guest => {
            const processedGuest = this.getGuestPartForRequest(guest);
            if (processedGuest.existing_guest != null) {
              existingGuests.push(processedGuest.existing_guest);
            }
            if (processedGuest.new_guest) {
              newGuests.push(processedGuest.new_guest);
            }
          });
        }

        if (existingGuests.length > 0) {
          processedRoom.existing_guests = existingGuests;
        }
        if (newGuests.length > 0) {
          processedRoom.new_guests = newGuests;
        }

        processedRoomList.push(processedRoom);
      });

      this.requestData.rooms = processedRoomList;
    }
  }

  private sendRequest(withEmail: boolean): void {
    this.prepareDataToSend();
    if (withEmail) {
      this.sendWithEmail = true;
    } else {
      this.sendWithoutEmail = true;
    }
    this.bookingService.addReservation(this.requestData).subscribe(result => {
      this.isModalVisible = false;
      this.isJumpToAnotherNavbarTab = true;
      this.sendWithEmail = false;
      this.sendWithoutEmail = false;
      this.stepMark.next('finish-booking');
      this.notificationService.successMessage('Бронирование было успешно добавлено!');
    });
  }

  public prevStep(): void {
    this.isJumpToAnotherNavbarTab = false;

    // @FIXME: temp solution to go back to guests or person info
    this.sendDataToStream();
    this.stepMark.emit('forth-go-back');
  }

  public confirm(): void {
    this.isModalVisible = true;
  }

  public handleCancel(): void {
    this.isModalVisible = false;
  }

  public handleSendAndContinue(): void {
    this.requestData.notify_guest = 1;
    this.sendRequest(true);
  }

  public handleJustSend(): void {
    this.sendRequest(false);
  }

  public getProcessedDate(dateStr, options = null): string {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }
}
