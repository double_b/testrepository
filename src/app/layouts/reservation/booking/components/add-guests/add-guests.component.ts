import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {BookingService} from '../../booking.service';
import * as moment from 'moment';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Patterns} from '@app/core/constants/validation-patterns';
import {delay, distinctUntilChanged} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';
import {IGuestFlowData} from '@app/core/models/booking-guest-data';
import {NzTabChangeEvent} from 'ng-zorro-antd';
import {Subscription} from 'rxjs';
import {TotalValueModel} from '@app/layouts/reservation/booking/components/total-counter/total-value.model';
import {ExtrasService} from '@app/shared/services/extras/extras.service';

@Component({
  selector: 'app-add-guests',
  templateUrl: './add-guests.component.html',
  styleUrls: ['./add-guests.component.scss']
})
export class AddGuestsComponent implements OnInit, OnDestroy {
  @Output() stepMark = new EventEmitter<string>();
  private items = [];
  public countriesList;
  public docTypes;
  public popupGuestCities = [];
  public guestSuggestions = [];
  private existingGuestId: number;

  public genderSelection = [
    {value: 0, label: 'Женский'},
    {value: 1, label: 'Мужской'},
  ];

  public selectedRoomIndex = 0;
  public selectedGuestIndex = 0;

  public isDeleteRoomModalVisible = false;
  public roomIndexToDelete = null;

  public guestForm: FormGroup;
  public roomNumberForm = new FormGroup({
    rooms: new FormArray([]),
  });

  public isExtraInfoPanelShown = false;

  private roomsAvailableForBookingSubscription: Subscription[] = [];
  private roomTypeChangeSubscription: Subscription[] = [];
  private formCangeSubscription: Subscription[] = [];
  private apiGuestsSubscription: Subscription[] = [];

  private addedRoomsSubscription: Subscription;

  private isJumpToAnotherNavbarTab = true;

  private shouldResetForm = true;

  public get totalAddedRoomPriceWithoutTax(): number {
    const priceArray = this.addedItems.map(item => item.totalPrice);
    return priceArray.length === 0 ? 0 : priceArray.reduce((prev, curr) => prev + curr);
  }

  // TODO посчитать цену с налогами - логика пока неизвестна
  public get totalAddedRoomPriceWithTax(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  // TODO посчитать предложенный депозит - логика пока неизвестна
  public get totalProposedDeposit(): number {
    return 0;
  }

  // TODO посчитать итоговую сумму оплаты - логика пока неизвестна
  public get totalPrice(): number {
    return this.totalAddedRoomPriceWithoutTax;
  }

  public get totalValue(): TotalValueModel {
    const result = new TotalValueModel();
    result.totalPriceWithoutTax = this.totalAddedRoomPriceWithoutTax;
    result.totalPriceWithTax = this.totalAddedRoomPriceWithTax;
    result.proposedDeposit = this.totalProposedDeposit;
    result.total = this.totalPrice;
    return result;
  }

  public get addedItems(): any[] {
    return this.items;
  }

  public getTitleForGuestTab(guestsCount): string {
    const title = guestsCount === 1
    && this.addedItems[this.selectedRoomIndex].selectedPerson > 0
    && this.addedItems[this.selectedRoomIndex].selectedPerson === 1
      ? '' : ` ${guestsCount}`;
    return `Гость${title}`;
  }

  private setDataToForm(guest: any): void {
    this.guestForm.get('first_name').setValue(guest.first_name);
    this.guestForm.get('last_name').setValue(guest.last_name);
    this.guestForm.get('email').setValue(guest.email);
    this.guestForm.get('phone').setValue(guest.phone);
    this.guestForm.get('country_id').setValue(guest.country_id);
    this.guestForm.get('birthday').setValue(this.getProcessedDate(guest.birthday, true));
    this.guestForm.get('gender').setValue(guest.gender);
    this.guestForm.get('inn').setValue(guest.inn);
    this.guestForm.get('company_name').setValue(guest.company_name);
    this.guestForm.get('company_number').setValue(guest.company_number);
    this.guestForm.get('city_id').setValue(guest.city_id);
    this.guestForm.get('region').setValue(guest.region);
    this.guestForm.get('address').setValue(guest.address);
    this.guestForm.get('address2').setValue(guest.address2);
    this.guestForm.get('zip_code').setValue(guest.zip_code);
    this.guestForm.get('document_type_id').setValue(guest.document_type_id);
    this.guestForm.get('document_number').setValue(guest.document_number);
    this.guestForm.get('document_issued_at').setValue(this.getProcessedDate(guest.document_issued_at, true));
    this.guestForm.get('document_country_id').setValue(guest.document_country_id);
    this.guestForm.get('document_expired_at').setValue(this.getProcessedDate(guest.document_expired_at, true));
  }

  public handleOptionClicking(guest: any): void {
    this.setDataToForm(guest);
    this.existingGuestId = guest.id;
  }

  private sendRoomsToStream(): void {
    this.setGuestToRoom();

    this.addedItems.forEach(room => {
      room.guestData = room.guestData.filter(obj => obj); // remove all 'null's
    });

    this.bookingService.addedItems.next(this.addedItems);
  }

  private sendDataToStream(): void {
    this.sendRoomsToStream();
  }

  private createFormControls(): void {
    this.items.forEach((room, index) => {
      (this.roomNumberForm.get('rooms') as FormArray)
        .setControl(index, new FormGroup({
          count: new FormControl(null)
        }));
    });
  }

  private getAvailableRooms() {
    this.items.forEach(room => {
      const roomType = room.roomTypeId;
      const availableRoomSubscription = this.bookingService
        .getRoomsAvailableForBooking(roomType, room.checkinDate, room.checkoutDate)
        .subscribe(data => {
          room.availableForBooking = data;
        });
      this.roomsAvailableForBookingSubscription.push(availableRoomSubscription);
    });
  }

  private addEmptyGuests(): void {
    // add empty guests array
    this.items.forEach((room, i) => {
      if (room.guestData == null) {
        room.guestData = new Array(room.selectedPerson).fill(null);
      }
    });
  }

  private handleRoomTypeChanges(): void {
    this.items.forEach((room, i) => {
      const roomSubscription = (this.roomNumberForm.get('rooms') as FormArray)
        .controls[i].get('count').valueChanges
        .subscribe(newValue => {
          room.chosenRoomType = newValue;
        });
      this.roomTypeChangeSubscription.push(roomSubscription);
    });
  }

  constructor(private bookingService: BookingService,
              private extrasService: ExtrasService) {
    this.extrasService.countries().subscribe(data => {
      this.countriesList = data;
    });

    this.extrasService.docTypes().subscribe(data => {
      this.docTypes = data;
    });

    this.guestForm = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.pattern(Patterns.emailPattern)]),
      phone: new FormControl(null, Validators.required),
      country_id: new FormControl(null, Validators.required),
      birthday: new FormControl(null),
      gender: new FormControl(null),
      inn: new FormControl(null),
      company_name: new FormControl(null),
      company_number: new FormControl(null),
      city_id: new FormControl(null),
      region: new FormControl(null),
      address: new FormControl(null),
      address2: new FormControl(null),
      zip_code: new FormControl(null),
      document_type_id: new FormControl(null),
      document_number: new FormControl(null),
      document_issued_at: new FormControl(null),
      document_country_id: new FormControl(null),
      document_expired_at: new FormControl(null),
    });
  }

  public ngOnInit(): void {
    this.addedRoomsSubscription = this.bookingService.addedItems.subscribe(items => {
      this.items = items;

      if (this.items != null) {
        // create form controls for the rooms
        this.createFormControls();
        this.getAvailableRooms();
        this.handleRoomTypeChanges();
        this.addEmptyGuests();

        // restore first-tab guest to form, after going back from the 4th step
        this.selectedRoomIndex = 0;
        if (this.items.length > 0) {
          const firstRoom = this.items[this.selectedRoomIndex];
          if (firstRoom.guestData != null && firstRoom.guestData.length > 0) {
            // array is filled with null to handle index of a tab for a guest
            if (firstRoom.guestData[0] != null) {
              const firstGuestTabValue = firstRoom.guestData[0].guestFormData;
              this.setDataToForm(firstGuestTabValue);
            }
          }
        }
      }
    });

    this.setSubscribtionFormForAutocomplete();
  }

  public ngOnDestroy(): void {
    // if we click on a navbar tab, the streams should be cleared for a new reservation
    if (this.isJumpToAnotherNavbarTab) {
      this.bookingService.mainGuestInfo.next(null);
      this.bookingService.addedItems.next([]);
    }

    this.roomsAvailableForBookingSubscription.forEach(subscr => subscr.unsubscribe());
    this.roomTypeChangeSubscription.forEach(subscr => subscr.unsubscribe());
    this.formCangeSubscription.forEach(subscr => subscr.unsubscribe());
    this.apiGuestsSubscription.forEach(subscr => subscr.unsubscribe());

    this.roomsAvailableForBookingSubscription = [];
    this.roomTypeChangeSubscription = [];
    this.formCangeSubscription = [];
    this.apiGuestsSubscription = [];

    this.addedRoomsSubscription.unsubscribe();
  }

  private setSubscribtionFormForAutocomplete(): void {
    const ctrlsSearchBy = ['first_name', 'last_name', 'email', 'phone'];
    ctrlsSearchBy.forEach(key => {
      const controlSubscription = this.guestForm.get(key).valueChanges
        .pipe(
          distinctUntilChanged(),
          delay(1000)
        )
        .subscribe(data => {
          if (data != null && data !== '') {
            const params = new HttpParams().set(key, data);
            const guestSubscription = this.bookingService.getGuests(params)
              .subscribe(resp => {
                this.setGuestSuggestions(resp);
              });
            this.apiGuestsSubscription.push(guestSubscription);
          }
        });
      this.formCangeSubscription.push(controlSubscription);
    });
  }

  private setGuestSuggestions(data: any[]): void {
    this.guestSuggestions = data;
  }

  public selectRoom(index: number): void {
    // update guest for room of old index
    const room = this.addedItems[this.selectedRoomIndex];
    if (room != null && room.guestData != null && room.guestData[this.selectedGuestIndex] == null) {
      const data = this.guestForm.value;
      data.city_id = null;
      const streamData: IGuestFlowData = {
        guestFormData: data,
        existingGuestId: this.existingGuestId
      };
      room.guestData[this.selectedGuestIndex] = streamData;
    }

    this.selectedRoomIndex = index;
    this.guestForm.reset();
    // restore data for the prev tab
    this.applyGuest(this.selectedGuestIndex);
  }

  public getGuestCount(): number[] {
    const room = this.addedItems[this.selectedRoomIndex];
    const guestCount = room != null ? this.addedItems[this.selectedRoomIndex].selectedPerson : 0;
    const arr = [];
    for (let i = 0; i < guestCount; i++) {
      arr.push(i + 1);
    }
    return arr;
  }

  public getProcessedDate(dateStr, asString = false, options = null): string | Date | null {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    if (asString) {
      return dateProcessed.isValid() ? dateProcessed.toDate() : null;
    } else {
      return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
    }
  }

  private setGuestToRoom(): void {
    // @FIXME: the cities are absent in DB for 12.09.2019
    // so the city_id field should be null
    if (this.guestForm.valid) {
      const data = this.guestForm.value;

      data.city_id = null;
      const streamData: IGuestFlowData = {
        guestFormData: data,
        existingGuestId: this.existingGuestId
      };
      const room = this.addedItems[this.selectedRoomIndex];
      if (room != null) {
        this.addedItems[this.selectedRoomIndex].guestData[this.selectedGuestIndex] = streamData;
      }
    }

    if (this.shouldResetForm) {
      this.guestForm.reset();
    }
  }

  public saveForm(index: number): void {
    if (this.selectedGuestIndex === index) {
      this.shouldResetForm = false;
    }
    this.setGuestToRoom();
    this.shouldResetForm = true;
  }

  private applyGuest(selectedGuestIndex: number,): void {
    const room = this.addedItems[this.selectedRoomIndex];

    if (room != null && room.guestData != null
      && room.guestData[selectedGuestIndex] != null
      && room.guestData[selectedGuestIndex].guestFormData) {
      this.guestForm.setValue(room.guestData[selectedGuestIndex].guestFormData);
    }
  }

  public selectTab(tab: NzTabChangeEvent): void {
    this.selectedGuestIndex = tab.index;
    this.applyGuest(this.selectedGuestIndex);
  }

  public toggleExtraInfoPanel(): void {
    this.isExtraInfoPanelShown = !this.isExtraInfoPanelShown;
  }

  public prevStep(): void {
    this.isJumpToAnotherNavbarTab = false;

    this.sendDataToStream();
    this.stepMark.emit('second-page');
  }

  public nextStep(): void {
    this.setGuestToRoom();
    this.isJumpToAnotherNavbarTab = false;

    this.sendDataToStream();
    this.stepMark.next('forth-page');
  }

  public deleteItem(index: number): void {
    this.roomIndexToDelete = index;
    this.isDeleteRoomModalVisible = true;
  }

  public handleCancel(): void {
    this.isDeleteRoomModalVisible = false;
  }

  public handleOk(): void {
    if (this.roomIndexToDelete != null) {
      this.items = this.items.filter((value, i, arr) => i !== this.roomIndexToDelete);
      this.selectedRoomIndex = 0;
      (this.roomNumberForm.get('rooms') as FormArray).removeAt(this.roomIndexToDelete);
      this.bookingService.addedItems.next(this.items);
      this.isDeleteRoomModalVisible = false;
      if (this.items.length === 0) {
        this.bookingService.mainGuestInfo.next(null);
        this.bookingService.addedItems.next([]);
        this.stepMark.emit('first-page');
      }
    }
  }
}
