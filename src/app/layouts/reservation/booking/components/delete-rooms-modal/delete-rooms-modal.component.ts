import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-delete-rooms-modal',
  templateUrl: './delete-rooms-modal.component.html',
  styleUrls: ['./delete-rooms-modal.component.scss']
})
export class DeleteRoomsModalComponent {

  @Input() isVisible = false;
  @Output() onCancel = new EventEmitter();
  @Output() onOk = new EventEmitter();

  handleCancel() {
    this.onCancel.emit();
  }

  handleOk() {
    this.onOk.emit();
  }
}
