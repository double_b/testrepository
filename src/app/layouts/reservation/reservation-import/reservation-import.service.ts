import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SourcesApiService} from '@app/shared/services/sources-api/sources-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {ReservationApiService} from '@app/shared/services/reservation-api/reservation-api.service';
import {SourceModel} from '@app/shared/models/source.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationImportService {

  constructor(private roomsService: RoomsService,
              private reservationApiService: ReservationApiService,
              private sourcesApiService: SourcesApiService) {
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }

  getSources(): Observable<SourceModel[]> {
    return this.sourcesApiService.getSources(this.getPropertyId());
  }

  public send = (data: any, successFunction: Function, errorFunction: Function) => {
    data.reservations.map(el => {
      if (Number.isInteger(el.source_id.id)) {
        el.source_id = el.source_id.id;
      } else {
        const childId = el.source_id.id.toString().split('.')[1];
        el.source_id = Math.trunc(el.source_id.id);
        el.company_id = Number(childId);
      }

      el.rooms.map(room => {
        room.children = room.children === 0 ? null : room.children;
      });
    });
    this.reservationApiService.saveImportReservations(this.getPropertyId(), data).subscribe(
      response => {
        successFunction(response);
      },
      error => {
        errorFunction(error.error);
      });
  };

  public getAllRoomTypes(): Observable<any> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId());
  }

  public getAllSources(): Observable<any> {
    return this.sourcesApiService.getSources(this.getPropertyId());
  }

}
