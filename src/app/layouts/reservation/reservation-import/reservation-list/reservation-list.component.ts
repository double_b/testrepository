import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Reservation} from '../domain/Reservation';
import {Room} from '../domain/Room';
import {ReservationImportService} from '../reservation-import.service';
import {Validator} from '../domain/Validator';
import {SourceModel} from '@app/shared/models/source.model';
import {differenceInCalendarDays} from 'date-fns';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {CountryModel} from '@app/shared/models/common/country.model';


@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {
  public data: Reservation[] = [];
  private dataIsValid = false;

  dateFormat = 'dd/MM/yy';
  private countries: CountryModel[] = [];
  private sources: SourceModel[] = [];
  public nodes = [];
  private roomTypes: [] = [];
  private numbers: number[] = [1, 2, 3, 4, 5, 6];
  idCounter: number;
  roomIdCounter: number;
  private validator: Validator = new Validator();
  @Output() sendIsEditable = new EventEmitter<boolean>();
  @Output() sendDataToEdit = new EventEmitter<Reservation[]>();

  nzFormatter = (value: number) => value ? value.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + ' ') : '';
  nzParser = (value: number) => value.toString().trim();

  constructor(private notification: NotificationService,
              private extrasService: ExtrasService,
              private reservationService: ReservationImportService) {
  }

  ngOnInit() {
    this.idCounter = 0;
    this.roomIdCounter = 0;
    this.getCountries();
    this.getSources();
    this.getRoomTypes();
    this.addReservation();
  }

  sendData = () => {
    this.validateData();
    if (this.dataIsValid) {
      this.sendDataToEdit.emit(this.data);
      this.openEditableTable();
    } else if (!this.dataIsValid) {
      this.createFailNotification();
    }
  };

  getCountries = () => {
    this.extrasService.countries().subscribe(countries => {
      this.countries = countries;
    }, (error) => {
    });
  };

  // TODO override with new method in service - double_b
  getSources = () => {
    this.reservationService.getSources().subscribe(sources => {
      this.sources = sources;
      this.processTree();
    }, (error) => {
    });
  };

  private processTree() {
    this.nodes = [];
    this.sources.forEach((el, i) => {
      let item: any = {
        title: el.name,
        id: el.id,
        children: [],
        isLeaf: true,
        key: {id: i + 1, children: el.companies}
      };

      if (el.companies.length > 0) {
        item.isLeaf = false;
        item.disabled = true;
        el.companies.forEach(childEl => {
          item.children.push({
            title: childEl.name,
            id: childEl.id,
            isLeaf: true,
            parentId: el.id,
            key: {id: (i + 1 + '.' + childEl.id).toString()}
          });
        });
      } else {
        delete item.children;
      }

      this.nodes.push(item);
    });
  }

  getRoomTypes = () => {
    this.reservationService.getAllRoomTypes().subscribe(roomTypes => {
      this.roomTypes = roomTypes;
    }, (error) => {
    });
  };


  openEditableTable() {
    this.sendIsEditable.emit(true);
  }


// --- work with Json
  addRoom(index: any, reservation: any) {
    let newRoom = new Room();
    newRoom.id = this.roomIdCounter;
    newRoom.checkin = reservation.rooms[0].checkin;
    newRoom.checkout = reservation.rooms[0].checkout;
    this.roomIdCounter++;
    this.data[index].rooms.push(newRoom);

    this.setDisabledCheckin(index, newRoom);
  }

  deleteRoom(index: any, roomIndex: any) {
    this.data[index].rooms.splice(roomIndex + 1, 1);
  }

  addReservation() {
    let newReservation = new Reservation();
    newReservation.id = this.idCounter;
    newReservation.status = 'confirmed';
    this.idCounter++;

    let newRoom = new Room();
    newRoom.id = this.roomIdCounter;
    this.roomIdCounter++;

    newReservation.rooms.push(newRoom);
    this.data.push(newReservation);
  }

  deleteReservation(index: any) {
    this.data.splice(index, 1);
  }


  setCheckout(room: Room) {
    let date = room.checkin;
    if (room.checkout == null) {
      room.checkout = new Date(room.checkin);
      room.checkout.setDate(date.getDate() + 1);
    } else {
      if (room.checkout <= room.checkin) {
        room.checkout = new Date(room.checkin);
        room.checkout.setDate(date.getDate() + 1);
      }
    }
  }

  setDisabledCheckin(index: any, room: Room) {
    let currentRoomId = this.data[index].rooms.indexOf(room);
    let prevRoomId = currentRoomId - 1;
    this.data[index].rooms[currentRoomId].previousRoomCheckOut = this.data[index].rooms[prevRoomId].checkout;
  }

  disableStartDate = (current: Date): boolean => {
    const date = new Date();
    return differenceInCalendarDays(date, current) > 0;
  };

  disableEndDate = (current: Date): boolean => {
    let date;
    if (this.data[0].rooms[0].checkin) {
      date = new Date(this.data[0].rooms[0].checkin);
    } else {
      date = new Date();
    }
    return current < date;
  };


  validateData() {
    if (this.data.length > 0) {
      for (var reservation of this.data) {
        var nameIsNotValid = this.validator.validateName(reservation);
        var statusIsNotValid = this.validator.validateStatus(reservation);
        var sourceIsNotValid = this.validator.validateSource(reservation);

        if (nameIsNotValid || statusIsNotValid || sourceIsNotValid) {
          this.dataIsValid = false;
        } else {
          this.dataIsValid = true;
        }
        for (var room of reservation.rooms) {
          var checkInIsNotValid = this.validator.validateCheckIn(room);
          var checkOutIsNotValid = this.validator.validateCheckOut(room);
          var childsIsNotValid = this.validator.validateChilds(room);
          var roomTypeIsNotValid = this.validator.validateRoomType(room);
          var adultsNotValid = this.validator.validateAdults(room);
          var sumIsNotValid = this.validator.validateSum(room);
          var sumIsNotValid = this.validator.validateSum(room);

          if (!this.dataIsValid) {
            return;
          }

          if (checkInIsNotValid || checkOutIsNotValid || childsIsNotValid || sumIsNotValid
            || adultsNotValid || roomTypeIsNotValid || sumIsNotValid) {
            this.dataIsValid = false;
          } else {
            this.dataIsValid = true;
          }
        }

      }
    }
  }

  openCheckOut(room: Room) {
    room.isOpenedCheckout = true;
  }

  closeCheckOut(room: Room) {
    room.isOpenedCheckout = false;
  }

  cancel(): void {
  }

  confirmDeleteRoom(i: number, y: number): void {
    this.deleteRoom(i, y);
    this.notification.successMessage('Бронь удалена');
  }

  confirmDeleteReservation(i: number): void {
    this.deleteReservation(i);
    this.notification.successMessage('Бронь удалена');
  }

  createFailNotification(): void {
    this.notification.warningMessage('Предупреждение!', 'Заполните все поля.');
  }

  setCounts(room) {
    const foundRoom: any = this.roomTypes.find((el: any) => {
      return el.id === room.room_type_id;
    });

    room.adults = null;
    room.children = null;
    room.max_adults = new Array(foundRoom.max_adults);
    const array = new Array(foundRoom.max_children);
    room.max_children = [0].concat(array);
  }


}
