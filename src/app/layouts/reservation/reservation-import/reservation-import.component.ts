import {Component} from '@angular/core';
import {Reservation} from './domain/Reservation';

@Component({
  selector: 'app-reservation-import',
  templateUrl: './reservation-import.component.html',
  styleUrls: ['./reservation-import.component.scss']
})
export class ReservationImportComponent {

  isEditable: boolean;
  data: Reservation[] = [];

  getMessage(message: boolean) {
    this.isEditable = message;
  }

  getData(data: Reservation[]) {
    this.data = data;
  }
}
