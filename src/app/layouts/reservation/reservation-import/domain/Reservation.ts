import { Room } from './Room';

export class Reservation{
    public id:any;
    public first_name: string = "";
    public last_name: string  = "";
    public email:string  = "";
    public country_id:String ="";
    public country_name:String ="";
    public source_id:any = '';
    public source_name:String  = "";
    public status:string  = "";
    public status_name: string = '';
    public number:string  = "";
    public rooms:  Room []  = [];

    public validationState= {
        isNameEmpty : false,
        isLastNameEmpty : false,
        isEmailInvalid : false,
        isCountryIdEmpty: false,
        isSourceIdEmpty: false,
        isStatusEmpty : false,
    }
}
