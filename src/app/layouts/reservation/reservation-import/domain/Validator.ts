import {Reservation} from './Reservation';
import {Room} from './Room';


export class Validator {

  public validateName(reservation: Reservation): boolean {
    if (reservation.first_name) {
      reservation.validationState.isNameEmpty = false;
      return false;
    } else {
      reservation.validationState.isNameEmpty = true;
      return true;
    }
  }

  public validateLastName(reservation: Reservation): boolean {
    if (reservation.last_name) {
      reservation.validationState.isLastNameEmpty = false;
      return false;
    } else {
      reservation.validationState.isLastNameEmpty = true;
      return true;
    }
  }

  public validateEmail(reservation: Reservation): boolean {
    let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    reservation.validationState.isEmailInvalid = !emailRegex.test(reservation.email);
    return !emailRegex.test(reservation.email);
    ;
  }

  public validateCheckIn(room: Room): boolean {
    this.validateCheckOut(room);
    if (room.checkin) {
      room.validationState.isCheckInInvalid = false;
      return false;
    } else {
      room.validationState.isCheckInInvalid = true;
      return true;
    }
  }

  public validateCheckOut(room: Room): boolean {
    room.isOpenedCheckout = false;
    if (room.checkout) {
      room.validationState.isCheckOutInvalid = false;
      return false;
    } else {
      room.validationState.isCheckOutInvalid = true;
      return true;
    }
  }

  public validateChilds(room: Room): boolean {
    if (this.isEmpty(room.children) || room.children < 0) {
      room.validationState.isChildsEmpty = true;
      return true;
    } else {
      room.validationState.isChildsEmpty = false;
      return false;
    }
  }


  public validateSum(room: Room): boolean {
    if (!room.sum || room.sum < 1) {
      room.validationState.isSumEmpty = true;
      return true;
    } else {
      room.validationState.isSumEmpty = false;
      return false;
    }
  }

  public validateRoomType(room: Room): boolean {
    if (!room.room_type_id) {
      room.validationState.isRoomTypeEmpty = true;
      return true;
    } else {
      room.validationState.isRoomTypeEmpty = false;
      return false;
    }
  }

  public validateAdults(room: Room): boolean {
    if (room.adults < 1) {
      room.validationState.isAdultsEmpty = true;
      return true;
    } else {
      room.validationState.isAdultsEmpty = false;
      return false;
    }
  }

  public validateStatus(reservation: Reservation): boolean {
    if (!reservation.status) {
      reservation.validationState.isStatusEmpty = true;
      return true;
      ;
    } else {
      reservation.validationState.isStatusEmpty = false;
      return false;
    }
  }

  public validateCountry(reservation: Reservation): boolean {
    if (!reservation.country_id) {
      reservation.validationState.isCountryIdEmpty = true;
      return true;
    } else {
      reservation.validationState.isCountryIdEmpty = false;
      return false;
    }
  }

  public validateSource(reservation: Reservation): boolean {
    if (!reservation.source_id) {
      reservation.validationState.isSourceIdEmpty = true;
      return true;
    } else {
      reservation.validationState.isSourceIdEmpty = false;
      return false;
    }
  }

  private isEmpty(value: any) {
    if (value && (value.toString().length < 1 || value === '')) {
      return true;
    } else {
      return false;
    }
  }

}
