import { differenceInCalendarDays } from 'date-fns';

export class Room {
  public id: any;
  public checkin: Date;
  public checkout: Date;
  public room_type_id: String = '';
  public adults = 0;
  public children = 0;
  public sum = 0;
  public isEdit = false;
  public max_adults: [];
  public max_children: [];
  public room_type: any;

  public isOpenedCheckout = false;
  public previousRoomCheckOut: Date;
  public validationState = {
    isCheckInInvalid: false,
    isCheckOutInvalid: false,
    isRoomTypeEmpty: false,
    isAdultsEmpty: false,
    isChildsEmpty: false,
    isSumEmpty: false,
  };

  disabledCheckIn = (current: Date): boolean => {
    const date = new Date();
    return differenceInCalendarDays(date, current) > 0;
    // if (this.previousRoomCheckOut) {
    //   return current > this.previousRoomCheckOut || current < new Date();
    // }
  }

  disabledCheckOut = (current: Date): boolean => {
    let date;
    if (this.checkin) {
      date = new Date(this.checkin);
    } else {
      date = new Date();
    }
    return current < date;
    // return differenceInCalendarDays(this.checkin, current) > 0;
    // if (this.previousRoomCheckOut) {
    //   return current > this.previousRoomCheckOut || current < new Date();
    // }
  }

}
