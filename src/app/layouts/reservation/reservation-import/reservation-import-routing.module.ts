import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReservationImportComponent} from './reservation-import.component';

const routes: Routes = [
  {
    path: '',
    component: ReservationImportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationImportRoutingModule {
}
