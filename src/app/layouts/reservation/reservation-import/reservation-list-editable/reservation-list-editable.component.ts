import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reservation} from '../domain/Reservation';
import {Room} from '../domain/Room';
import {ReservationImportService} from '../reservation-import.service';
import {DatePipe} from '@angular/common';
import {Validator} from '../domain/Validator';
import {forkJoin} from 'rxjs';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {Router} from '@angular/router';
import {differenceInCalendarDays} from 'date-fns';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {ExtrasService} from '@app/shared/services/extras/extras.service';

@Component({
  selector: 'app-reservation-list-editable',
  templateUrl: './reservation-list-editable.component.html',
  styleUrls: ['./reservation-list-editable.component.scss']
})
export class ReservationListEditableComponent implements OnInit {
  @Input() public data: Reservation[];
  @Output() addNewReservation = new EventEmitter<boolean>();
  @Output() returnData = new EventEmitter<Reservation[]>();
  editCache: { [key: string]: { edit: boolean; data: Reservation } } = {};

  private countries = [];
  private sources = [];
  private roomTypes = [];
  private numbers = [1, 2, 3, 4, 5, 6];
  private dataIsValid = false;
  private validator: Validator = new Validator();
  public isVisible = false;
  public modalTitle = 'Импорт бронирований';
  public statuses: any = {
    confirmed: 'Подтверждено',
    canceled: 'Отменено',
    no_show: 'Незаезд'
  };
  public isRoomEdit = false;
  public nodes = [];

  nzFormatter = (value: number) => value ? value.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + ' ') : '';
  nzParser = (value: number) => value.toString().trim();

  constructor(private notification: NotificationService,
              private reservationService: ReservationImportService,
              private extrasService: ExtrasService,
              public datepipe: DatePipe,
              private router: Router) {
  }

  ngOnInit() {
    this.updateEditCache();

    forkJoin([
      this.extrasService.countries(),
      this.reservationService.getAllRoomTypes(),
      this.reservationService.getAllSources()
    ]).subscribe(res => {
      this.countries = res[0];
      this.roomTypes = res[1];
      this.sources = res[2];
      this.processTree();
      this.processData();
    }, error => {
    });
  }

  private processData() {
    this.data.map((el: any) => {
      el.rooms.map((room: any) => {
        room.room_type = this.getRoomType(room.room_type_id);
      });

      el.status_name = this.statuses[el.status];
      el.country_name = this.getCountryName(el.country_id).name;
      el.source_name = this.getSource(el.source_id.id).name;
    });
  }

  private getRoomType(id) {
    return this.roomTypes.find(el => {
      return el.id === id;
    });
  }

  private getCountryName(id) {
    return this.countries.find(el => {
      return el.id === id;
    });
  }

  private getSource(id) {
    if (Number.isInteger(id)) {
      return this.sources.find(el => {
        return el.id === id;
      });
    } else {
      const parentId = Math.trunc(id);
      const childId = id.toString().split('.')[1];
      const foundParentIndex = this.sources.findIndex(el => {
        return el.id === parentId;
      });
      return this.sources[foundParentIndex].companies.find(el => {
        return el.id == childId;
      });
    }

  }

  sendData = () => {
    this.validateData();
    if (this.dataIsValid) {
      this.isVisible = true;
    } else if (!this.dataIsValid) {
      this.createFailNotification();
    }
    this.processData();
  };

  openAddReservation() {
    this.addNewReservation.emit(false);
  }

  startEdit(id: number): void {
    this.editCache[id].edit = true;
    this.setSelectedSource(id);
    this.isRoomEdit = true;
  }

  private setSelectedSource(id) {
    const foundIndex = this.nodes.findIndex(el => {
      return el.key.id == this.editCache[id].data.source_id.id;
    });
    if (foundIndex > -1) {
      this.editCache[id].data.source_id = this.nodes[foundIndex].key;
    } else {
      const childId = this.editCache[id].data.source_id.id.toString().split('.')[1];
      const parentId = Math.trunc(Number(this.editCache[id].data.source_id.id));
      const foundChild = this.nodes[parentId - 1].children.find(el => {
        return el.id == childId;
      });
      this.editCache[id].data.source_id = foundChild.key;
    }
  }

  cancelEdit(id: number): void {
    const index = this.data.findIndex(item => item.id === id);
    this.editCache[id] = {
      data: {...this.data[index]},
      edit: false,
    };
  }

  saveEdit(id: number, room: Room): void {
    this.validateData();
    if (this.dataIsValid) {
      this.isRoomEdit = false;
      const index = this.data.findIndex(item => item.id === id);

      const foundRoomIndex = this.data[index].rooms.findIndex(el => {
        return el.id === room.id;
      });

      this.data[index].rooms[foundRoomIndex] = room;
      this.editCache[id].edit = false;
      this.data[index] = this.editCache[id].data;
      this.processData();
    } else if (!this.dataIsValid) {
      this.createFailNotification();
    }
  }

  saveEditRoom(id: number, room: Room): void {
    const index = this.data.findIndex(item => item.id === id);

    const foundRoomIndex = this.data[index].rooms.findIndex(el => {
      return el.id === room.id;
    });

    this.data[index].rooms[foundRoomIndex] = room;
    room.isEdit = false;
    this.processData();
  }

  updateEditCache(): void {
    this.data.forEach(item => {
      this.editCache[item.id] = {
        edit: false,
        data: {...item}
      };
    });
  }

  startEditRoom(room: Room): void {
    room.isEdit = true;
  }

  cancelEditRoom(id: number, room: Room): void {
    const index = this.data.findIndex(item => item.id === id);
    this.editCache[id] = {
      data: {...this.data[index]},
      edit: false,
    };
    room.isEdit = false;
  }

  disabledDate = (current: Date): boolean => {
    return current < new Date();
  };


  setCheckout(room: Room) {
    let date = room.checkin;
    if (room.checkout == null) {
      room.checkout = new Date(room.checkin);
      room.checkout.setDate(date.getDate() + 1);
    } else {
      if (room.checkout <= room.checkin) {
        room.checkout = new Date(room.checkin);
        room.checkout.setDate(date.getDate() + 1);
      }
    }
  }

  //-----Check if data to send is valid
  validateData() {
    if (this.data.length > 0) {
      for (var reservation of this.data) {
        var nameIsNotValid = this.validator.validateName(reservation);
        var statusIsNotValid = this.validator.validateStatus(reservation);
        var sourceIsNotValid = this.validator.validateSource(reservation);

        if (nameIsNotValid || statusIsNotValid || sourceIsNotValid) {
          this.dataIsValid = false;
        } else {
          this.dataIsValid = true;
        }

        if (!this.dataIsValid) {
          return;
        }

        for (var room of reservation.rooms) {
          var checkInIsNotValid = this.validator.validateCheckIn(room);
          var checkOutIsNotValid = this.validator.validateCheckOut(room);
          var childsIsNotValid = this.validator.validateChilds(room);
          var roomTypeIsNotValid = this.validator.validateRoomType(room);
          var adultsNotValid = this.validator.validateAdults(room);
          var sumIsNotValid = this.validator.validateSum(room);

          if (checkInIsNotValid || checkOutIsNotValid || childsIsNotValid || sumIsNotValid
            || adultsNotValid || roomTypeIsNotValid || sumIsNotValid) {
            this.dataIsValid = false;
          } else {
            this.dataIsValid = true;
          }
        }

      }
    }
  }

  private saveData = () => {
    this.reservationService.send(this.generateDataToSend(), (data) => {
      this.data = [];
      this.editCache = {};
      this.createSuccessNotification();
      // tslint:disable-next-line:max-line-length
      this.router.navigate([`/${RoutesEnum.HOTEL}`, this.reservationService.getPropertyId(), RoutesEnum.RESERVATION, RoutesEnum.LIST]).finally();
    }, (error) => {
      this.createFailNotification();
    });
  };


  generateDataToSend() {
    let data = {reservations: []};
    for (let item of this.data) {
      this.convertDate(item);
      data.reservations.push(
        {
          first_name: item.first_name,
          last_name: item.last_name,
          email: item.email,
          country_id: item.country_id,
          source_id: item.source_id,
          status: item.status,
          number: item.number,
          rooms: item.rooms
        });
    }
    return data;
  }

  convertDate(item) {
    for (let room of item.rooms) {
      room.checkin = this.datepipe.transform(room.checkin, 'yyyy-MM-dd');
      room.checkout = this.datepipe.transform(room.checkout, 'yyyy-MM-dd');
    }
  }

  createSuccessNotification(): void {
    this.notification.successMessage('Успешно!', 'Ваши данные были сохранены.');
  }

  createFailNotification(): void {
    this.notification.warningMessage('Предупреждение!', 'Заполните все поля.');
  }

  handleCancel() {
    this.isVisible = false;
  }

  handleOk() {
    this.isVisible = false;
    this.saveData();
  }

  setCounts(room) {
    const foundRoom: any = this.roomTypes.find((el: any) => {
      return el.id === room.room_type_id;
    });

    room.adults = null;
    room.children = null;
    room.max_adults = new Array(foundRoom.max_adults);
    const array = new Array(foundRoom.max_children);
    room.max_children = [0].concat(array);
  }

  private processTree() {
    this.nodes = [];
    this.sources.forEach((el, i) => {
      let item: any = {
        title: el.name,
        id: el.id,
        children: [],
        isLeaf: true,
        key: {id: i + 1, children: el.companies}
      };

      if (el.companies.length > 0) {
        item.isLeaf = false;
        item.disabled = true;
        el.companies.forEach(childEl => {
          item.children.push({
            title: childEl.name,
            id: childEl.id,
            isLeaf: true,
            parentId: el.id,
            key: {id: (i + 1 + '.' + childEl.id).toString()}
          });
        });
      } else {
        delete item.children;
      }

      this.nodes.push(item);
    });
  }

  disableStartDate = (current: Date): boolean => {
    const date = new Date();
    return differenceInCalendarDays(date, current) > 0;
  };

  disableEndDate = (current: Date): boolean => {
    let date;
    if (this.data[0].rooms[0].checkin) {
      date = new Date(this.data[0].rooms[0].checkin);
    } else {
      date = new Date();
    }
    return current < date;
  };
}
