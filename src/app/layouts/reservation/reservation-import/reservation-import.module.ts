import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ReservationImportRoutingModule} from './reservation-import-routing.module';
import {ReservationImportComponent} from './reservation-import.component';
import {
  NzButtonModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzFormModule,
  NzIconModule,
  NzInputModule,
  NzInputNumberModule,
  NzModalModule,
  NzNotificationModule,
  NzPopconfirmModule,
  NzSelectModule,
  NzSpinModule,
  NzTableModule,
  NzToolTipModule
} from 'ng-zorro-antd';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReservationListComponent} from './reservation-list/reservation-list.component';
import {ReservationListEditableComponent} from './reservation-list-editable/reservation-list-editable.component';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';
import {CustomPipesModule} from "@app/core/pipes/custom-pipes.module";

@NgModule({
  declarations: [
    ReservationImportComponent,
    ReservationListComponent,
    ReservationListEditableComponent,],
    imports: [
        CommonModule,
        ReservationImportRoutingModule,
        NzButtonModule,
        NzCheckboxModule,
        NzFormModule,
        NzInputModule,
        NzInputNumberModule,
        NzModalModule,
        NzTableModule,
        NzSpinModule,
        NzIconModule,
        FormsModule,
        NzDatePickerModule,
        NzSelectModule,
        NzNotificationModule,
        ReactiveFormsModule,
        NzPopconfirmModule,
        NzToolTipModule,
        OldShared,
        CustomPipesModule
    ],
  providers: [DatePipe]
})
export class ReservationImportModule {
}
