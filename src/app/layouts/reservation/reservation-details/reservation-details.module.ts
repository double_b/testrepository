import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReservationDetailsRoutingModule} from './reservation-details-routing.module';
import {ReservationDetailsComponent} from './reservation-details.component';
import {ReservationDetailsRoomsTabComponent} from './components/reservation-details-rooms-tab/reservation-details-rooms-tab.component';
import {ReservationDetailsTopPanelComponent} from './components/reservation-details-top-panel/reservation-details-top-panel.component';
import {ReservationDetailsGuestsTabComponent} from './components/reservation-details-guests-tab/reservation-details-guests-tab.component';
import {ReservationDetailsTabsPanelComponent} from './components/reservation-details-tabs-panel/reservation-details-tabs-panel.component';
import {ReservationDetailsCreditCardTabComponent} from './components/reservation-details-credit-card-tab/reservation-details-credit-card-tab.component';
import {ReservationDetailsNoteTabComponent} from './components/reservation-details-note-tab/reservation-details-note-tab.component';
import {ReservationDetailsBookingHistoryTabComponent} from './components/reservation-details-booking-history-tab/reservation-details-booking-history-tab.component';
import {ReservationDetailsCorrespondenceTabComponent} from './components/reservation-details-correspondence-tab/reservation-details-correspondence-tab.component';

import {DigitOnlyModule} from '@uiowa/digit-only';
import {CustomPipesModule} from 'src/app/core/pipes/custom-pipes.module';
import {SharedModule} from '@app/core/shared/shared.module';
import {CalendarService} from '@app/layouts/calendar/calendar.service';
import {ReservationDetailsEditPriceComponent} from '@app/layouts/reservation/reservation-details/components/reservation-details-edit-price/reservation-details-edit-price.component';
import {ReservationDetailsEditComponent} from '@app/layouts/reservation/reservation-details/components/reservation-details-edit/reservation-details-edit.component';


@NgModule({
  declarations: [
    ReservationDetailsComponent,
    ReservationDetailsTopPanelComponent,
    ReservationDetailsTabsPanelComponent,
    ReservationDetailsEditPriceComponent,
    ReservationDetailsEditComponent,
    ReservationDetailsRoomsTabComponent,
    ReservationDetailsGuestsTabComponent,
    ReservationDetailsCreditCardTabComponent,
    ReservationDetailsNoteTabComponent,
    ReservationDetailsBookingHistoryTabComponent,
    ReservationDetailsCorrespondenceTabComponent,
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    CustomPipesModule,

    ReactiveFormsModule,
    FormsModule,
    ReservationDetailsRoutingModule,

    DigitOnlyModule,
    SharedModule,
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    CalendarService
  ]
})
export class ReservationDetailsModule {
}
