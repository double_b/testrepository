import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {StatusApiService} from '@app/shared/services/status-api/status-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ReservationApiService} from '@app/shared/services/reservation-api/reservation-api.service';
import {GuestApiService} from '@app/shared/services/guest-api/guest-api.service';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReservationDetailsService {

  constructor(private reservationApiService: ReservationApiService,
              private statusApiService: StatusApiService,
              private guestApiService: GuestApiService) {
  }

  getGuests(params?: HttpParams): Observable<any> {
    return this.guestApiService.getGuests(this.getHotelId(), params);
  }

  addSingleGuestForReservation(bookingId: number, body: any): Observable<any> {
    return this.reservationApiService.addSingleGuestForReservation(this.getHotelId(), bookingId, body);
  }

  updateSingleGuest(guestId: number, body: any): Observable<any> {
    return this.guestApiService.updateGuest(this.getHotelId(), guestId, body);
  }

  deleteSingleGuestForReservation(bookingId: number, guestId: number): Observable<any> {
    return this.reservationApiService.deleteSingleGuestForReservation(this.getHotelId(), bookingId, guestId);
  }

  getSingleReservation(bookingId: number): Observable<any> {
    return this.reservationApiService.getSingleReservation(this.getHotelId(), bookingId);
  }

  confirmReservation(bookingId: number): Observable<any> {
    return this.statusApiService.confirmReservation(this.getHotelId(), bookingId);
  }

  cancelReservation(bookingId: number): Observable<any> {
    return this.statusApiService.cancelReservation(this.getHotelId(), bookingId);
  }

  arrivedReservation(bookingId: number): Observable<any> {
    return this.statusApiService.arrivedReservation(this.getHotelId(), bookingId);
  }

  leftReservation(bookingId: number): Observable<any> {
    return this.statusApiService.leftReservation(this.getHotelId(), bookingId);
  }

  noShowReservation(bookingId: number): Observable<any> {
    return this.statusApiService.noShowReservation(this.getHotelId(), bookingId);
  }

  getReservationNotes(bookingId: number, isArchived: boolean = false): Observable<any> {
    return this.reservationApiService.getReservationNotes(this.getHotelId(), bookingId, isArchived);
  }

  addReservationNote(bookingId: number, body: any): Observable<any> {
    return this.reservationApiService.addReservationNote(this.getHotelId(), bookingId, body);
  }

  deleteReservationNote(bookingId: number, noteId: number, forceDelete: boolean = false): Observable<any> {
    return this.reservationApiService.deleteReservationNote(this.getHotelId(), bookingId, noteId, forceDelete);
  }

  getHotelId(): number {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));
    return Number(hotel.id);
  }
}
