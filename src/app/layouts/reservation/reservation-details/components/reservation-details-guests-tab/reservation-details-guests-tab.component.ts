import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Patterns} from '@app/core/constants/validation-patterns';
import * as moment from 'moment';
import * as _ from 'lodash';
import {Observable, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {ReservationDetailsService} from '@app/layouts/reservation/reservation-details/reservation-details.service';

@Component({
  selector: 'app-reservation-details-guests-tab',
  templateUrl: './reservation-details-guests-tab.component.html',
  styleUrls: ['./reservation-details-guests-tab.component.scss']
})
export class ReservationDetailsGuestsTabComponent implements OnInit, OnChanges, OnDestroy {
  @Input() reservation: any;

  private existingGuestId: number;

  public editMode: boolean = false;
  private newGuest = false;

  public guestList: any[] = [];

  public guestForm: FormGroup;

  public isExtraInfoPanelShown: boolean = false;
  public selectedIndex: number = 0;

  public guestToDelete = null;

  public isGuestDeleteModalVisible = false;

  public docTypes = [];
  public countriesList = [];

  public guestSuggestions = [];

  private countriesSubscription: Subscription;
  private docTypesSubscription: Subscription;
  private guestUpdateSubscription: Subscription;
  private guestDeleteSubscription: Subscription;
  private formCangeSubscription: Subscription[] = [];
  private apiGuestsSubscription: Subscription[] = [];

  public couldGuestBeAttached: boolean = true;

  public get isExistingGuestToChange(): boolean {
    return this.editMode && !this.newGuest;
  }

  public genderSelection = [
    {value: 0, label: 'Женский'},
    {value: 1, label: 'Мужской'},
  ];

  private getProcessedDate(dateStr, asString = false, options = null): string | Date | null {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    if (asString) {
      return dateProcessed.isValid() ? dateProcessed.toDate() : null;
    } else {
      return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
    }
  }

  public getValueWithFallback(controlName): string {
    let result = '';
    if (this.guestForm.get(controlName).value == null || this.guestForm.get(controlName).value === '') {
      result = '---';
    } else {
      if (controlName === 'country_id' || controlName === 'document_country_id') {
        const country = this.countriesList
          .find(ctry => ctry.id === this.guestForm.get(controlName).value);
        result = country != null ? country.name : '---';
      } else if (controlName === 'gender') {
        const gender = this.genderSelection
          .find(d => d.value === this.guestForm.get(controlName).value);
        result = gender != null ? gender.label : '---';
      } else if (controlName === 'document_type_id') {
        const docType = this.docTypes
          .find(doc => doc.id === this.guestForm.get(controlName).value);
        result = docType != null ? docType.name : '---';
      } else if (controlName === 'birthday' || controlName === 'document_issued_at' || controlName === 'document_expired_at') {
        result = <any> this.getProcessedDate(this.guestForm.get(controlName).value);
      } else {
        result = `${this.guestForm.get(controlName).value}`;
      }
    }

    return result;
  }

  private setDataToForm(guest: any): void {
    this.guestForm.get('first_name').setValue(guest.first_name);
    this.guestForm.get('last_name').setValue(guest.last_name);
    this.guestForm.get('email').setValue(guest.email);
    this.guestForm.get('phone').setValue(guest.phone);
    this.guestForm.get('country_id').setValue(guest.country_id);
    this.guestForm.get('birthday').setValue(this.getProcessedDate(guest.birthday, true));
    this.guestForm.get('gender').setValue(guest.gender);
    this.guestForm.get('inn').setValue(guest.inn);
    this.guestForm.get('company_name').setValue(guest.company_name);
    this.guestForm.get('company_number').setValue(guest.company_number);
    this.guestForm.get('city_id').setValue(guest.city_id);
    this.guestForm.get('region').setValue(guest.region);
    this.guestForm.get('address').setValue(guest.address);
    this.guestForm.get('address2').setValue(guest.address2);
    this.guestForm.get('zip_code').setValue(guest.zip_code);
    this.guestForm.get('document_type_id').setValue(guest.document_type_id);
    this.guestForm.get('document_number').setValue(guest.document_number);
    this.guestForm.get('document_issued_at').setValue(this.getProcessedDate(guest.document_issued_at, true));
    this.guestForm.get('document_country_id').setValue(guest.document_country_id);
    this.guestForm.get('document_expired_at').setValue(this.getProcessedDate(guest.document_expired_at, true));
  }

  private initializeFormWithFirstGuest(guestList: any[]): void {
    const guest = guestList[0];
    this.setDataToForm(guest);
  }

  private setSubscribtionFormForAutocomplete(): void {
    const ctrlsSearchBy = ['first_name', 'last_name', 'email', 'phone'];
    ctrlsSearchBy.forEach(key => {
      const controlSubscription = this.guestForm.get(key).valueChanges
        .pipe(
          distinctUntilChanged(),
          debounceTime(500)
        )
        .subscribe(data => {
          if (data != null && data !== '') {
            const params = new HttpParams().set(key, data);
            const guestSubscription = this.reservationDetailsService.getGuests(params)
              .subscribe(resp => {
                this.handleNewGuestAttachPossibility(resp);
                this.guestSuggestions = resp.data;
              });
            this.apiGuestsSubscription.push(guestSubscription);
          }
        });
      this.formCangeSubscription.push(controlSubscription);
    });
  }

  private handleNewGuestAttachPossibility(resp: any): void {
    if (resp.can_attach_guest != null) {
      this.couldGuestBeAttached = resp.can_attach_guest;
    }
  }

  constructor(private extrasService: ExtrasService,
              private reservationDetailsService: ReservationDetailsService) {
    this.guestForm = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(Patterns.emailPattern)]),
      phone: new FormControl(''),
      country_id: new FormControl(null, Validators.required),
      birthday: new FormControl(''),
      gender: new FormControl(''),
      inn: new FormControl(''),
      company_name: new FormControl(''),
      company_number: new FormControl(''),
      city_id: new FormControl(''),
      region: new FormControl(''),
      address: new FormControl(''),
      address2: new FormControl(''),
      zip_code: new FormControl(''),
      document_type_id: new FormControl(''),
      document_number: new FormControl(''),
      document_issued_at: new FormControl(''),
      document_country_id: new FormControl(''),
      document_expired_at: new FormControl(''),
    });

    this.countriesSubscription = this.extrasService.countries()
      .subscribe(data => {
        this.countriesList = data;
      });

    this.docTypesSubscription = this.extrasService.docTypes()
      .subscribe(data => {
        this.docTypes = data;
      });
  }

  private addExtraGuests(reservation: any): void {
    const mainGuest = reservation.guest;
    if (reservation.rooms) {
      const guests = reservation.rooms.map(room => room.guests);
      let result = [].concat.apply([], guests);

      // 1) remove guest entities that is equal to the main guest
      result = result.filter(guest => guest.id !== mainGuest.id);
      // 2) remove other duplicate entities
      result = _.uniqBy(result, guest => guest.id);

      if (result.length > 0) {
        result.forEach(guest => this.guestList.push(guest));
      }
    }

  }

  public ngOnInit(): void {
    this.setSubscribtionFormForAutocomplete();
  }

  public ngOnChanges(): void {
    if (this.reservation !== null && Object.keys(this.reservation).length > 0 && this.reservation.guest != null) {
      this.couldGuestBeAttached = this.reservation.can_attach_guest;
      this.guestList.push(this.reservation.guest);
      this.addExtraGuests(this.reservation);
      this.initializeFormWithFirstGuest(this.guestList);
    }
  }

  public ngOnDestroy(): void {
    if (this.countriesSubscription != null) {
      this.countriesSubscription.unsubscribe();
    }
    if (this.docTypesSubscription != null) {
      this.docTypesSubscription.unsubscribe();
    }
    if (this.guestUpdateSubscription != null) {
      this.guestUpdateSubscription.unsubscribe();
    }
    if (this.guestDeleteSubscription != null) {
      this.guestDeleteSubscription.unsubscribe();
    }

    this.formCangeSubscription.forEach(subscr => subscr.unsubscribe());
    this.apiGuestsSubscription.forEach(subscr => subscr.unsubscribe());
    this.formCangeSubscription = [];
    this.apiGuestsSubscription = [];
  }

  public handleOptionClicking(guest: any): void {
    this.setDataToForm(guest.nzValue);
    this.existingGuestId = guest.id;
  }

  public handleCancel(): void {
    this.isGuestDeleteModalVisible = false;
  }

  public handleOk(): void {
    if (this.guestToDelete != null) {
      this.guestDeleteSubscription = this.reservationDetailsService
        .deleteSingleGuestForReservation(this.reservation.id, this.guestToDelete.id)
        .subscribe(resp => {
          this.handleNewGuestAttachPossibility(resp);
          this.guestList = this.guestList.filter(item => item.id !== this.guestToDelete.id);
          this.selectedIndex -= 1;
          this.isGuestDeleteModalVisible = false;

          const guest = this.guestList[this.selectedIndex];
          this.setDataToForm(guest);
        });
    }
  }

  private getFormDataPayload(formData: any) {
    formData.birthday = formData.birthday != null
      ? moment(formData.birthday).format('YYYY-MM-DD')
      : null;
    formData.document_issued_at = formData.document_issued_at != null
      ? moment(formData.document_issued_at).format('YYYY-MM-DD')
      : null;
    formData.document_expired_at = formData.document_expired_at != null
      ? moment(formData.document_expired_at).format('YYYY-MM-DD')
      : null;

    const result: any = {};
    Object.keys(formData).forEach(key => {
      if (formData[key] != null) {
        result[key] = formData[key];
      }
    });

    return result;
  }

  private setNewValueToGuest(guest: any, formValue: any): Observable<any> {
    Object.keys(formValue).forEach(key => {
      guest[key] = formValue[key];
    });
    let payload = null;
    let request$ = null;

    if (this.newGuest) {
      if (this.existingGuestId != null) {
        payload = {
          existing_guest: this.existingGuestId
        };
      } else {
        payload = {
          new_guest: this.getFormDataPayload(formValue)
        };
      }
      request$ = this.reservationDetailsService.addSingleGuestForReservation(this.reservation.id, payload);
    } else {
      payload = this.getFormDataPayload(formValue);
      request$ = this.reservationDetailsService.updateSingleGuest(guest.id, payload);
    }

    return request$;
  }

  public changeMode(mode: 'edit' | 'cancel' | 'save'): void {
    if (mode === 'edit') {
      this.editMode = true;
    }
    if (mode === 'cancel') {
      // if 'Cancel' clicked for empty form of new user, just fallback to prev guest
      this.checkAndDeleteEmptyUserForm();
      const guest = this.guestList[this.selectedIndex];
      this.setDataToForm(guest);

      this.editMode = false;
    }
    if (mode === 'save') {
      if (this.guestForm.invalid) {
        return;
      }
      const guest = this.guestList[this.selectedIndex];
      const formData = this.guestForm.value;

      this.guestUpdateSubscription = this.setNewValueToGuest(guest, formData)
        .subscribe(resp => {
          this.handleNewGuestAttachPossibility(resp);
          // @FIX see comment
          // this.guestList[this.selectedIndex] = resp.data.attached_guest; // update from server (because there is no guest id!)
          this.existingGuestId = null;
          this.newGuest = false;
          this.editMode = false;
        });
    }
  }

  public deleteItem(index: number, guest: any): void {
    if (this.isPersonNonEmpty(index)) {
      this.guestToDelete = guest;
      this.isGuestDeleteModalVisible = true;
    } else {
      this.guestList = this.guestList.filter((item, i) => i !== index);
      this.selectedIndex = this.guestList.length - 1;

      const guest = this.guestList[this.selectedIndex];
      this.setDataToForm(guest);
    }
  }

  public isPersonNonEmpty(index: number): boolean {
    if (index == null || this.guestList[index] == null) {
      return;
    }
    const person = this.guestList[index];
    return person.first_name != null && person.first_name !== ''
      && person.last_name != null && person.last_name !== '';
  }

  public getTabName(person: any, index: number): string {
    return this.isPersonNonEmpty(index)
      ? `${person.first_name} ${person.last_name}` : 'Новый гость';
  }

  public addNewGuest(): void {
    const newMockedUser = {
      first_name: '',
      last_name: '',
      email: '',
      phone: '',
      country_id: '',
      birthday: '',
      gender: '',
      inn: '',
      company_name: '',
      company_number: '',
      city_id: '',
      region: '',
      address: '',
      address2: '',
      zip_code: '',
      document_type_id: '',
      document_number: '',
      document_issued_at: '',
      document_country_id: '',
      document_expired_at: ''
    };
    this.guestList.push(newMockedUser);
    this.selectedIndex = this.guestList.length - 1;
    this.guestForm.reset();
    this.editMode = true;
    this.newGuest = true;

    this.guestSuggestions = []; // clear guest suggestions from other tabs
  }

  private checkAndDeleteEmptyUserForm(): void {
    if (!this.isPersonNonEmpty(this.selectedIndex)) {
      this.guestList = this.guestList.filter((item, i) => i !== this.selectedIndex);
      this.selectedIndex = this.guestList.length - 1;
      this.existingGuestId = null;
    }
  }

  public selectPerson(index: number): void {
    if (this.selectedIndex === index) {
      return;
    }
    this.checkAndDeleteEmptyUserForm();
    this.selectedIndex = index;

    this.editMode = false;

    const guest = this.guestList[index];
    this.setDataToForm(guest);
  }

  public toggleExtraInfoPanel(): void {
    this.isExtraInfoPanelShown = !this.isExtraInfoPanelShown;
  }
}
