import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {StatusEnum} from '@app/core/constants/status.enum';
import {ReservationDetailsService} from '@app/layouts/reservation/reservation-details/reservation-details.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {Utils} from '@app/shared/helpers/utils';
import {NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-reservation-details-top-panel',
  templateUrl: './reservation-details-top-panel.component.html',
  styleUrls: ['./reservation-details-top-panel.component.scss']
})
export class ReservationDetailsTopPanelComponent implements OnChanges {

  statuses = [
    {
      color: '#1890ff',
      label: 'STATUS.CONFIRM',
      value: StatusEnum.CONFIRM
    },
    {
      color: '#FA541C',
      label: 'STATUS.CANCEL',
      value: StatusEnum.CANCEL
    },
    {
      color: '#52c41a',
      label: 'STATUS.IN_HOUSE',
      value: StatusEnum.IN_HOUSE
    },
    {
      color: '#919090',
      label: 'STATUS.CHECKOUT',
      value: StatusEnum.CHECKOUT
    },
    {
      color: '#ff4d4f',
      label: 'STATUS.NO_SHOW',
      value: StatusEnum.NO_SHOW
    },
  ];
  selectedStatus = null;
  currentStatus = null;
  isStatusesPopupVisible = false;

  @Output() switchLoader = new EventEmitter<boolean>();

  hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
  @Input() reservation: any;

  private guestList = [];

  private today = new Date();

  private gestStatusOptions = [];

  private setStatuses(reservation: any): Array<{ label: string, value: string }> {
    const statuses = [];
    if (reservation != null && Object.keys(reservation).length > 0) {
      statuses.push({value: StatusEnum.CONFIRM, label: 'Подтверждено'});
      // { value: 'interior', label: 'Проживает' },
      // { value: 'checked', label: 'Выехал' },

      const dateDiff = moment(reservation.checkout, 'YYYY-MM-DD').diff(moment(this.today), 'days');

      // datediff > 1 === tomorrow or next days
      if (reservation.status === StatusEnum.CONFIRM && dateDiff > 1 && reservation.channel.key === 'smartbooking') {
        statuses.push({value: StatusEnum.CANCEL, label: 'Отменено'});
      }

      // datediff < 0 === previous days, before today
      if (reservation.status === StatusEnum.CONFIRM && dateDiff < 0) {
        statuses.push({value: StatusEnum.NO_SHOW, label: 'Незаезд'});
      }
    }

    return statuses;
  }

  public mockedTotalPriceList = {
    suggestedDeposit: 'US$ 0,00',
    subtotal: 'US$ 128,00',
    extraOptions: 'US$ 0,00',
    totalPrice: 'US$ 128,00',
    paid: 'US$ 0,00',
    debt: 'US$ 128,00',
  };
  selectedGuestStatus: string;
  isPopupVisible = false;

  selectedStatusIndex = null;

  noteControl = new FormControl(null);

  // public get selectedStatusLabel(): string {
  //   const selection = this.allReservationStatuses.find(item => item.value === this.selectedGuestStatus);
  //   return selection != null ? selection.label : '';
  // }

  public getProcessedDate(dateStr, options = null): string {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }

  private setGuests(): void {
    this.guestList.push(this.reservation.guest);
    // const extraGuests = [].concat.apply([], this.reservation.rooms.map(room => room.guests));
    // this.guestList.concat.apply([], extraGuests);
  }

  constructor(private router: Router,
              private message: NotificationService,
              public reservationDetailsService: ReservationDetailsService,
              private modal: NzModalService
  ) {
  }

  public ngOnChanges(): void {
    if (this.reservation != null && Object.keys(this.reservation).length > 0) {
      this.selectedStatus = this.reservation.status;
      this.currentStatus = this.reservation.status;
      this.gestStatusOptions = this.setStatuses(this.reservation);
      this.setGuests();
      this.noteControl.setValue(this.reservation.channel_remarks);
    } else if (this.reservation != null && Object.keys(this.reservation).length === 0) {
      this.router.navigate([`/${RoutesEnum.HOTEL}`, this.hotelID, RoutesEnum.RESERVATION, RoutesEnum.LIST]).finally();
    }
  }

  public getGuestsFormatted(): string {
    if (this.guestList.length === 1) {
      if (!Utils.isExist(this.guestList[0])) {
        return '';
      }
    }
    if (this.guestList.includes(item => item === null)) {
      return '';
    }
    return this.guestList.length > 0
      ? this.guestList.map(guest => `${guest.first_name} ${guest.last_name}`).join(', ')
      : '';
  }

  public getGuestsPluralTitle(): string {
    let suffix = '';
    const count = this.guestList.length;
    if (count % 10 === 1 && count !== 11) {
      suffix = 'гость';
    } else if (count % 10 === 2 || count % 10 === 3 || count % 10 === 4) {
      suffix = 'гостя';
    } else {
      suffix = 'гостей';
    }
    return `${count} ${suffix}`;
  }

  public getNightPluralWithSuffix(nights: number): string {
    let suffix = '';
    if (nights % 10 === 1 && nights !== 11) {
      suffix = 'ночь';
    } else if (nights % 10 === 2 || nights % 10 === 3 || nights % 10 === 4) {
      suffix = 'ночи';
    } else {
      suffix = 'ночей';
    }
    return `${nights} ${suffix}`;
  }

  public handleCancel() {
    this.selectedStatus = this.currentStatus;
    this.isStatusesPopupVisible = false;
  }

  openModal() {
    if (this.reservation != null) {
      switch (this.selectedStatus) {
        case StatusEnum.CHECKOUT: {
          if (Utils.compareDates(new Date(), this.reservation.checkout)) {
            this.isStatusesPopupVisible = true;
          } else {
            this.modal.info({
              nzTitle: 'Предупреждение',
              nzContent: 'Вы должны обновить дату выезда',
              nzOnOk: () => this.handleCancel()
            });
          }
          break;
        }
        case StatusEnum.IN_HOUSE: {
          if (Utils.compareDates(new Date(), this.reservation.checkin, true)) {
            this.isStatusesPopupVisible = true;
          } else {
            this.modal.info({
              nzTitle: 'Предупреждение',
              nzContent: 'Не возможно установить статус \"В номере\". Дата заезда должна быть меньше или равна сегодняшней!',
              nzOnOk: () => this.handleCancel()
            });
          }
          break;
        }
        case StatusEnum.CONFIRM:
        case StatusEnum.CANCEL:
        case StatusEnum.NO_SHOW: {
          this.isStatusesPopupVisible = true;
          break;
        }
      }
    }
  }

  public handleOk() {
    if (this.reservation != null) {
      let request$: Observable<any> = null;
      this.switchLoader.emit(true);
      switch (this.selectedStatus) {
        case StatusEnum.CONFIRM:
          request$ = this.reservationDetailsService.confirmReservation(this.reservation.id);
          break;
        case StatusEnum.CANCEL:
          request$ = this.reservationDetailsService.cancelReservation(this.reservation.id);
          break;
        case StatusEnum.IN_HOUSE:
          request$ = this.reservationDetailsService.arrivedReservation(this.reservation.id);
          break;
        case StatusEnum.CHECKOUT:
          request$ = this.reservationDetailsService.leftReservation(this.reservation.id);
          break;
        case StatusEnum.NO_SHOW:
          request$ = this.reservationDetailsService.noShowReservation(this.reservation.id);
          break;
      }

      if (request$ != null) {
        request$.subscribe(data => {
          this.currentStatus = this.selectedStatus;
          this.message.successMessage('Выполнено');
          this.switchLoader.emit(false);
        }, error => {
          this.selectedStatus = this.currentStatus;
          this.message.errorMessage('error', error);
          this.switchLoader.emit(false);
        });
      }
    }
    this.isStatusesPopupVisible = false;
  }

  getSource(reservation: any): string {
    return Utils.getSource(reservation);
  }
}
