import {Component, Input, OnChanges, OnDestroy} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {ReservationDetailsService} from '@app/layouts/reservation/reservation-details/reservation-details.service';

type Tab = 'active' | 'archived';

@Component({
  selector: 'app-reservation-details-note-tab',
  templateUrl: './reservation-details-note-tab.component.html',
  styleUrls: ['./reservation-details-note-tab.component.scss']
})
export class ReservationDetailsNoteTabComponent implements OnChanges, OnDestroy {
  @Input() reservation: any;

  private archivedNotesGetSubscription: Subscription;
  private activeNotesGetSubscription: Subscription;
  private notesPostSubscription: Subscription;
  private activeNotesPostSubscription: Subscription;

  public activeNotes: any[] = [];
  public archivedNotes: any[] = [];

  public activeNotesTitle = 'Aктивные заметки';
  public archivedNotesTitle = 'Архивные заметки';

  public activeTab: Tab = 'active';

  public get selectedNoteList(): any[] {
    return this.activeTab === 'active'
      ? this.activeNotes
      : this.archivedNotes;
  }

  public selectTab(tabName: 'active' | 'archived'): void {
    this.activeTab = tabName;
  }

  public messageForm: FormGroup;

  public isDeleteMessageModalVisible = false;

  public noteToDelete = null;

  private addNote(noteText: string): void {
    const payload = {text: noteText};
    const bookingId = this.reservation.id;

    this.notesPostSubscription = this.reservationDetailsService.addReservationNote(bookingId, payload)
      .subscribe(resp => {
        this.retrieveAllNotes();
      });
  }

  constructor(private reservationDetailsService: ReservationDetailsService) {
    this.messageForm = new FormGroup({
      message: new FormControl(''),
    });
  }

  public ngOnChanges(): void {
    if (this.reservation !== null && Object.keys(this.reservation).length > 0) {
      this.retrieveAllNotes();
    }
  }

  public ngOnDestroy(): void {
    if (this.archivedNotesGetSubscription != null) {
      this.archivedNotesGetSubscription.unsubscribe();
    }
    if (this.activeNotesGetSubscription != null) {
      this.activeNotesGetSubscription.unsubscribe();
    }
    if (this.notesPostSubscription != null) {
      this.notesPostSubscription.unsubscribe();
    }
    if (this.activeNotesPostSubscription != null) {
      this.activeNotesPostSubscription.unsubscribe();
    }
  }

  private processNotes(notes: any[]): any[] {
    return notes.map(item => {
      item.created_at = moment(item.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD.MM.YYYY (HH:mm)');
      return item;
    });
  }

  private retrieveAllNotes(): void {
    const bookingId = this.reservation.id;
    this.archivedNotesGetSubscription = this.reservationDetailsService.getReservationNotes(bookingId).pipe(
      map(res => this.processNotes(res)))
      .subscribe(notes => {
        this.activeNotes = notes;
      });
    this.activeNotesGetSubscription = this.reservationDetailsService.getReservationNotes(bookingId, true).pipe(
      map(res => this.processNotes(res)))
      .subscribe(notes => {
        this.archivedNotes = notes;
      });
  }

  public handleCancel(): void {
    this.isDeleteMessageModalVisible = false;
  }

  public handleOk(): void {
    if (this.noteToDelete != null) {
      const request$ = this.activeTab === 'active'
        ? this.reservationDetailsService.deleteReservationNote(this.reservation.id, this.noteToDelete.id)
        : this.reservationDetailsService.deleteReservationNote(this.reservation.id, this.noteToDelete.id, true);
      request$.subscribe(resp => {
        this.retrieveAllNotes();
        this.isDeleteMessageModalVisible = false;
      });
    }
  }

  public deleteItem(post: any): void {
    this.noteToDelete = post;
    this.isDeleteMessageModalVisible = true;
  }

  public submitForm(): void {
    const value = this.messageForm.get('message').value;
    if (value == null || value === '') {
      return;
    }
    this.addNote(value);
    this.messageForm.reset();
  }
}
