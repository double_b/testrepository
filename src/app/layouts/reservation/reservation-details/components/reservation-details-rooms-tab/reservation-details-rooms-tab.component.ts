import {Component, Input} from '@angular/core';
import * as moment from 'moment';
import {NzDrawerService} from 'ng-zorro-antd';
// tslint:disable-next-line:max-line-length
import {ReservationDetailsEditPriceComponent} from '@app/layouts/reservation/reservation-details/components/reservation-details-edit-price/reservation-details-edit-price.component';
// tslint:disable-next-line:max-line-length
import {ReservationDetailsEditComponent} from '@app/layouts/reservation/reservation-details/components/reservation-details-edit/reservation-details-edit.component';
import {Utils} from '@app/shared/helpers/utils';
import {StatusEnum} from '@app/core/constants/status.enum';

@Component({
  selector: 'app-reservation-details-rooms-tab',
  templateUrl: './reservation-details-rooms-tab.component.html',
  styleUrls: ['./reservation-details-rooms-tab.component.scss']
})
export class ReservationDetailsRoomsTabComponent {
  @Input() reservation: any;

  public roomToDelete: any;
  public isVisibleDelete = false;
  public popupVisible = false;

  constructor(private drawerService: NzDrawerService) {
  }

  public get guestList(): any[] {
    return this.reservation != null && Object.keys(this.reservation).length > 0
      ? this.reservation.rooms : [];
  }

  public getProcessedDate(dateStr, options = null): string {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }

  public getGuestsFormatted(guestList): string {
    return guestList.length > 0
      ? guestList.map(guest => `${guest.first_name} ${guest.last_name}`).join(', ')
      : '';
  }

  public generateArrayForNumber(count: number): number[] {
    const arr = [];
    for (let i = 0; i < count; i++) {
      arr.push(i);
    }
    return arr;
  }

  public deleteRoom(room) {
    this.roomToDelete = room;
    this.isVisibleDelete = true;
    this.popupVisible = false;
  }

  public cancelDelete() {
    this.isVisibleDelete = false;
    this.roomToDelete = null;
  }

  public deleteElm() {
    this.isVisibleDelete = false;
    this.roomToDelete = null;
  }

  public editPrice(data): void {
    this.drawerService.create<ReservationDetailsEditPriceComponent, {}, string>({
      nzTitle: null,
      nzWidth: 750,
      nzClosable: false,
      nzBodyStyle: {padding: 0},
      nzPlacement: 'left',
      nzContent: ReservationDetailsEditPriceComponent,
      nzContentParams: {
        guest: 'test',
        period: {
          checkin: data.checkin, checkout: data.checkout, isReservation: true,
          room: {type: 1, count: 1, name: data.room.name}
        },
        roomTypes: [{id: data.room_type.id, name: data.room_type.name}],
      },
    });
  }

  public edit(data): void {
    let reservationData = {...this.reservation};
    reservationData.rooms = [data];
    this.drawerService.create<ReservationDetailsEditComponent, {}, string>({
      nzTitle: null,
      nzWidth: 750,
      nzClosable: false,
      nzBodyStyle: {padding: 0},
      nzPlacement: 'left',
      nzContent: ReservationDetailsEditComponent,
      nzContentParams: {
        data: reservationData,
        guest: 'test',
        period: {
          checkin: data.checkin, checkout: data.checkout,
          room: {type: 1, count: 1, name: data.room.name}
        },
        roomTypes: [{id: data.room_type.id, name: data.room_type.name}],
      },
    });
  }

  getSwitchDisabledText(room: any): string {
    if (!Utils.isExist(room.status)) {
      return '';
    }
    switch (room.status) {
      case StatusEnum.CANCEL: {
        return 'STATUS.CANCEL';
      }
      case StatusEnum.NO_SHOW: {
        return 'STATUS.NO_SHOW';
      }
      case StatusEnum.CHECKOUT: {
        return 'STATUS.CHECKOUT';
      }
      default: {
        return '';
      }
    }
  }

  getSwitchDisabled(room: any): boolean {
    if (!Utils.isExist(room.status)) {
      return true;
    }
    switch (room.status) {
      case StatusEnum.CANCEL:
      case StatusEnum.NO_SHOW:
      case StatusEnum.CHECKOUT: {
        return true;
      }
      default: {
        return false;
      }
    }
  }
}
