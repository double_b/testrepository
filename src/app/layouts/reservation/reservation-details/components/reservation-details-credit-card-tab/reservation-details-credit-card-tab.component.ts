import {Component, Input, OnChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-reservation-details-credit-card-tab',
  templateUrl: './reservation-details-credit-card-tab.component.html',
  styleUrls: ['./reservation-details-credit-card-tab.component.scss']
})
export class ReservationDetailsCreditCardTabComponent implements OnChanges {
  @Input() creditCardHideStatus: BehaviorSubject<boolean>;
  @Input() reservation: any;

  // @FIXME: mocked logic, later review
  public mockedCreditCards = [
    {
      cardNumber: '1234567800009587',
      validity: '02/19',
      userName: 'William Sheppard',
      cvv: '123',
    },
    {
      cardNumber: '0987654311115674',
      validity: '12/21',
      userName: 'William Sheppard',
      cvv: '234',
    },
  ];

  public selectedIndex: number = 0;
  public isCardShownArray = [];

  public cardForm: FormGroup;

  public isConfirmModalVisible: boolean = false;
  public modalConfirmForm: FormGroup;

  public indexOfCardToDelete = null;

  public isCreditCardDeleteModalVisible = false;

  public handleCancel(): void {
    this.isCreditCardDeleteModalVisible = false;
  }

  public handleOk(): void {
    if (this.indexOfCardToDelete != null) {
      this.mockedCreditCards = this.mockedCreditCards.filter((item, i) => i !== this.indexOfCardToDelete);
      this.selectedIndex -= 1;
    }
    this.isCreditCardDeleteModalVisible = false;
  }

  public deleteItem(index: number): void {
    if (this.isCardNonEmpty(index)) {
      this.indexOfCardToDelete = index;
      this.isCreditCardDeleteModalVisible = true;
    } else {
      this.mockedCreditCards = this.mockedCreditCards.filter((item, i) => i !== index);
      this.selectedIndex = this.mockedCreditCards.length - 1;
      this.cardForm.setValue(this.mockedCreditCards[this.selectedIndex]);
    }
  }

  public get isCardVisible(): boolean {
    return this.isCardShownArray[this.selectedIndex].visible;
  }

  // @FIXME: mocked logic, later review
  constructor() {
    this.mockedCreditCards.forEach(card => this.isCardShownArray.push({visible: false}));
    this.cardForm = new FormGroup({
      cardNumber: new FormControl(''),
      validity: new FormControl(''),
      userName: new FormControl(''),
      cvv: new FormControl(''),
    });

    this.modalConfirmForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });

    const defaultCard = this.mockedCreditCards[0];
    defaultCard.cardNumber = this.formatCreditNumber(defaultCard.cardNumber);
    this.cardForm.setValue(defaultCard);
  }

  public ngOnChanges(): void {
    if (this.creditCardHideStatus != null && this.isCardShownArray.length > 0) {
      this.creditCardHideStatus.asObservable().subscribe(value => {
        this.hideAllCards();
      });
    }
  }

  public isCardNonEmpty(index: number): boolean {
    const card = this.mockedCreditCards[index];
    return card.userName != null && card.userName !== ''
      && card.cvv != null && card.cvv !== ''
      && card.cardNumber != null && card.cardNumber !== ''
      && card.validity != null && card.validity !== '';
  }

  private checkAndDeleteEmptyUserForm(): void {
    if (!this.isCardNonEmpty(this.selectedIndex)) {
      this.mockedCreditCards = this.mockedCreditCards.filter((item, i) => i !== this.selectedIndex);
      this.selectedIndex = this.mockedCreditCards.length - 1;
    }
  }

  private hideAllCards(): void {
    this.isCardShownArray.forEach(flag => flag.visible = false);
  }

  // @FIXME: mocked logic, later review
  public selectTab(index: number): void {
    if (this.selectedIndex === index) {
      return;
    }
    this.checkAndDeleteEmptyUserForm();

    this.hideAllCards();
    this.selectedIndex = index;

    const chosenCard = this.mockedCreditCards[this.selectedIndex];
    chosenCard.cardNumber = this.formatCreditNumber(chosenCard.cardNumber);
    this.cardForm.setValue(chosenCard);
  }

  public formatCreditNumber(number: string): string {
    return number.replace(/\B(?=(\d{4})+(?!\d))/g, ' ');
  }

  public hideCreditCardNumber(number: string): string {
    const hiddenPart = number
      .slice(0, length - 4)
      .split('')
      .map(char => {
        if (char === ' ') {
          return ' ';
        } else {
          return '*';
        }
      })
      .join('');
    const shownPart = number.slice(-4);
    return `${hiddenPart}${shownPart}`;
  }

  // @FIXME: mocked logic, later review
  public getCreditCardNumberFormatted(index: number, formarFlag: string = ''): string {
    if (formarFlag === 'hide-number') {
      return this.hideCreditCardNumber(this.formatCreditNumber(this.mockedCreditCards[index].cardNumber));
    } else if (formarFlag === 'show-number') {
      return this.formatCreditNumber(this.mockedCreditCards[index].cardNumber);
    } else {
      if (!this.isCardNonEmpty(index)) {
        return 'Новая карта';
      }

      return this.isCardShownArray[index].visible
        ? this.formatCreditNumber(this.mockedCreditCards[index].cardNumber)
        : this.hideCreditCardNumber(this.formatCreditNumber(this.mockedCreditCards[index].cardNumber));
    }
  }

  /**
   * @FIXME: later review
   * add empty card object
   * make the tab with empty card active
   * hide all card data except chosen empty one
   */
  public addNewCard(): void {
    const newCard = {
      cardNumber: '',
      validity: '',
      userName: '',
      cvv: '',
    };
    this.mockedCreditCards.push(newCard);
    this.selectedIndex = this.mockedCreditCards.length - 1;
    this.cardForm.reset();
    this.isCardShownArray = [];
    this.mockedCreditCards.forEach(card => this.isCardShownArray.push({visible: false}));
    this.isCardShownArray[this.selectedIndex].visible = true;
  }

  public showCardInfo(): void {
    this.isConfirmModalVisible = true;
  }

  public closeConfirmModal(): void {
    this.isConfirmModalVisible = false;
    this.modalConfirmForm.reset();
  }

  public onConfirmSubmited(): boolean {
    return this.modalConfirmForm.valid;
  }

  public submitConfirmForm(): void {
    if (this.modalConfirmForm.invalid) {
      return;
    }

    const currentCard = this.selectedIndex;
    this.isCardShownArray[currentCard].visible = true;

    this.isConfirmModalVisible = false;
  }

  // @FIXME: mocked logic, later review
  public setControlValue(controlName: string): void {
    const chosenElement = this.selectedIndex;
    let value = this.cardForm.get(controlName).value;
    if (value != null && controlName === 'cardNumber') {
      value = value.split(' ').join('');
    }
    this.mockedCreditCards[chosenElement][controlName] = value;
  }

  public formatVisibleCardNumber(event: any): void {
    const currentStr = this.cardForm.get('cardNumber').value;
    const strWoSpaces = currentStr.split(' ').join('');
    const result = this.formatCreditNumber(strWoSpaces);
    this.cardForm.get('cardNumber').setValue(result);
  }

  public formatVisibleValidity(event: any): void {
    const code = event.keyCode;
    const allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
      return;
    }

    const result = event.target.value
      .replace(/^([1-9]\/|[2-9])$/g, '0$1/') // 3 > 03/
      .replace(/^(0[1-9]|1[0-2])$/g, '$1/') // 11 > 11/
      .replace(/^([0-1])([3-9])$/g, '0$1/$2') // 13 > 01/3
      .replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2') // 141 > 01/41
      .replace(/^([0]+)\/|[0]+$/g, '0') // 0/ > 0 and 00 > 0
      .replace(/[^\d\/]|^[\/]*$/g, '') // To allow only digits and `/`
      .replace(/\/\//g, '/'); // Prevent entering more than 1 `/`
    this.cardForm.get('validity').setValue(result);
  }

  public getValidityFormatted(index: number): string {
    return '**/**';
  }

  public getCvvFormatted(index: number): string {
    return '***';
  }
}
