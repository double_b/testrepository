import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {UploadXHRArgs} from 'ng-zorro-antd';
import {of} from 'rxjs';

import * as moment from 'moment';

@Component({
  selector: 'app-reservation-details-correspondence-tab',
  templateUrl: './reservation-details-correspondence-tab.component.html',
  styleUrls: ['./reservation-details-correspondence-tab.component.scss']
})
export class ReservationDetailsCorrespondenceTabComponent {
  @Input() reservation: any;

  public isLoadingModal = false;
  public isLoading = false;
  public imageUrl: any;
  public maxFileUpload = 10;

  public newMessageForm = {
    userName: 'Kukaldosh Hotel',     // @FIXME: mocked logic
    time: null,
    text: '',
    files: []
  };

  public uploadedFiles = [];

  // @FIXME: mocked logic
  public mockedUserMessage = {
    userName: 'William Sheppard',
    time: '12:45',
    text: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    `,
    files: []
  };

  public messageList = [];

  public get uploadedFilesCount(): number {
    return this.uploadedFiles.length;
  }

  public isMineMessage(index: number): boolean {
    return this.messageList[index].userName === 'Kukaldosh Hotel'; //@FIXME: mocked authorized user
  }

  imageUploadingRequestHandler = (item: UploadXHRArgs) => {
    this.isLoadingModal = true;

    // @FIXME: temp logic, later review
    // must return Subscribtion
    of(item).subscribe(
      res => {
        this.isLoading = false;
        const file = res.file;
        file.status = 'done';
        file.response = '{"status": "success"}';
        this.loadFile(file);
      },
      err => {
        // this.utils.presentErrorResponseMessage(err);
      },
      () => {
        this.isLoadingModal = false;
      }
    );
  };

  public loadFile(file: any): void {
    this.uploadedFiles.push(file);
  }

  public messageControl = new FormControl('');

  private clearAll(): void {
    this.newMessageForm = {
      userName: 'Kukaldosh Hotel',
      time: null,
      text: '',
      files: []
    };

    this.uploadedFiles = [];

    this.messageControl.reset();
  }

  constructor() {
  }

  public sendMessage(): void {
    if (this.messageControl.value == null && this.messageControl.value === '' && this.uploadedFiles.length === 0) {
      return;
    }

    const currentTime = moment(new Date()).format('HH:mm');
    this.newMessageForm.files = this.uploadedFiles;
    this.newMessageForm.time = currentTime;
    this.newMessageForm.text = this.messageControl.value;

    this.messageList.push(this.newMessageForm);

    this.clearAll();
    this.scrollToLastMessage();

    // @FIXME: mocked logic
    this.imitateUserResponse();
    this.scrollToLastMessage();
  }

  private scrollToLastMessage(): void {
    const messageContainer = document.getElementById('messageListWrapper');
    if (messageContainer != null) {
      setTimeout(() => {
        messageContainer.scrollTop = messageContainer.scrollHeight;
      }, 0);

    }
  }

  private imitateUserResponse(): void {
    setTimeout(() => {
      this.messageList.push(this.mockedUserMessage);
      this.scrollToLastMessage();
    }, 1000);
  }
}
