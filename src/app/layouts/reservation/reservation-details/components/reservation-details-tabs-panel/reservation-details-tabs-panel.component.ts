import {Component, Input, OnChanges} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ReservationDetailsService} from '@app/layouts/reservation/reservation-details/reservation-details.service';

@Component({
  selector: 'app-reservation-details-tabs-panel',
  templateUrl: './reservation-details-tabs-panel.component.html',
  styleUrls: ['./reservation-details-tabs-panel.component.scss']
})
export class ReservationDetailsTabsPanelComponent implements OnChanges {
  @Input() reservation: any;

  public hideCardInfoOnTabSwitch = new BehaviorSubject<boolean>(null);

  constructor(private router: Router, private reservationDetailsService: ReservationDetailsService) {
  }

  public ngOnChanges(): void {
    if (this.reservation != null && Object.keys(this.reservation).length === 0) {
      // tslint:disable-next-line:max-line-length
      this.router.navigate([`/${RoutesEnum.HOTEL}`, this.reservationDetailsService.getHotelId(), RoutesEnum.RESERVATION, RoutesEnum.LIST]).finally();
    }
  }

  public hideCardInfo(): void {
    this.hideCardInfoOnTabSwitch.next(true);
  }
}
