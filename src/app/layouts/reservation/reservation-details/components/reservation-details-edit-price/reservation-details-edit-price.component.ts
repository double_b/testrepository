import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-reservation-details-edit-price',
  templateUrl: './reservation-details-edit-price.component.html',
  styleUrls: ['./reservation-details-edit-price.component.scss']
})
export class ReservationDetailsEditPriceComponent implements OnInit {
  @Input() guest: any;
  @Input() period: any;
  @Input() roomTypes: any;

  datesSpan = [];
  totalCost = 0;

  constructor() {
  }

  ngOnInit() {
    // this.setDateSpan(this.period.checkin, this.period.checkout);
  }

  setDateSpan(start, end?) {
    // let startMoment;
    // let endMoment;
    // if (end === null) {
    //   startMoment = moment(start);
    //   endMoment = moment(start).add(6, 'days');
    // } else {
    //   startMoment = moment(start);
    //   endMoment = moment(end);
    // }
    //
    // this.datesSpan = [];
    //
    // const dates = Utils.getAllDatesBetween(startMoment, endMoment);
    // const prices = this.calendarService.getPrices(null, null, startMoment, endMoment, null, null);
    // const availability = this.calendarService.getRoomsAvailability(null, null, startMoment, endMoment);
    // const reservedDates = this.calendarService.getAllDatesBetween(moment(this.period.checkin), moment(this.period.checkout));
    // for (let i = 0; i < dates.length; i++) {
    //   let status = 'available';
    //   if (reservedDates.includes(dates[i])) {
    //     status = 'self';
    //   } else if (availability[i].rooms <= (availability[i].booked + availability[i].closed)) {
    //     status = 'unavailable';
    //   }
    //   this.datesSpan.push({
    //     date: moment(dates[i]),
    //     price: prices[i].price,
    //     status,
    //   });
    // }
  }
}
