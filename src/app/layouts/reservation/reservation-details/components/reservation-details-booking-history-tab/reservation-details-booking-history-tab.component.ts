import {Component, Input} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {differenceInCalendarDays} from 'date-fns';

@Component({
  selector: 'app-reservation-details-booking-history-tab',
  templateUrl: './reservation-details-booking-history-tab.component.html',
  styleUrls: ['./reservation-details-booking-history-tab.component.scss']
})
export class ReservationDetailsBookingHistoryTabComponent {
  @Input() reservation: any;

  public userFilterForm: FormGroup;

  public userRoles = [
    {label: 'Все пользователи', value: 'allUsers'},
    {label: 'role 1', value: 'role1'},
    {label: 'role 2', value: 'role2'},
    {label: 'role 3', value: 'role3'},
    {label: 'role 4', value: 'role4'},
    {label: 'role 5', value: 'role5'},
    {label: 'role 6', value: 'role6'},
  ];

  public userChangesDetailsVisibility = [];

  public mockedUserList = [
    {
      date: '26.07.2019 (17:00)',
      user: 'William Sheppard',
      email: 'williamsheppard@gmail.com',
      changes: 'Номер назначен',
      changeDetails: 'Some extra details about room reservation'
    },
    {
      date: '26.07.2019 (17:00)',
      user: 'William Sheppard Sheppard',
      email: 'williamsheppard@gmail.com',
      changes: 'Резервирование создано',
      changeDetails: `
        Room SNG/TW 104 status changed to In-House for Reservation 476928175332
      `
    },
    {
      date: '26.07.2019 (17:00)',
      user: 'William Sheppard',
      email: 'williamsheppard@gmail.com',
      changes: 'Номер назначен',
      changeDetails: 'Some extra details about room reservation'
    },
    {
      date: '26.07.2019 (17:00)',
      user: 'William Sheppard',
      email: 'williamsheppard@gmail.com',
      changes: 'Резервирование создано',
      changeDetails: `
        Room SNG/TW 104 status changed to In-House for Reservation 476928175332
      `
    },
  ];

  constructor() {
    this.userFilterForm = new FormGroup({
      checkin: new FormControl(''),
      checkout: new FormControl(''),
      userRole: new FormControl(''),
    });

    this.userFilterForm.get('checkin').valueChanges.subscribe(value => {
      if (value != null && value != '') {
        this.userFilterForm.get('checkout').setValue(this.getNextDay());
      } else {
        this.userFilterForm.get('checkout').reset();
      }
    });

    // set all-users for user-role by default
    this.userFilterForm.get('userRole').setValue(this.userRoles[0].value);

    // @FIXME: mocked logic, later review
    this.mockedUserList.forEach(user => this.userChangesDetailsVisibility.push({visible: false}));
  }

  public areDetailsVisible(index: number): boolean {
    return this.userChangesDetailsVisibility[index].visible;
  }

  public showChangeDetails(index: number): void {
    this.userChangesDetailsVisibility[index].visible = !this.userChangesDetailsVisibility[index].visible;
  }

  public clearFilters(): void {
    this.userFilterForm.reset();
    // set all-users for user-role by default
    this.userFilterForm.get('userRole').setValue(this.userRoles[0].value);
  }

  public getPreviousDay(): Date | null {
    const date = this.userFilterForm.get('checkout').value;
    if (date != null && date !== '') {
      return new Date(date.getTime() - 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  public getNextDay(): Date | null {
    const date = this.userFilterForm.get('checkin').value;
    if (date != null && date !== '') {
      return new Date(date.getTime() + 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  public applyFilters(): void {

  }

  disabledCheckinDate = (current: Date): boolean => {
    const checkoutDateNext = this.getPreviousDay();
    if (checkoutDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkoutDateNext, current) < 0;
  };

  disabledCheckoutDate = (current: Date): boolean => {
    const checkinDateNext = this.getNextDay();
    if (checkinDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkinDateNext, current) > 0;
  };
}
