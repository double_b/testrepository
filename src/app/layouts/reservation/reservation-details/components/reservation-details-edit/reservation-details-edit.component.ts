import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-reservation-details-edit',
  templateUrl: './reservation-details-edit.component.html',
  styleUrls: ['./reservation-details-edit.component.scss']
})
export class ReservationDetailsEditComponent {
  @Input() data: any;
  @Input() guest: any;
  @Input() period: any;
  @Input() roomTypes: any;

  close(): void {
  }
}
