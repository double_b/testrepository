import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReservationDetailsComponent} from './reservation-details.component';

const routes: Routes = [
  {
    path: '',
    component: ReservationDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationDetailsRoutingModule {
}
