import {Component} from '@angular/core';
import {ReservationListService} from '../reservation-list/reservation-list.service';
import html2pdf from 'html2pdf.js';
import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {ReservationDetailsService} from '@app/layouts/reservation/reservation-details/reservation-details.service';

@Component({
  selector: 'app-reservation-details',
  templateUrl: './reservation-details.component.html',
  styleUrls: ['./reservation-details.component.scss']
})
export class ReservationDetailsComponent {
  public reservation = null;
  isLoading = false;

  private processBookingId(id: number): Observable<any> {
    const result = id != null && id !== -1
      ? this.reservationDetailsService.getSingleReservation(id)
      : of({});
    return result;
  }

  constructor(private reservationDetailsService: ReservationDetailsService,
              private reservationListService: ReservationListService,
              private notification: NotificationService
  ) {
    this.isLoading = true;
    this.reservationListService.bookingId
      .pipe(mergeMap(id => this.processBookingId(id)))
      .subscribe(data => {
        this.isLoading = false;
        this.reservation = data;
      }, error => {
        this.isLoading = false;
        this.notification.errorMessage('error', error);
      });
  }

  private getHtmlContent(): HTMLElement {
    const parentDiv = document.createElement('div');
    const headerRef = document.getElementById('reservationDetailsHeader');
    const contentRef = document.getElementById('reservationDetailsContent');

    // clone these elements because direct reference access
    // makes entire component's content disappeared
    const clonedHeader = headerRef.cloneNode(true);
    const clonedContent = contentRef.cloneNode(true);

    parentDiv.appendChild(clonedHeader);
    parentDiv.appendChild(clonedContent);

    return parentDiv;
  }

  public handleDownload(): void {
    const content = this.getHtmlContent();

    const options = {
      newWindow: true,
      margin: 0.2,
      filename: 'reservation-details.pdf',
      image: {type: 'jpeg', quality: 0.98},
      html2canvas: {scale: 1},
      jsPDF: {unit: 'in', format: 'tabloid', orientation: 'landscape'}
    };
    html2pdf().from(content).set(options).save();
  }

  public handlePrint(): void {
  }

}
