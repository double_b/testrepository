import {RouterModule, Routes} from '@angular/router';
import {ReservationListComponent} from './reservation-list.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ReservationListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationListRoutingModule {
}
