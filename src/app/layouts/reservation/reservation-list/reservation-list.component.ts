import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {ReservationListService} from './reservation-list.service';
import html2pdf from 'html2pdf.js';
import {Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';
import {differenceInCalendarDays} from 'date-fns';
import {HttpParams} from '@angular/common/http';
import * as _ from 'lodash';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {StatusEnum} from '@app/core/constants/status.enum';
import {Utils} from '@app/shared/helpers/utils';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnDestroy {

  public dateTypeFilterByControl = new FormControl(null);
  public startDateControl = new FormControl(null);
  public endDateControl = new FormControl(null);
  public keywordInputControl = new FormControl(null);

  public pageIndex = 1;
  public pageSize = 10;
  public totalElementsQty = 1;

  public isExtendedSearch = false;

  public reservations = [];

  public isDataLoading = false;

  public dateTypeSelection = [
    {label: 'RESERVATION.FILTER_BOOKING', value: 'booking'},
    {label: 'RESERVATION.FILTER_CHECKIN', value: 'checkin'},
    {label: 'RESERVATION.FILTER_CHECKOUT', value: 'checkout'},
    {label: 'RESERVATION.FILTER_LIVING', value: 'stay_dates'},
  ];

  public reservationStatus = [
    {label: 'Подтверждено', value: StatusEnum.CONFIRM, checked: false},
    {label: 'Отменено', value: StatusEnum.CANCEL, checked: false},
    {label: 'В номере', value: StatusEnum.IN_HOUSE, checked: false},
    {label: 'Выехал', value: StatusEnum.CHECKOUT, checked: false},
    {label: 'Незаезд', value: StatusEnum.NO_SHOW, checked: false}
  ];

  public communicationWithGuests = [
    {label: 'Необработанные запросы гостей', value: 'unprocessed-requests', checked: false},
  ];

  private bookingSubscription: Subscription;
  private startDateControlSubscribtion: Subscription;

  private initializeFilters(): void {
    const dateType = this.dateTypeSelection.find(item => item.value === 'booking');
    this.dateTypeFilterByControl.setValue(dateType.value);
  }

  public searchDataByIndex(): void {
    this.showResults(this.pageIndex);
  }

  public get areFiltersValid(): boolean {
    return this.dateTypeFilterByControl.value != null
      && this.startDateControl.value != null
      && this.endDateControl.value != null;
  }

  private getNextControlDay(): Date | null {
    const date = this.startDateControl.value;
    if (date != null && date !== '') {
      return new Date(date.getTime() + 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  disabledCheckinDate = (current: Date): boolean => {
    return false;
  };

  disabledCheckoutDate = (current: Date): boolean => {
    // date should be greater than today && the checkin date
    const checkinDateNext = this.getNextControlDay();
    if (checkinDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkinDateNext, current) > 0;
  };

  private retireveData(params?: HttpParams): void {
    this.route.params.subscribe(res => {
      this.isDataLoading = true;
      this.bookingSubscription = this.reservationListService.getReservations(params).subscribe(response => {
        this.reservations = response.data;
        this.setRooms();
        window.scroll(0, 0);
        this.isDataLoading = false;

        this.totalElementsQty = response.meta.total;
        this.pageIndex = response.meta.current_page;
      });

    });

  }

  constructor(private router: Router,
              private reservationListService: ReservationListService,
              private route: ActivatedRoute,
  ) {
    this.initializeFilters();
    this.startDateControlSubscribtion = this.startDateControl.valueChanges.subscribe(value => {
      if (value != null && value !== '') {
        const checkout = this.endDateControl.value;
        if (checkout != null && checkout >= value) {
          this.endDateControl.setValue(checkout);
        } else {
          this.endDateControl.setValue(this.getNextControlDay());
        }
      } else {
        this.endDateControl.reset();
      }
    });

    const params = new HttpParams()
      .set('perPage', `${this.pageSize}`)
      .set('page', `${this.pageIndex}`);
    this.retireveData(params);
  }

  public getReservationId(data: any): string {
    if (data.channel_booking_id) {
      return data.channel_booking_id;
    }
    return data.id;
  }

  public getGuestName(data: any): string {
    if (data.guest) {
      return data.guest.first_name + ' ' + data.guest.last_name;
    }

    if (data.reservation_guest) {
      return data.reservation_guest.first_name + ' ' + data.reservation_guest.last_name;
    }
    return '';
  }

  public getGuestPluralTitle(data: any): string {
    const suffix = data.total_guests % 10 === 1 && data.total_guests < 10
      ? 'гость'
      : data.total_guests % 10 >= 2 && data.total_guests % 10 <= 4 && data.total_guests < 10
        ? 'гостя'
        : 'гостей';

    return `${data.total_guests}&#160;${suffix}`;
  }

  public ngOnDestroy(): void {
    this.bookingSubscription.unsubscribe();
    this.startDateControlSubscribtion.unsubscribe();
  }

  public getUniqGuestList(reservation: any): any[] {
    const mainGuest = reservation.guest;

    const guests = reservation.rooms.map(room => room.guests);
    let result = [].concat.apply([], guests);

    // 1) remove guest entities that is equal to the main guest
    result = result.filter(guest => guest.id !== mainGuest.id);
    // 2) remove other duplicate entities
    result = _.uniqBy(result, guest => guest.id);

    return result;
  }

  public getFormattedDate(dateStr: string, options = null): string {
    const dateProcessed = options === 'no-template'
      ? moment(dateStr)
      : options === 'cut-time'
        ? moment(dateStr, 'YYYY-MM-DD hh:mm:ss')
        : moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }

  public setRooms() {
    this.reservations.map(el => {
      let names = el.rooms.map(room => {
        return room.room_type.short_name;
      });
      let roomUniqNames = {};
      names.forEach(name => {
        if (roomUniqNames[name]) {
          roomUniqNames[name] = roomUniqNames[name] + 1;
        } else {
          roomUniqNames[name] = 1;
        }
      });
      let namesArray = [];
      const keys = Object.keys(roomUniqNames);
      keys.forEach(key => {
        namesArray.push({name: key, roomCount: roomUniqNames[key]});
      });
      el.roomNames = namesArray;
    });
  }

  public showResults(page?: number): void {
    let params = new HttpParams();

    const dateType = this.dateTypeFilterByControl.value;
    const checkin = this.startDateControl.value;
    const checkout = this.endDateControl.value;

    if (dateType != null) {
      let type: string;
      switch (dateType) {
        case 'booking':
          type = 'type_booking_date';
          break;
        case 'checkin':
          type = 'type_checkin';
          break;
        case 'checkout':
          type = 'type_checkout';
          break;
        case 'stay_dates':
          type = 'type_stay_dates';
          break;
      }
      params = params.set('date_range_type', type);
    }

    if (checkin != null) {
      const checkinProcessed = moment(checkin).format('YYYY-MM-DD');
      params = params.set('start_date', checkinProcessed);
    }
    if (checkout != null) {
      const checkoutProcessed = moment(checkout).format('YYYY-MM-DD');
      params = params.set('end_date', checkoutProcessed);
    }

    if (this.isExtendedSearch) {
      const bookingStatuses = this.reservationStatus
        .filter(item => item.checked)
        .map(item => item.value);
      const keywords = this.keywordInputControl.value;

      if (bookingStatuses.length > 0) {
        bookingStatuses.forEach((status, index) => params = params.set(`status[${index}]`, status));
      }

      if (keywords != null && keywords !== '') {
        params = params.set('search_text', keywords);
      }
    }

    params = params.set('perPage', `${this.pageSize}`);

    if (page != null) {
      params = params.set('page', `${page}`);
    }

    this.retireveData(params);
  }

  public toggleOtherFilters(): void {
    this.isExtendedSearch = !this.isExtendedSearch;
  }

  public handleDownload(): void {
    const parentDiv = document.createElement('div');
    const headerRef = document.getElementById('reservationListHeader');
    const contentRef = document.getElementById('reservationListContent');

    // clone these elements because direct reference access
    // makes entire component's content disappeared
    const clonedHeader = headerRef.cloneNode(true);
    const clonedContent = contentRef.cloneNode(true);

    parentDiv.appendChild(clonedHeader);
    parentDiv.appendChild(clonedContent);

    const options = {
      margin: 0.2,
      filename: 'reservation-list.pdf',
      image: {type: 'jpeg', quality: 0.98},
      html2canvas: {scale: 1},
      jsPDF: {unit: 'in', format: 'tabloid', orientation: 'landscape'}
    };
    const worker = html2pdf().from(parentDiv).set(options).save();
  }

  getRoomsCount(rooms: any[]): number {
    let result = 0;
    for (const item of rooms) {
      result += item.roomCount;
    }
    return result;
  }

  public handlePrint(): void {

  }

  public seeReservationDetails(data): void {
    this.reservationListService.bookingId.next(data.id);
    // tslint:disable-next-line:max-line-length
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.reservationListService.getPropertyId(), RoutesEnum.RESERVATION, RoutesEnum.DETAILS]).finally();
  }

  getStatusTheme(status: string): string {
    switch (status) {
      case StatusEnum.CONFIRM: {
        return 'STATUS.CONFIRM';
      }
      case StatusEnum.CANCEL: {
        return 'STATUS.CANCEL';
      }
      case StatusEnum.IN_HOUSE: {
        return 'STATUS.IN_HOUSE';
      }
      case StatusEnum.CHECKOUT: {
        return 'STATUS.CHECKOUT';
      }
      case StatusEnum.NO_SHOW: {
        return 'STATUS.NO_SHOW';
      }
    }
  }

  getSource(reservation: any): string {
    return Utils.getSource(reservation);
  }
}
