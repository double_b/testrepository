import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {CommonModule} from '@angular/common';
import {ReservationListComponent} from './reservation-list.component';
import {ReservationListRoutingModule} from './reservation-list-routing.module';
import {CustomPipesModule} from 'src/app/core/pipes/custom-pipes.module';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';

@NgModule({
  declarations: [
    ReservationListComponent
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    FormsModule,
    ReservationListRoutingModule,
    CustomPipesModule,
    OldShared
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ReservationListModule {
}
