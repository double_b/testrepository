import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {ReservationApiService} from '@app/shared/services/reservation-api/reservation-api.service';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Injectable({
  providedIn: 'root'
})
export class ReservationListService {
  private bookingId$: BehaviorSubject<number>; // reservation id

  public get bookingId(): BehaviorSubject<number> {
    return this.bookingId$;
  }

  constructor(private reservationApiService: ReservationApiService) {
    this.bookingId$ = new BehaviorSubject(-1);
  }

  getReservations(params?: HttpParams): Observable<any> {
    return this.reservationApiService.getReservations(this.getPropertyId(), params);
  }

  public getPropertyId(): number {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return Number(hotel);
  }
}
