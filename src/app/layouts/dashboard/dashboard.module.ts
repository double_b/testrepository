import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SharedModule} from '@app/core/shared/shared.module';
import {RouterModule} from '@angular/router';
import {DashboardRoutingModule} from './dashboard-routing.module';

import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import {AgmCoreModule} from '@agm/core';

// Components
import {DashboardComponent} from './dashboard.component';
import {DashCountersComponent} from './dash-counters/dash-counters.component';
import {BookReviewComponent} from './book-review/book-review.component';
import {CheckInBooksComponent} from './book-review/check-in-books/check-in-books.component';
import {CheckOutBooksComponent} from './book-review/check-out-books/check-out-books.component';
import {LiveBooksComponent} from './book-review/live-books/live-books.component';
import {SaleBooksComponent} from './book-review/sale-books/sale-books.component';
import {CancellationBooksComponent} from './book-review/cancellation-books/cancellation-books.component';
import {RebookingBooksComponent} from './book-review/rebooking-books/rebooking-books.component';
import {UnansweredMsgComponent} from './unanswered-msg/unanswered-msg.component';
import {RecentReviewsComponent} from './recent-reviews/recent-reviews.component';
import {ForecastComponent} from './forecast/forecast.component';
import {ForecastBookComponent} from './forecast/forecast-book/forecast-book.component';
import {ForecastAvailableComponent} from './forecast/forecast-available/forecast-available.component';
import {BookItemComponent} from './book-review/book-item/book-item.component';
import {SaleItemComponent} from './book-review/sale-item/sale-item.component';
import {CellItemComponent} from './forecast/forecast-available/cell-item/cell-item.component';
import {NgApexchartsModule} from 'ng-apexcharts';
import {CustomPipesModule} from "@app/core/pipes/custom-pipes.module";
import { CountContainerComponent } from './book-review/count-container/count-container.component';

@NgModule({
  declarations: [
    DashboardComponent,
    DashCountersComponent,
    BookReviewComponent,
    CheckInBooksComponent,
    CheckOutBooksComponent,
    LiveBooksComponent,
    SaleBooksComponent,
    CancellationBooksComponent,
    RebookingBooksComponent,
    UnansweredMsgComponent,
    RecentReviewsComponent,
    ForecastComponent,
    ForecastBookComponent,
    ForecastAvailableComponent,
    BookItemComponent,
    SaleItemComponent,
    CellItemComponent,
    CountContainerComponent,
  ],
    imports: [
        CommonModule,
        RouterModule,
        DashboardRoutingModule,
        ReactiveFormsModule,
        GooglePlaceModule,
        AgmCoreModule,
        FormsModule,
        SharedModule,
        NgApexchartsModule,
        CustomPipesModule
    ],
  entryComponents: [
    DashboardComponent,
  ],
  exports: [
    DashboardComponent,
  ]
})
export class DashboardModule {
}
