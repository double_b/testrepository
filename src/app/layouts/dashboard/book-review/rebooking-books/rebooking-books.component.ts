import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {RebookingBooksService} from '@app/layouts/dashboard/book-review/rebooking-books/rebooking-books.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

@Component({
  selector: 'app-rebooking-books',
  templateUrl: './rebooking-books.component.html',
  styleUrls: ['./rebooking-books.component.scss']
})
export class RebookingBooksComponent implements OnChanges {
  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();
  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public rebookingBooksService: RebookingBooksService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.rebookingBooksService.getRebookingReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.id,
      type: 'rebooking'
    };
    this.openNotesEmit.emit(newModel);
  }

}
