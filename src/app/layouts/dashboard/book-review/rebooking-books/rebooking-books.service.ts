import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RebookingBooksService {
  isLoading = false;

  readonly rebookingBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly rebooking$ = this.rebookingBehavior.asObservable();

  readonly titleStatisticBehavior = new BehaviorSubject<any>({});
  readonly titleStatistic$ = this.titleStatisticBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  constructor(public dashboardService: DashboardApiService) {
  }

  set TITLE_STATISTIC(value: number) {
    this.titleStatisticBehavior.next(value);
  }

  set REBOOKING(list: ReservationForViewModel[]) {
    this.rebookingBehavior.next(list);
  }

  get REBOOKING(): ReservationForViewModel[] {
    return this.rebookingBehavior.getValue();
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  getRebookingReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.dashboardService.getRebookingReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      this.TITLE_STATISTIC = res.overload;
      this.TOTAL = res.total;
      const rebooking: ReservationForViewModel[] = [];
      res.reservations.forEach(item => {
        rebooking.push(DashboardApiService.convertToViewModel(item, DashboardApiService.VIEW_MODE_OVERLOAD));
      });
      if (page === 1) {
        this.REBOOKING = [];
        this.REBOOKING = rebooking;
      } else {
        this.REBOOKING = [...this.REBOOKING, ...rebooking];
      }
      this.isLoading = false;
    });
  }

  public updateNotesCount(model: any) {
    this.REBOOKING.map(el => {
      if (el.reservationId === model.id) {
        el.notesCount += 1;
      }
    });
  }
}
