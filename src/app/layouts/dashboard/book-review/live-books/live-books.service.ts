import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {BehaviorSubject} from 'rxjs';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {Utils} from '@app/shared/helpers/utils';
import {StatusEnum} from '@app/core/constants/status.enum';

@Injectable({
  providedIn: 'root'
})
export class LiveBooksService {

  public isLoading = false;
  readonly livingBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly living$ = this.livingBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  constructor(public dashboardApiService: DashboardApiService, private notification: NotificationService) {
  }

  set LIVING(list: ReservationForViewModel[]) {
    this.livingBehavior.next(list);
  }

  get LIVING(): ReservationForViewModel[] {
    return this.livingBehavior.getValue();
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  public getLivingReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.dashboardApiService.getLivingReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      const result: ReservationForViewModel[] = [];
      let tempTotal = res.total;
      for (const reservation of res.reservations) {
        if (reservation.status !== StatusEnum.IN_HOUSE) {
          tempTotal--;
          continue;
        }
        reservation.actionBtnDisabled = !Utils.compareDates(new Date(), reservation.checkout, true);
        result.push(DashboardApiService.convertToViewModel(reservation, DashboardApiService.VIEW_MODE_LIVING));
      }
      this.TOTAL = tempTotal;
      if (page === 1) {
        this.LIVING = [];
        this.LIVING = result;
      } else {
        this.LIVING = [...this.LIVING, ...result];
      }
      this.isLoading = false;
    });
  }

  public setGuestAsLeft(reservation: ReservationForViewModel) {
    if (!Utils.isExist(reservation.id)) {
      this.notification.errorMessage('Ошибка', 'У брони не указан точный номер комнаты!');
      return;
    }
    this.isLoading = true;
    this.dashboardApiService.setGuestAsLeft(reservation.id).subscribe(res => {
      if (res.data === null && res.message !== null) {
        this.notification.errorMessage('Неизвестная ошибка');
        this.isLoading = false;
        return;
      }
      if (Utils.compareDates(new Date(), reservation.checkout)) {
        this.dashboardApiService.getStatistics.next();
      }
      const item = this.LIVING.find(_ => _.id === reservation.id);
      if (item) {
        const inx = this.LIVING.indexOf(item);
        this.LIVING.splice(inx, 1);
        this.LIVING = [...this.LIVING];
        let tempTotal = this.TOTAL;
        tempTotal--;
        this.TOTAL = tempTotal;
        this.notification.successMessage('Статус бронирования изменен!');
      } else {
        this.notification.errorMessage('Неизвестная ошибка');
      }
      this.isLoading = false;
    });
  }

  public updateNotesCount(model: any) {
    this.LIVING.map(el => {
      if (el.reservationId === model.id) {
        el.notesCount += 1;
      }
    });
  }
}
