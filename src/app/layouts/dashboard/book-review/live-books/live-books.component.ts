import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {LiveBooksService} from '@app/layouts/dashboard/book-review/live-books/live-books.service';

@Component({
  selector: 'app-live-books',
  templateUrl: './live-books.component.html',
  styleUrls: ['./live-books.component.scss']
})
export class LiveBooksComponent implements OnChanges {

  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();

  public total: number;
  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public liveBooksService: LiveBooksService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  onActionClicked(event: any) {
    this.liveBooksService.setGuestAsLeft(event);
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.liveBooksService.getLivingReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.reservationId,
      type: 'live',
    };
    this.openNotesEmit.emit(newModel);
  }
}
