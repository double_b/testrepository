import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {StatusEnum} from '@app/core/constants/status.enum';
import {CheckOutBooksService} from '@app/layouts/dashboard/book-review/check-out-books/check-out-books.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';

@Component({
  selector: 'app-check-out-books',
  templateUrl: './check-out-books.component.html',
  styleUrls: ['./check-out-books.component.scss']
})
export class CheckOutBooksComponent implements OnChanges {

  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();

  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public checkOutBooksService: CheckOutBooksService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  onActionClicked(event: ReservationForViewModel) {
    if (event.status === StatusEnum.IN_HOUSE) {
      this.checkOutBooksService.setGuestAsLeft(event);
    } else {
      this.checkOutBooksService.setGuestAsArrived(event);
    }
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.checkOutBooksService.getLeftReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.reservationId,
      type: 'checkOut',
    };
    this.openNotesEmit.emit(newModel);
  }
}
