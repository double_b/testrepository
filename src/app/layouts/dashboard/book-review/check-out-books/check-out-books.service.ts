import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {BehaviorSubject} from 'rxjs';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {StatusEnum} from '@app/core/constants/status.enum';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class CheckOutBooksService {
  public isLoading = false;
  readonly leftBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly left$ = this.leftBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  set LEFT(list: ReservationForViewModel[]) {
    this.leftBehavior.next(list);
  }

  get LEFT(): ReservationForViewModel[] {
    return this.leftBehavior.getValue();
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  constructor(public dashboardApiService: DashboardApiService, private notification: NotificationService) {
  }

  public getLeftReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.dashboardApiService.getLeftReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      const result: ReservationForViewModel[] = [];
      let tempTotal = res.total;
      for (const reservation of res.reservations) {
        if (reservation.status === StatusEnum.NO_SHOW || reservation.status === StatusEnum.CANCEL) {
          tempTotal--;
          continue;
        }
        const leftItem: any = DashboardApiService.convertToViewModel(reservation, DashboardApiService.VIEW_MODE_LEFT);
        if (leftItem.status === StatusEnum.IN_HOUSE) {
          leftItem.actionBtnDisabled = !Utils.compareDates(new Date(), leftItem.checkout, true);
        }
        if (leftItem.status === StatusEnum.CHECKOUT) {
          leftItem.actionBtnDisabled = true;
        }
        if (leftItem.status === StatusEnum.CONFIRM) {
          leftItem.actionBtnDisabled = !Utils.compareDates(new Date(), leftItem.checkin, true);
        }
        // tslint:disable-next-line:max-line-length
        leftItem.buttonTitle = leftItem.status === StatusEnum.CONFIRM ? 'DASHBOARD.BOOK_ITEM.ACTION_CHECK_IN' : 'DASHBOARD.BOOK_ITEM.ACTION_CHECK_OUT';
        result.push(leftItem);
      }
      this.TOTAL = tempTotal;
      if (page === 1) {
        this.LEFT = [];
        this.LEFT = [...DashboardApiService.sortByStatus(result)];
      } else {
        const tempArray = [...this.LEFT, ...result];
        this.LEFT = [...DashboardApiService.sortByStatus(tempArray)];
      }
      this.isLoading = false;
    });
  }

  public setGuestAsArrived(reservation: ReservationForViewModel) {
    if (!Utils.isExist(reservation.id)) {
      this.notification.errorMessage('Ошибка', 'У брони не указан точный номер комнаты!');
      return;
    }
    this.isLoading = true;
    this.dashboardApiService.setGuestAsArrived(reservation.id).subscribe(res => {
      if (res.data === null && res.message !== null) {
        if (res.message === 'Guest has already arrived') {
          this.notification.errorMessage('Ошибка', 'В данном номере уже проживает гость');
        }
        this.isLoading = false;
        return;
      }
      if (Utils.compareDates(new Date(), reservation.checkin)) {
        this.dashboardApiService.getStatistics.next();
      }
      const item = this.LEFT.find(_ => _.id === reservation.id);
      if (item) {
        const inx = this.LEFT.indexOf(item);
        this.LEFT[inx].status = StatusEnum.IN_HOUSE;
        this.LEFT[inx].buttonTitle = 'DASHBOARD.BOOK_ITEM.ACTION_CHECK_OUT';
        this.LEFT[inx].actionBtnDisabled = !Utils.compareDates(new Date(), this.LEFT[inx].checkout, true);
        this.LEFT = [...DashboardApiService.sortByStatus(this.LEFT)];
        this.notification.successMessage('Статус бронирования изменен!', 'Бронь перемещена в конец списка');
      } else {
        this.notification.errorMessage('Неизвестная ошибка');
      }
      this.isLoading = false;
    });
  }

  public setGuestAsLeft(reservation: ReservationForViewModel) {
    if (!Utils.isExist(reservation.id)) {
      this.notification.errorMessage('Ошибка', 'У брони не указан точный номер комнаты!');
      return;
    }
    this.isLoading = true;
    this.dashboardApiService.setGuestAsLeft(reservation.id).subscribe(res => {
      if (res.data === null && res.message !== null) {
        this.notification.errorMessage('Неизвестная ошибка');
        this.isLoading = false;
        return;
      }
      if (Utils.compareDates(new Date(), reservation.checkout)) {
        this.dashboardApiService.getStatistics.next();
      }
      const item = this.LEFT.find(_ => _.id === reservation.id);
      if (item) {
        const inx = this.LEFT.indexOf(item);
        this.LEFT[inx].status = StatusEnum.CHECKOUT;
        this.LEFT[inx].actionBtnDisabled = true;
        this.LEFT[inx].buttonTitle = 'DASHBOARD.BOOK_ITEM.ACTION_CHECK_OUT';
        this.LEFT = [...DashboardApiService.sortByStatus(this.LEFT)];
        this.notification.successMessage('Статус бронирования изменен!', 'Бронь перемещена в конец списка');
      } else {
        this.notification.errorMessage('Неизвестная ошибка');
      }
      this.isLoading = false;
    });
  }

  public updateNotesCount(model: any) {
    this.LEFT.map(el => {
      if (el.reservationId === model.id) {
        el.notesCount += 1;
      }
    });
  }

  compareDates(firstDate: Date, secondDate: Date): boolean {
    firstDate.setHours(0, 0, 0, 0);
    secondDate.setHours(0, 0, 0, 0);
    return firstDate >= secondDate;
  }
}
