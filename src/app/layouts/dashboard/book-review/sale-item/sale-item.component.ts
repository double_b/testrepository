import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {ReservationForViewModel} from '@src/app/core/models/dashboard/view/reservation-for-view.model';
import {DashboardApiService} from '@src/app/shared/services/dashboard-api/dashboard-api.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ReservationListService} from "@app/layouts/reservation/reservation-list/reservation-list.service";
import {Router} from "@angular/router";
import {StatusEnum} from '@app/core/constants/status.enum';

@Component({
  selector: 'app-sale-item',
  templateUrl: './sale-item.component.html',
  styleUrls: ['./sale-item.component.scss']
})
export class SaleItemComponent implements OnInit {

  hotelID: number;
  @Input() isRebooking: boolean = null;
  @Input() model: ReservationForViewModel = new ReservationForViewModel();
  @Output() notesEvent = new EventEmitter();
  @Output() onInfoEvent = new EventEmitter();

  constructor(private reservationListService: ReservationListService,
              private router: Router, private ref: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.isRebooking == null) {
      this.isRebooking = false;
    }
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));
    this.hotelID = Number(hotel.id);
  }

  onActionClicked() {
    this.onInfoEvent.emit('info clicked');
  }

  onNotesClicked(model: any) {
    this.notesEvent.emit(model);
  }

  getStatusTheme(status: string): string {
    switch (status) {
      case StatusEnum.CONFIRM: {
        return 'DASHBOARD.BOOK_ITEM.CONFIRMED';
      }
      case StatusEnum.CANCEL: {
        return 'DASHBOARD.BOOK_ITEM.CANCELLED';
      }
      case StatusEnum.IN_HOUSE: {
        return 'DASHBOARD.BOOK_ITEM.IN_ROOM';
      }
      case StatusEnum.CHECKOUT: {
        return 'DASHBOARD.BOOK_ITEM.CHECK_OUT';
      }
      case StatusEnum.NO_SHOW: {
        return 'DASHBOARD.BOOK_ITEM.NO_SHOW';
      }
    }
  }

  getReservationDetailRoute(data) {
    this.reservationListService.bookingId.next(data.id);
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.hotelID, RoutesEnum.RESERVATION, RoutesEnum.DETAILS]).finally();
  }
}
