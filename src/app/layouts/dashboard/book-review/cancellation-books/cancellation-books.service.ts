import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {BehaviorSubject} from 'rxjs';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class CancellationBooksService {
  public isLoading = false;

  readonly cancellationBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly cancellation$ = this.cancellationBehavior.asObservable();

  readonly titleStatisticBehavior = new BehaviorSubject<any>({});
  readonly titleStatistic$ = this.titleStatisticBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  constructor(public dashboardService: DashboardApiService) {
  }

  set TITLE_STATISTIC(value: any) {
    this.titleStatisticBehavior.next(value);
  }

  set CANCELLATION(list: ReservationForViewModel[]) {
    this.cancellationBehavior.next(list);
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  getCancelledReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.dashboardService.getCancelledReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      const cancellations: ReservationForViewModel[] = [];
      const reservationsCount = res.reservationsCount;
      const revenue = res.revenue;
      this.TITLE_STATISTIC = {reservationsCount, revenue};
      this.TOTAL = res.total;
      res.reservations.forEach(item => cancellations.push(DashboardApiService.convertToSaleViewModel(item)));
      if (page === 1) {
        this.CANCELLATION = [];
        this.CANCELLATION = cancellations;
      } else {
        this.CANCELLATION = [...this.CANCELLATION, ...cancellations];
      }
      this.isLoading = false;
    });
  }

  public updateNotesCount(model: any) {
    this.CANCELLATION.map(el => {
      if (el.reservationId === model.id) {
        el.notesCount += 1;
      }
    });
  }
}
