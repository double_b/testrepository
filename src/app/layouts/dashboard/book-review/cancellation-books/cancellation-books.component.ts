import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CancellationBooksService} from '@app/layouts/dashboard/book-review/cancellation-books/cancellation-books.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

@Component({
  selector: 'app-cancellation-books',
  templateUrl: './cancellation-books.component.html',
  styleUrls: ['./cancellation-books.component.scss']
})
export class CancellationBooksComponent implements OnInit, OnChanges {
  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();
  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public cancellationBooksService: CancellationBooksService) {
  }

  ngOnInit() {
    this.getItems();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.cancellationBooksService.getCancelledReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.id,
      type: 'cancellation'
    };
    this.openNotesEmit.emit(newModel);
  }

}
