import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonConstants} from '@app/layouts/dashboard/common-constants';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {FormControl} from '@angular/forms';
import {NzMessageService} from 'ng-zorro-antd';
import {CheckInBooksService} from '@app/layouts/dashboard/book-review/check-in-books/check-in-books.service';
import {CheckOutBooksService} from '@app/layouts/dashboard/book-review/check-out-books/check-out-books.service';
import {CancellationBooksService} from '@app/layouts/dashboard/book-review/cancellation-books/cancellation-books.service';
import {RebookingBooksService} from '@app/layouts/dashboard/book-review/rebooking-books/rebooking-books.service';
import {SaleBooksService} from '@app/layouts/dashboard/book-review/sale-books/sale-books.service';
import {LiveBooksService} from '@app/layouts/dashboard/book-review/live-books/live-books.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-book-review',
  templateUrl: './book-review.component.html',
  styleUrls: ['./book-review.component.scss']
})
export class BookReviewComponent implements OnInit {

  hotelID: number;
  datesForFilter: any[];
  dayFilterState: any;
  fromDate: Date;
  byDate: Date;
  isVisible = false;
  modelForNotes: any;
  note: FormControl = new FormControl('');
  notes: any[] = [];

  constructor(
    private router: Router,
    public saleBooksService: SaleBooksService,
    private notificationService: NotificationService,
    public checkOutBooksService: CheckOutBooksService,
    public liveBooksService: LiveBooksService,
    public rebookingBooksService: RebookingBooksService,
    public checkInBooksService: CheckInBooksService,
    public cancellationBooksService: CancellationBooksService
  ) {
  }

  ngOnInit() {
    this.hotelID = Number(localStorage.getItem(KeysEnum.HOTEL_ID));
    this.setDateFilter();
  }

  setDateFilter() {
    this.datesForFilter = [];
    const today = new Date();
    this.fromDate = new Date(today);
    this.byDate = new Date(today);
    this.fromDate.setDate(this.fromDate.getDate());
    this.byDate.setDate(this.byDate.getDate());
    let dayForConfig = new Date();
    dayForConfig.setDate(this.byDate.getDate() - 1);
    this.datesForFilter.push({
      date: dayForConfig,
      label: 'DASHBOARD.YESTERDAY'
    });
    this.datesForFilter.push({
      date: today,
      label: 'DASHBOARD.TODAY'
    });
    dayForConfig = new Date();
    dayForConfig.setDate(this.byDate.getDate() + 1);
    this.datesForFilter.push({
      date: dayForConfig,
      label: 'DASHBOARD.TOMORROW'
    });
    this.datesForFilter.push({
      date: CommonConstants.MORE_DATE,
      label: 'DASHBOARD.MORE_DATES'
    });
    this.dayFilterState = today;
  }

  onDateFilterChanged() {
    if (this.dayFilterState === CommonConstants.MORE_DATE) {
      this.dayFilterState = new Date(this.fromDate.getDate() + 1);
      this.router.navigate([RoutesEnum.HOTEL, this.hotelID, RoutesEnum.RESERVATION, RoutesEnum.LIST]).finally();
    } else {
      this.fromDate = this.dayFilterState;
      this.byDate = this.dayFilterState;
    }
  }

  public openNotes(model) {
    this.modelForNotes = model;
    this.getNotes();
    this.showModal();
  }

  private getNotes() {
    this.saleBooksService.getNotes(this.modelForNotes.id).subscribe(res => {
      this.notes = res;
      this.notes.reverse();
    });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  addNote() {
    this.saleBooksService.addNotes(this.modelForNotes.id, this.note.value).subscribe(res => {
      this.note.setValue('');
      this.getNotes();
      this.notificationService.successMessage(`Комментарий был добавлен!`);
      this.updateNotesCount(this.modelForNotes);
    });
  }

  private updateNotesCount(model) {
    switch (model.type) {
      case 'checkIn':
        this.checkInBooksService.updateNotesCount(model);
        break;
      case 'checkOut':
        this.checkOutBooksService.updateNotesCount(model);
        break;
      case 'live':
        this.liveBooksService.updateNotesCount(model);
        break;
      case 'sale':
        this.saleBooksService.updateNotesCount(model);
        break;
      case 'cancellation':
        this.cancellationBooksService.updateNotesCount(model);
        break;
      case 'rebooking':
        this.rebookingBooksService.updateNotesCount(model);
        break;
    }
  }

  getReservationListRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.hotelID}/${RoutesEnum.RESERVATION}/${RoutesEnum.LIST}`;
  }
}
