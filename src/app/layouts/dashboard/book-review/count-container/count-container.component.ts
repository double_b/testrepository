import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-count-container',
  template: `
    <div class="count-container">
      <p class="sub-title">
        {{'DASHBOARD.SHOWED'| translate}} {{showed}} {{'DASHBOARD.OF'| translate}} {{total}}
      </p>
    </div>
  `,
  styles: [
      `.count-container {
      height: 40px;
      display: flex;
      justify-content: flex-end;
      padding-right: 50px;
      padding-top: 8px;
    }

    .sub-title {
      font-style: italic;
      font-weight: normal;
      font-size: 12px;
      line-height: 22px;
      color: #8C8C8C;
      margin-bottom: 0;
    }`
  ]
})
export class CountContainerComponent {
  @Input() showed = 0;
  @Input() total = 0;
}
