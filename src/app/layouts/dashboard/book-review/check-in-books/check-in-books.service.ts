import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {BehaviorSubject} from 'rxjs';
import {StatusEnum} from '@app/core/constants/status.enum';
import {Utils} from '@app/shared/helpers/utils';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Injectable({
  providedIn: 'root'
})
export class CheckInBooksService {
  public isLoading = false;

  readonly arrivedBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly arrived$ = this.arrivedBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  filterState = {
    getFrom: null,
    getBy: null
  };

  constructor(private dashboardApiService: DashboardApiService, private notification: NotificationService) {
  }

  set ARRIVED(list: ReservationForViewModel[]) {
    this.arrivedBehavior.next(list);
  }

  get ARRIVED(): ReservationForViewModel[] {
    return this.arrivedBehavior.getValue();
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  get TOTAL(): number {
    return this.totalBehavior.getValue();
  }

  public getArrivedReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.filterState = {getFrom, getBy};
    this.dashboardApiService.getArrivedReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      const result: ReservationForViewModel[] = [];
      let tempTotal = res.total;
      for (const reservation of res.reservations) {
        if (reservation.status !== StatusEnum.CONFIRM) {
          tempTotal--;
          continue;
        }
        reservation.actionBtnDisabled = !Utils.compareDates(new Date(), reservation.checkin, true);
        result.push(DashboardApiService.convertToViewModel(reservation, DashboardApiService.VIEW_MODE_ARRIVED));
      }
      this.TOTAL = tempTotal;
      if (page === 1) {
        this.ARRIVED = [];
        this.ARRIVED = result;
      } else {
        this.ARRIVED = [...this.ARRIVED, ...result];
      }
      this.isLoading = false;
    });
  }

  public setGuestAsArrived(reservation: ReservationForViewModel) {
    if (!Utils.isExist(reservation.id)) {
      this.notification.errorMessage('Ошибка', 'У брони не указан точный номер комнаты!');
      return;
    }
    this.isLoading = true;
    this.dashboardApiService.setGuestAsArrived(reservation.id).subscribe(res => {
      if (res.data == null && res.message != null) {
        if (res.message === 'Guest has already arrived') {
          this.notification.errorMessage('Ошибка', 'В данном номере уже проживает гость');
        }
        this.isLoading = false;
        return;
      }
      if (Utils.compareDates(new Date(), reservation.checkin)) {
        this.dashboardApiService.getStatistics.next();
      }
      const item = this.ARRIVED.find(_ => _.id === reservation.id);
      if (item) {
        const inx = this.ARRIVED.indexOf(item);
        this.ARRIVED.splice(inx, 1);
        this.ARRIVED = [...this.ARRIVED];
        let tempTotal = this.TOTAL;
        tempTotal--;
        this.TOTAL = tempTotal;
        this.notification.successMessage('Статус бронирования изменен!', 'Вы можете просмотреть информацию в разделе "Проживают".');
        if (this.ARRIVED.length < DashboardApiService.MIN_ITEMS_COUNT) {
          this.getArrivedReservations(this.filterState.getFrom, this.filterState.getBy, DashboardApiService.MIN_ITEMS_COUNT, 1);
        } else {
          this.isLoading = false;
        }
      }
    });
  }

  public updateNotesCount(model: any) {
    this.ARRIVED.map(el => {
      if (el.reservationId === model.id) {
        el.notesCount += 1;
      }
    });
  }
}
