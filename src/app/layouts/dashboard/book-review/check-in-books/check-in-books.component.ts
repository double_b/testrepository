import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {CheckInBooksService} from '@app/layouts/dashboard/book-review/check-in-books/check-in-books.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

@Component({
  selector: 'app-check-in-books',
  templateUrl: './check-in-books.component.html',
  styleUrls: ['./check-in-books.component.scss']
})
export class CheckInBooksComponent implements OnChanges {

  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();

  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public checkInBooksService: CheckInBooksService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  onActionClicked(event: any) {
    this.checkInBooksService.setGuestAsArrived(event);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.reservationId,
      type: 'checkIn',
    };
    this.openNotesEmit.emit(newModel);
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.checkInBooksService.getArrivedReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }
}
