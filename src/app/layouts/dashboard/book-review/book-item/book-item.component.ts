import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReservationForViewModel} from '@src/app/core/models/dashboard/view/reservation-for-view.model';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ReservationListService} from '@app/layouts/reservation/reservation-list/reservation-list.service';
import {Router} from '@angular/router';
import {StatusEnum} from '@app/core/constants/status.enum';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss']
})
export class BookItemComponent implements OnInit {

  onload = false;
  hotelID: number;
  @Input() model: ReservationForViewModel;
  @Input() actionBtnTitle = null;
  @Output() actionEvent = new EventEmitter();
  @Output() notesEvent = new EventEmitter();

  constructor(private reservationListService: ReservationListService,
              private router: Router) {
  }

  ngOnInit() {
    // console.log(this.model);
    // this.setButtonTitle();
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));
    this.hotelID = Number(hotel.id);
  }

  // private setButtonTitle() {
  //   if (this.model.status === 'confirmed') {
  //     this.actionBtnTitle = 'DASHBOARD.BOOK_ITEM.IN_ROOM';
  //   } else {
  //     this.actionBtnTitle = 'DASHBOARD.BOOK_ITEM.ACTION_CHECK_OUT';
  //   }
  // }

  onActionClicked(model: ReservationForViewModel) {
    this.actionEvent.emit(model);
  }

  onNotesClicked(model: any) {
    this.notesEvent.emit(model);
  }

  getStatusTheme(status: string): string {
    switch (status) {
      case StatusEnum.CONFIRM: {
        return 'STATUS.CONFIRM';
      }
      case StatusEnum.CANCEL: {
        return 'STATUS.CANCEL';
      }
      case StatusEnum.IN_HOUSE: {
        return 'STATUS.IN_HOUSE';
      }
      case StatusEnum.CHECKOUT: {
        return 'STATUS.CHECKOUT';
      }
      case StatusEnum.NO_SHOW: {
        return 'STATUS.NO_SHOW';
      }
    }
  }

  getReservationDetailRoute(data) {
    this.reservationListService.bookingId.next(data.reservationId);
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.hotelID, RoutesEnum.RESERVATION, RoutesEnum.DETAILS]).finally();
  }

}
