import {Injectable} from '@angular/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaleBooksService {
  public isLoading = false;
  readonly soldBehavior = new BehaviorSubject<ReservationForViewModel[]>([]);
  readonly sold$ = this.soldBehavior.asObservable();

  readonly titleStatisticBehavior = new BehaviorSubject<any>({});
  readonly titleStatistic$ = this.titleStatisticBehavior.asObservable();

  readonly totalBehavior = new BehaviorSubject<number>(0);
  readonly total$ = this.totalBehavior.asObservable();

  constructor(public dashboardService: DashboardApiService) {
  }

  set TITLE_STATISTIC(value: any) {
    this.titleStatisticBehavior.next(value);
  }

  set SOLD(list: ReservationForViewModel[]) {
    this.soldBehavior.next(list);
  }

  get SOLD(): ReservationForViewModel[] {
    return this.soldBehavior.getValue();
  }

  set TOTAL(value: number) {
    this.totalBehavior.next(value);
  }

  getConfirmedReservations(getFrom: Date, getBy: Date, pageSize: number, page: number) {
    this.isLoading = true;
    this.dashboardService.getConfirmedReservations(getFrom, getBy, pageSize, page).subscribe(res => {
      this.TOTAL = res.total;
      const bookedToday = res.bookedToday;
      const roomNights = res.roomNights;
      const revenue = res.revenue;
      this.TITLE_STATISTIC = {bookedToday, roomNights, revenue};
      const result: ReservationForViewModel[] = [];
      res.reservations.forEach(item => result.push(DashboardApiService.convertToSaleViewModel(item)));
      if (page === 1) {
        this.SOLD = [];
        this.SOLD = result;
      } else {
        this.SOLD = [...this.SOLD, ...result];
      }
      this.isLoading = false;
    });
  }

  getNotes(bookingId): Observable<any> {
    this.isLoading = true;
    return this.dashboardService.getNote(bookingId).pipe(
      map((res) => {
        this.isLoading = false;
        return res;
      })
    );
  }

  addNotes(bookingId, note): Observable<any> {
    this.isLoading = true;
    return this.dashboardService.addNote(bookingId, note).pipe(
      map((res) => {
        this.isLoading = false;
        return res;
      })
    );
  }

  public updateNotesCount(model: any) {
    this.SOLD.map(el => {
      if (el.id === model.id) {
        el.notesCount += 1;
      }
    });
  }
}
