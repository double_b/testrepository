import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {SaleBooksService} from '@app/layouts/dashboard/book-review/sale-books/sale-books.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

@Component({
  selector: 'app-sale-books',
  templateUrl: './sale-books.component.html',
  styleUrls: ['./sale-books.component.scss']
})
export class SaleBooksComponent implements OnChanges {
  @Input() fromDate: Date;
  @Input() byDate: Date;
  @Output() openNotesEmit = new EventEmitter();

  public page = 1;

  minItems = DashboardApiService.MIN_ITEMS_COUNT;

  constructor(public saleBooksService: SaleBooksService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.page = 1;
    this.getItems();
  }

  getItems(downloadMore: boolean = false) {
    if (downloadMore) {
      this.page++;
    }
    this.saleBooksService.getConfirmedReservations(this.fromDate, this.byDate, DashboardApiService.MIN_ITEMS_COUNT, this.page);
  }

  public openNotes(model) {
    const newModel = {
      firstName: model.firstName,
      lastName: model.lastName,
      id: model.id,
      type: 'sale',
    };
    this.openNotesEmit.emit(newModel);
  }
}
