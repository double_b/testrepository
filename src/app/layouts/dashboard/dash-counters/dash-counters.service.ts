import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {OverAllStatisticModel} from '@app/core/models/dashboard/view/over-all-statistic.model';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class DashCountersService {

  readonly arrivedBehavior = new BehaviorSubject<number>(0);
  readonly arrivedPercentBehavior = new BehaviorSubject<number>(0);
  readonly arrivedPercent$ = this.arrivedPercentBehavior.asObservable();
  readonly shouldArriveBehavior = new BehaviorSubject<number>(0);
  readonly shouldArrive$ = this.shouldArriveBehavior.asObservable();

  readonly leftBehavior = new BehaviorSubject<number>(0);
  readonly leftPercentBehavior = new BehaviorSubject<number>(0);
  readonly leftPercent$ = this.leftPercentBehavior.asObservable();
  readonly shouldLeftBehavior = new BehaviorSubject<number>(0);
  readonly shouldLeft$ = this.shouldLeftBehavior.asObservable();

  readonly livesBehavior = new BehaviorSubject<number>(0);
  readonly livesPercentBehavior = new BehaviorSubject<number>(0);
  readonly livesPercent$ = this.livesPercentBehavior.asObservable();

  readonly loadOnMonthBehavior = new BehaviorSubject<number>(0);
  readonly loadOnMonth$ = this.loadOnMonthBehavior.asObservable();

  constructor(private dashboardService: DashboardApiService) {
  }

  getStatistic(getFrom: Date, getBy: Date) {
    this.dashboardService.getOverallStatistic(getFrom, getBy).subscribe(res => {
      if (res.arrived !== this.arrivedBehavior.getValue() || res.shouldArrive !== this.shouldArriveBehavior.getValue()) {
        this.manageArrive(res);
      }
      if (res.left !== this.leftBehavior.getValue() || res.shouldLeft !== this.shouldLeftBehavior.getValue()) {
        this.manageLeft(res);
      }
      if (res.lives !== this.livesBehavior.getValue()) {
        this.manageLives(res);
      }
      if (res.load_on_month !== this.loadOnMonthBehavior.getValue()) {
        this.manageLoadOnMonth(res);
      }
    });
  }

  manageArrive(res: OverAllStatisticModel) {
    if (res.shouldArrive > 0) {
      if (res.arrived > 0 && res.arrived <= res.shouldArrive) {
        res.arrivePercent = Math.floor((res.arrived * 100) / res.shouldArrive);
      } else {
        res.arrivePercent = 0;
      }
    } else {
      res.arrivePercent = 0;
    }
    this.shouldArriveBehavior.next(res.shouldArrive);
    if (this.arrivedPercentBehavior.getValue() === 0) {
      let tempArrived = 0;
      const interval = setInterval(() => {
        let tempArriveState = true;
        if (tempArrived !== res.arrived) {
          tempArrived++;
          tempArriveState = false;
          const tempPercent = (tempArrived * 100) / res.shouldArrive;
          this.arrivedBehavior.next(tempArrived);
          this.arrivedPercentBehavior.next(tempPercent);
        }

        if (tempArriveState) {
          clearInterval(interval);
        }
      }, 100);
    } else {
      this.arrivedBehavior.next(res.arrived);
      this.arrivedPercentBehavior.next(res.arrivePercent);
    }
  }

  manageLeft(res: OverAllStatisticModel) {
    if (res.shouldLeft > 0) {
      if (res.left > 0 && res.left <= res.shouldLeft) {
        res.leftPercent = Math.floor((res.left * 100) / res.shouldLeft);
      } else {
        res.leftPercent = 0;
      }
    } else {
      res.leftPercent = 0;
    }
    this.shouldLeftBehavior.next(res.shouldLeft);
    if (this.leftPercentBehavior.getValue() === 0) {
      let tempLeft = 0;
      const interval = setInterval(() => {
        let tempLeftState = true;
        if (tempLeft !== res.left) {
          tempLeft++;
          tempLeftState = false;
          const tempPercent = (tempLeft * 100) / res.shouldLeft;
          this.leftBehavior.next(tempLeft);
          this.leftPercentBehavior.next(tempPercent);
        }
        if (tempLeftState) {
          clearInterval(interval);
        }
      }, 100);
    } else {
      this.leftBehavior.next(res.left);
      this.leftPercentBehavior.next(res.leftPercent);
    }
  }

  manageLives(res: OverAllStatisticModel) {
    if (res.lives === 0) {
      this.livesBehavior.next(res.lives);
      this.livesPercentBehavior.next(0);
      return;
    }
    if (this.livesBehavior.getValue() === 0) {
      let tempLives = 0;
      const interval = setInterval(() => {
        let tempLivesState = true;
        if (tempLives !== res.lives) {
          tempLives++;
          tempLivesState = false;
          const tempPercent = (tempLives * 100) / res.lives;
          this.livesBehavior.next(tempLives);
          this.livesPercentBehavior.next(tempPercent);
        }
        if (tempLivesState) {
          clearInterval(interval);
        }
      }, 100);
    } else {
      /**
       * будущие разрабы смарта
       * не пугайтесь если увидите такой вот код
       * батя вам все объяснит
       * 1. Процент заполненности счетчика проживают всегда больше 100 - то есть всегда заполнен цветом
       * если кол-во проживающих больше 0
       * 2. Использован счетчик Ant Design - а он так устроен, что label внутри круга обновляется только когда обновится процент
       * даже если использовать async pipe
       * поэтому чтобы просто обновить label
       * - обновляем процент тоже (все равно процент уже как минимум равен 100 и плюс 1 тут никак не помешает)
       */
      this.livesBehavior.next(res.lives);
      this.livesPercentBehavior.next(this.livesPercentBehavior.getValue() + 1);
    }
  }

  manageLoadOnMonth(res: OverAllStatisticModel) {
    if (this.loadOnMonthBehavior.getValue() === 0) {
      let tempLoad = 0;
      const fullPercent = Math.trunc(res.load_on_month);
      const remainder = Utils.round(res.load_on_month - fullPercent);
      const interval = setInterval(() => {
        let tempLoadState = true;
        if (tempLoad !== fullPercent) {
          tempLoad++;
          tempLoadState = false;
          this.loadOnMonthBehavior.next(tempLoad);
        }
        if (tempLoadState) {
          const sum = tempLoad + remainder;
          const strSum = sum.toString();
          if (strSum.includes('.')) {
            const before = strSum.split('.')[0];
            let after = strSum.split('.')[1];
            if (after.length > 2) {
              after = strSum.split('.')[1].substr(0, 2);
            }
            const result = before + '.' + after;
            this.loadOnMonthBehavior.next(Number(result));
          } else {
            this.loadOnMonthBehavior.next(sum);
          }
          clearInterval(interval);
        }
      }, 75);
    } else {
      this.loadOnMonthBehavior.next(res.load_on_month);
    }
  }

  public getArrivedCounterTitle = () => `${this.arrivedBehavior.getValue()}/${this.shouldArriveBehavior.getValue()}`;
  public getLeftCounterTitle = () => `${this.leftBehavior.getValue()}/${this.shouldLeftBehavior.getValue()}`;
  public getLivesCounterTitle = () => `${this.livesBehavior.getValue()}`;
  public getOverloadCounterTitle = () => `${this.loadOnMonthBehavior.getValue()}%`;
}
