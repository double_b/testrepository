import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DashboardApiService} from '@src/app/shared/services/dashboard-api/dashboard-api.service';
import {DashCountersService} from '@app/layouts/dashboard/dash-counters/dash-counters.service';

@Component({
  selector: 'app-dash-counters',
  templateUrl: './dash-counters.component.html',
  styleUrls: ['./dash-counters.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashCountersComponent implements OnInit {

  constructor(public dashboardService: DashboardApiService, public dashCountersService: DashCountersService) {
  }

  ngOnInit() {
    this.getStatistics();
    // commented by double_b - не понятно зачем обновлять счетчики при каждом переключении табов
    this.dashboardService.getStatistics.subscribe(() => {
      this.getStatistics();
    });
  }

  private getStatistics() {
    const from = new Date();
    const by = new Date(from.getFullYear(), from.getMonth() + 1, from.getDate());
    this.dashCountersService.getStatistic(from, by);
  }

}
