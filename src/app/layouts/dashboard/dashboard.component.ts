import {Component, OnInit} from '@angular/core';

// Services
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  routesEnum = new RoutesEnum();

  hotelID = null;
  hotelName: string;

  constructor() {
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.getHotelData();
  }

  // Получаю данные отеля
  getHotelData() {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL));
    this.hotelName = hotel.name;
    this.hotelID = hotel.id;
  }

  getNewReservationRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.hotelID}/${RoutesEnum.RESERVATION}/${RoutesEnum.CREATE}`;
  }
}
