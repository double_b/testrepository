import {Injectable} from '@angular/core';
import {DateLabel, ForecastAvailableModel, ForecastRoom, ValueByDate} from '@app/core/models/dashboard/view/forecast-available.model';
import {DayNames} from '@app/core/models/dashboard/day-names';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';
import {ForecastResponseModel} from '@app/core/models/dashboard/response/forecast-response.model';
import {BehaviorSubject} from 'rxjs';
import {ForecastHeaderModel} from '@app/core/models/dashboard/view/forecast-header.model';
import {ForecastChartModel, Series} from '@app/core/models/dashboard/view/forecast-chart.model';
import {TranslateService} from '@ngx-translate/core';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  public isLoading = false;
  public isNextLoading = false;
  public isPrevLoading = false;

  readonly headerValuesBehavior = new BehaviorSubject<ForecastHeaderModel>(new ForecastHeaderModel());
  readonly $headerValues = this.headerValuesBehavior.asObservable();

  readonly forecastAvailableBehavior = new BehaviorSubject<ForecastAvailableModel>(new ForecastAvailableModel());
  readonly $forecastAvailable = this.forecastAvailableBehavior.asObservable();

  readonly forecastAreaChartBehavior = new BehaviorSubject<ForecastChartModel>(new ForecastChartModel());
  readonly $forecastAreaChart = this.forecastAreaChartBehavior.asObservable();

  readonly forecastResponseBehavior = new BehaviorSubject<ForecastResponseModel>(null);

  startDate = new Date();

  constructor(private dashboardApiService: DashboardApiService,
              private translate: TranslateService) {
  }

  set HEADER_VALUES(values: ForecastHeaderModel) {
    this.headerValuesBehavior.next(values);
  }

  set FORECAST(value: ForecastAvailableModel) {
    this.forecastAvailableBehavior.next(value);
  }

  get FORECAST_AREA_CHART(): ForecastChartModel {
    return this.forecastAreaChartBehavior.getValue();
  }

  set FORECAST_AREA_CHART(value: ForecastChartModel) {
    this.forecastAreaChartBehavior.next(value);
  }

  onDateChange(toNext: boolean) {
    switch (toNext) {
      case true: {
        this.isNextLoading = true;
        this.startDate.setDate(this.startDate.getDate() + DashboardApiService.FILTER_DAYS_COUNT);
        break;
      }
      case false: {
        this.isPrevLoading = true;
        this.startDate.setDate(this.startDate.getDate() - DashboardApiService.FILTER_DAYS_COUNT);
        break;
      }
    }
    this.getData();
  }

  getData() {
    this.isLoading = true;
    const getBy = new Date(this.startDate);
    getBy.setDate(this.startDate.getDate() + (DashboardApiService.FILTER_DAYS_COUNT - 1));
    this.dashboardApiService.getForecast(this.startDate, getBy).subscribe(res => {
      this.forecastResponseBehavior.next(res);
    }, error => {
      this.isLoading = false;
      this.isNextLoading = false;
      this.isPrevLoading = false;
    });
  }

  public getForecast() {
    if (this.forecastResponseBehavior.getValue() === null) {
      this.getData();
    }
    this.forecastResponseBehavior.subscribe(response => {
      if (response === null) {
        return;
      }
      this.isLoading = true;
      this.HEADER_VALUES = {
        loaded: response.loaded_average,
        profit: response.earned,
        currency: response.currency === null || response.currency === undefined ? 'UZS' : response.currency
      };
      const model = new ForecastAvailableModel();
      model.dates = [];
      model.rooms = [];

      for (let i = 0; i < DashboardApiService.FILTER_DAYS_COUNT; i++) {
        const temp: DateLabel = new DateLabel();
        temp.date = new Date(response.loaded_by_dates[i].date);
        temp.label = DayNames.getDayName(temp.date.getDay());
        temp.value = 0;
        model.dates.push(temp);
      }

      response.room_types.forEach(item => {
        const room = new ForecastRoom();
        room.label = item.room_type_short_name;
        room.values = [];

        model.dates.forEach(i => {
          const date = Utils.convertDateToQuery(i.date);
          const object = item.availability[date];
          const roomData = new ValueByDate();
          roomData.date = i.date;
          if (object === null || object === undefined) {
            roomData.value = 0;
          } else {
            roomData.value = object[0].rooms - object[0].reserved;
          }
          i.value += roomData.value;
          room.values.push(roomData);
        });
        model.rooms.push(room);
      });
      this.FORECAST = model;
      this.isLoading = false;
      this.isNextLoading = false;
      this.isPrevLoading = false;
    });
  }

  public getForecastForChart() {
    if (this.forecastResponseBehavior.getValue() === null) {
      this.getData();
    }
    this.translate.get('LOADING').subscribe(downloadWord => {
      this.forecastResponseBehavior.subscribe(response => {
        if (response === null) {
          return;
        }
        this.isLoading = true;
        this.HEADER_VALUES = {
          loaded: response.loaded_average,
          profit: response.earned,
          currency: response.currency === null || response.currency === undefined ? 'UZS' : response.currency
        };
        const temp = new ForecastChartModel();
        temp.series = [];
        temp.dates = [];
        const tempSeries = new Series();
        tempSeries.name = downloadWord;
        tempSeries.data = [];
        for (let i = 0; i < DashboardApiService.FILTER_DAYS_COUNT; i++) {
          temp.dates.push(response.loaded_by_dates[i].date);
          tempSeries.data.push(response.loaded_by_dates[i].value);
        }
        temp.series.push(tempSeries);
        this.FORECAST_AREA_CHART = temp;
        this.isLoading = false;
        this.isNextLoading = false;
        this.isPrevLoading = false;
      });
    });
  }
}
