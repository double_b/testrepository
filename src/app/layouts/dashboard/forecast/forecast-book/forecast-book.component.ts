import {Component, OnInit, ViewChild} from '@angular/core';
import {ForecastService} from '@app/layouts/dashboard/forecast/forecast.service';
import {ApexChart, ApexDataLabels, ApexStroke, ApexXAxis, ApexYAxis, ChartComponent, ApexGrid} from 'ng-apexcharts';
// @ts-ignore
import ru from 'apexcharts/dist/locales/ru.json';
// @ts-ignore
import en from 'apexcharts/dist/locales/en.json';
// @ts-ignore
import uz from '../../../../../assets/i18n/uz.apexcharts.json';
import {TranslateService} from '@ngx-translate/core';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

export interface ChartOptions {
  colors: string[];
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  markers: any;
}

@Component({
  selector: 'app-forecast-book',
  templateUrl: './forecast-book.component.html',
  styleUrls: ['./forecast-book.component.scss']
})

export class ForecastBookComponent implements OnInit {

  startDate: Date;
  FILTER_DAYS_COUNT = DashboardApiService.FILTER_DAYS_COUNT;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  constructor(public forecastService: ForecastService,
              private translate: TranslateService) {
    this.chartOptions = {
      colors: ['#5EB1FF'],
      chart: {
        locales: [ru, en, uz],
        defaultLocale: this.translate.getDefaultLang(),
        type: 'area',
        height: 350,
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        },
        sparkline: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        width: 2
      },
      xaxis: {
        type: 'datetime'
      },
      yaxis: {
        max: 100,
        decimalsInFloat: 0,
        labels: {
          /**
           * Allows users to apply a custom formatter function to yaxis labels.
           *
           * @param { String } value - The generated value of the y-axis tick
           * @param { index } index of the tick / currently executing iteration in yaxis labels array
           */
          formatter: function(val, index) {
            if (typeof index === 'number') {
              val = index * 10;
            }
            return val + '%';
          }
        }
      },
      markers: {
        size: 0,
        colors: ['#52C41A'],
        hover: {
          size: 5
        }
      }
    };
  }

  ngOnInit() {
    this.getData();
  }

  onDateChange(toNext: boolean) {
    this.forecastService.onDateChange(toNext);
  }

  getData() {
    this.forecastService.getForecastForChart();
  }

}
