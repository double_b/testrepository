import {Component, OnInit} from '@angular/core';
import {ForecastService} from '@app/layouts/dashboard/forecast/forecast.service';
import {DashboardApiService} from '@app/shared/services/dashboard-api/dashboard-api.service';

@Component({
  selector: 'app-forecast-available',
  templateUrl: './forecast-available.component.html',
  styleUrls: ['./forecast-available.component.scss']
})
export class ForecastAvailableComponent implements OnInit {
  dateCellWidth = '';
  FILTER_DAYS_COUNT = DashboardApiService.FILTER_DAYS_COUNT;

  constructor(public forecastService: ForecastService) {
  }

  ngOnInit() {
    const elmnt = document.getElementById('date-row');
    this.dateCellWidth = `${elmnt.offsetWidth / (this.FILTER_DAYS_COUNT + 3)}px`;
    this.getData();
  }

  onDateChange(toNext: boolean) {
    this.forecastService.onDateChange(toNext);
  }

  getData() {
    this.forecastService.getForecast();
  }

  getOverAllTheme(value: number): string {
    if (value === 0 || value === null || value === undefined) {
      return 'fill-non-cell';
    } else {
      return 'fill-free-cell';
    }
  }

}
