import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cell-item',
  template: `
    <nz-divider [nzText]="cell"></nz-divider>
    <ng-template #cell>
      <div [className]="'value-cell' + ' ' + getStyleClass()">
        <p>{{value}}</p>
      </div>
    </ng-template>`,
  styleUrls: ['./cell-item.component.scss']
})
export class CellItemComponent {

  @Input() value: number;

  getStyleClass(): string {
    return this.value > 0 ? 'empty-free-cell' : 'empty-non-cell';
  }

}
