import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Routs
import {LayoutsRoutingModule} from './layouts-routing.module';

// Modules
import {SharedModule} from '@src/app/core/shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
    SharedModule
  ]
})
export class LayoutsModule {
}
