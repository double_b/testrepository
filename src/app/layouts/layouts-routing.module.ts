import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: RoutesEnum.DASHBOARD,
        loadChildren: () => import('./dashboard/dashboard.module')
          .then(m => m.DashboardModule)
      },
      {
        path: RoutesEnum.CALENDAR,
        loadChildren: () => import('./calendar/calendar.module')
          .then(m => m.CalendarModule)
      },
      {
        path: RoutesEnum.RESERVATION,
        loadChildren: () => import('./reservation/reservation.module')
          .then(m => m.ReservationModule)
      },
      {
        path: RoutesEnum.CHESS,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('../views/chess/chess.module')
          .then(m => m.ChessModule)
      },
      {
        path: RoutesEnum.CHANNELS,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('../views/channels/channels.module')
          .then(m => m.ChannelsModule)
      },
      {
        path: RoutesEnum.SOURCES,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('./sources/sources.module')
          .then(m => m.SourcesModule)
      },
      {
        path: RoutesEnum.GUESTS,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('../views/guests/guests.module')
          .then(m => m.GuestsModule)
      },
      {
        path: RoutesEnum.DISTRIBUTION,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('../views/distribution/distribution.module')
          .then(m => m.DistributionModule)
      },
      {
        path: RoutesEnum.TARIFF_PLANS,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('./tariff-plans/tariff-plans.module')
          .then(m => m.TariffPlansModule)
      },
      {
        path: RoutesEnum.PRICE_SET,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('../views/price-setting/price-setting.module')
          .then(m => m.PriceSettingModule)
      },
      {
        path: RoutesEnum.STATISTIC,
        // canActivate: [NewHotelGuard],
        loadChildren: () => import('./statistic/statistic.module')
          .then(m => m.StatisticModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule {
}
