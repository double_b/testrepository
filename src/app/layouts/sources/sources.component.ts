import {Component, OnInit} from '@angular/core';
import {SourceModel} from '@app/shared/models/source.model';
import {SourcesService} from '@app/layouts/sources/sources-service.service';

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.scss']
})
export class SourcesComponent implements OnInit {
  isShowFormModal = false;
  isShowDeleteModal = false;
  selectedSource: SourceModel = null;

  constructor(public sourcesService: SourcesService) {
  }

  ngOnInit() {
    this.sourcesService.getSources();
  }

  onSwitch(source: SourceModel) {
    this.sourcesService.editSource(source);
  }

  onOpenModal(source: SourceModel, forDelete: boolean = false) {
    // если источник неактивен - модалку для редактирования не открываем
    if (source !== null && !source.active) {
      return;
    }
    // если источник - стандартный - то модалку для удаления не запускаем
    if (forDelete && source !== null && source.default === 1) {
      return;
    }
    if (forDelete) {
      this.isShowDeleteModal = true;
    } else {
      this.isShowFormModal = true;
    }
    this.selectedSource = source;
  }

  onCloseModal() {
    this.selectedSource = null;
    this.isShowFormModal = false;
    this.isShowDeleteModal = false;
  }
}
