import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {SourceModel} from '@app/shared/models/source.model';
import {Utils} from '@app/shared/helpers/utils';
import {SourcesService} from '@app/layouts/sources/sources-service.service';

@Component({
  selector: 'app-source-form',
  templateUrl: './source-form.component.html',
  styleUrls: ['./source-form.component.scss']
})
export class SourceFormComponent implements OnChanges {

  modalTitle = 'SOURCES.ADD_SOURCE';
  @Input() isVisible = true;
  @Input() source: SourceModel = new SourceModel();
  @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public sourcesService: SourcesService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.source && changes.source.currentValue !== null) {
      // значит модалку для редактирование - заполняем форму
      this.source = changes.source.currentValue;
      // с сервера приходит 1/0 вместо true/false - поэтому переопределяем значение
      this.source.overbooking = this.source.overbooking === 1;
      this.modalTitle = 'SOURCES.EDIT_SOURCE';
    } else {
      // значит модалку вызвали для создания
      this.source = new SourceModel();
      this.modalTitle = 'SOURCES.ADD_SOURCE';
    }
  }

  handleCancel() {
    this.source = null;
    this.isVisible = false;
    this.onClose.emit(false);
  }

  handleOk() {
    // сервер требует 1/0 вместо true/false - поэтому переопределяем значение перед отправкой
    this.source.overbooking = this.source.overbooking ? 1 : 0;
    if (this.source.id === null) {
      this.sourcesService.createSource(this.source);
    } else {
      this.sourcesService.editSource(this.source);
    }
    this.handleCancel();
  }

  existName(): boolean {
    return !Utils.isExist(this.source.name);
  }

  isDefault(): boolean {
    return this.source.default === 1;
  }
}
