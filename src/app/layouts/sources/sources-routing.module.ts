import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Components
import {SourcesComponent} from './sources.component';

const routes: Routes = [
  {
    path: '',
    component: SourcesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SourcesRoutingModule {
}
