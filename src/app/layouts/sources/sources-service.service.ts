import {Injectable, OnDestroy} from '@angular/core';
import {SourcesApiService} from '@app/shared/services/sources-api/sources-api.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {SourceModel} from '@app/shared/models/source.model';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Injectable({
  providedIn: 'root'
})
export class SourcesService implements OnDestroy {

  private readonly _sources = new BehaviorSubject<SourceModel[]>([]);
  readonly $sources = this._sources.asObservable();

  // true/false - таблица слушает его для отображение лоадера при загрузке данных
  public readonly _loader = new BehaviorSubject<boolean>(false);
  readonly loader$ = this._loader.asObservable();

  getSourcesSub: Subscription = null;
  createSourcesSub: Subscription = null;
  editSourcesSub: Subscription = null;
  deleteSourcesSub: Subscription = null;

  constructor(private sourcesApiService: SourcesApiService) {
  }

  getSources() {
    this._loader.next(true);
    this.getSourcesSub = this.sourcesApiService.getSources(this.getPropertyId()).subscribe(res => {
      this._sources.next([]);
      this._sources.next(this.sort(res));
      this._loader.next(false);
    });
  }

  createSource(source: SourceModel) {
    this._loader.next(true);
    this.createSourcesSub = this.sourcesApiService.createSource(source, this.getPropertyId()).subscribe(res => {
      const list: SourceModel[] = [...this._sources.getValue(), res];
      this._sources.next([]);
      this._sources.next(this.sort(list));
      this._loader.next(false);
    });
  }

  editSource(source: SourceModel) {
    this._loader.next(true);
    this.editSourcesSub = this.sourcesApiService.editSource(source, this.getPropertyId()).subscribe(res => {
      const _source = this._sources.getValue().find(item => item.id === res.id);
      const index = this._sources.getValue().indexOf(_source);
      this._sources.getValue()[index] = res;
      this._loader.next(false);
    });
  }

  deleteSource(source: SourceModel) {
    if (source.default === 1) {
      return;
    }
    this._loader.next(true);
    this.deleteSourcesSub = this.sourcesApiService.deleteSource(source.id, this.getPropertyId()).subscribe(res => {
      const _source = this._sources.getValue().find(item => item.id === source.id);
      const index = this._sources.getValue().indexOf(_source);
      this._sources.getValue().splice(index, 1);
      this._loader.next(false);
    });
  }

  sort(sources: SourceModel[]): SourceModel[] {
    return sources.sort((itemA, itemB) => {
      if (itemA.sort_order > itemB.sort_order) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }

  ngOnDestroy() {
    if (this.getSourcesSub != null) {
      this.getSourcesSub.unsubscribe();
    }
    if (this.createSourcesSub != null) {
      this.createSourcesSub.unsubscribe();
    }
    if (this.editSourcesSub != null) {
      this.editSourcesSub.unsubscribe();
    }
    if (this.deleteSourcesSub != null) {
      this.deleteSourcesSub.unsubscribe();
    }
  }

  /**
   * @deprecated
   */
  public getSourcesOld = (successFunction: Function, errorFunction: Function) => {
    this.sourcesApiService.getSources(this.getPropertyId()).pipe().subscribe(
      sources => {
        successFunction(sources);
      },
      error => {
        errorFunction(error.error);
      });
  };
}
