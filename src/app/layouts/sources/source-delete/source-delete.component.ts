import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SourcesService} from '../sources-service.service';
import {SourceModel} from '@app/shared/models/source.model';

@Component({
  selector: 'app-source-delete',
  templateUrl: './source-delete.component.html',
  styleUrls: ['./source-delete.component.scss']
})
export class SourceDeleteComponent {

  @Input() isVisible = true;
  @Input() source: SourceModel = new SourceModel();
  @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private sourcesService: SourcesService) {
  }

  handleOk(): void {
    this.sourcesService.deleteSource(this.source);
    this.handleCancel();
  }

  handleCancel(): void {
    this.source = null;
    this.isVisible = false;
    this.onClose.emit(false);
  }

}
