import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SourcesRoutingModule} from './sources-routing.module';
import {SourcesComponent} from './sources.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  NzAlertModule,
  NzButtonModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzDividerModule,
  NzFormModule,
  NzIconModule,
  NzInputModule,
  NzModalModule,
  NzSpinModule,
  NzSwitchModule,
  NzTableModule,
  NzToolTipModule,
} from 'ng-zorro-antd';
import {SourceDeleteComponent} from './source-delete/source-delete.component';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';
import {SourceFormComponent} from './source-form/source-form.component';


@NgModule({
  declarations: [
    SourcesComponent,
    SourceDeleteComponent,
    SourceFormComponent
  ],
  imports: [
    FormsModule,
    SourcesRoutingModule,
    NzSpinModule,
    ReactiveFormsModule,
    NzAlertModule,
    NzButtonModule,
    NzCheckboxModule,
    NzDatePickerModule,
    NzFormModule,
    NzIconModule,
    NzInputModule,
    NzModalModule,
    NzSwitchModule,
    NzTableModule,
    NzDividerModule,
    NzToolTipModule,
    OldShared,
    CommonModule
  ]
})

export class SourcesModule {
}
