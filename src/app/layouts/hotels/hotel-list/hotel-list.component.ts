import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription, timer} from 'rxjs';
import {finalize} from 'rxjs/operators';

// Models
import {HotelModel} from '@app/shared/models/hotel.model';

// Services
import {NavBarService} from '@app/shared/components/nav-bar/nav-bar.service';
import {HotelService} from '@app/shared/services/hotels/hotel.service';

// Zorro modules
import {KeysEnum} from '@app/core/constants/keys.enum';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.scss']
})
export class HotelListComponent implements OnInit, OnDestroy {

  mainLoading = false;

  hotelsSubscription: Subscription;
  iconHeight = 150;
  loading = true;

  hotelsList: HotelModel[] = [];

  @ViewChild('imageBlock') elementView: ElementRef;

  constructor(
    private router: Router,
    private navBarService: NavBarService,
    private hotelsService: HotelService,
    private notificationService: NotificationService) {
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    localStorage.setItem(KeysEnum.GET_HOTEL, null);
    localStorage.setItem(KeysEnum.HOTEL_ID, null);
    this.getHotels();
  }

  getHotels() {
    this.mainLoading = true;
    this.hotelsSubscription = this.hotelsService.list()
      .pipe(finalize(() => timer(1000)))
      .subscribe(data => {
        this.loading = false;
        this.hotelsList = data;

        // Записываю все отели в localStorage
        const hts: {} = data.reduce((acc, hotel) => {
          acc[hotel.id] = hotel;
          return acc;
        }, {});

        localStorage.setItem(KeysEnum.HOTELS, JSON.stringify(hts));
        this.mainLoading = false;
      }, error => {
        this.notificationService.errorMessage('Неизвестная ошибка', 'при загрузке списка отелей');
        this.mainLoading = false;
      });
  }

  ngOnDestroy() {
    if (this.hotelsSubscription) {
      this.hotelsSubscription.unsubscribe();
    }
  }

  openHotelDashboard(hotel) {
    if (!hotel) {
      localStorage.setItem(KeysEnum.GET_HOTEL, null);
      localStorage.setItem(KeysEnum.HOTEL_ID, null);
      this.router.navigate([RoutesEnum.HOTEL, RoutesEnum.CREATE]).finally();
    } else {
      localStorage.setItem(KeysEnum.GET_HOTEL, JSON.stringify(hotel));
      localStorage.setItem(KeysEnum.HOTEL_ID, hotel.id);
      if (hotel.status === 'published') {
        this.router.navigate([RoutesEnum.HOTEL, hotel.id, RoutesEnum.DASHBOARD]).finally();
      }
      if (hotel.status === 'registration') {
        this.router.navigate([RoutesEnum.HOTEL, hotel.id, RoutesEnum.EDIT]).finally();
      }
    }
  }

}
