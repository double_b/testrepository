import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

// Components
import {HotelListComponent} from './hotel-list.component';


const routes: Routes = [
  {
    path: '',
    component: HotelListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelListRoutingModule {
}
