import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HotelListRoutingModule } from './hotel-list-routing.module';
import { HotelListComponent } from './hotel-list.component';
import {NzCardModule, NzGridModule, NzIconModule, NzSkeletonModule} from "ng-zorro-antd";
import {SharedModule} from '@app/core/shared/shared.module';


@NgModule({
  declarations: [HotelListComponent],
    imports: [
        CommonModule,
        HotelListRoutingModule,
        NzCardModule,
        NzGridModule,
        NzIconModule,
        NzSkeletonModule,
        SharedModule
    ]
})
export class HotelListModule { }
