import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing module
import { HotelLayoutRoutingModule } from './hotel-layout-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HotelLayoutRoutingModule
  ]
})
export class HotelLayoutModule { }
