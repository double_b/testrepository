import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';


const routes: Routes = [
  {
    path: RoutesEnum.LIST,
    loadChildren: () => import('./hotel-list/hotel-list.module')
      .then(m => m.HotelListModule)
  },
  {
    path: RoutesEnum.CREATE,
    loadChildren: () => import('./hotel-form/hotel-form.module')
      .then(m => m.HotelFormModule)
  },
  {
    path: `:${RoutesEnum.HOTEL_ID}`,
    children: [
      {
        path: '',
        loadChildren: () => import('../layouts.module').then(m => m.LayoutsModule)
      },
      {
        path: RoutesEnum.EDIT,
        loadChildren: () => import('./hotel-form/hotel-form.module')
          .then(m => m.HotelFormModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelLayoutRoutingModule {
}
