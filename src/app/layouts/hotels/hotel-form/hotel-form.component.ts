import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {FormNavigatorModel} from '@app/layouts/hotels/hotel-form/helper/form-navigator.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {Utils} from '@app/shared/helpers/utils';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-hotel-form',
  templateUrl: './hotel-form.component.html',
  styleUrls: ['./hotel-form.component.scss']
})
export class HotelFormComponent implements OnInit, OnDestroy {

  mode: string = RoutesEnum.CREATE;
  formNavigator: FormNavigatorModel = new FormNavigatorModel();
  hotelSub: Subscription = null;
  status: string = null;

  constructor(private router: Router, public hotelFormService: HotelFormService) {
    this.formNavigator = this.hotelFormService.formNavigator;
  }

  ngOnInit() {
    if (Utils.isExist(localStorage.getItem(KeysEnum.HOTEL_ID))) {
      this.hotelSub = this.hotelFormService.getHotelById().subscribe(res => {
        this.status = res.status;
        if (res.status === 'registration') {
          this.navigateByStep(res.step);
        }
      });
    }
    this.mode = this.router.url.split('/').pop();
  }

  ngOnDestroy() {
    if (this.hotelSub != null) {
      this.hotelSub.unsubscribe();
    }
  }

  getCurrentStepInx(): number {
    return this.hotelFormService.getCurrentStepInx(this.router.url.split('/').pop());
  }

  navigateByStep(step: number, clicked: boolean = false) {
    if (clicked && this.status !== 'published') {
      return;
    }
    switch (step) {
      case 1: {
        this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT]).finally();
        break;
      }
      case 2: {
        this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.SERVICES]).finally();
        break;
      }
      case 3: {
        this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.GALLERY]).finally();
        break;
      }
      case 4: {
        this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.TYPES]).finally();
        break;
      }
      case 5: {
        this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.REGULATIONS]).finally();
        break;
      }
    }
  }

}
