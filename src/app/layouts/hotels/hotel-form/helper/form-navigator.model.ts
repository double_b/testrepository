export class FormNavigatorModel {
  canGoNext: any;
  items: FormNavigatorItemModel[] = [];
}

export class FormNavigatorItemModel {
  path: string;
  title: string;
  step: number;
}
