import {Injectable} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {FormNavigatorModel} from '@app/layouts/hotels/hotel-form/helper/form-navigator.model';
import {HotelService} from '@app/shared/services/hotels/hotel.service';
import {Observable} from 'rxjs';
import {HotelModel} from '@app/shared/models/hotel.model';
import {LanguageModel} from '@app/shared/models/common/language.model';
import {ServiceModel} from '@app/shared/models/common/service.model';
import {ImageModel} from '@app/shared/models/common/image.model';
import {ProfileModel} from '@app/shared/models/profile.model';
import {ProfileApiService} from '@app/shared/services/profile-api/profile-api.service';
import {Utils} from '@app/shared/helpers/utils';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Injectable({
  providedIn: 'root'
})
export class HotelFormService {

  // Навигатор формы регистрации
  formNavigator: FormNavigatorModel = {
    canGoNext: null,
    items: [
      {
        path: '',
        title: 'HOTEL_FORM.COMMON_INFO',
        step: 1
      },
      {
        path: RoutesEnum.SERVICES,
        title: 'HOTEL_FORM.HOTEL_SERVICES',
        step: 2
      },
      {
        path: RoutesEnum.GALLERY,
        title: 'HOTEL_FORM.GALLERY',
        step: 3
      },
      {
        path: RoutesEnum.TYPES,
        title: 'HOTEL_FORM.ROOM_TYPES',
        step: 4
      },
      {
        path: RoutesEnum.REGULATIONS,
        title: 'HOTEL_FORM.REGULATIONS',
        step: 5
      }
    ]
  };

  constructor(private hotelService: HotelService, private profileApiService: ProfileApiService,
              private roomService: RoomsService) {
  }

  public getPropertyId() {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return Number(hotel);
  }

  getHotelById(): Observable<HotelModel> {
    return this.hotelService.getHotelByID(this.getPropertyId());
  }

  createHotel(hotel: HotelModel): Observable<HotelModel> {
    return this.hotelService.registerOverride(hotel);
  }

  updateHotel(hotel: HotelModel): Observable<HotelModel> {
    return this.hotelService.changeOverride(hotel.id, this.convertHotelToServerFormat(hotel));
  }

  getRoomTypes(): Observable<RoomsTypesListModel[]> {
    return this.roomService.getRoomsTypeList(this.getPropertyId());
  }

  convertHotelToServerFormat(hotel: HotelModel): HotelModel {
    if (hotel.status !== 'complete') {
      hotel.status = null;
    }
    if (hotel.languages != null && hotel.languages.length > 0) {
      const tempLanguages: number[] | LanguageModel[] = [];
      hotel.languages.forEach((lang) => {
        if (Utils.isExist(lang.id)) {
          tempLanguages.push(lang.id);
        } else {
          tempLanguages.push(lang);
        }
      });
      hotel.languages = [];
      tempLanguages.forEach(item => {
        hotel.languages.push(item);
      });
    }

    if (hotel.services != null && hotel.services.length > 0) {
      const tempServices: number[] | ServiceModel[] = [];
      hotel.services.forEach((service) => {
        if (Utils.isExist(service.id)) {
          tempServices.push(service.id);
        } else {
          tempServices.push(service);
        }
      });
      hotel.services = [];
      tempServices.forEach(item => {
        hotel.services.push(item);
      });
    }

    if (hotel.images != null && hotel.images.length > 0) {
      const tempImages: number[] | ImageModel[] = [];
      hotel.images.forEach((image) => {
        if (Utils.isExist(image.id)) {
          tempImages.push(image.id);
        } else {
          tempImages.push(image);
        }
      });
      hotel.images = [];
      tempImages.forEach(item => {
        hotel.images.push(item);
      });
    }
    return hotel;
  }

  getCurrentStepInx(path: string): number {
    let result = 0;
    this.formNavigator.items.forEach(item => {
      if (item.path === path) {
        result = item.step - 1;
      }
    });
    return result;
  }

  getProfileData(): Observable<ProfileModel> {
    return this.profileApiService.getProfileData();
  }

  deleteRoomType(roomTypeId: number): Observable<any> {
    return this.roomService.deleteRoomType(this.getPropertyId(), roomTypeId);
  }
}
