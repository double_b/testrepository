import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HotelFormRoutingModule} from './hotel-form-routing.module';
import {HotelFormComponent} from './hotel-form.component';
import {GeneralInfoComponent} from './steps/general-info/general-info.component';
import {SharedModule as OldSharedModule} from '@app/core/shared/shared.module';
import {ServicesComponent} from './steps/services/services.component';
import {GalleryComponent} from './steps/gallery/gallery.component';
import {SharedModule} from '@app/shared/shared.module';
import {RoomTypesComponent} from './steps/room-types/room-types.component';
// tslint:disable-next-line:max-line-length
import {EmptyRoomsPlaceholderComponent} from '@app/layouts/hotels/hotel-form/helper/empty-rooms-placeholder/empty-rooms-placeholder.component';
import {RegulationsComponent} from './steps/regulations/regulations.component';
import {RoomTypeFormModule} from '@app/shared/components/room-type-form/room-type-form.module';


@NgModule({
  declarations: [
    HotelFormComponent,
    GeneralInfoComponent,
    ServicesComponent,
    GalleryComponent,
    RoomTypesComponent,
    EmptyRoomsPlaceholderComponent,
    RegulationsComponent],
  imports: [
    CommonModule,
    HotelFormRoutingModule,
    OldSharedModule,
    SharedModule,
    RoomTypeFormModule
  ]
})
export class HotelFormModule {
}
