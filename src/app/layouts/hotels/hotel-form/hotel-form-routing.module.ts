import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HotelFormComponent} from '@app/layouts/hotels/hotel-form/hotel-form.component';
import {GeneralInfoComponent} from '@app/layouts/hotels/hotel-form/steps/general-info/general-info.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ServicesComponent} from '@app/layouts/hotels/hotel-form/steps/services/services.component';
import {GalleryComponent} from '@app/layouts/hotels/hotel-form/steps/gallery/gallery.component';
import {RoomTypesComponent} from '@app/layouts/hotels/hotel-form/steps/room-types/room-types.component';
import {RegulationsComponent} from '@app/layouts/hotels/hotel-form/steps/regulations/regulations.component';


const routes: Routes = [
  {
    path: '',
    component: HotelFormComponent,
    children: [
      {
        path: '',
        component: GeneralInfoComponent
      },
      {
        path: RoutesEnum.SERVICES,
        component: ServicesComponent
      },
      {
        path: RoutesEnum.GALLERY,
        component: GalleryComponent
      },
      {
        path: RoutesEnum.TYPES,
        component: RoomTypesComponent
      },
      {
        path: RoutesEnum.REGULATIONS,
        component: RegulationsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelFormRoutingModule {
}
