import {Component, OnDestroy, OnInit, ElementRef, ViewChild} from '@angular/core';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {Utils} from '@app/shared/helpers/utils';
import {forkJoin, Subscription} from 'rxjs';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {PropertyTypesModel} from '@app/shared/models/property-types.model';
import {CityModel} from '@app/shared/models/common/city.model';
import {CountryModel} from '@app/shared/models/common/country.model';
import {GeneralValidateModel} from '@app/layouts/hotels/hotel-form/steps/general-info/general-validate.model';
import {HotelModel} from '@app/shared/models/hotel.model';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.scss']
})
export class GeneralInfoComponent implements OnInit, OnDestroy {
  stars = [0, 1, 2, 3, 4, 5];
  isLoading = false;
  title = 'HOTEL_FORM.CREATING_HOTEL';
  hotelId = null;
  hotel = new HotelModel();

  hotelSub: Subscription = null;
  hotelSaveSub: Subscription = null;
  profileDataSub: Subscription = null;
  getExtrasSubscription: Subscription;

  propertyTypes: PropertyTypesModel[] = [];
  cities: CityModel[] = [];
  filteredCities: CityModel[] = [];
  countries: CountryModel[] = [];
  contactPerson: string = null;
  phone: string = null;
  hotelName: string = null;

  validateState = new GeneralValidateModel();

  @ViewChild('cardContainer')
  cardContainer: ElementRef;

  constructor(private router: Router,
              private hotelFormService: HotelFormService,
              private extrasService: ExtrasService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.profileDataSub = this.hotelFormService.getProfileData().subscribe(profileData => {
      this.contactPerson = `${profileData.first_name} ${profileData.last_name}`;
      this.phone = profileData.phone;
      this.hotel.name = profileData.notes;
      this.getExtras();
      if (Utils.isExist(localStorage.getItem(KeysEnum.HOTEL_ID))) {
        this.isLoading = true;
        this.hotelId = localStorage.getItem(KeysEnum.HOTEL_ID);
        this.title = 'HOTEL_FORM.HOTEL_SET';
        this.hotelSub = this.hotelFormService.getHotelById().subscribe(res => {
          this.contactPerson = res.contact_person;
          this.phone = res.phone;
          this.hotel = res;
          this.getExtras();
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.hotelSub != null) {
      this.hotelSub.unsubscribe();
    }
    if (this.getExtrasSubscription != null) {
      this.getExtrasSubscription.unsubscribe();
    }
    if (this.profileDataSub != null) {
      this.profileDataSub.unsubscribe();
    }
    if (this.hotelSaveSub != null) {
      this.hotelSaveSub.unsubscribe();
    }
  }

  submit() {
    if (!this.checkValid()) {
      this.cardContainer.nativeElement.scrollTo(0, 0);
      return;
    }
    this.isLoading = true;
    this.hotel.contact_person = this.contactPerson;
    this.hotel.phone = this.phone;
    if (Utils.isExist(this.hotel.city.name)) {
      this.hotel.city_id = this.cities.find(item => item.name === this.hotel.city.name).id;
    }
    this.hotel.step = this.hotel.step > 1 ? this.hotel.step : 2;
    if (this.hotelId === null) {
      this.hotelSaveSub = this.hotelFormService.createHotel(this.hotel).subscribe(res => this.onSuccessResponse(res),
        error => this.onErrorResponse());
    } else {
      this.hotelSaveSub = this.hotelFormService.updateHotel(this.hotel).subscribe(res => this.onSuccessResponse(res),
        error => this.onErrorResponse());
    }
  }

  onSuccessResponse(res: HotelModel) {
    this.isLoading = false;
    this.notificationService.successMessage('Данные успешно сохранены');
    localStorage.setItem(KeysEnum.HOTEL_ID, res.id.toString());
    this.router.navigate([RoutesEnum.HOTEL, res.id.toString(), RoutesEnum.EDIT, RoutesEnum.SERVICES]).finally();
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

  checkValid(): boolean {
    let isValid = true;
    if (!Utils.isExist(this.hotel.city.name)) {
      isValid = false;
      this.validateState.invalidCity();
    }
    if (!Utils.isExist(this.hotel.property_type_id)) {
      isValid = false;
      this.validateState.invalidPropertyType();
    }
    if (!Utils.isExist(this.hotel.name)) {
      isValid = false;
      this.validateState.invalidName();
    }
    if (!Utils.isExist(this.hotel.address)) {
      isValid = false;
      this.validateState.invalidAddress();
    }
    if (!Utils.isExist(this.hotel.stars)) {
      isValid = false;
      this.validateState.invalidStars();
    }
    if (!Utils.isExist(this.contactPerson)) {
      isValid = false;
      this.validateState.invalidContactPerson();
    }
    if (!Utils.isExist(this.phone)) {
      isValid = false;
      this.validateState.invalidPhone();
    }
    return isValid;
  }

  getExtras() {
    // Получаю типы, города и страны
    this.getExtrasSubscription = forkJoin([
      this.extrasService.propertiesType(),
      this.extrasService.cities(),
      this.extrasService.countries()
    ]).subscribe(res => {
      // Список типов объектов
      this.propertyTypes = res[0];
      // Список городов
      this.cities = res[1];
      this.hotel.city = this.cities.find(el => {
        if (this.hotel.city_id) {
          return el.id === this.hotel.city_id;
        } else {
          return el.id === 1;
        }
      });
      // // Список стран
      this.countries = res[2];
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  cityFilter() {
    this.validateState.invalidCity(this.hotel.city.name);
    if (!this.hotel.city.name) {
      this.filteredCities = this.cities;
    } else {
      this.filteredCities = this.cities
        .filter(option => option.name.toLowerCase()
          .indexOf((this.hotel.city.name || '')
            .toLowerCase()) === 0);
    }
  }

}
