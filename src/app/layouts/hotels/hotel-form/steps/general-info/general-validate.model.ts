export class GeneralValidateModel {
  city = '';
  propertyType = '';
  name = '';
  address = '';
  stars = '';
  contactPerson = '';
  phone = '';
  geoCodeLat = '';
  geoCodeLang = '';

  invalidCity(value: string = null) {
    this.city = value == null || '' ? 'error' : '';
  }

  invalidPropertyType(value: number = null) {
    this.propertyType = value == null || '' ? 'error' : '';
  }

  invalidName(value: string = null) {
    this.name = value == null || '' ? 'error' : '';
  }

  invalidAddress(value: string = null) {
    this.address = value == null || '' ? 'error' : '';
  }

  invalidStars(value: number = null) {
    this.stars = value == null || '' ? 'error' : '';
  }

  invalidContactPerson(value: string = null) {
    this.contactPerson = value == null || '' ? 'error' : '';
  }

  invalidPhone(value: string = null) {
    this.phone = value == null || '' ? 'error' : '';
  }

  invalidGeoLat(value: string = null) {
    this.geoCodeLat = value == null || '' ? 'error' : '';
  }

  invalidGeoLang(value: string = null) {
    this.geoCodeLang = value == null || '' ? 'error' : '';
  }
}
