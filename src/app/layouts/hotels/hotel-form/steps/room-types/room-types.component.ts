import {Component, OnDestroy, OnInit} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {Router} from '@angular/router';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {Subscription} from 'rxjs';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {HotelModel} from '@app/shared/models/hotel.model';

@Component({
  selector: 'app-room-types',
  templateUrl: './room-types.component.html',
  styleUrls: ['./room-types.component.scss']
})
export class RoomTypesComponent implements OnInit, OnDestroy {
  isVisible = false;
  isLoading = false;
  roomsTypeList: RoomsTypesListModel[] = [];
  roomTypesSub: Subscription = null;
  hotelSub: Subscription = null;
  hotelSaveSub: Subscription = null;
  hotel = new HotelModel();
  deleteSubscription: Subscription = null;

  roomTypeModalTitle = 'ROOM_TYPE.ADD_ROOM';

  roomTypeIdForEdit: number = null;

  constructor(private extrasService: ExtrasService,
              private router: Router,
              public hotelFormService: HotelFormService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.getRoomTypes();
  }

  ngOnDestroy() {
    if (this.roomTypesSub != null) {
      this.roomTypesSub.unsubscribe();
    }
    if (this.deleteSubscription != null) {
      this.deleteSubscription.unsubscribe();
    }
    if (this.hotelSub != null) {
      this.hotelSub.unsubscribe();
    }
    if (this.hotelSaveSub != null) {
      this.hotelSaveSub.unsubscribe();
    }
  }

  getRoomTypes() {
    this.isLoading = true;
    this.roomTypesSub = this.hotelFormService.getRoomTypes().subscribe(res => {
      this.roomsTypeList = res;
      this.hotelSub = this.hotelFormService.getHotelById().subscribe(hotel => {
        this.isLoading = false;
        this.hotel = hotel;
      }, error => this.onErrorResponse());
    }, error => this.onErrorResponse());
  }

  submit() {
    this.isLoading = true;
    this.hotel.step = this.hotel.step > 4 ? this.hotel.step : 5;
    this.hotelSaveSub = this.hotelFormService.updateHotel(this.hotel).subscribe(res => {
      this.isLoading = false;
      this.notificationService.successMessage('Данные успешно сохранены');
      this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.REGULATIONS]).finally();
    }, error => this.onErrorResponse());
  }

  onBack() {
    this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.GALLERY]).finally();
  }

  // Когда закрываю модалку добавления комнат сбрасываю данные
  onCloseModalEvent() {
    this.isVisible = false;
    this.roomTypeIdForEdit = null;
    this.getRoomTypes();
  }

  // Открываю номер для редактирывания
  openRoomTypeForm(roomTypeId: number = null) {
    this.roomTypeIdForEdit = roomTypeId;
    this.roomTypeModalTitle = roomTypeId === null ? 'ROOM_TYPE.ADD_ROOM' : 'ROOM_TYPE.EDIT_ROOM';
    this.isVisible = true;
  }

  // Удаляю тип номеров
  deleteItemFromTableHandler(item) {
    this.isLoading = true;
    this.deleteSubscription = this.hotelFormService.deleteRoomType(item.id)
      .subscribe(res => {
          this.getRoomTypes();
          this.notificationService.successMessage('Тип комнаты успешно удалён');
        },
        error => () => this.onErrorResponse());
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }
}
