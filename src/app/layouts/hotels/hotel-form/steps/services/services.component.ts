import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {forkJoin, Subscription} from 'rxjs';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {LanguageModel} from '@app/shared/models/common/language.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ServiceGroupModel} from '@app/shared/models/common/service-group.model';
import {HotelModel} from '@app/shared/models/hotel.model';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {
  isLoading = false;

  hotel = new HotelModel();
  languages: LanguageModel[] = [];
  serviceGroups: ServiceGroupModel[] = [];

  extrasSubscription: Subscription;
  hotelSub: Subscription = null;
  hotelSaveSub: Subscription = null;

  langError = '';
  @ViewChild('cardContainer')
  cardContainer: ElementRef;

  constructor(private extrasService: ExtrasService,
              private router: Router,
              private hotelFormService: HotelFormService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    window.history.pushState({}, '', `${RoutesEnum.HOTEL}/${this.hotelFormService.getPropertyId()}/${RoutesEnum.EDIT}`);
    // tslint:disable-next-line:max-line-length
    window.history.pushState({}, '', `${RoutesEnum.HOTEL}/${this.hotelFormService.getPropertyId()}/${RoutesEnum.EDIT}/${RoutesEnum.SERVICES}`);

    this.isLoading = true;
    this.extrasSubscription = forkJoin([
      this.extrasService.servicesGroup('?with=services'),
      this.extrasService.getLanguages()
    ]).subscribe(res => {
      res[1].forEach(lang => {
        if (lang.active === 1) {
          this.languages.push(lang);
        }
      });
      res[0].forEach(serviceGroup => {
        if (serviceGroup.active === 1 && serviceGroup.services.length > 0) {
          this.serviceGroups.push(serviceGroup);
        }
      });
      this.hotelSub = this.hotelFormService.getHotelById().subscribe(hotel => {
        hotel.languages = this.convertLangFromObjToNumber(hotel);
        this.serviceGroups.forEach(serviceGroup => {
          hotel.services.forEach(service => {
            if (service.serviceGroup.id === serviceGroup.id) {
              serviceGroup.services.find(item => item.id === service.id).checked = true;
            }
          });
        });
        this.hotel = hotel;
        this.isLoading = false;
      }, error => this.onErrorResponse());
    }, error => this.onErrorResponse());
  }

  ngOnDestroy() {
    if (this.hotelSub) {
      this.hotelSub.unsubscribe();
    }
    if (this.extrasSubscription) {
      this.extrasSubscription.unsubscribe();
    }
    if (this.hotelSaveSub != null) {
      this.hotelSaveSub.unsubscribe();
    }
  }

  submit() {
    if (!this.checkValid()) {
      this.cardContainer.nativeElement.scrollTo(0, 0);
      return;
    }
    this.hotel.services = [];
    this.serviceGroups.forEach(group => {
      group.services.forEach(service => {
        if (service.checked === true) {
          // @ts-ignore
          this.hotel.services.push(service.id);
        }
      });
    });
    this.hotel.step = this.hotel.step > 2 ? this.hotel.step : 3;
    this.hotelSaveSub = this.hotelFormService.updateHotel(this.hotel).subscribe(res => {
      this.isLoading = false;
      this.notificationService.successMessage('Данные успешно сохранены');
      this.router.navigate([RoutesEnum.HOTEL, this.hotel.id, RoutesEnum.EDIT, RoutesEnum.GALLERY]).finally();
    }, error => this.onErrorResponse());
    // this.router.navigate([RoutesEnum.HOTEL, this.hotelId, RoutesEnum.EDIT, RoutesEnum.GALLERY]).finally();
  }

  checkValid(): boolean {
    let result = true;
    if (this.hotel.languages.length === 0) {
      this.langError = 'error';
      result = false;
    }
    let serviceUnChecked = true;
    for (const group of this.serviceGroups) {
      if (group.services.some(service => service.checked)) {
        serviceUnChecked = false;
        break;
      }
    }
    if (serviceUnChecked) {
      this.notificationService.errorMessage('Пожалуйста', 'Отметьте минимум 1 услугу');
      result = false;
    }
    return result;
  }

  resetLangError() {
    if (this.hotel.languages.length > 0) {
      this.langError = '';
    }
  }

  convertLangFromObjToNumber(hotel: HotelModel): number[] | LanguageModel[] {
    const tempLanguages: number[] | LanguageModel[] = [];
    if (hotel.languages != null && hotel.languages.length > 0) {
      hotel.languages.forEach((lang) => {
        tempLanguages.push(lang.id);
      });
      hotel.languages = [];
      tempLanguages.forEach(item => {
        hotel.languages.push(item);
      });
    }
    return tempLanguages;
  }

  onBack() {
    this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT], {replaceUrl: true}).finally();
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

}
