import {Component, OnDestroy, OnInit} from '@angular/core';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {Router} from '@angular/router';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {HotelModel} from '@app/shared/models/hotel.model';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnDestroy {
  isLoading = false;
  hotel = new HotelModel();
  selectedImages = [];
  hotelSaveSub: Subscription = null;

  constructor(private extrasService: ExtrasService,
              private router: Router,
              public hotelFormService: HotelFormService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.hotelFormService.getHotelById().subscribe(res => {
      res.images.forEach(image => {
        this.selectedImages.push(image.id);
      });
      this.hotel = res;
    });
  }

  ngOnDestroy() {
    if (this.hotelSaveSub != null) {
      this.hotelSaveSub.unsubscribe();
    }
  }

  submit() {
    this.isLoading = true;
    this.hotel.images = [];
    this.hotel.images.push(...this.selectedImages);
    this.hotel.step = this.hotel.step > 3 ? this.hotel.step : 4;
    this.hotelSaveSub = this.hotelFormService.updateHotel(this.hotel).subscribe(res => {
      this.isLoading = false;
      this.notificationService.successMessage('Данные успешно сохранены');
      this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.TYPES]).finally();
    }, error => this.onErrorResponse());
  }

  onGallerySubmit(images: number[]) {
    const unique = new Set(images);
    this.selectedImages = [];
    this.selectedImages.push(...unique);
  }

  onBack() {
    this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.SERVICES]).finally();
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

}
