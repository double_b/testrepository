import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {Router} from '@angular/router';
import {HotelFormService} from '@app/layouts/hotels/hotel-form/helper/hotel-form.service';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {HotelModel} from '@app/shared/models/hotel.model';
import {Subscription} from 'rxjs';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {TimeModel} from '@app/layouts/hotels/hotel-form/steps/regulations/time.model';
import {Utils} from '@app/shared/helpers/utils';

@Component({
  selector: 'app-regulations',
  templateUrl: './regulations.component.html',
  styleUrls: ['./regulations.component.scss']
})
export class RegulationsComponent implements OnInit, OnDestroy {
  isLoading = false;
  hotel = new HotelModel();
  hotelSub: Subscription = null;
  hotelSaveSub: Subscription = null;
  time = new TimeModel();
  @ViewChild('cardContainer')
  cardContainer: ElementRef;
  constructor(private extrasService: ExtrasService,
              private router: Router,
              private hotelFormService: HotelFormService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.hotelSub = this.hotelFormService.getHotelById().subscribe(res => {
      this.hotel = res;
      if (Utils.isExist(this.hotel.checkin_start)) {
        this.time.checkInStart = this.getAsDate(this.hotel.checkin_start);
      }
      if (Utils.isExist(this.hotel.checkin_end)) {
        this.time.checkInEnd = this.getAsDate(this.hotel.checkin_end);
      }
      if (Utils.isExist(this.hotel.checkout_start)) {
        this.time.checkOutStart = this.getAsDate(this.hotel.checkout_start);
      }
      if (Utils.isExist(this.hotel.checkout_end)) {
        this.time.checkOutEnd = this.getAsDate(this.hotel.checkout_end);
      }
      this.isLoading = false;
    }, error => this.onErrorResponse());
  }

  ngOnDestroy() {
    if (this.hotelSub) {
      this.hotelSub.unsubscribe();
    }
    if (this.hotelSaveSub != null) {
      this.hotelSaveSub.unsubscribe();
    }
  }

  submit() {
    if (!this.checkValid()) {
      this.cardContainer.nativeElement.scrollTo(0, 0);
      return;
    }
    this.isLoading = true;
    this.hotel.step = 0;
    if (this.hotel.status === 'registration') {
      this.hotel.status = 'complete';
    }
    this.hotel.checkin_start = this.getTime(this.time.checkInStart);
    this.hotel.checkin_end = this.getTime(this.time.checkInEnd);
    this.hotel.checkout_start = this.getTime(this.time.checkOutStart);
    this.hotel.checkout_end = this.getTime(this.time.checkOutEnd);
    this.hotelSaveSub = this.hotelFormService.updateHotel(this.hotel).subscribe(res => {
      this.isLoading = false;
      this.notificationService.successMessage('Данные успешно сохранены');
      localStorage.setItem(KeysEnum.GET_HOTEL, JSON.stringify(this.hotel));
      this.router.navigate([RoutesEnum.HOTEL, this.hotel.id, RoutesEnum.DASHBOARD]).finally();
    }, error => {
      if (Utils.isExist(error.error.errors.images)) {
        this.isLoading = false;
        this.notificationService.errorMessage('Пожалуйста, вернитесь к шагу Фотографии',
          'и добавьте минимум 1 фото для окончания регистрации');
      } else {
        this.onErrorResponse();
      }
    });
  }

  checkValid(): boolean {
    let result = true;

    if (!Utils.isExist(this.time.checkInStart)) {
      this.time.invalidCheckInStart();
      result = false;
    }

    if (!Utils.isExist(this.time.checkInEnd)) {
      this.time.invalidCheckInEnd();
      result = false;
    }

    if (!Utils.isExist(this.time.checkOutStart)) {
      this.time.invalidCheckOutStart();
      result = false;
    }

    if (!Utils.isExist(this.time.checkOutEnd)) {
      this.time.invalidCheckOutEnd();
      result = false;
    }

    return result;
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

  onBack() {
    this.router.navigate([RoutesEnum.HOTEL, this.hotelFormService.getPropertyId(), RoutesEnum.EDIT, RoutesEnum.TYPES]).finally();
  }

  getAsDate(time: string): Date {
    return new Date(0, 0, 0,
      Number(time.split(':')[0]), Number(time.split(':')[1]));
  }

  getTime(date: Date): string {
    let hour = String(date.getHours());
    let minute = String(date.getMinutes());
    if (hour.length === 1) {
      hour = '0' + hour;
    }
    if (minute.length === 1) {
      minute = '0' + minute;
    }
    return `${hour}:${minute}`;
  }
}
