export class TimeModel {
  checkInStart: Date = new Date();
  checkInEnd: Date = new Date();
  checkOutStart: Date = new Date();
  checkOutEnd: Date = new Date();

  checkInStartError = '';
  checkInEndError = '';
  checkOutStartError = '';
  checkOutEndError = '';

  invalidCheckInStart() {
    this.checkInStartError = this.checkInStart == null || '' ? 'error' : '';
  }

  invalidCheckInEnd() {
    this.checkInEndError = this.checkInEnd == null || '' ? 'error' : '';
  }

  invalidCheckOutStart() {
    this.checkOutStartError = this.checkOutStart == null || '' ? 'error' : '';
  }

  invalidCheckOutEnd() {
    this.checkOutEndError = this.checkOutEnd == null || '' ? 'error' : '';
  }
}
