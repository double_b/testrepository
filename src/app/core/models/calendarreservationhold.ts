export interface CalendarReservationHold {
  id: number;
  reservation_id: number;
  expire_date: string|null;
  guest_first_name: string|null;
  guest_last_name: string|null;
  guest_email: string|null;
  guest_phone: string|null;
  hold_duration: string|null;
  duration_format: string|null;
  checkin: string|null;
  checkout: string|null;
  room_ids: string|null;
}
