export class LocationModel {
  visible: boolean;
  lat: number;
  lng: number;
  draggable: boolean;
  label: string;
}
