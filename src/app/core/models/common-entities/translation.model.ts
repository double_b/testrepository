export class TranslationModel {
  locale?: string;
  name?: string;
  description?: string;
  address?: string;
}
