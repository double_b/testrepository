export interface IGuestData {
  first_name: string;
  last_name: string;
  email: string;
  phone: string | number;
  country_id: number | null;
  checkin_hours?: string | number;
  checkin_minutes?: string | number;
  room_number?: string | number;
  birthday?: string | Date;
  gender?: number;
  inn?: string | number;
  company_name?: string;
  company_number?: string | number;
  city_id?: number | null;
  guest_country_id?: number;
  region?: string;
  address?: string;
  address2?: string;
  zip_code?: string | number;
  document_type_id?: string | number;
  document_number?: string | number;
  document_issued_at?: string | Date;
  document_country_id?: number;
  document_expired_at?: string | Date;
}

export interface IGuestFlowData {
  guestFormData: IGuestData;
  existingGuestId?: number;
}
