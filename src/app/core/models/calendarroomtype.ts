import {CalendarReservation} from "@src/app/core/models/calendarreservation";
import {Calendarroom} from "@src/app/core/models/calendarroom";

export interface Calendarroomtype {
  room_type_id: number;
  short_name: string;
  prices: any[];
  availability: number[];
  rooms: Calendarroom[];
  not_assigned: CalendarReservation[];
  expanded: boolean | null;
  isExpanded: any;
}
