import {CalendarReservation} from "@src/app/core/models/calendarreservation";

export interface Calendarroom {
  name: string;
  room_id: number;
  isSelectedRoom: boolean | void;
  condition: any;
  isSelectedCell: boolean;
  bookingByDate: CalendarReservation;
  className: string;
  reservations: CalendarReservation[];
}
