import {ObjectType} from './hotelier.model';
import {ImageModule} from './commun';

export interface ReservationModel {
  active: number;
  address: string;
  checkin_end: string;
  checkin_start: string;
  checkout_end: string;
  checkout_start: string;
  city: ReservationCityModel;
  city_id: number;
  contact_person: string;
  created_at: string;
  deleted_at: string;
  description: string;
  geocode_lat: number;
  geocode_lng: number;
  id: number;
  image: ImageModule;
  images: any[];
  languages: any[];
  name: string;
  phone: string;
  phone2: string;
  propertyType: ObjectType;
  property_type_id: string;
  rooms_count: number;
  services: any[];
  site: string;
  stars: string;
  status: string;
  step: number;
  tempTranslations: any[];
  translations: ReservationTranslationModel[];
  updated_at: string;
  user_id: number;
  zip_code: number;

}

export interface ReservationCityModel {
  active: number;
  country: ReservationCountryModel;
  country_id: number;
  id: number;
  name: string;
}
export interface ReservationCountryModel {
  active: number;
  code: string;
  id: number;
  iso3: string;
  name: string;
  numcode: string;
}

export interface ReservationTranslationModel {
  address: string;
  description: string;
  locale: string;
  name: string;
}

export interface ReservationLanguageModel {
  active: number;
  id: number;
  name: string;
  sort_order: number;
}

export interface ReservationServiceModel {
  active: number;
  id: number;
  name: string;
  selected: boolean;
  service_group_id: number;
}

export interface ReservationServiceGroupModel {
  active: number;
  id: number;
  name: string;
  sort_order: number;
  services: ReservationServiceModel[];
}

export interface ReservationBuilding {
  id: number;
  name: string;
  property_id: number;
  sort_order: number;
  rooms: any[];
  open: any;
}
