export interface MapMarker {
  lat: number;
  visible: boolean;
  lng: number;
  label?: string;
  draggable: boolean;
}

export interface ImageModule {
  big: string;
  created_at: string;
  id: number;
  image_name_id: any;
  medium: string;
  name: any;
  original: string;
  property_id: number;
  small: string;
  thumb: string;
  updated_at: string;
  checked: boolean;
}

export interface ImageName {
  id: number;
  name: string;
  sort_order: number;
}

export interface MenuItem {
  title: string;
  disabled: boolean;
  active?: boolean;
  path?: string;
  navigable: boolean;
  subMenu?: MenuItem[];
}

export const countries = [
  {
    name: 'Uzbekistan',
    dial_code: '+998',
    code: 'UZ'
  }
];

export interface TariffModule {
  breakfast: number;
  dinner: number;
  id: number;
  lunch: number;
  min_advance_res: number;
  min_stay: number;
  name: string;
  property_id: number;
  room_types: any[];
}

export interface LocationGoogle {
  lat: number;
  lng: number;
  viewport?: object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: MapMarker;
}
