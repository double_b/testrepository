import {CalendarReservation} from "@src/app/core/models/calendarreservation";
import {Calendarroomtype} from "@src/app/core/models/calendarroomtype";
import {Calendarroom} from "@src/app/core/models/calendarroom";

export interface CalendarReservationInfoData {
  booking: CalendarReservation;
  roomType: Calendarroomtype;
  room: Calendarroom;
  guestStatus: string|null;
  statusForShow: string;
}
