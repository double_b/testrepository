import { Room } from './room.model';

export interface Rates {
    data: Rate[];
}

export interface Rate {
    breakfast: number;
    dinner: number;
    id: number;
    lunch: number;
    min_advance_res: number;
    min_stay: number;
    name: string;
    property_id: number;
    room_types: Room[];
}
