export interface User {
  active: number;
  avatar: string;
  birthday: string;
  created_at: string;
  email: string;
  email_verified: number;
  first_name: string;
  gender: string;
  id: number;
  last_name: string;
  middle_name: string;
  notes: string;
  phone: number;
  phone_verified: number;
  role: string;
  sms_phone: string;
  updated_at: string;
}
