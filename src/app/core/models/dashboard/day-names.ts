export class DayNames {
  private static days: string[] = [
    'DAYS.SHORT.MONDAY',
    'DAYS.SHORT.TUESDAY',
    'DAYS.SHORT.WEDNESDAY',
    'DAYS.SHORT.THURSDAY',
    'DAYS.SHORT.FRIDAY',
    'DAYS.SHORT.SATURDAY',
    'DAYS.SHORT.SUNDAY'
  ];

  public static getDayName(dayNumber: number): string {
    if (dayNumber == null || dayNumber > this.days.length) {
      return null;
    } else if (dayNumber === 0) {
      return this.days[6];
    } else {
      return this.days[dayNumber - 1];
    }
  }
}
