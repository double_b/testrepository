export class ForecastAvailableModel {
  dates: DateLabel[] = [];
  rooms: ForecastRoom[] = [];
}

export class DateLabel {
  label: string;
  date: Date = new Date();
  value?: number;
}

export class ForecastRoom {
  label: string;
  values: ValueByDate[] = [];
}

export class ValueByDate {
  date: Date;
  value: number;
}
