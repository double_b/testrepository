export class ForecastChartModel {
  series: Series[] = [];
  dates: string[];
}

export class Series {
  name: string;
  data: any[] = [];
}
