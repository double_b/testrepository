export class ForecastHeaderModel {
  loaded: number;
  profit: number;
  currency: string;
}
