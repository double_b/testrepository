export class OverAllStatisticModel {
  arrived: number;
  left: number;
  lives: number;
  load_on_month: number;
  shouldArrive: number;
  shouldLeft: number;
  shouldLive: number;
  arrivePercent?: number;
  leftPercent?: number;
  livePercent?: number;
  loadOnMonthPercent?: number;
}
