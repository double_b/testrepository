export class ForecastResponseModel {
  loaded_by_dates: DateValue[];
  loaded_average: number;
  earned: number;
  currency: string;
  room_types: Room[];
}

class DateValue {
  date: string;
  value: number;
}

class Room {
  room_type_short_name: string;
  availability: any[];
}
