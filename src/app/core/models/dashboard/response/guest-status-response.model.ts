export class GuestStatusResponseModel {
  reservation_room_id: number;
  reservation_id: number;
  arrival_time: number;
  checkin: string;
  checkout: string;
  type: string;
  sum: number;
  guestsCount: number;
  source: string;
  is_group: boolean;
  moved_from: string;
  moved_to: string;
  notes_count: number;
  guest: {
    first_name: string;
    last_name: string;
  };
  guest_arrived_at: string;
  guest_left_at: string;
  reservation_status: string;
}
