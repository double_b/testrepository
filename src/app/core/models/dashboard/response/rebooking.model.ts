import {ReservationRooms} from '@src/app/core/models/dashboard/response/reservation-overview.model';

export class RebookingModel {
  overload: number;
  reservation_rooms: any;
}

export class RebookingReservationModel {
  data: ReservationRooms[] = [];
}
