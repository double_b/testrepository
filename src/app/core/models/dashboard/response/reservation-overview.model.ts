export class ReservationRooms {
  id: number;
  order: number;
  reservation_id: number;
  room: any;
  room_id: number;
  adults: number;
  children: number;
  nights: string;
  sum: number;
  guest_arrived_at: string;
  guest_left_at: string;
  checkin?: string;
  checkout?: string;
  reservation: Reservation;
  room_type: RoomType;
  status: string;
}

class Reservation {
  status: string;
  notes_count: number;
  currency: string;
  guest: Guest;
}

class RoomType {
  room_type_short_name: string;
}

class Guest {
  first_name: string;
  last_name: string;
  middle_name: string;
}
