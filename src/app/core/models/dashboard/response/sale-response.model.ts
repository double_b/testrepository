export class SaleResponceModel {
  room_nights: number;
  reservations_count: number;
  revenue: number;
  reservations: Reservation[];
}

export class CancellationResponseModel {
  reservations_count: number;
  revenue: number;
  reservations: DataReservation;
}

export class DataReservation {
  data: Reservation[] = [];
  total: number;
}

export class Reservation {
  id: number;
  order: number;
  channel: string;
  source: any;
  total_guests: number;
  nights: number;
  checkin: Date;
  checkout: Date;
  total_rooms: number;
  status: string;
  reservation_rooms: any;
  total_sum: number;
  currency: string;
  guest: Guest;
  notes_count: number;
}

class Guest {
  first_name: string;
  last_name: string;
  middle_name: string;
}
