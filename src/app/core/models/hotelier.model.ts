export interface ObjectType {
  active: number;
  id: number;
  name: string;
}

export interface CityType {
  active: number;
  country_id: number;
  id: number;
  name: string;
}