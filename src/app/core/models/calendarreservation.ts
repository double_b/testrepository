import {CalendarReservationHold} from "@src/app/core/models/calendarreservationhold";

export interface CalendarReservation {
  arrival_time: string|null;
  reservation_room_id: number;
  reservation_id: number;
  checkin: string;
  checkout: string;
  type: string;
  sum: number|null;
  debt: number|null;
  guestsCount: 0;
  source: any;
  is_group: boolean;
  moved_from: any;
  moved_to: any;
  notes_count: number;
  guest: {
    first_name: string;
    last_name: string;
  };
  hold: CalendarReservationHold | null;
  visible: boolean | null;
  guest_arrived_at: string|null;
  guest_left_at: string|null;
  guestStatus: string|null;
  reason: string|null;
}
