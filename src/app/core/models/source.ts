export interface Sources {
    Array: Source;
}

export interface Source {
  active: number;
  default: number;
  id: number;
  name: string;
  overbooking: number;
  sort_order: number;
}


