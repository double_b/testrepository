import {Calendarroomtype} from '@src/app/core/models/calendarroomtype';

export interface Calendardata {
  dates: string[];
  rooms_booked: string[];
  total_availability: number[];
  not_assigned: number[];
  room_types: Calendarroomtype[];
}
