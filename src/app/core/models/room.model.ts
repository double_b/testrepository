export interface RoomTypes {
  data: RoomModel[];
  links: any;
  meta: any;
}

export interface RoomModel {
  active: number;
  altBeds: RoomBedsArray[];
  area: number;
  created_at: string;
  deleted_at: string;
  facilities: any;
  id: number;
  image: any;
  images: any;
  mainBeds: RoomBedsArray[];
  max_children: number;
  max_persons: number;
  max_adults: number;
  name: string;
  property_id: number;
  quantity: number;
  room_name_id: number;
  smoking: number;
  standard_price: number;
  updated_at: string;
  selected: boolean;
}

export interface RoomReservationGroupName {
  active: number;
  id: number;
  name: string;
  sort_order: number;
}

export interface RoomReservationName {
  active: number;
  id: number;
  name: string;
  room_name_group_id: number;
  sort_order: number;
}


export interface RoomReservationFacilityName {
  active: number;
  facility_group_id: number;
  id: number;
  name: string;
  sort_order: number;
}

export interface RoomReservationFacilityGroupName {
  active: number;
  id: number;
  name: string;
  sort_order: number;
}

export interface RoomBedsArray {
  id: number;
  count: number;
}


export interface Room {
  active: number;
  area: number;
  created_at: string;
  deleted_at: string;
  facilities: any;
  id: number;
  max_children: number;
  max_persons: number;
  max_adults: number;
  name: string;
  property_id: number;
  quantity: number;
  room_name_id: number;
  smoking: number;
  standard_price: number;
  updated_at: string;
}
