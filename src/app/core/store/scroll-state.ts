export class ScrollState {
  press: null;
  table: null;
  canScroll: false;
  tableY: 0;
  prevX: 0;
  clientX: 0;
  canUpdate: false;
}
