export class CalendarState {
  selectedDate: null | Date;
  selectedBooking: any;
  selectedRoom: null | string;
  selectedCell: null | string;
  openedReservCount: number;
  searchRoomQuery: string;
  isFixedTopBar: boolean = false;
  tableScrollLeft: number;
  expandedRoomTypes: string[] = [];
  newReservationData: any = {
    from: null,
    to: null,
    room: null,
    visible: false,
    width: null,
  };
  modal: any = {};
  count: number = 0;
}
