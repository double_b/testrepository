import {Injectable} from '@angular/core';
import {CalendarState} from './calendar-state';
import {Store} from '../services/store/store.service';

@Injectable()
export class CalendarStore extends Store<CalendarState> {
  constructor() {
    super(new CalendarState());
  }
}
