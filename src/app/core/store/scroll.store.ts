import {Injectable} from '@angular/core';
import {ScrollState} from './scroll-state';
import {Store} from '../services/store/store.service';

@Injectable()
export class ScrollStore extends Store<ScrollState> {
  constructor() {
    super(new ScrollState());
  }
}
