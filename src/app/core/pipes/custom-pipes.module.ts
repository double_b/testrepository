import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Pipes
import {FormatPricePipe} from './format-price.pipe';
import {ThousandFormatPricePipe} from './thousand-format-price.pipe';
import {LocalizedDatePipe} from './localized-date.pipe';

@NgModule({
  declarations: [
    FormatPricePipe,
    ThousandFormatPricePipe,
    LocalizedDatePipe
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    FormatPricePipe,
    ThousandFormatPricePipe,
    LocalizedDatePipe
  ]
})
export class CustomPipesModule {
}
