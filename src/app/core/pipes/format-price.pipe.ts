import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPrice'
})
export class FormatPricePipe implements PipeTransform {
  transform(price: string | number, currency: string = '') {
    const priceStr = String(price);
    currency = currency === null ? 'UZB' : currency;
    // 1822500 -> 1 822 500
    return priceStr.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + `${currency !== '' ? ' ' + currency.toUpperCase() : ''}`;
  }
}
