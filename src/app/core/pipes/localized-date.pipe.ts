import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'localizeDate'
})
export class LocalizedDatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  async transform(value: string): Promise<string> {
    const date = value.split(' ')[0];
    const year = value.split(' ')[2];
    const month = value.split(' ')[1];
    const translatedMonth = await this.translateService.get(month).toPromise();
    return `${date} ${translatedMonth} ${year}`;
  }
}
