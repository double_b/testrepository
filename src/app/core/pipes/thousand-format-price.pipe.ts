import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thousandFormatPrice'
})
export class ThousandFormatPricePipe implements PipeTransform {
  transform(price: string | number) {
    if (typeof price === 'number') {
      price = price.toString().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ');
    }
    return price;
  }
}
