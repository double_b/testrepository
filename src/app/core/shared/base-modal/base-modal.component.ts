import {Component, Input, OnInit, Renderer2} from '@angular/core';

@Component({
  selector: 'app-base-modal',
  templateUrl: './base-modal.component.html',
  styleUrls: ['./base-modal.component.scss']
})
export class BaseModalComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() type: string = 'cancel';

  constructor(private renderer: Renderer2) {
  }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'hide_badge_backdrop');
  }
}
