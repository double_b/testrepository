import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-view-inner-content',
  template: `
    <div style="width: 100%; margin: 0 auto;">
      <h2 style="font-size: 18px; font-weight: 600">{{name}} </h2>
    </div>
    <div>
      <div nz-row nzGutter="16" nzType="flex" nzJustify="left">
        <nz-card style="width: 100%; margin: 0 auto">
          <ng-content></ng-content>
        </nz-card>
      </div>
    </div>
  `,
  styleUrls: []
})
export class ViewInnerContentComponent {
  @Input() name;
}
