import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-view-container',
  template: `
    <nz-layout class="layout">
      <app-nav-bar (loadingDone)="loadingHandler($event)"></app-nav-bar>
      <div
        nz-col [nzSpan]="isFullPage ? 24 : 20"
        [nzOffset]="isFullPage ? 0 : 2"
        [ngStyle]="isFullPage ? {} : {'padding': '20px 0'}">
        <nz-spin [nzSpinning]="isLoading">
          <div *ngIf="!isLoading">
            <ng-content></ng-content>
          </div>
        </nz-spin>
      </div>
    </nz-layout>
  `,
  styleUrls: []
})
export class ViewContainerComponent implements OnInit {

  @Input() isLoading = false;

  isFullPage = false;
  fullPage = ['calendar', 'chess'];

  loadingHandler(isLoading) {
    this.isLoading = isLoading;
  }

  constructor(private router: Router) {
  }

  ngOnInit() {
    // данный кусок закомментирован, потому что в динамическом изменении размера окна просто нет необходимости
    // возможно данная опция понадобится в календаре или шахматке - by double_b
    for (const path of this.fullPage) {
      if (this.router.url.includes(path)) {
        this.isFullPage = true;
        break;
      }
    }
  }

}
