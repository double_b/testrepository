import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DragulaModule} from 'ng2-dragula';
import {TranslateModule} from '@ngx-translate/core';

// Modules
import {NzComponentsModule} from '@src/app/core/nzcomponents/nz-components.module';

// Components
import {ViewContainerComponent} from './view-container/view-container.component';
import {ViewInnerContentComponent} from './view-inner-content/view-inner-content.component';
import {WeekPriceTableComponent} from './form-components/week-price-table/week-price-table.component';
import {RateTableComponent} from './form-components/rate-table/rate-table.component';
import {EmptyComponent} from './empty/empty.component';
import {NavBarComponent} from '@app/shared/components/nav-bar/nav-bar.component';
import {CustomPipesModule} from '@app/core/pipes/custom-pipes.module';


@NgModule({
  declarations: [
    ViewContainerComponent,
    NavBarComponent,
    ViewInnerContentComponent,
    WeekPriceTableComponent,
    RateTableComponent,
    EmptyComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    DragulaModule,
    ReactiveFormsModule,
    TranslateModule,
    NzComponentsModule,
    CustomPipesModule,
    CustomPipesModule
  ],
  exports: [
    ViewContainerComponent,
    NavBarComponent,
    ViewInnerContentComponent,
    WeekPriceTableComponent,
    RateTableComponent,
    EmptyComponent,
    NzComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class SharedModule {
}
