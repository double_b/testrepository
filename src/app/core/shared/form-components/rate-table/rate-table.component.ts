import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PriceSettingsService} from '@app/views/price-setting/price-settings.service';

@Component({
  selector: 'app-rate-table',
  templateUrl: './rate-table.component.html',
  styleUrls: ['./rate-table.component.scss']
})
export class RateTableComponent implements OnInit {
  @Output() tableDataEmitter: EventEmitter<any> = new EventEmitter<any>();

  tableData = [];

  isVisibleDelete = false;
  dataToDelete;
  deleteLoading = false;

  roomTypes: any[] = [];
  tariffList: any[] = [];

  @Input() actions = true;
  @Input() viewOnly = true;

  constructor(private priceSettingsService: PriceSettingsService) {
  }

  ngOnInit() {
    this.priceSettingsService.getRoomTypesList().subscribe(res => {
      this.roomTypes = res;
      this.getList();
    });
  }

  private getList() {
    this.priceSettingsService.getBasePriceList().subscribe(res => {
      this.tariffList = res;
      let array = [];
      this.tableData = [];

      this.tariffList.forEach(el => {
        array.push(this.setPrices(el));
      });

      this.tableData = [...array];
    });
  }

  private setPrices(el) {
    let array_types = [];

    const types = el.prices.map(price => {
      return price.room_type_id;
    });
    let unique = [...new Set(types)];

    unique.forEach(type_id => {
      let prices = {};
      prices = el.prices.filter(price => {
        return price.room_type_id === type_id;
      });

      let found_type = this.roomTypes.find(type => {
        return type.id === type_id;
      });

      let new_found_type = Object.assign({prices: prices}, found_type)
      array_types.push(new_found_type)
    });

    return {
      id: el.id,
      name: el.name,
      date: new Date(el.start_date).toLocaleDateString() + ' — ' + new Date(el.end_date).toLocaleDateString(),
      min: el.min_stay,
      types: array_types,
      basic_price: el.basic_price
    };
  }

  prepareDataToDelete(data) {
    this.dataToDelete = data;
    this.isVisibleDelete = true;
  }

  cancelDelete() {
    this.dataToDelete = null;
    this.isVisibleDelete = false;
  }

  deleteElm() {
    delete this.dataToDelete;
    this.isVisibleDelete = false;
    this.deleteLoading = false;
  }

  setChangedData(data, index, tableDataRow) {
    const foundIndex = this.tableData.findIndex(el => {
      return el === tableDataRow;
    });
    const prices = this.getPrices(data, this.tableData[foundIndex].types[index].id);
    this.tableData[foundIndex].types[index].prices = prices;
  }

  private getPrices(data, type_id) {
    let prices: any[] = [];
    data.forEach(row => {
      if (!row.monday.disabled && row.monday.val) {
        let price = row.monday.val;
        if (typeof row.monday.val == 'string') {
          price = row.monday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.monday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 0,
        });
      }
      if (!row.tuesday.disabled && row.tuesday.val) {
        let price = row.tuesday.val;
        if (typeof row.tuesday.val == 'string') {
          price = row.tuesday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.tuesday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 1,
        });
      }
      if (!row.wednesday.disabled && row.wednesday.val) {
        let price = row.wednesday.val;
        if (typeof row.wednesday.val == 'string') {
          price = row.wednesday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.wednesday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 2,
        });
      }
      if (!row.thursday.disabled && row.thursday.val) {
        let price = row.thursday.val;
        if (typeof row.thursday.val == 'string') {
          price = row.thursday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.thursday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 3,
        });
      }
      if (!row.friday.disabled && row.friday.val) {
        let price = row.friday.val;
        if (typeof row.friday.val == 'string') {
          price = row.friday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.friday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 4,
        });
      }
      if (!row.saturday.disabled && row.saturday.val) {
        let price = row.saturday.val;
        if (typeof row.saturday.val == 'string') {
          price = row.saturday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.saturday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 5,
        });
      }
      if (!row.sunday.disabled && row.sunday.val) {
        let price = row.sunday.val;
        if (typeof row.sunday.val == 'string') {
          price = row.sunday.val.replace(/\s+/g, '');
        }
        prices.push({
          price: Number(price),
          rate_id: row.sunday.rate_id,
          guest_quantity: row.icons.length,
          local: row.guest,
          room_type_id: type_id,
          w: 6,
        });
      }
    });
    return prices;
  }
  addRate() {
    const data = [];
    this.tableData.forEach(el => {
      if (el.selected) {
        data.push(el);
      }
    });
    this.tableDataEmitter.emit(data);
  }
}
