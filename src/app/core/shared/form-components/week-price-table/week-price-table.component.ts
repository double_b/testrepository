import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-week-price-table',
  templateUrl: './week-price-table.component.html',
  styleUrls: ['./week-price-table.component.scss']
})
export class WeekPriceTableComponent implements OnInit {
  @Output() changedData: EventEmitter<any> = new EventEmitter<any>();

  @Input() set roomType(roomType: any) {
    if (roomType) {
      this.processRoomType(roomType);
    }
  }

  @Input() set daysForRooms(days: any) {
    if (days && days.length > 0) {
      this.daysForDisable = days;
      this.setDisabledDates();
    }
  }

  nzFormatter = (value: number) => value ? value.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ') : '';
  nzParser = (value: number) => value.toString().trim();

  daysForDisable: any[] = [];

  weekInputs = [
    {
      name: 'Пн',
      key: 'monday',
      selected: true
    },
    {
      name: 'Вт',
      key: 'tuesday',
      selected: true
    },
    {
      name: 'Ср',
      key: 'wednesday',
      selected: true
    },
    {
      name: 'Чт',
      key: 'thursday',
      selected: true
    },
    {
      name: 'Пт',
      key: 'friday',
      selected: true
    },
    {
      name: 'Сб',
      key: 'saturday',
      selected: true
    },
    {
      name: 'Вс',
      key: 'sunday',
      selected: true
    }
  ];

  selectedInput = [];
  tableData = [];

  @Input() disabled = false;
  @Input() viewOnly = false;

  constructor( ) { }

  ngOnInit() {
  }

  private processRoomType(type) {
    if (type.prices) {
      for (let i = type.max_adults; i >= 1; i--) {
        const arr = [];
        type.prices.forEach(el => {
          if (el.guest_quantity === i && el.local === 0 || !el.local) {
            if (typeof el.price === 'string') {
              el.price = el.price.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ');
            }
            arr.push(el);
          }
        });
        this.tableData.push(this.getRow(false, i, arr));
      }

      for (let i = type.max_adults; i >= 1; i--) {
        const arr = [];
        type.prices.forEach(el => {
          if (el.guest_quantity === i && el.local === 1 || el.local) {
            if (typeof el.price === 'string') {
              el.price = el.price.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ');
            }
            arr.push(el);
          }
        });
        this.tableData.push(this.getRow(true, i, arr));
      }

      this.changedData.emit(this.tableData);
    } else {
      for (let i = type.max_adults; i >= 1; i--) {
        this.tableData.push(this.getRow(false, i));
      }

      for (let i = type.max_adults; i >= 1; i--) {
        this.tableData.push(this.getRow(true, i));
      }
    }
  }

  private getRow(title, count, arr = null) {
    return {
      guest: title,
      icons: [...Array(count).keys()],
      monday: {
        val: arr ? arr.find(el => el.w === 0 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 0 && el.guest_quantity === count)?.rate_id : null,
        w: 1
      },
      tuesday: {
        val: arr ? arr.find(el => el.w === 1 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 1 && el.guest_quantity === count)?.rate_id : null,
        w: 2
      },
      wednesday: {
        val: arr ? arr.find(el => el.w === 2 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 2 && el.guest_quantity === count)?.rate_id : null,
        w: 3
      },
      thursday: {
        val: arr ? arr.find(el => el.w === 3 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 3 && el.guest_quantity === count)?.rate_id : null,
        w: 4
      },
      friday: {
        val: arr ? arr.find(el => el.w === 4 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 4 && el.guest_quantity === count)?.rate_id : null,
        w: 5
      },
      saturday: {
        val: arr ? arr.find(el => el.w === 5 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 5 && el.guest_quantity === count)?.rate_id : null,
        w: 6
      },
      sunday: {
        val: arr ? arr.find(el => el.w === 6 && el.guest_quantity === count)?.price : null,
        selected: false,
        rate_id: arr ? arr.find(el => el.w === 6 && el.guest_quantity === count)?.rate_id : null,
        w: 7
      }
    };
  }

  ctrlPress(event) {
    if (event.code === 'Escape') {
      this.selectedInput.forEach(input => {
        input.selected = false;
      });
      this.selectedInput = [];
    }
  }

  selectInput(event, data) {
    if (event.ctrlKey) {
      data.selected = true;
      this.selectedInput.push(data);
    }
  }

  dataChange(event, day: string, data) {
    if (this.selectedInput.length > 0) {
      this.selectedInput.map(input => input.val = event.target.value);
    }

    let found_index = this.tableData.findIndex(el => {
      return el === data;
    });

    this.tableData[found_index][day].val = event.target.value;
    this.changedData.emit(this.tableData);
  }

  selectDay(day) {
    this.tableData.forEach(guest => {
      Object.keys(guest).forEach(key => {
        if (key === day.key) {
          guest[key].disabled = !guest[key].disabled;
        }
      });
    });
    day.selected = true;
    this.changedData.emit(this.tableData);
  }

  setAllDays(data) {

    Object.keys(data).forEach(elm => {
      if (data[elm].val !== undefined) {
        if (!data[elm].disabled) {
          data[elm].val = data.monday.val;
        }
      }
    });
  }

  public setPriceToAllDays(data, event) {
    if (event.type == 'keyup' || (event.type === 'click' && event.detail === 1)) {
      const index = this.tableData.findIndex(el => {
        return data === el;
      });

      this.tableData[index].tuesday.val = !data.tuesday.disabled ? data.monday.val : '';
      this.tableData[index].wednesday.val = !data.wednesday.disabled ? data.monday.val : '';
      this.tableData[index].thursday.val = !data.thursday.disabled ? data.monday.val : '';
      this.tableData[index].friday.val = !data.friday.disabled ? data.monday.val : '';
      this.tableData[index].saturday.val = !data.saturday.disabled ? data.monday.val : '';
      this.tableData[index].sunday.val = !data.sunday.disabled ? data.monday.val : '';

      this.changedData.emit(this.tableData);
    }
  }

  private setDisabledDates() {
    this.tableData.map(el => {
      const keys = Object.keys(el);
      keys.splice(0, 1);
      keys.splice(0, 1);
      keys.forEach(key => {
        const foundDay = this.daysForDisable.findIndex(day => {
          return el[key].w === day;
        });
        if (foundDay > -1) {
          el[key].disabled = false;
        } else {
          el[key].disabled = true;
          el[key].val = '';
        }
      });
    });
  }
}
