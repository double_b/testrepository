import {Injectable} from '@angular/core';
import {NzMessageService} from 'ng-zorro-antd';
import * as _ from 'lodash';
import {ReservationModel} from '../../models/reservation.model';
import {RoomModel} from '../../models/room.model';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private alert: NzMessageService
  ) {
  }

  tokenObjToString(tokenObj) {
    return `${tokenObj.token_type} ${tokenObj.access_token}`;
  }

  presentErrorResponseMessage(response) {
    let errorArr = [response.error.message];

    if (response.error.errors) {
      Object.keys(response.error.errors).forEach(er => {
        errorArr = errorArr.concat(response.error.errors[er]);
      });
    }

    errorArr.forEach(error => {
      console.error(error);
      this.alert.create('error', error, {nzDuration: 5000});
    });
  }

  removeFromArray(arr, value) {
    return arr.filter(ele => {
      return ele !== value;
    });
  }

  populateObjectOnKey(
    root: any[],
    child: any[],
    addDataToRootKey: string,
    rootKeyToCompare: string,
    childKeyToCompare: string) {

    const childObject = {};

    child.forEach(elm => {
      if (childObject[`elm_${elm[childKeyToCompare]}`]) {
        childObject[`elm_${elm[childKeyToCompare]}`].push(elm);
      } else {
        childObject[`elm_${elm[childKeyToCompare]}`] = [elm];
      }
    });

    root.map(value => {
      value[addDataToRootKey] = childObject[`elm_${value[rootKeyToCompare]}`];
    });

    return root;
  }

  arrayToObject(array, key) {
    const arrayObj = {};

    array.forEach(elm => {
      arrayObj[`key_${elm[key]}`] = elm;
    });

    return arrayObj;
  }

  isObjectEmpty(obj: object) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  intNormalizer(data: object, keys: string[]) {

    Object.keys(data).forEach(key => {
      if (_.includes(keys, key)) {
        data[key] = parseInt(data[key]);
      }
    });

    return data;
  }

  removeFromArrayWhere(data: any[], val: string, key: string) {

    let temp = [];

    temp = data.filter(elm => {
      return elm[key] !== val;
    });

    return temp;

  }

  simplyError(error) {
    this.alert.create('error', error, {nzDuration: 5000});
  }

  normalizeReservationData(reservation: ReservationModel) {

    if (reservation.languages && reservation.languages[0] && reservation.languages[0].id) {
      reservation.languages = reservation.languages.map(elm => elm.id);
    }
    if (reservation.services && reservation.services[0] && reservation.services[0].id) {
      reservation.services = reservation.services.map(elm => elm.id);
    }
    if (reservation.images && reservation.images[0] && reservation.images[0].id) {
      reservation.images = reservation.images.map(elm => elm.id);
    }

    return reservation;
  }

  normalizeRoomData(room: RoomModel) {
    if (room.images && room.images[0] && room.images[0].id) {
      room.images = room.images.map(elm => elm.id);
    }

    if (room.facilities && room.facilities[0] && room.facilities[0].id) {
      room.facilities = room.facilities.map(elm => elm.id);
    }

    return room;
  }

  getIds(array: any[], keys: string[]) {

    const tempArray = {...array};

    array.forEach(elm => {
      Object.keys(elm).forEach(key => {

        if (_.includes(keys, key)) {
          elm[key] = elm[key].map(e => e = e.id);
        }
      });
    });

    return tempArray;
  }

  unsubscriber(data: Subscription) {
    if (data) {
      data.unsubscribe();
    }
  }

  listParser(array: string[]) {
    let temp = '';

    if (array.length === 1) {
      return array[0];
    } else {
      temp = array.join(', ');
    }

    return temp;

  }

}
