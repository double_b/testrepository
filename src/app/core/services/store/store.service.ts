import {BehaviorSubject, Observable} from 'rxjs';

export abstract class Store<T> {
  state$: Observable<T>;
  private stateSubject$: BehaviorSubject<T>;

  protected constructor(initialState: T) {
    this.stateSubject$ = new BehaviorSubject(initialState);
    this.state$ = this.stateSubject$.asObservable();
  }

  get state(): T {
    return this.stateSubject$.getValue();
  }

  setState(nextState): void {
    const newState = {...this.state, ...nextState as T};
    this.stateSubject$.next(newState);
  }
}
