import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

// Services
import {AuthService} from '@app/shared/services/auth/auth.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private rout: Router,
              private authService: AuthService,
              private notificationService: NotificationService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Когда пользаватель авторизирован добавляю токен в заголовки
    if (this.authService.isAuthenticated()) {
      request = request.clone({
        setHeaders: {Authorization: this.authService.getUserToken()}
      });
    }

    if (!request.headers.has('Content-Type') && !(request.body instanceof FormData)) {
      request = request.clone({
        setHeaders: {'Content-Type': 'application/json'}
      });
    }

    // Добавляю Url адрес в запрос
    request = request.clone({
      headers: request.headers.set('Accept', 'application/json'),
      // url: `${this.DEV_ENV.baseURL}api/${request.url}`
    });

    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => this.handleAuthError(error))
      );
  }

  // Обрабочик ошибок
  /*  Коды ошибок и их описание
      Код | Описание
     --- | ---
       1 | Время действия токена истекло
       2 | Неверный токен
       3 | Приложение заблокировано администратором
       4 | Токен недействителен. Пользователь вышел из приложения.
       5 | У пользователя нет доступа к данному ресурсу.
       6 | Пользователь не подтвердил email адрес
       7 | Пользователь не подтвердил номер телефона
       10 | Введены неверные данные
   */

  protected handleAuthError(error: HttpErrorResponse): Observable<any> {
    let titleError = 'Ошибка';

    if (error.status === 401 && error.error.code === 1) {
      this.notificationService.errorMessage(
        titleError,
        `${error.error.message} \n code: ${error.error.code} \n`
      );
      console.log(true);
      this.authService.authApp();
    }

    if (error.status === 401 && error.error.code === 2) {
      this.notificationService.errorMessage(
        titleError,
        `${error.error.message} \n code: ${error.error.code} \n`
      );
    }

    if (error.status === 401 && error.error.code === 3) {
      this.notificationService.errorMessage(
        titleError,
        `${error.error.message} \n code: ${error.error.code} \n`
      );
    }

    if (error.status === 401 && error.error.code === 4) {
      this.authService.logOut();
      this.rout.navigate([RoutesEnum.AUTH_LOGIN]).finally(() => {
        this.notificationService.errorMessage(
          titleError = 'Ошибка автаризации',
          `${error.error.message} code: ${error.error.code}`
        );
      });
    }

    if (error.status === 403 && error.error.code === 6) {
      return throwError(error.error);
    }

    if (error.status === 403) {
      return throwError(error);
    }

    if (error.status === 422) {
      return throwError(error);
    }

    if (error.error.errors != null) {
      if (error.error.errors.email != null) {
        return throwError(error.error.errors.email[0]);
      }
    }

    return throwError(error.error.message);

  }
}
