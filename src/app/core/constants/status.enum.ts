export class StatusEnum {
  static CONFIRM = 'confirmed';
  static IN_HOUSE = 'in_house';
  static CHECKOUT = 'check_out';
  static CANCEL = 'canceled';
  static NO_SHOW = 'no_show';
}
