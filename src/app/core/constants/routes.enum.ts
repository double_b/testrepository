export class RoutesEnum {
  static AUTH = 'auth';
  static HOTEL = 'hotel';
  static MESSAGES = 'messages';
  static PROFILE = 'profile';
  static ACC_MANAGE = 'account-management';
  static REVIEWS = 'reviews';

  static DASHBOARD = 'dashboard';
  static CALENDAR = 'calendar';
  static RESERVATION = 'reservation';
  static TARIFF_PLANS = 'tariff-plans';
  static CHESS = 'chess';
  static CHANNELS = 'channels';
  static SOURCES = 'sources';
  static GUESTS = 'guests';
  static DISTRIBUTION = 'distribution';
  static TRAVEL_AGENCIES = 'travel-agencies';
  static AGENCY = 'agency';
  static CONTRACT = 'contract';
  static CONTRACTS = 'contracts';
  static VIEW_CONTRACT = 'view_contract';
  static PRICE_SET = 'price-setting';
  static STATISTIC = 'statistic';

  static LOGIN = 'login';
  static REGISTRATION = 'registration';
  static RESTORE_PASS = 'restore-password';
  static CHANGE_PASS = 'change-password';
  static CONFIRM_EMAIL = 'confirm-email';

  static SERVICES = 'services';
  static GALLERY = 'gallery';
  static FEES = 'fees';
  static TYPES = 'types';
  static REGULATIONS = 'regulations';

  static LIST = 'list';
  static CREATE = 'create';
  static EDIT = 'edit';
  static DETAILS = 'details';
  static IMPORT = 'import';
  static STEP = 'step';
  static CHANNEL = 'channel';
  static VIEW_CHANNEL = 'view-selectedChannel';
  static CONNECTION = 'connection';

  static CHANNEL_ID = 'selectedChannel-id';
  static STEP_ID = 'step-id';
  static HOTEL_ID = 'hotel-id';
  static SECONDS = 'seconds';
  static KEY = 'key';
  static ID = 'id';

  static NEW_HOTEL = 'new-hotel';

  static NEW_HOTEL_REG_SERVICES = `${RoutesEnum.NEW_HOTEL}/${RoutesEnum.REGISTRATION}/${RoutesEnum.SERVICES}`;

  static AUTH_LOGIN = `${RoutesEnum.AUTH}/${RoutesEnum.LOGIN}`;

  LOGIN = RoutesEnum.LOGIN;
  HOTEL = RoutesEnum.HOTEL;
  CREATE = RoutesEnum.CREATE;
  NEW_HOTEL_REG_SERVICES_CLEAR = `/${RoutesEnum.NEW_HOTEL_REG_SERVICES}`;
}
