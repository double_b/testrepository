export class KeysEnum {
  static APP_TOKEN = 'appToken';
  static USER_TOKEN = 'userToken';
  static USER = 'user';
  static EMAIL = 'email';
  static PHONE = 'phone';
  static GET_HOTEL = 'getHotel';
  static LANG = 'lang';
  static HOTEL_ID = 'hotel-id';
  static HOTELIER = 'hotelier';
  static HOTEL_IN_CREATION = 'hotelInCreation';
  static HOTEL_ID_LC = 'hotelId';
  static HOTELS = 'hotels';
  static CONFIRMED_EMAIL = 'confirmedEmail';
  static HAS_HOTELS = 'has-hotels';
}
