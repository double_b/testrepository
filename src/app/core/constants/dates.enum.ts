export class DatesEnum {
  static MONTHS = [
    'DATE.JANUARY',
    'DATE.FEBRUARY',
    'DATE.MARCH',
    'DATE.APRIL',
    'DATE.MAY',
    'DATE.JUNE',
    'DATE.JULY',
    'DATE.AUGUST',
    'DATE.SEPTEMBER',
    'DATE.OCTOBER',
    'DATE.NOVEMBER',
    'DATE.DECEMBER',
  ];
  static MONTHS_LC = [
    'DATE.JANUARY_LC',
    'DATE.FEBRUARY_LC',
    'DATE.MARCH_LC',
    'DATE.APRIL_LC',
    'DATE.MAY_LC',
    'DATE.JUNE_LC',
    'DATE.JULY_LC',
    'DATE.AUGUST_LC',
    'DATE.SEPTEMBER_LC',
    'DATE.OCTOBER_LC',
    'DATE.NOVEMBER_LC',
    'DATE.DECEMBER_LC',
  ];

  static WEEK_DOT = [
    'DATE.MON_DOT',
    'DATE.TUES_DOT',
    'DATE.WED_DOT',
    'DATE.THURS_DOT',
    'DATE.FRI_DOT',
    'DATE.SAT_DOT',
    'DATE.SUN_DOT',
  ];

  static WEEK = [
    'DATE.MON',
    'DATE.TUES',
    'DATE.WED',
    'DATE.THURS',
    'DATE.FRI',
    'DATE.SAT',
    'DATE.SUN',
  ];
}
