export class AgencyInfoModel {
  company_legal_information: CompanyInformationModel;
  contr_agent: number;
  id: number;
  name: string;
  tour_agent: number;
  tour_company_id?: number;
}


export class CompanyInformationModel {
  address: string;
  bank: string;
  bank_account: string;
  bank_account_usd: string;
  city: string;
  company_id: number;
  company_name: string;
  contract_number?: number;
  created_at: string;
  director_name: string;
  id: number;
  inn: string;
  mfo: string;
  oked: string;
  okonh?: any;
  phone: string;
  swift_code: string;
  updated_at: string;
}
