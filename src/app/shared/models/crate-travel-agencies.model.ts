export class CrateTravelAgenciesModel {
  contr_agent: boolean;
  id: number;
  name: string;
  tour_agent: boolean;
  tour_company_id?: number;
}
