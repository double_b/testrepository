export class PropertyTypesModel {
  id: number;
  active: number;
  name: string;
  short_name: string;
  sort_order: number;
  room_name_group_id: number;
}
