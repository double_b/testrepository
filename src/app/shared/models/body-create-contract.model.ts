export class BodyCreateContractModel {
  address: string;
  bank: string;
  bank_account: string;
  bank_account_usd: string;
  city: string;
  city_tax: number;
  company_id: number;
  company_name: string;
  created_at: string;
  director_name: string;
  end: string;
  inn: string;
  mfo: string;
  name: string;
  number: string;
  oked: string;
  phone: string;
  prices: BodyCreateContractPricesModel[];
  rules: BodyCreateContractRulesModel[];
  start: string;
  swift_code: string;
  tax?: number;
  updated_at: string;
}


export class BodyCreateContractRulesModel {
  days: string;
  commission: number;
  cancel_commission: number;
  prepayment: boolean;
}

export class BodyCreateContractPricesModel {
  price: number;
  rate_id: number;
  guest_quantity: number;
  local: boolean;
  room_type_id: number;
  w: number;
}
