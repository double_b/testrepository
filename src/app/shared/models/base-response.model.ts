export class BaseResponseModel<T> {
  data: T;
  message?: string;
}
