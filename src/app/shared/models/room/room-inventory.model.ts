export class RoomInventoryModel {
  channel?: string;
  rate_id?: number;
  min_advance_res?: number;
  min_stay?: number;
  rooms?: number;
  start: string;
  end?: string;
  w?: number[] = [];
}
