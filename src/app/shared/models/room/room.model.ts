export class RoomModel {
  id: number;
  name: string;
  room_type_id: number;
  sort_order: number;
  building_id: number;
}
