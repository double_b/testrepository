export class BedModel {
  id: number;
  active: number;
  sort_order: number;
  name: string;
  count: number;
}
