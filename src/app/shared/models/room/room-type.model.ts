import {BedModel} from '@app/shared/models/room/bed.model';
import {RoomImageModel} from '@app/shared/models/room/room-image.model';
import {RoomModel} from '@app/shared/models/room/room.model';
import {FacilityModel} from '@app/shared/models/common/facility.model';

export class RoomTypeModel {
  public id: number;
  public property_id: number;
  public room_name_id: number;
  public name: string;
  public short_name: string;
  public max_persons: number;
  public max_adults: number;
  public max_children: number;
  public quantity: number;
  public standard_price: number;
  public area: number;
  public smoking: number;
  public active: number;
  public deleted_at: string;
  public created_at: string;
  public updated_at: string;
  public mainBeds: BedModel[];
  public altBeds: BedModel[];
  public image: RoomImageModel;
  public room_names: string[] = [];
  public facilities: number[] | FacilityModel[];
  public images: number[] = [];
  public rooms: RoomModel[] = [];
}
