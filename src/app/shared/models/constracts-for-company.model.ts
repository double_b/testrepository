import {PricesModel} from '@app/shared/models/price.model';

export class ConstractsForCompanyModel {
  address: string;
  bank_account: string;
  city: string;
  city_tax?: number;
  company_id: number;
  company_name: string;
  email: string;
  end: string;
  id: number;
  number: string;
  phone: string;
  prices: PricesModel[];
  property_id: number;
  rules: ConstractsForCompanyRulesModel[];
  start: string;
  status: string;
  tax?: number;
  tour_company_id?: number;
}


export class ConstractsForCompanyRulesModel {
  days: number;
  commission: number;
  cancel_commission: number;
  prepayment: number;
}
