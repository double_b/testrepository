export interface Beds {
  id: number;
  active: number;
  sort_order: number;
  count: number;
  name: string;
}

export interface ImageRoom {
  id: number;
  property_id: number;
  image_name_id: number;
  name: string;
  original: string;
  big: string;
  medium: string;
  small: string;
  thumb: string;
  sort_order: number;
  default: number;
  created_at: string;
  updated_at: string;
}

export class RoomsTypesListModel {
  constructor(
    public id: number,
    public property_id: number,
    public room_name_id: number,
    public name: string,
    public short_name: string,
    public max_persons: number,
    public max_children: number,
    public quantity: number,
    public standard_price: number,
    public area: number,
    public smoking: number,
    public active: number,
    public deleted_at: string,
    public created_at: string,
    public updated_at: string,
    public mainBeds: Beds[],
    public altBeds: Beds[],
    public image: ImageRoom,
    public selected = false
  ) {
  }
}
