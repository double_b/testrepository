export class SourceModel {
  id: number = null;
  name: string;
  overbooking: boolean|number = false;
  sort_order: number;
  active: boolean|number;
  companies: any;
  default: boolean|number = 0;
}
