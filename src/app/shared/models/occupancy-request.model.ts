import {OccupancyModel} from '@app/shared/models/occupancy.model';

export class OccupancyRequestModel {
  channel: string;
  rate_id: number;
  occupancy: OccupancyModel[] = [];
}
