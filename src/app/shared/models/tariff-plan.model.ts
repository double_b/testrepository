import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';

export class TariffPlanModel {
  id: number;
  property_id: number;
  name: string;
  breakfast: boolean|number = false;
  lunch: boolean|number = false;
  dinner: boolean|number = false;
  // минимальное значение всегда должно быть 1
  min_stay: number = 1;
  // минимальное значение всегда должно быть 0
  min_advance_res: number = 0;
  room_types: RoomsTypesListModel[] | number[] = [];
}
