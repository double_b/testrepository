export class GalleryTypesImageModel {
  constructor(
    public id: number,
    public sort_order: number,
    public name: string
  ) {}
}
