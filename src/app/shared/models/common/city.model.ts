import {CountryModel} from '@app/shared/models/common/country.model';

export class CityModel {
    id: number;
    country_id: number;
    active: number;
    name = '';
    country?: CountryModel;
}
