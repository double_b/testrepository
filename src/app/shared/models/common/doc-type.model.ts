export class DocTypeModel {
  id: number;
  active: number;
  sort_order: number;
  name: string;
}
