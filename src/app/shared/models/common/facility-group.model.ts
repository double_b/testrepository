import {FacilityModel} from '@app/shared/models/common/facility.model';

export class FacilityGroupModel {
  id: number;
  active: number;
  name: number;
  sort_order: number;
  facilities: FacilityModel[] = [];
}
