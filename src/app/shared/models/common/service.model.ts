import {ServiceGroupModel} from '@app/shared/models/common/service-group.model';

export class ServiceModel {
  id?: number;
  service_group_id?: number;
  active?: number;
  sort_order?: number;
  serviceGroup?: ServiceGroupModel;
  name?: string;
  checked = false;
}
