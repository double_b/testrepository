export class ImageModel {
  id?: number;
  property_id?: number;
  image_name_id?: number;
  name?: string;
  original?: string;
  big?: string;
  medium?: string;
  small?: string;
  thumb?: string;
  sort_order?: number | null;
  default?: string | null;
  created_at?: string;
  updated_at?: string;
}
