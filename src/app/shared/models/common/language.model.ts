export class LanguageModel {
  id: number;
  sort_order: number;
  active: number;
  name: string;
}
