export interface CountryModel {
  id: number;
  code: string;
  iso3: string;
  numcode: string;
  active: number;
  name: string;
}
