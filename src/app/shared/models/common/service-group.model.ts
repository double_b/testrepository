import {ServiceModel} from '@app/shared/models/common/service.model';

export class ServiceGroupModel {
  id: number;
  active: number;
  sort_order: number;
  name: string;
  services?: ServiceModel[];
}
