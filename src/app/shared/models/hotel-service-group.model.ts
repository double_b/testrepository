import {HotelServiceModel} from '@app/shared/models/hotel-service.model';

export class HotelServiceGroupModel {
  constructor(
    public id: number,
    public active: number,
    public sort_order: number,
    public name: string,
    public services?: HotelServiceModel[],
    public hover?: boolean
  ) {
  }
}
