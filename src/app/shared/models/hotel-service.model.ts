export class HotelServiceModel {
  constructor(
    public id: number,
    public service_group_id: number,
    public active: number,
    public sort_order: number,
    public name: string,
    public checked?: boolean
  ) {
  }
}
