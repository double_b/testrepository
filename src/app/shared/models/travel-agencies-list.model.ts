export class TravelAgenciesModel {
  contr_agent: number;
  id: number;
  name: string;
  tour_agent: number;
  tour_company_id?: number;
}
