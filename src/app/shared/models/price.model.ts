import {ImageModel} from '@app/shared/models/common/image.model';
import { BedModel } from './room/bed.model';

export class PriceModel {
  channel: string;
  price?: number;
  rate_id: number;
  start: string;
  end: string;
  w?: number[] = [];
}

export class TourAgentRatesModel {
  id: number;
  end_date: string;
  min_stay?: number;
  name?: string;
  start_date: string;
  prices: PricesModel[];
}

export class PricesModel {
   date: string;
   guest_quantity: number;
   local: number;
   price: string;
   rate_id: number;
   room_type_id: number;
   w: number;
}

export class RoomTypesModel {
  active: number;
  altBeds: any[];
  area: number;
  created_at: string;
  deleted_at?: string;
  id: number;
  image: ImageModel;
  mainBeds: BedModel[];
  max_children: number;
  max_persons: number;
  name: string;
  property_id: number;
  quantity: number;
  room_name_id: number;
  short_name: string;
  smoking: number;
  standard_price: number;
  updated_at: string;
}

export class CreateBasePricesBodyModel {
  name: string;
  property_id: number;
  start_date: string;
  end_date: string;
  prices: PricesBody[];
  min_stay?: number;
}

export class CreateBasePricesModel {
  name: string;
  property_id: number;
  start_date: string;
  end_date: string;
  prices: PricesModel[];
  min_stay?: number;
}

export class DeleteBasePriceModel {
  message: string;
}

export class PricesBody {
   guest_quantity: number;
   local: boolean;
   price: number;
   room_type_id: number;
   w: number;
}
