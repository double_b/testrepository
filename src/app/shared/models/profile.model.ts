export class ProfileModel {
  active: number;
  avatar?: any;
  birthday?: string;
  created_at: string;
  email: string;
  email_verified: number;
  first_name: string;
  gender?: string;
  id: number;
  last_name: string;
  last_seen?: string;
  middle_name: string;
  notes?: any;
  phone: string;
  phone_verified: number;
  role: string;
  sms_phone?: string;
  updated_at: string;
}
