export class ChannelModel {
  id?: number;
  property_id?: number;
  active?: number;
  channel?: ChannelInfoModel;
  channel_key?: string;
  channel_property_id?: string;
  message?: string;
  status?: string;
  status_name?: string;
  step?: string;
  synced_at?: Date;
}

export class ChannelInfoModel {
  auth_info?: string;
  before_info?: string;
  currency?: string;
  description?: string;
  discount_local?: number;
  key?: string;
  logo?: string;
  name?: string;
}

export interface Channel {
  auth_info: string | null;
  before_info: string | null;
  currency: string;
  description: string;
  discount_local: number;
  key: string;
  logo: string;
  name: string
}

export interface Rooms {
  channel_room_type_id: string;
  channel_room_type_name: string;
  id: number;
  property_channel_id: number;
  rates: any[];
  room_type: any;
  room_type_id: any;
}


export class CreateChanelModel {
  constructor(
    channel_key: string,
    channel_property_id: number
  ) {}
}

export class CheckAuthyModel {
  constructor(
    channel_propert_id: number
  ) {}
}

export class RoomTypesForChannelModel {
  constructor(
    public active: number,
    public altBeds: any[],
    public area: number,
    public created_at: string,
    public deleted_at: any,
    public id: number,
    public image: any,
    public mainBeds: any[],
    public max_children: number,
    public max_persons: number,
    public name: string,
    public property_id: number,
    public quantity: number,
    public room_name_id: number,
    public short_name: string,
    public smoking: number,
    public standard_price: number,
    public updated_at: string,
    public empty: boolean
  ) {}
}
