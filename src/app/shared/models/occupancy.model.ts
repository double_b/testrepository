export class OccupancyModel {
  id: number;
  room_type_id: number;
  channel: string;
  rate_id: number;
  guests: number;
  mode: number;
  amount: number;
  discount_local: number;
  mode_local: number;
  amount_local: number;
}
