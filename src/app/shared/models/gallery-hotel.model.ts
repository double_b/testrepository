export class GalleryHotelModel {
  constructor(
  public id: number,
  public property_id: number,
  public image_name_id: number,
  public name: string,
  public original: string,
  public big: string,
  public medium: string,
  public small: string,
  public thumb: string,
  public created_at: string,
  public updated_at: string
  ) {}
}
