export class BodyCreateTravelAgencyModel {
  address: string;
  bank: string;
  bank_account: string;
  bank_account_usd: string;
  city: string;
  company_name: string;
  contr_agent: boolean;
  director_name: string;
  email: string;
  inn: string;
  mfo: string;
  name: string;
  oked: string;
  okonh: string;
  phone: string;
  swift_code: string;
  tour_agent: boolean;
}

