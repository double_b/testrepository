import {formatDate} from '@angular/common';

export class Utils {
  static isNotNull(item: any): boolean {
    return item !== null && item !== 'null';
  }

  static isNotUndefined(item: any): boolean {
    return item !== undefined && item !== 'undefined';
  }

  static isExist(item: any): boolean {
    return item !== '' && item !== null && item !== 'null' && item !== undefined && item !== 'undefined';
  }

  static getStepStyle(currentStep: number, index: number): string {
    if (currentStep === index + 1) {
      return 'process';
    }
    if (currentStep < index + 1) {
      return 'wait';
    } else {
      return 'finish';
    }
  }

  static normalizeDateFormat(value: number): string {
    if (value < 10) {
      return `0${value}`;
    } else {
      return value.toString();
    }
  }

  static convertDateToQuery(date: Date): string {
    const year = date.getFullYear();
    const month = Utils.normalizeDateFormat(date.getMonth() + 1);
    const day = Utils.normalizeDateFormat(date.getDate());
    return `${year}-${month}-${day}`;
  }

  /**
   * получаем кол-во дней между двумя датами
   * @param fromDate - начало периода
   * @param byDate - конец периода
   * @param considerEndDay - учитывать последний день периода или нет
   */
  static getDateDifference(fromDate: any, byDate: any, considerEndDay: boolean = true): number {
    const from = new Date(fromDate);
    const by = new Date(byDate);
    // берем разницу дат в секундах
    // @ts-ignore
    const delta = Math.floor((by - from) / 1000);
    // вычисляем кол-во полных дней
    let days = Math.floor(delta / 86400);
    // если указано включать и последний день, то к результату добавляем еще один день
    if (considerEndDay) {
      days++;
    }
    // непонятный баг - возвращает на 1 день меньше
    // поэтому просто инкрементируем
    // не удалять - иначе 31 декабря не учтется
    days++;
    return days;
  }

  /**
   * округление числа
   * @param value - округляемое число
   * @param decimals - кол-во чисел после запятой - опционально - по дефолту = 2
   */
  static round(value: number, decimals: number = 2) {
    return Number(Math.round(Number(value + 'e' + decimals)) + 'e-' + decimals);
  }

  /**
   * приводим даты в вид 01/01 - 31/12
   * @param startDate - начальная дата
   * @param endDate - конечная дата
   * @param isMonthMode
   */
  static formatDateChart(startDate: Date, endDate: Date = null, isMonthMode: boolean = false): string {
    if (isMonthMode) {
      switch (startDate.getMonth()) {
        case 0: {
          return 'Янв.';
        }
        case 1: {
          return 'Фев.';
        }
        case 2: {
          return 'Март';
        }
        case 3: {
          return 'Апр.';
        }
        case 4: {
          return 'Май';
        }
        case 5: {
          return 'Июнь';
        }
        case 6: {
          return 'Июль';
        }
        case 7: {
          return 'Авг.';
        }
        case 8: {
          return 'Сен.';
        }
        case 9: {
          return 'Окт.';
        }
        case 10: {
          return 'Ноя.';
        }
        case 11: {
          return 'Дек.';
        }
      }
    }
    if (endDate !== null) {
      const startStr = formatDate(startDate, 'dd/MM', 'ru');
      const endStr = formatDate(endDate, 'dd/MM', 'ru');
      return `${startStr} - ${endStr}`;
    } else {
      return formatDate(startDate, 'dd/MM', 'ru');
    }
  }

  static parseDate(date: Date): string {
    const dateString = new Date(date).toISOString();
    return dateString.split('T')[0];
  }

  static compareDates(firstDate: any, secondDate: any, withMore: boolean = false): boolean {
    if (withMore) {
      const date1 = new Date(firstDate);
      date1.setHours(0, 0, 0, 0);
      const date2 = new Date(secondDate);
      date2.setHours(0, 0, 0, 0);
      return date1 >= date2;
    } else {
      if (typeof firstDate !== 'string') {
        firstDate = this.convertDateToQuery(firstDate);
      }
      if (typeof secondDate !== 'string') {
        secondDate = this.convertDateToQuery(secondDate);
      }
      return firstDate === secondDate;
    }
  }

  static getSource(reservation: any): string {
    let result = '';

    if (!Utils.isExist(reservation)) {
      return result;
    }

    if (Utils.isExist(reservation.channel)) {
      if (typeof reservation.channel === 'string') {
        if (reservation.channel !== 'smartbooking') {
          result = reservation.channel;
        }
      } else if (reservation.channel.key !== 'smartbooking') {
        result = reservation.channel.name;
      }
      if (Utils.isExist(reservation.company)) {
        result += ' ' + reservation.company.name;
      } else if (Utils.isExist(reservation.source)) {
        result += ' ' + reservation.source.name;
      }
    }
    return result;
  }

}
