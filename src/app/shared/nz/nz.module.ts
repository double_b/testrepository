import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NzAlertModule,
  NzAutocompleteModule, NzAvatarModule,
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule, NzCollapseModule, NzDatePickerModule, NzDividerModule, NzDrawerModule,
  NzDropDownModule, NzEmptyModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule, NzInputNumberModule,
  NzLayoutModule,
  NzMenuModule,
  NzModalModule, NzNoAnimationModule, NzPaginationModule, NzPopoverModule, NzRadioModule,
  NzSelectModule,
  NzSpinModule,
  NzStepsModule, NzSwitchModule, NzTabsModule, NzTimePickerModule, NzToolTipModule, NzUploadModule,
} from 'ng-zorro-antd';
import {NzTableModule} from 'ng-zorro-antd/table';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NzGridModule,
    NzSpinModule,
    NzFormModule,
    NzInputModule,
    NzCheckboxModule,
    NzButtonModule,
    NzIconModule,
    NzAlertModule,
    NzDropDownModule,
    NzStepsModule,
    NzLayoutModule,
    NzMenuModule,
    NzCardModule,
    NzTableModule,
    NzSelectModule,
    NzAutocompleteModule,
    NzModalModule,
    NzSwitchModule,
    NzAvatarModule,
    NzInputNumberModule,
    NzRadioModule,
    NzTabsModule,
    NzUploadModule,
    NzTimePickerModule,
    NzDividerModule,
    NzEmptyModule,
    NzPaginationModule,
    NzDatePickerModule,
    NzNoAnimationModule,
    NzCollapseModule,
    NzDrawerModule,
    NzPopoverModule,
    NzToolTipModule
  ],
  exports: [
    NzGridModule,
    NzSpinModule,
    NzFormModule,
    NzInputModule,
    NzCheckboxModule,
    NzButtonModule,
    NzIconModule,
    NzAlertModule,
    NzDropDownModule,
    NzStepsModule,
    NzLayoutModule,
    NzMenuModule,
    NzCardModule,
    NzTableModule,
    NzToolTipModule,
    NzSelectModule,
    NzAutocompleteModule,
    NzModalModule,
    NzSwitchModule,
    NzAvatarModule,
    NzInputNumberModule,
    NzRadioModule,
    NzTabsModule,
    NzUploadModule,
    NzTimePickerModule,
    NzDividerModule,
    NzEmptyModule,
    NzPaginationModule,
    NzNoAnimationModule,
    NzCollapseModule,
    NzDrawerModule,
    NzPopoverModule,
    NzDatePickerModule
  ]
})
export class NzModule {
}
