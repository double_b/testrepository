import {Injectable} from '@angular/core';
import {GalleryService} from '@app/shared/services/gallery/gallery.service';
import {Observable} from 'rxjs';
import {GalleryHotelModel} from '@app/shared/models/gallery-hotel.model';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Injectable({
  providedIn: 'root'
})
export class PhotoManagerService {

  constructor(private galleryService: GalleryService) {
  }

  getHotelGallery(): Observable<GalleryHotelModel[]> {
    return this.galleryService.getGalleryHotel(this.getPropertyId());
  }

  uploadImage(file: any): Observable<any> {
    return this.galleryService.uploadPhoto(this.getPropertyId(), file);
  }

  changeImage(imageId: number, image_name_id: string): Observable<GalleryHotelModel> {
    return this.galleryService.changeImage(this.getPropertyId(), imageId, image_name_id);
  }

  deleteImage(imageId: number): Observable<any> {
    return this.galleryService.deletePhoto(this.getPropertyId(), imageId);
  }

  getImagesUrl(): string {
    return this.galleryService.getImagesUrl(this.getPropertyId());
  }

  getPropertyId(): number {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return Number(hotel);
  }
}
