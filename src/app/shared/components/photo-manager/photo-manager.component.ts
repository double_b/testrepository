import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {NzMessageService, UploadXHRArgs} from 'ng-zorro-antd';
import {ImageModule, ImageName} from '@app/core/models/commun';
import * as lodash from 'lodash';
import {DragulaService} from 'ng2-dragula';

// Services
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {PhotoManagerService} from '@app/shared/components/photo-manager/photo-manager.service';

@Component({
  selector: 'app-photo-manager',
  templateUrl: './photo-manager.component.html',
  styleUrls: ['./photo-manager.component.scss']
})
export class PhotoManagerComponent implements OnInit, OnChanges {
  isVisible = false;
  isLoading = false;
  isLoadingLoading = false;
  isLoadingModal = false;
  @Input() inputSelected: any;
  @Output() selected: EventEmitter<any[]> = new EventEmitter();

  images = [];
  selectedImages: ImageModule[] = [];
  imagesCategory: ImageName[] = [];

  imagesUrl = '';

  validateForm: FormGroup;

  photoNavigator = {
    selected: 0,
    selectedImage: null,
    tabs: [
      {
        active: false,
        name: 'Галерея',
        icon: 'picture'
      },
      {
        active: false,
        name: 'Загрузить',
        icon: 'upload'
      },
      {
        active: false,
        name: 'Редактировать',
        icon: 'edit'
      }
    ]
  };

  constructor(private msg: NzMessageService,
              private photoManagerService: PhotoManagerService,
              private dragulaService: DragulaService,
              private fb: FormBuilder,
              private extrasService: ExtrasService) {
    this.validateForm = this.fb.group({
      image_name_id: [null, [Validators.required]],
    });
  }

  ngOnInit() {
    this.isLoading = true;

    // Url для uploader
    this.imagesUrl = this.photoManagerService.getImagesUrl();

    // this.galleryService.getGalleryHotel(this.hotelId).subscribe(res => {
    //     this.images = res;
    //
    //     const selected = this.inputSelected.reduce((acc: any, image: any) => {
    //         acc[image.id] = image;
    //         return acc;
    //     }, {});
    //
    //     this.images.forEach(img => {
    //         if (selected[img.id]) {
    //             img.checked = true;
    //             this.selectedImages.push(img);
    //         }
    //     });
    //
    //     this.isLoading = false;
    // }, error => error);

    this.init();

    this.extrasService.galleryTypesImage().subscribe(res => {
      this.imagesCategory = res;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.init();
  }

  init() {
    this.photoManagerService.getHotelGallery().subscribe(res => {
      this.selectedImages = [];
      this.images = res;

      const selected = this.inputSelected.reduce((acc: any, image: any) => {
        acc[image.id] = image;
        return acc;
      }, {});

      this.images.forEach(img => {
        if (selected[img.id]) {
          img.checked = true;
          this.selectedImages.push(img);
        }
      });
      this.isLoading = false;
    }, error => error);
  }

  showModal() {
    this.isVisible = true;
  }

  onCancelModalHandler() {
    this.isVisible = false;
    this.photoNavigator.selectedImage = null;
    this.photoNavigator.selected = 0;
    this.validateForm.reset();

  }

  onConfirmModalHandler() {
    this.isVisible = false;
  }

  checkImage(image) {
    image.checked = !image.checked;

    if (!lodash.includes(this.selectedImages, image)) {
      this.selectedImages.push(image);
    } else {
      this.selectedImages = this.selectedImages.filter(img => img.id !== image.id);
    }
    const data = [];
    this.selectedImages.forEach(elm => data.push(elm.id));

    this.selected.emit(data);
  }

  editImage(image) {
    if (!this.isVisible) {
      this.isVisible = true;
    }
    this.photoNavigator.selected = 2;
    this.photoNavigator.selectedImage = image;

    if (image.image_name_id) {
      this.validateForm.controls.image_name_id.setValue(image.image_name_id);
    }
  }

  updateImageName() {
    if (this.validateForm.valid) {
      this.photoManagerService.changeImage(this.photoNavigator.selectedImage.id,
        this.validateForm.value).subscribe(res => {
          this.images.forEach(elm => {
            if (elm.id === res.id) {
              elm.name = res.name;
            }
          });
          this.selectedImages.forEach(elm => {
            if (elm.id === res.id) {
              elm.name = res.name;
            }
          });
          this.photoNavigator.selected = 0;
          this.validateForm.reset();
        },
        error => error,
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  deleteImage(image) {
    this.isLoadingLoading = true;
    this.photoManagerService.deleteImage(image.id).subscribe(
      () => {
      },
      error => error,
      () => {
        this.isLoadingLoading = false;
        this.images = this.images.filter(e => e.id !== image.id);
      }
    );
  }

  // Отправляю фото на сервер
  imageUploadingRequestHandler = (item: UploadXHRArgs) => {

    this.isLoadingModal = true;

    const formData = new FormData();
    formData.append('file', item.file as any);

    return this.photoManagerService.uploadImage(item.file)
      .subscribe(res => {
          this.isLoading = false;
          this.images.push(res);
          this.msg.success(`Фото загружена успешно!`);
          this.photoNavigator.selected = 0;
          this.init();
        },
        error => error,
        () => {
          this.isLoadingModal = false;
        }
      );
  };
}
