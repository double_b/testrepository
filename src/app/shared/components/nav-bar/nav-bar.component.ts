import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {AuthService} from '@app/shared/services/auth/auth.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

// Environments
import {environment} from '@src/environments/environment';

// Services
import {NavBarService} from './nav-bar.service';

import {RoutesEnum} from '@app/core/constants/routes.enum';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {MenuItem} from '@app/core/models/commun';
import {Utils} from '@app/shared/helpers/utils';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnDestroy {

  hotelsData = [];
  hotel = null;

  hotelsSubscription: Subscription;
  lang = environment.lang;
  selectedLang;
  loading = false;
  basePath = `/${RoutesEnum.HOTEL}/${this.navBarService.getPropertyId()}`;

  routesEnum = new RoutesEnum();

  @Output() loadingDone: EventEmitter<boolean> = new EventEmitter();

  menuItems: MenuItem[];

  constructor(private authService: AuthService,
              private translate: TranslateService,
              private router: Router,
              private rout: ActivatedRoute,
              private navBarService: NavBarService) {
    this.hotel = JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL)) || '';
    this.setNavBarItems();
    this.loadingDone.emit(true);

    this.hotelsSubscription = this.navBarService.getHotels()
      .subscribe(res => {
        this.hotelsData = res;
        if (this.navBarService.getPropertyId()) {
          const hotel = res.find(elm => elm.id === this.navBarService.getPropertyId());
          this.navBarService.data.currentHotel.next(hotel);
        }
        this.loadingDone.emit(false);
      });
    this.selectedLang = localStorage.getItem(KeysEnum.LANG) || this.lang[0].value;

  }

  ngOnDestroy() {
    if (this.hotelsSubscription) {
      this.hotelsSubscription.unsubscribe();
    }
  }

  openRoute(path: string) {
    this.router.navigate([path]).finally();
  }

  public get isCurrentHotelNonNullable(): boolean {
    if (Utils.isExist(localStorage.getItem(KeysEnum.GET_HOTEL))) {
      return JSON.parse(localStorage.getItem(KeysEnum.GET_HOTEL)).status !== 'registration';
    } else {
      return false;
    }
  }

  logOut() {
    this.authService.logOut();
    this.router.navigate([RoutesEnum.AUTH, RoutesEnum.LOGIN]).finally();
    this.navBarService.clean();
  }

  switchLanguage(language: string) {
    localStorage.setItem(KeysEnum.LANG, language);
    this.translate.use(language);
  }

  // Перехожу на отель по клику в списке отелей
  setNewHotelHandler(hotel) {
    localStorage.setItem(KeysEnum.HOTEL_ID, hotel.id);
    localStorage.setItem(KeysEnum.GET_HOTEL, JSON.stringify(hotel));
    this.navBarService.data.currentHotel.next(hotel);

    /* Если отель со статусом 'registration', то прерхожу к экрану регистрацци
    * иначе к 'dashboard'
    */
    if (hotel.status === 'registration') {
      this.router.navigate([RoutesEnum.HOTEL, hotel.id, RoutesEnum.EDIT]).finally();
    } else {
      this.router.navigate([RoutesEnum.HOTEL, hotel.id, RoutesEnum.DASHBOARD]).finally();
    }
  }

  isDisabledHotelSetting(hotel): boolean {
    if (hotel.status === 'registration') {
      if (this.router.url.includes(RoutesEnum.HOTEL) && this.router.url.includes(RoutesEnum.EDIT)) {
        return true;
      }
    }
    return false;
  }

  setNavBarItems() {
    this.basePath = `/${RoutesEnum.HOTEL}/${this.navBarService.getPropertyId()}`;
    this.menuItems = [
      {
        title: 'NAV_BAR.MAIN',
        navigable: true,
        disabled: false,
        path: `${this.basePath}/${RoutesEnum.DASHBOARD}`,
      },
      {
        title: 'NAV_BAR.CALENDAR',
        navigable: true,
        disabled: false,
        path: `${this.basePath}/${RoutesEnum.CALENDAR}`,
      },
      {
        title: 'NAV_BAR.TARIFFS',
        navigable: false,
        disabled: false,
        subMenu: [
          {
            title: 'NAV_BAR.TARIFF_PLANS',
            navigable: true,
            path: `${this.basePath}/${RoutesEnum.TARIFF_PLANS}`,
            disabled: false,
          },
          {
            title: 'NAV_BAR.CHESS',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.CHESS}`,
          }
        ]
      },
      {
        title: 'NAV_BAR.RESERVATIONS_REQUESTS',
        navigable: false,
        disabled: false,
        subMenu: [
          {
            title: 'NAV_BAR.RESERVATIONS',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.RESERVATION}`,
          },
          {
            title: 'NAV_BAR.RESERVATIONS_IMPORT',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.RESERVATION}/${RoutesEnum.IMPORT}`,
          },
        ]
      },
      {
        title: 'NAV_BAR.GUESTS',
        navigable: true,
        disabled: false,
        path: `${this.basePath}/${RoutesEnum.GUESTS}`,
      },
      {
        title: 'NAV_BAR.TRAVEL_AGENCIES',
        navigable: false,
        disabled: false,
        subMenu: [
          {
            title: 'NAV_BAR.ALL_TRAVEL_AGENCIES',
            navigable: true,
            path: `${this.basePath}/${RoutesEnum.DISTRIBUTION}`,
            disabled: false,
          },
          {
            title: 'NAV_BAR.SET_PRICES',
            navigable: true,
            path: `${this.basePath}/${RoutesEnum.PRICE_SET}`,
            disabled: false,
          },
          // {
          //   title: 'NAV_BAR.CONTRACTS',
          //   navigable: true,
          //   path: `${this.basePath}/${RoutesEnum.DISTRIBUTION}/${RoutesEnum.CONTRACTS}`,
          //   disabled: false,
          // }
        ]
      },
      {
        title: 'NAV_BAR.HOTEL',
        navigable: false,
        disabled: false,
        subMenu: [
          {
            title: 'NAV_BAR.SETTINGS',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.EDIT}`,
          },
          {
            title: 'NAV_BAR.SALES_CHANNELS',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.CHANNELS}`,
          },
          {
            title: 'NAV_BAR.SOURCES',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.SOURCES}`,
          },
          {
            title: 'NAV_BAR.STATISTICS',
            navigable: true,
            disabled: false,
            path: `${this.basePath}/${RoutesEnum.STATISTIC}`,
          },
          {
            title: 'NAV_BAR.MY_HOTELS',
            navigable: true,
            disabled: false,
            path: `/${RoutesEnum.HOTEL}/${RoutesEnum.LIST}`,
          }
        ]
      },
    ];
    for (const menu of this.menuItems) {
      if (menu?.subMenu != null && menu?.subMenu?.length > 0) {
        for (const sub of menu.subMenu) {
          if (this.router.url.includes(sub.path)) {
            menu.active = true;
            break;
          }
        }
      }
    }
  }

  isNavbarItemSelected(path: string): boolean {
    return this.router.url.includes(path);
  }

  getProfileRoute(): string {
    return `/${RoutesEnum.PROFILE}`;
  }

  getAccManagerRoute(): string {
    return `/${RoutesEnum.ACC_MANAGE}`;
  }

  getDashboardRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.navBarService.getPropertyId()}/${RoutesEnum.DASHBOARD}`;
  }

  getListRoute(): string {
    return `/${RoutesEnum.HOTEL}/${RoutesEnum.LIST}`;
  }

}
