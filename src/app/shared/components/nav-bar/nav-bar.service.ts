import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {HotelService} from '@app/shared/services/hotels/hotel.service';
import {HotelModel} from '@app/shared/models/hotel.model';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {

  data = {
    currentHotel: new BehaviorSubject<HotelModel>(null as HotelModel),
    tariffHotel: new BehaviorSubject<any[]>(null as any)
  };

  constructor(private hotelService: HotelService) {
  }

  clean() {
    Object.keys(this.data).forEach(key => this.data[key].next(null));
  }

  getHotels(): Observable<HotelModel[]> {
    return this.hotelService.list();
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }
}
