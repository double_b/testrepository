import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormNavigatorModel} from '@app/shared/components/room-type-form/helpers/form-navigator.model';
import {RoomTypeModel} from '@app/shared/models/room';
import {Subscription} from 'rxjs';
import {RoomTypeFormService} from '@app/shared/components/room-type-form/helpers/room-type-form.service';
import {Utils} from '@app/shared/helpers/utils';
import {FacilityModel} from '@app/shared/models/common/facility.model';
import {NotificationService} from '@app/shared/services/notification/notification.service';

@Component({
  selector: 'app-room-type-form',
  templateUrl: './room-type-form.component.html',
  styleUrls: ['./room-type-form.component.scss']
})
export class RoomTypeFormComponent implements OnInit, OnDestroy {
  isLoading = false;
  roomType = new RoomTypeModel();

  @Input() roomTypeId: number = null;
  @Input() hotelId: number;
  @Output() onClose = new EventEmitter();

  formNavigator: FormNavigatorModel = new FormNavigatorModel();
  steps = [
    {
      title: 'CREATE_ROOM_TYPE.COMMON_INFO',
      step: 0
    },
    {
      title: 'CREATE_ROOM_TYPE.BEDS',
      step: 1
    },
    {
      title: 'CREATE_ROOM_TYPE.FACILITIES',
      step: 2
    },
    {
      title: 'CREATE_ROOM_TYPE.GALLERY',
      step: 3
    }
  ];

  roomTypeSub: Subscription = null;
  roomTypeSaveSub: Subscription = null;

  selectedImages: number[] = [];

  constructor(private roomTypeFormService: RoomTypeFormService,
              private notificationService: NotificationService) {
    this.formNavigator.currentStep = 0;
    this.formNavigator.items = this.steps;
  }

  ngOnInit() {
    if (this.roomTypeId != null) {
      this.isLoading = true;
      this.roomTypeSub = this.roomTypeFormService.getRoomType(this.hotelId, this.roomTypeId).subscribe(res => {
        this.roomType = res;
        this.isLoading = false;
      });
    }
  }

  ngOnDestroy() {
    if (this.roomTypeSub != null) {
      this.roomTypeSub.unsubscribe();
    }
    if (this.roomTypeSaveSub != null) {
      this.roomTypeSaveSub.unsubscribe();
    }
  }

  isVisible(value: number): boolean {
    return this.formNavigator.currentStep === value;
  }

  onGeneralInfoSubmit(roomType: RoomTypeModel) {
    this.roomType = roomType;
    this.formNavigator.currentStep = 1;
  }

  onBedsSubmit(roomType: RoomTypeModel) {
    this.roomType = roomType;
    this.formNavigator.currentStep = 2;
  }

  onFacilitiesSubmit(roomType: RoomTypeModel) {
    this.roomType = roomType;
    this.formNavigator.currentStep = 3;
  }

  onGallerySubmit(images: number[]) {
    if (images.length === 0) {
      this.roomType.images.forEach(image => {
        // @ts-ignore
        images.push(image.id);
      });
    }
    this.roomType.images = [];
    this.roomType.images.push(...images);

    if (!Utils.isExist(this.roomType.standard_price)) {
      this.roomType.standard_price = 0;
    }
    if (!Utils.isExist(this.roomType.quantity)) {
      this.roomType.quantity = 0;
    }

    const tempFacilities: number[] | FacilityModel[] = [];
    this.roomType.facilities.forEach((facility) => {
      if (Utils.isExist(facility.id)) {
        tempFacilities.push(facility.id);
      } else {
        facility.push(facility);
      }
    });
    this.roomType.facilities = [];
    tempFacilities.forEach(item => {
      this.roomType.facilities.push(item);
    });

    this.isLoading = true;
    if (Utils.isExist(this.roomType.id)) {
      this.roomTypeSaveSub = this.roomTypeFormService.editRooType(this.hotelId, this.roomType).subscribe(res => {
        this.isLoading = false;
        this.notificationService.successMessage('Данные успешно сохранены');
        this.onClose.emit();
      }, error => this.onErrorResponse());
      // update
    } else {
      this.roomTypeSaveSub = this.roomTypeFormService.createRoomType(this.hotelId, this.roomType).subscribe(res => {
        this.isLoading = false;
        this.notificationService.successMessage('Данные успешно сохранены');
        this.onClose.emit();
      }, error => this.onErrorResponse());
      // create
    }
  }

  onBackPress(toStep: number) {
    this.formNavigator.currentStep = toStep;
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

}
