import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RoomTypeModel} from '@app/shared/models/room';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';

@Injectable({
  providedIn: 'root'
})
export class RoomTypeFormService {

  constructor(private roomsService: RoomsService) {
  }

  getRoomType(hotelId: number, roomTypeId: number): Observable<RoomTypeModel> {
    return this.roomsService.getRoomInfoById(hotelId, roomTypeId);
  }

  createRoomType(hotelId: number, roomType: RoomTypeModel): Observable<RoomTypeModel> {
    return this.roomsService.createRoomsType(hotelId, roomType);
  }

  editRooType(hotelId: number, roomType: RoomTypeModel): Observable<RoomTypeModel> {
    return this.roomsService.editRoomInfo(hotelId, roomType);
  }
}
