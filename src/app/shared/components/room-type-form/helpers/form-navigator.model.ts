export class FormNavigatorModel {
  currentStep: number;
  navigationType?: string;
  isDone: boolean;
  items: NavItemModel[];
}

class NavItemModel {
  title: string;
  step: number;
}
