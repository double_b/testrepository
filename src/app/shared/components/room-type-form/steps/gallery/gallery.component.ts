import {Component, EventEmitter, Input, Output} from '@angular/core';
import {RoomTypeModel} from '@app/shared/models/room';

@Component({
  selector: 'app-room-type-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {
  isLoading = false;
  photoManagerInputSelected = [];
  selectedImages: number[] = [];

  @Input() roomType = new RoomTypeModel();
  @Output() onSubmit = new EventEmitter<number[]>();
  @Output() onBackPress = new EventEmitter<number>();

  submit() {
    this.onSubmit.emit(this.selectedImages);
  }

  back() {
    this.onBackPress.emit(2);
  }

}
