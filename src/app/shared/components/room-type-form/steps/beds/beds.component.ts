import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {BedValidateModel} from '@app/shared/components/room-type-form/steps/beds/bed-validate.model';
import {SimpleBedModel} from '@app/shared/components/room-type-form/steps/beds/simple-bed.model';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {Subscription} from 'rxjs';
import {BedModel, RoomTypeModel} from '@app/shared/models/room';
import {Utils} from '@app/shared/helpers/utils';

@Component({
  selector: 'app-room-type-beds',
  templateUrl: './beds.component.html',
  styleUrls: ['./beds.component.scss']
})
export class BedsComponent implements OnInit, OnDestroy {
  isLoading = false;
  bedErrors: BedValidateModel[] = [];
  selectedBeds: SimpleBedModel[] = [];

  commonBeds: BedModel[] = [];

  bedSubscription: Subscription = null;

  altBedsIncluded = false;
  altBedErrors: BedValidateModel[] = [];
  altSelectedBeds: SimpleBedModel[] = [];

  @Input() roomType = new RoomTypeModel();
  @Output() onSubmit = new EventEmitter<RoomTypeModel>();
  @Output() onBackPress = new EventEmitter<number>();

  constructor(private extrasService: ExtrasService) {
  }

  ngOnInit() {
    this.isLoading = true;
    // Получаю список beds
    this.bedSubscription = this.extrasService.getBeds().subscribe(bedsNameList => {
      this.commonBeds = bedsNameList;
      if (this.roomType.mainBeds && this.roomType.mainBeds.length > 0) {
        this.roomType.mainBeds.forEach(bed => {
          const temp = new SimpleBedModel();
          temp.id = bed.id;
          temp.count = bed.count;
          this.selectedBeds.push(temp);
          this.bedErrors.push(new BedValidateModel());
        });
      }
      if (this.roomType.altBeds && this.roomType.altBeds.length > 0) {
        this.roomType.altBeds.forEach(bed => {
          const temp = new SimpleBedModel();
          temp.id = bed.id;
          temp.count = bed.count;
          this.altSelectedBeds.push(temp);
          this.altBedErrors.push(new BedValidateModel());
        });
      }
      if (this.altSelectedBeds.length > 0) {
        this.altBedsIncluded = true;
      }
      if (this.selectedBeds.length === 0) {
        this.addBed();
      }
      this.isLoading = false;
    });
  }

  ngOnDestroy() {
    if (this.bedSubscription != null) {
      this.bedSubscription.unsubscribe();
    }
  }

  submit() {
    if (!this.checkValidate()) {
      return;
    }
    this.roomType.mainBeds = [];
    this.selectedBeds.forEach(bed => {
      const temp = new BedModel();
      temp.id = bed.id;
      temp.count = bed.count;
      this.roomType.mainBeds.push(temp);
    });
    this.roomType.altBeds = [];
    this.altSelectedBeds.forEach(bed => {
      const temp = new BedModel();
      temp.id = bed.id;
      temp.count = bed.count;
      this.roomType.altBeds.push(temp);
    });
    this.onSubmit.emit(this.roomType);
  }

  back() {
    this.onBackPress.emit(0);
  }

  checkValidate(): boolean {
    let result = true;

    this.selectedBeds.forEach((bed, i) => {
      if (!Utils.isExist(bed.id)) {
        result = false;
        this.bedErrors[i].invalidName();
      }
      if (!Utils.isExist(bed.count)) {
        result = false;
        this.bedErrors[i].invalidCount();
      }
    });

    if (this.altBedsIncluded) {
      this.altSelectedBeds.forEach((bed, i) => {
        if (!Utils.isExist(bed.id)) {
          result = false;
          this.altBedErrors[i].invalidName();
        }
        if (!Utils.isExist(bed.count)) {
          result = false;
          this.altBedErrors[i].invalidCount();
        }
      });
    }

    return result;
  }

  addBed(isAltBed: boolean = false) {
    if (isAltBed) {
      this.altSelectedBeds.push(new SimpleBedModel());
      this.altBedErrors.push(new BedValidateModel());
    } else {
      this.selectedBeds.push(new SimpleBedModel());
      this.bedErrors.push(new BedValidateModel());
    }
  }

  removeBed(inx: number, isAltBed: boolean = false) {
    if (isAltBed) {
      this.altSelectedBeds.splice(inx, 1);
      this.altBedErrors.splice(inx, 1);
      if (this.altSelectedBeds.length === 0) {
        this.altBedsIncluded = false;
      }
    } else {
      this.selectedBeds.splice(inx, 1);
      this.bedErrors.splice(inx, 1);
    }
  }

  isSelectedBed(commonBedId: number, isAltBed: boolean = false): boolean {
    if (isAltBed) {
      return this.altSelectedBeds.some(item => item.id === commonBedId);
    } else {
      return this.selectedBeds.some(item => item.id === commonBedId);
    }
  }

  onAltBedSwitch() {
    if (this.altBedsIncluded) {
      this.addBed(true);
    } else {
      this.altBedErrors = [];
      this.altSelectedBeds = [];
    }
  }

}
