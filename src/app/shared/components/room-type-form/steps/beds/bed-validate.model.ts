export class BedValidateModel {
  nameError = '';
  countError = '';

  invalidCount(value: number = 0) {
    this.countError = value > 0 ? '' : 'error';
  }

  invalidName(value: number = 0) {
    this.nameError = value > 0 ? '' : 'error';
  }
}
