import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {forkJoin, Subscription} from 'rxjs';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {FacilityGroupModel} from '@app/shared/models/common/facility-group.model';
import {FacilityModel} from '@app/shared/models/common/facility.model';
import {RoomTypeModel} from '@app/shared/models/room';

@Component({
  selector: 'app-room-type-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss']
})
export class FacilitiesComponent implements OnInit, OnDestroy {
  isLoading = false;
  facilityGroups: FacilityGroupModel[] = [];
  facilitiesList: FacilityModel[] = [];
  extrasSub: Subscription = null;

  @Input() roomType = new RoomTypeModel();
  @Output() onSubmit = new EventEmitter<RoomTypeModel>();
  @Output() onBackPress = new EventEmitter<number>();

  constructor(private extrasService: ExtrasService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.extrasSub = forkJoin([
      this.extrasService.getFacilityGroups(),
      this.extrasService.getFacility()
    ]).subscribe(res => {
      // Копия масива не изменяя входящий
      this.facilityGroups = res[0];
      this.facilitiesList = res[1];
      this.facilityGroups.forEach(group => {
        group.facilities = [];
        this.facilitiesList.forEach(facility => {
          if (group.id === facility.facility_group_id) {
            group.facilities.push(facility);
          }
        });
      });
      if (this.roomType.facilities && this.roomType.facilities.length > 0) {
        this.roomType.facilities.forEach(facility => {
          this.facilityGroups.forEach(group => {
            if (group.id === facility.facility_group_id) {
              group.facilities.find(item => item.id === facility.id).checked = true;
            }
          });
        });
      }
      this.isLoading = false;
    });
  }

  ngOnDestroy() {
    if (this.extrasSub != null) {
      this.extrasSub.unsubscribe();
    }
  }

  submit() {
    this.roomType.facilities = [];
    this.facilityGroups.forEach(group => {
      group.facilities.forEach(facility => {
        if (facility.checked === true) {
          // @ts-ignore
          this.roomType.facilities.push(facility);
        }
      });
    });
    this.onSubmit.emit(this.roomType);
  }

  back() {
    this.onBackPress.emit(1);
  }

}
