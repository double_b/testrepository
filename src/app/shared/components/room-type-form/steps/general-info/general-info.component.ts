import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {GeneralInfoValidateModel} from '@app/shared/components/room-type-form/steps/general-info/general-info-validate.model';
import {forkJoin, Subscription} from 'rxjs';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {PropertyTypesModel} from '@app/shared/models/property-types.model';
import {RoomTypeModel} from '@app/shared/models/room';
import {RoomNameModel} from '@app/shared/components/room-type-form/steps/general-info/room-name.model';
import {Utils} from '@app/shared/helpers/utils';

@Component({
  selector: 'app-room-type-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.scss']
})
export class GeneralInfoComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('scrollFrame', {static: false}) scrollFrame: ElementRef;
  private scrollContainer: any;
  isLoading = true;
  selectedRoomTypeId: number = null;
  roomGroupName: PropertyTypesModel[] = [];
  roomNamesList: PropertyTypesModel[] = [];
  roomNames: PropertyTypesModel[] = [];

  @Input() roomType = new RoomTypeModel();
  @Output() onSubmit = new EventEmitter<RoomTypeModel>();

  validateState = new GeneralInfoValidateModel();

  extrasSub: Subscription = null;

  enteredRoomNames: RoomNameModel[] = [];


  constructor(private extrasService: ExtrasService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.isLoading = true;
    // Получаю типы и названия номеров
    this.extrasSub = forkJoin([
      this.extrasService.getGroupsRoom(),
      this.extrasService.getRoomNames()
    ]).subscribe(res => {
      this.isLoading = false;
      this.roomGroupName = res[0];
      this.roomNamesList = res[1];
      this.roomType.max_children = 0;
      if (Utils.isExist(this.roomType.id)) {
        this.initEditMode();
      }
    }, error => this.onErrorResponse());
  }

  ngAfterViewInit() {
    this.scrollContainer = this.scrollFrame.nativeElement;
  }

  ngOnDestroy() {
    if (this.extrasSub != null) {
      this.extrasSub.unsubscribe();
    }
  }

  initEditMode() {
    const roomName = this.roomNamesList.find(name => name.id === this.roomType.room_name_id);
    this.selectedRoomTypeId = this.roomGroupName.find(group => group.id === roomName.room_name_group_id).id;
    this.onRoomTypeChange();

    this.roomType.area = !Utils.isExist(this.roomType.area) || this.roomType.area === 0 ? null : this.roomType.area;
    this.roomType.standard_price =
      !Utils.isExist(this.roomType.standard_price) || this.roomType.standard_price === 0 ? null : this.roomType.standard_price;
    this.roomType.max_adults =
      !Utils.isExist(this.roomType.max_adults) || this.roomType.max_adults === 0 ? null : this.roomType.max_adults;
    this.roomType.max_children =
      !Utils.isExist(this.roomType.max_children) || this.roomType.max_children === 0 ? 0 : this.roomType.max_children;
    this.roomType.quantity = !Utils.isExist(this.roomType.quantity) || this.roomType.quantity === 0 ? null : this.roomType.quantity;

    this.roomType.rooms.forEach(room => {
      const tempRoom = new RoomNameModel();
      tempRoom.prefix = this.getPrefix();
      if (!room.name.includes(' ')) {
        const name = Number(room.name);
        if (!isNaN(name)) {
          tempRoom.num = name.toString();
        }
      } else {
        tempRoom.num = room.name.split(' ')[1];
      }
      tempRoom.name = tempRoom.prefix + ' ' + tempRoom.num;
      this.enteredRoomNames.push(tempRoom);
    });

    if (this.roomType.room_names && this.roomType.room_names.length > this.roomType.rooms.length) {
      const different = this.roomType.room_names.length - this.roomType.rooms.length;
      for (let i = this.roomType.room_names.length - different; i < this.roomType.room_names.length; i++) {
        const tempRoom = new RoomNameModel();
        tempRoom.prefix = this.getPrefix();
        tempRoom.num = this.roomType.room_names[i].split(' ')[1];
        tempRoom.name = tempRoom.prefix + ' ' + tempRoom.num;
        this.enteredRoomNames.push(tempRoom);
      }
    }
  }

  submit() {
    if (!this.checkValid()) {
      this.scrollContainer.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
      return;
    }
    this.roomType.room_names = [];
    this.enteredRoomNames.forEach(item => {
      this.roomType.room_names.push(item.name);
    });
    this.onSubmit.emit(this.roomType);
  }

  onRoomTypeChange() {
    this.validateState.invalidRoomType(this.selectedRoomTypeId);
    this.roomNames = this.roomNamesList.filter(group => group.room_name_group_id === this.selectedRoomTypeId);
  }

  onRoomTypeNameChange() {
    this.validateState.invalidRoomName(this.roomType.room_name_id);
    this.roomType.quantity = null;
    this.enteredRoomNames = [];
  }

  onQuantityChange() {
    if (this.roomType.quantity === null) {
      return;
    }
    this.enteredRoomNames = [];
    this.validateState.invalidQuantity(this.roomType.quantity);

    for (let i = 0; i < this.roomType.quantity; i++) {
      const room = new RoomNameModel();
      room.prefix = this.getPrefix();
      room.name = this.getPrefix();
      this.enteredRoomNames.push(room);
    }
  }

  onRoomNameChange(inx: number) {
    const num = this.enteredRoomNames[inx].num === null ? '' : this.enteredRoomNames[inx].num;
    this.enteredRoomNames[inx].name = `${this.enteredRoomNames[inx].prefix} ${num}`;
    this.enteredRoomNames[inx].error = false;
  }

  removeEnteredName(inx: number = null) {
    inx = inx == null ? this.enteredRoomNames.length - 1 : inx;
    this.enteredRoomNames.splice(inx, 1);
    this.roomType.quantity = this.enteredRoomNames.length;
    if (this.enteredRoomNames.length === 0) {
      this.roomType.quantity = null;
    }
  }

  getPrefix(): string {
    return this.roomNamesList.find(item => item.id === this.roomType.room_name_id).short_name;
  }

  onErrorResponse() {
    this.isLoading = false;
    this.notificationService.errorMessage('Произошла неизвестная ошибка', 'Пожалуйста, попробуйте позже');
  }

  checkValid(): boolean {
    let result = true;

    if (!Utils.isExist(this.selectedRoomTypeId)) {
      this.validateState.invalidRoomType();
      result = false;
    }

    if (!Utils.isExist(this.roomType.room_name_id)) {
      this.validateState.invalidRoomName();
      result = false;
    }

    if (!Utils.isExist(this.roomType.max_adults) || this.roomType.max_adults === 0) {
      this.validateState.invalidAdultsCount();
      result = false;
    }

    if (!Utils.isExist(this.roomType.max_children)) {
      this.validateState.invalidChildrenCount();
      result = false;
    }

    if (!Utils.isExist(this.roomType.area) || this.roomType.area === 0) {
      this.validateState.invalidRoomArea();
      result = false;
    }

    if (!Utils.isExist(this.roomType.quantity) || this.roomType.quantity === 0) {
      this.validateState.invalidQuantity();
      result = false;
    }

    if (this.enteredRoomNames.length > 0) {
      this.enteredRoomNames.forEach(item => {
        if (!Utils.isExist(item.num)) {
          result = false;
          item.error = true;
        }
      });
    }

    return result;
  }

}
