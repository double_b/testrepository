export class RoomNameModel {
  prefix: string;
  num: string;
  name: string;
  error: boolean;
}
