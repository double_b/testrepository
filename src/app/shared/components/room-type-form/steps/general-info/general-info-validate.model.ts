import {Utils} from '@app/shared/helpers/utils';

export class GeneralInfoValidateModel {
  roomTypeError = '';
  roomNameError = '';
  roomArea = '';
  adultsCount = '';
  childrenCount = '';
  quantity = '';

  invalidRoomType(value: any = null) {
    this.roomTypeError = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }

  invalidRoomName(value: any = null) {
    this.roomNameError = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }

  invalidRoomArea(value: any = null) {
    this.roomArea = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }

  invalidAdultsCount(value: any = null) {
    this.adultsCount = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }

  invalidChildrenCount(value: any = null) {
    this.childrenCount = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }

  invalidQuantity(value: any = null) {
    this.quantity = !Utils.isExist(value) || '' || value === 0 ? 'error' : '';
  }
}
