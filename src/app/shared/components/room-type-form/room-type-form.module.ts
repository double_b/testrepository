import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FacilitiesComponent} from './steps/facilities/facilities.component';
import {GalleryComponent} from './steps/gallery/gallery.component';
import {BedsComponent} from './steps/beds/beds.component';
import {GeneralInfoComponent} from './steps/general-info/general-info.component';
import {RoomTypeFormComponent} from '@app/shared/components/room-type-form/room-type-form.component';
import {SharedModule} from '@app/shared/shared.module';


@NgModule({
  declarations: [FacilitiesComponent, GalleryComponent, BedsComponent, GeneralInfoComponent, RoomTypeFormComponent],
  exports: [
    RoomTypeFormComponent
  ],
  imports: [
    SharedModule,
    CommonModule
  ]
})
export class RoomTypeFormModule {
}
