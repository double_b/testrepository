import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// Models
import {environment} from '@src/environments/environment';

// Model
import {GalleryHotelModel} from '@app/shared/models/gallery-hotel.model';
import {BaseResponseModel} from '@app/shared/models/base-response.model';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {
  // Environments
  DEV_ENV = environment;

  constructor(private http: HttpClient) {
  }

  //  Получаю галерею фото отеля
  getGalleryHotel(hotelId: number): Observable<GalleryHotelModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/gallery`;
    return this.http.get<BaseResponseModel<GalleryHotelModel[]>>(url)
      .pipe(map((res) => res.data));
  }

  // Загружаю фото
  uploadPhoto(hotelId: number, file: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/gallery`;
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post<BaseResponseModel<any>>(url, formData).pipe(
      map((res) => res.data));
  }

  // Изменить фото
  changeImage(hotelId: number, imageId: number, image_name_id: string): Observable<GalleryHotelModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/gallery/${imageId}`;
    return this.http.put<BaseResponseModel<GalleryHotelModel>>(url, image_name_id)
      .pipe(map((res) => res.data));
  }

  // Удолить фото
  deletePhoto(hotelId: number, imageId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/gallery/${imageId}`;
    return this.http.delete(url);
  }

  getImagesUrl(hotelId: number): string {
    return `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/gallery`;
  }
}
