import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatusApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  confirmReservation(hotelID: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/bookings/${bookingId}/confirm`;
    return this.httpClient.patch(url, null).pipe(
      map(_ => _)
    );
  }

  cancelReservation(hotelID: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/bookings/${bookingId}/cancel`;
    return this.httpClient.patch(url, null).pipe(
      map(_ => _)
    );
  }

  arrivedReservation(hotelID: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/reservation-room-living/${bookingId}/mass-arrived`;
    return this.httpClient.post(url, null).pipe(
      map(_ => _)
    );
  }

  leftReservation(hotelID: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/reservation-room-living/${bookingId}/mass-left`;
    return this.httpClient.post(url, null).pipe(
      map(_ => _)
    );
  }

  noShowReservation(hotelID: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/bookings/${bookingId}/no-show`;
    return this.httpClient.patch(url, null).pipe(
      map(_ => _)
    );
  }
}
