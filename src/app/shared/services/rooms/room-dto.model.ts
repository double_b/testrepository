import {Beds} from '@app/shared/models/rooms-types-list.model';

export class RoomDtoModel {
  constructor(
  public room_name_id: number,
  public max_persons: number,
  public max_children: number,
  public quantity: number,
  public room_names: string[],
  public standard_price: number,
  public area: number,
  public smooking: number,
  public mainBeds: Beds[],
  public altBeds: Beds[],
  public facilities: number[],
  public images: number[],
  ) {
  }
}
