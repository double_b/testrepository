import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// Environments
import {environment} from '@src/environments/environment';

// Models
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {RoomTypeModel} from '@app/shared/models/room';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {RoomInventoryModel} from '@app/shared/models/room/room-inventory.model';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  // Environments
  DEV_ENV = environment;
  baseURL = this.DEV_ENV.baseURL + '/hotelier';

  constructor(private http: HttpClient) {
  }

  getRoomsAvailableForBooking(hotelId: number, roomType: number, checkinDate: Date, checkoutDate: Date): Observable<any[]> {
    const checkin = Utils.convertDateToQuery(checkinDate);
    const checkout = Utils.convertDateToQuery(checkoutDate);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/rooms/available?room_type_id=${roomType}&checkin=${checkin}&checkout=${checkout}`;
    return this.http.get<BaseResponseModel<any[]>>(url)
      .pipe(map(response => response.data));
  }

  // Список комнат
  getRoomsTypeList(hotelId: number): Observable<RoomsTypesListModel[]> {
    return this.http.get<BaseResponseModel<RoomsTypesListModel[]>>(`${this.baseURL}/${hotelId}/room-types`)
      .pipe(map((res) => res.data));
  }

  // Получить комнату по id
  getRoomInfoById(hotelId = null, roomId = null): Observable<any> {
    return this.http.get<{ data: any }>(`${this.baseURL}/${hotelId}/room-types/${roomId}`)
      .pipe(map(({data}) => data));
  }

  // Изменить комнату
  editRoomInfo(hotelId = null, roomData: RoomTypeModel): Observable<RoomTypeModel> {
    return this.http.put<BaseResponseModel<RoomTypeModel>>(`${this.baseURL}/${hotelId}/room-types/${roomData.id}`, roomData)
      .pipe(map((res) => res.data));
  }

  // Создание комнаты
  createRoomsType(hotelId: number, typeData: RoomTypeModel): Observable<RoomTypeModel> {
    return this.http.post<BaseResponseModel<RoomTypeModel>>(`${this.baseURL}/${hotelId}/room-types?`, typeData)
      .pipe(map((res) => res.data));
  }

  // изменить доступность номеров
  changeRoomInventory(hotelId: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/inventory`;
    return this.http.post<any>(url, inventory).pipe(map(res => res));
  }

  // изменить доступность номеров
  openRoom(hotelId: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/inventory/open`;
    return this.http.post<any>(url, inventory).pipe(map(res => res));
  }

  // изменить доступность номеров
  closeRoom(hotelId: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/inventory/close`;
    return this.http.post<any>(url, inventory).pipe(map(res => res));
  }

  // изменить мин срок бронирования до заезда
  updateMinAdvance(hotelId: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/min-advance-res`;
    return this.http.post<any>(url, inventory).pipe(map(res => res));
  }

  // изменить мин срок бронирования проживания
  updateMinStay(hotelId: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/min-stay`;
    return this.http.post<any>(url, inventory).pipe(map(res => res));
  }

  deleteRoomType(hotelID, roomId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelID}/room-types/${roomId}`;
    return this.http.delete(url);
  }
}
