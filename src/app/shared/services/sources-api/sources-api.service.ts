import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Observable} from 'rxjs';
import {SourceModel} from '@app/shared/models/source.model';
import {map} from 'rxjs/operators';
import {BaseResponseModel} from '@app/shared/models/base-response.model';

@Injectable({
  providedIn: 'root'
})
export class SourcesApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getSources(propertyId: number): Observable<SourceModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/sources`;
    return this.httpClient.get<BaseResponseModel<SourceModel[]>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  createSource(source: SourceModel, propertyId: number): Observable<SourceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/sources`;
    return this.httpClient.post<BaseResponseModel<SourceModel>>(url, source).pipe(
      map((res) => res.data)
    );
  }

  editSource(source: SourceModel, propertyId: number): Observable<SourceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/sources/${source.id}`;
    return this.httpClient.put<BaseResponseModel<SourceModel>>(url, source).pipe(
      map((res) => res.data)
    );
  }

  deleteSource(sourceId: number, propertyId: number): Observable<SourceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/sources/${sourceId}`;
    return this.httpClient.delete<any>(url).pipe(
      map((res) => res)
    );
  }
}
