import {Injectable} from '@angular/core';
import {TariffPlansService} from '@app/layouts/tariff-plans/tariff-plans.service';
import {NavBarService} from '@app/shared/components/nav-bar/nav-bar.service';
import {KeysEnum} from '@app/core/constants/keys.enum';

@Injectable({
  providedIn: 'root'
})
export class CleanerService {

  constructor(private tariffPlansService: TariffPlansService,
              private navBarService: NavBarService) {
  }


  cleanAll() {
    this.navBarService.clean();
    localStorage.removeItem(KeysEnum.GET_HOTEL);
    localStorage.removeItem(KeysEnum.HOTEL_ID);
    localStorage.removeItem(KeysEnum.HOTELS);
    localStorage.removeItem(KeysEnum.EMAIL);
    localStorage.removeItem(KeysEnum.USER_TOKEN);
  }

  clean(serviceName: string) {
    this[serviceName].clean();
  }
}
