import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {ChannelModel, CreateChanelModel, RoomTypesForChannelModel} from '@app/shared/models/channel.model';

@Injectable({
  providedIn: 'root'
})
export class ChannelApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getChannelsList(propertyId): Observable<ChannelModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels`;
    return this.httpClient.get<BaseResponseModel<ChannelModel[]>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  getChannelsAvailable(propertyId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/available`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  addChannel(propertyId, body: CreateChanelModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/`;
    return this.httpClient.post<BaseResponseModel<any>>(url, body).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  updateAuthyData(propertyId, channelId, body: CreateChanelModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, body).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  viewChannel(propertyId, channelId): Observable<ChannelModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}`;
    return this.httpClient.get<BaseResponseModel<ChannelModel>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  checkAuthyChannel(propertyId, channelId, body): Observable<ChannelModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}/auth`;
    return this.httpClient.patch<BaseResponseModel<ChannelModel>>(url, body).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  getRoomTypesUrl(propertyId): Observable<RoomTypesForChannelModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/room-types`;
    return this.httpClient.get<BaseResponseModel<RoomTypesForChannelModel[]>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  stepConnectChannel(propertyId, channelId, body: CreateChanelModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}/connect`;
    return this.httpClient.post<BaseResponseModel<any>>(url, body).pipe(
      map((res) => {
        return res;
      })
    );
  }

  patchChannel(propertyId, channelId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}/sync`;
    return this.httpClient.patch<BaseResponseModel<any>>(url, {}).pipe(
      map((res) => {
        return res;
      })
    );
  }

  deleteChannel(propertyId, channelId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res;
      })
    );
  }

  stepUpdateChannel(propertyId, channelId, body: CreateChanelModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/channels/${channelId}/mapping`;
    return this.httpClient.put<BaseResponseModel<any>>(url, body).pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
