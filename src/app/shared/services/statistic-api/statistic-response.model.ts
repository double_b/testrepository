export class StatisticResponseModel<T> {
  data: T;
  message?: string;
}

export class ConfirmedReservationsModel {
  room_nights: number;
  reservations_count: number;
  revenue: number;
  reservations: Reservation[] = [];
}

export class DataReservations {
  current_page: number;
  data: Reservation[] = [];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string;
  to: number;
  total: number;
}

export class Reservation {
  id: number;
  property_id: number;
  type: string;
  channel: string;
  channel_booking_id: number;
  source_id: number;
  total_guests: number;
  nights: number;
  checkin: string;
  checkout: string;
  total_rooms: number;
  status: string;
  total_sum: number;
  currency: string;
  day_sum?: number;
  reservation_rooms: any[];
}
