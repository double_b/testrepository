import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Utils} from '@app/shared/helpers/utils';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ConfirmedReservationsModel, StatisticResponseModel} from '@app/shared/services/statistic-api/statistic-response.model';

@Injectable({
  providedIn: 'root'
})
export class StatisticApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  // апи для получения подтвержденных броней
  // пока что вызывается в сервисе статистики
  getConfirmedReservations(fromDate: Date, byDate: Date, propertyId: number): Observable<ConfirmedReservationsModel> {
    // конвертируем дату в формат запроса
    const from = Utils.convertDateToQuery(fromDate);
    const by = Utils.convertDateToQuery(byDate);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/dashboard/reservation-overview/reservations?from=${from}&by=${by}&sort=checkin`;
    return this.httpClient.get<StatisticResponseModel<ConfirmedReservationsModel>>(url).pipe(
      map((response) => {
        response.data.reservations.forEach(item => {
          // предварительно считаем оплату за один день
          const daySum = item.total_sum / item.nights;
          item.day_sum = Utils.round(daySum);
        });
        return response.data;
      })
    );
  }
}
