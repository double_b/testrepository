import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PriceModel} from '@app/shared/models/price.model';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class PriceApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getSearchPrice(hotelId: number, params?: HttpParams): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/prices`;
    return this.httpClient.get<any>(url, {params}).pipe(
      map((res) => res));
  }

  getSearchPriceForCompany(hotelId: number, endPoint: string, params?: HttpParams): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/${endPoint}`;
    return this.httpClient.get<any>(url, {params}).pipe(
      map((res) => res));
  }

  getPrice(hotelId: number, roomTypeId: number,
           checkinDate: Date, checkoutDate: Date,
           rateId: number, occupancy: number, local?: number): Observable<any> {
    const checkin = Utils.convertDateToQuery(checkinDate);
    const checkout = Utils.convertDateToQuery(checkoutDate);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/prices?checkin=${checkin}&checkout=${checkout}&rate_id=${rateId}&occupancy=${occupancy}&local=${local}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  getPriceByRoomType(hotelId: number, roomTypeId: number, endPoint: string, params?: HttpParams): Observable<any[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/${endPoint}`;
    return this.httpClient.get<any[]>(url, {params}).pipe(
      map((res) => res));
  }

  savePrices(hotelId: number, roomTypeId: number, price: PriceModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/prices`;
    return this.httpClient.post<any>(url, price).pipe(map((res) => res));
  }

  openPriceDay(hotelId: number, roomTypeId: number, price: PriceModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/prices/open`;
    return this.httpClient.post<any>(url, price).pipe(map((res) => res));
  }

  closePriceDay(hotelId: number, roomTypeId: number, price: PriceModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/prices/close`;
    return this.httpClient.post<any>(url, price).pipe(map((res) => res));
  }
}
