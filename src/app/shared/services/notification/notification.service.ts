import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  constructor(private toastr: ToastrService) {
  }

  successMessage(title: string, message?: string) {
    this.toastr.success(message ? message : '', title);
  }

  errorMessage(title, message?: string) {
    this.toastr.error(message ? message : '', title);
  }

  warningMessage(title: string, message?: string) {
    this.toastr.warning(message ? message : '', title);
  }

}
