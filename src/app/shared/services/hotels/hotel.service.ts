import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

// Environments
import {environment} from '@src/environments/environment';

// Models
import {RegisterHotelDtoModel} from '@app/shared/services/hotels/register-hotel-dto.model';
import {HotelModel} from '@app/shared/models/hotel.model';
import {BaseResponseModel} from '@app/shared/models/base-response.model';

// import {HotelModel} from '@app/core/models/hotel';


@Injectable({
  providedIn: 'root'
})
export class HotelService {

  // Environments
  DEV_ENV = environment;

  constructor(private http: HttpClient) {
  }

  // Список всех объектов размещения
  list(): Observable<HotelModel[]> {
    return this.http.get<BaseResponseModel<HotelModel[]>>(`${this.DEV_ENV.baseURL}/hotelier/properties?`)
      .pipe(map((res) => res.data));
  }

  // Получаю properties объекта размещения
  /**
   * Возвращает список объектов размещения.
   * Параметры запроса
   * name | string | Поиск по названию
   * country | int | Фильтр по id страны
   * city | int | Фильтр по id города
   * services | array | Фильтр по услугам. Массив с id услуг
   * active | boolean | Фильтрация активныx/заблокированных.
   * with | string | Загрузить отношения: `images` - все изображения
   * sort | string | Сортировка по полю: `id`,
   * `name`. Для сортировки в обратном порядке нужно постаивть знак `-` перед название поля, например `-id`
   */
  getHotelByProperties(hotel_id: number): Observable<HotelModel> {
    if (!hotel_id) {
      Error(`Must have hotel id`);
      return;
    }
    return this.http.get<{ data: HotelModel[] }>(`${this.DEV_ENV.baseURL}/hotelier/properties?=`)
      .pipe(
        map(({data}) => data.find(prop => String(prop.id) === String(hotel_id) ? prop : null)),
      );
  }

  // Получаю объект размещения по ID
  getHotelByID(hotel_id: number): Observable<HotelModel> {
    if (!hotel_id) {
      Error(`Must have hotel id`);
      return;
    }
    return this.http.get<BaseResponseModel<HotelModel>>(`${this.DEV_ENV.baseURL}/hotelier/${hotel_id}`)
      .pipe(map((res) => res.data));
  }

  // Регистрация нового объекта
  register(newData: RegisterHotelDtoModel): Observable<HotelModel> {
    return this.http.post<{ data: HotelModel }>(`${this.DEV_ENV.baseURL}/hotelier/properties`, newData)
      .pipe(map(({data}) => data));
  }

  // Регистрация нового объекта
  registerOverride(newData: HotelModel): Observable<HotelModel> {
    return this.http.post <BaseResponseModel<HotelModel>>(`${this.DEV_ENV.baseURL}/hotelier/properties`, newData)
      .pipe(map((res) => res.data));
  }

  // Информация об объекте
  accommodation(id): Observable<HotelModel> {
    return this.http.get<{ data: HotelModel }>(`${this.DEV_ENV.baseURL}/hotelier/${id}`)
      .pipe(
        map(({data}) => data),
        tap((data) => localStorage.setItem('getHotel', JSON.stringify(data)))
      );
  }

  // Изменения объекта
  change(id, dataHotel: RegisterHotelDtoModel): Observable<any> {
    return this.http.put<{ data: RegisterHotelDtoModel }>(`${this.DEV_ENV.baseURL}/hotelier/${id}`, dataHotel)
      .pipe(map(({data}) => data));
  }

  // Изменения объекта
  changeOverride(id, dataHotel: HotelModel): Observable<any> {
    return this.http.put<{ data: RegisterHotelDtoModel }>(`${this.DEV_ENV.baseURL}/hotelier/${id}`, dataHotel)
      .pipe(map(({data}) => data));
  }

}
