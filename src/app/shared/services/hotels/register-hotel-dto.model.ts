/**
 * @param {string} city_id, город
 * @param {number} property_type_id, тип отеля
 * @param {string} name, название отеля
 * @param {string} address, адрес отеля
 * @param {any} city, адрес отеля
 * @param {number} stars, уровень отеля 1-5 звезд
 * @param {number} id, id отеля
 * @param {number} user_id, id пользавателя
 * @param {string} contact_person, имя контактного лица
 * @param {string} phone, номер телефона контактного лица
 * @param {string} phone2, номер телефона контактного лица
 * @param {string} description, описание отеля
 * @param {string} site, сайт отеля
 * @param {string} active,
 * @param {string, null} geocode_lat, геолокация отеля
 * @param {string, null} geocode_lng, геолокация отеля
 * @param {string, null} zip_code, геолокация отеля
 * @param {string, null} checkin_start, время заезда в номер
 * @param {string, null} checkin_end, время заезда в номер
 * @param {string, null} checkout_start, время выезда
 * @param {string, null} checkout_end, время выезда
 * @param {number[], null} services, услуги
 * @param {number[], null} languages, языки
 * @param {number} step, шаг регистраци
 * @param {string} status, статус регистраци registration | published
 * @param {any} image,
 * @param {number[]} images, фотографии отеля
 * @param {string} deleted_at,
 * @param {string} created_at,
 * @param {string} updated_at,
 * @param {any[]} translations,
 * @param {any[]} tempTranslations,
 * @param {number} rooms_count,
 * @param {any} rooms_count,
 */
export interface RegisterHotelDtoModel {
  city_id: number;
  property_type_id: number;
  name: string;
  address: string;
  stars: number;
  contact_person: string;
  phone: string;
  geocode_lat?: string;
  geocode_lng?: string;
  zip_code?: string;
  id?: number;
  user_id?: number;
  description?: string;
  site?: string;
  active?: number;
  phone2?: string;
  checkin_start?: string;
  checkin_end?: string;
  checkout_start?: string;
  checkout_end?: string;
  services?: number[];
  languages?: number[];
  step?: number;
  status?: string;
  image?: any;
  images?: number[];
  deleted_at?: string;
  created_at?: string;
  updated_at?: string;
  translations?: any[];
  tempTranslations?: any[];
  city?: any;
  rooms_count?: number;
  propertyType?: any;
}
