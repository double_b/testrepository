import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TariffPlanModel} from '@app/shared/models/tariff-plan.model';
import {environment} from '@src/environments/environment';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {map} from 'rxjs/operators';
import {Utils} from '@app/shared/helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class TariffPlansApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  /**
   * получение списка тарифов (с фильтрацией на стороне сервера)
   * если используется сортировка - то один из видов сортировка нужно устанавливать как false
   * @param propertyId - id отеля
   * @param searchByName - название тарифа (для поиска по названию) (опционально)
   * @param sortById - сортировка по id (опционально)
   * @param sortByName - сортировка по названию (опционально)
   * @param reverseSort - для обратной сортировки (в запрос перед значением добавляется - (-id, -name)) (опционально)
   */
  getTariffPlans(propertyId: number, searchByName: string = null,
                 sortById: boolean = false,
                 sortByName: boolean = false,
                 reverseSort: boolean = false): Observable<TariffPlanModel[]> {
    // если указано название тарифа - готовим запрос по названию
    const nameQuery = Utils.isNotNull(searchByName) ? `name=${searchByName}` : null;
    let sortQuery = null;
    if (sortById || sortByName) {
      sortQuery = 'sort=';
      if (sortById) {
        sortQuery += reverseSort ? '-id' : 'id';
      } else if (sortByName) {
        sortQuery += reverseSort ? '-name' : 'name';
      }
    }
    let query = null;
    if (Utils.isNotNull(nameQuery)) {
      query = nameQuery;
    }
    if (Utils.isNotNull(sortQuery)) {
      query = Utils.isNotNull(query) ? `${query}&${sortQuery}` : sortQuery;
    }
    let url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/rates`;
    url += Utils.isNotNull(query) ? `?${query}` : '';
    return this.httpClient.get<BaseResponseModel<TariffPlanModel[]>>(url).pipe(
      map((res) => res.data)
    );
  }

  getTariffPlan(propertyId: number, planId: number): Observable<TariffPlanModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/rates/${planId}`;
    return this.httpClient.get<BaseResponseModel<TariffPlanModel>>(url).pipe(
      map((res) => res.data)
    );
  }

  createTariffPlan(propertyId: number, plan: TariffPlanModel): Observable<TariffPlanModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/rates`;
    return this.httpClient.post<BaseResponseModel<TariffPlanModel>>(url, plan).pipe(
      map(res => res.data)
    );
  }

  editTariffPlan(propertyId: number, plan: TariffPlanModel): Observable<TariffPlanModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/rates/${plan.id}`;
    return this.httpClient.put<BaseResponseModel<TariffPlanModel>>(url, plan).pipe(
      map(res => res.data)
    );
  }

  deleteTariffPlan(propertyId: number, plan: TariffPlanModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${propertyId}/rates/${plan.id}`;
    return this.httpClient.delete<any>(url).pipe(
      map(_ => _)
    );
  }
}
