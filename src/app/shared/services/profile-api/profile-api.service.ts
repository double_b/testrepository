import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProfileModel} from '@app/shared/models/profile.model';
import {environment} from '@src/environments/environment';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getProfileData(): Observable<ProfileModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/profile`;
    return this.httpClient.get<BaseResponseModel<ProfileModel>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  updateProfileData(body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/profile/update`;
    return this.httpClient.put(url, body);
  }

  updateEmail(body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/profile/update-email`;
    return this.httpClient.put(url, body);
  }

  updatePassword(body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/profile/update-password`;
    return this.httpClient.put(url, body);
  }

  updateAvatar(body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/common/avatar`;
    return this.httpClient.post(url, body);
  }
}
