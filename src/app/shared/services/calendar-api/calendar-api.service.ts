import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Utils} from '@app/shared/helpers/utils';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {CalendarReservation} from "@app/core/models/calendarreservation";

@Injectable({
  providedIn: 'root'
})
export class CalendarApiService {
  // Environments
  DEV_ENV = environment;
  baseURL = this.DEV_ENV.baseURL + '/hotelier';

  constructor(private http: HttpClient, private httpClient: HttpClient) {
  }

  getCalendarListData(hotelId: number, roomTypeId: number,
                      channel: string, start?: Date,
                      end?: Date): Observable<any> {
    const from = start ? Utils.convertDateToQuery(start) : null;
    const by = end ? Utils.convertDateToQuery(end) : null;
    const url = `${this.baseURL}/${hotelId}/room-types/${roomTypeId}/calendar/${channel}${from ? `?start=${from}&end=${by}` : ''}`;
    return this.http.get<BaseResponseModel<any>>(url).pipe(map(res => res.data));
  }

  cancelReservationHold(holdId, hotelId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/${holdId}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res;
      })
    );
  }

  clearBlockDates(reservationId, hotelId): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/blocks/${reservationId}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res;
      })
    );
  }

  moveReservation(hotelId, data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/reservation-room/${data.reservationRoomId}/move-to/room/${data.roomId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, data.data).pipe(
      map((res) => {
        return res;
      })
    );
  }

  clearCloseRoom(reservationId, hotelId) {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/services/${reservationId}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res;
      })
    );
  }

  getCalendar(hotelId: any, start?: string, end?: string): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/calendar${start ? `?start_date=${start}&end_date=${end}` : ''}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  getNotes(hotelId: any, id: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/booking/${id}/notes`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  getRoomsAvailableByGroupsForBooking(hotelierId, checking, checkout, reservationRoomId, reservationId = ''): Observable<any[]> {
    console.log(hotelierId);
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelierId}/rooms/available/by-groups?&checkin=${checking}&checkout=${checkout}&reservation=${reservationId}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  blockDate(data, hotelId: any): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/blocks`;
    return this.httpClient.post<BaseResponseModel<any>>(url, data).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  updateBlockDate(data, hotelId: any, reservationId: number): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/blocks/${reservationId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, data).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  outOfService(data, hotelId: any): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/services`;
    return this.httpClient.post<BaseResponseModel<any>>(url, data).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  updateOutOfService(data, hotelId: any, reservationId: number): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/services/${reservationId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, data).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  saveRoomServiceInfo(data, hotelId: any, itemId?: number): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds/service/room/${itemId}`;
    return this.httpClient.post<BaseResponseModel<any>>(url, data).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  saveReservationHoldInfo(data, hotelId: any, holdId?: number): Observable<CalendarReservation> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/holds`;

    if (holdId) {
      return this.httpClient.put<BaseResponseModel<any>>(`${url}/${holdId}`, data).pipe(
        map((res) => {
          return res.data;
        })
      );
    } else {
      return this.httpClient.post<BaseResponseModel<any>>(url, data).pipe(
        map((res) => {
          return res.data;
        })
      );
    }
  }
}
