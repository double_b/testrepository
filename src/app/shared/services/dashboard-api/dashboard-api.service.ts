import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {OverAllStatisticModel} from '@app/core/models/dashboard/view/over-all-statistic.model';
import {ReservationRooms} from '@app/core/models/dashboard/response/reservation-overview.model';
import {ReservationForViewModel} from '@app/core/models/dashboard/view/reservation-for-view.model';
import {CancellationResponseModel, Reservation} from '@app/core/models/dashboard/response/sale-response.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {environment} from '@src/environments/environment';
import {Utils} from '@app/shared/helpers/utils';
import {GuestStatusResponseModel} from '@app/core/models/dashboard/response/guest-status-response.model';
import {StatusEnum} from '@app/core/constants/status.enum';
import {RebookingModel} from '@app/core/models/dashboard/response/rebooking.model';
import {ForecastResponseModel} from '@app/core/models/dashboard/response/forecast-response.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardApiService {
  static VIEW_MODE_ARRIVED = 'arrived';
  static VIEW_MODE_LEFT = 'left';
  static VIEW_MODE_LIVING = 'living';
  static VIEW_MODE_OVERLOAD = 'overload';

  public static MIN_ITEMS_COUNT = 5;

  public static FILTER_DAYS_COUNT = 14;

  // Environments
  DEV_ENV = environment;

  public getStatistics: Subject<any> = new Subject<any>();

  constructor(private httpClient: HttpClient) {
  }

  public static convertToViewModel(model: ReservationRooms, mode: string): ReservationForViewModel {
    const result: ReservationForViewModel = new ReservationForViewModel();
    result.id = model.id;
    result.order = model.order;
    result.firstName = model.reservation.guest != null ? model.reservation.guest.first_name : 'No name';
    result.lastName = model.reservation.guest != null ? model.reservation.guest.last_name : null;
    result.middleName = model.reservation.guest != null ? model.reservation.guest.middle_name : null;
    result.roomId = model.room_id;
    result.room = model.room;
    result.reservationId = model.reservation_id;
    result.adults = model.adults;
    result.children = model.children;
    result.nights = model.nights;
    result.sum = model.sum;
    result.notesCount = model.reservation.notes_count;
    result.currency = model.reservation.currency;
    result.roomTypeShortName = model.room_type.room_type_short_name;
    if (mode === DashboardApiService.VIEW_MODE_OVERLOAD) {
      result.guestArrivedAt = DashboardApiService.getFormattedDate(model.checkin);
      result.guestLeftAt = DashboardApiService.getFormattedDate(model.checkout);
      result.checkin = DashboardApiService.getFormattedDate(model.checkin);
      result.checkout = DashboardApiService.getFormattedDate(model.checkout);
    } else {
      result.checkin = DashboardApiService.getFormattedDate(model.checkin);
      result.checkout = DashboardApiService.getFormattedDate(model.checkout);
      result.guestArrivedAt = DashboardApiService.getFormattedDate(model.guest_arrived_at);
      result.guestLeftAt = DashboardApiService.getFormattedDate(model.guest_left_at);
      result.status = model.status;
      result.actionBtnDisabled = DashboardApiService.disableByStatus(mode, result.status);
    }
    return result;
  }

  public static convertToSaleViewModel(model: Reservation): ReservationForViewModel {
    const result: ReservationForViewModel = new ReservationForViewModel();
    result.id = model.id;
    result.order = model.order;
    result.firstName = model.guest != null ? model.guest.first_name : 'No name';
    result.lastName = model.guest != null ? model.guest.last_name : null;
    result.middleName = model.guest != null ? model.guest.middle_name : null;
    result.channel = model.channel;
    result.source = model.source?.name;
    result.guestCount = model.total_guests;
    result.nights = model.nights.toString();
    result.guestArrivedAt = new Date(model.checkin);
    result.guestLeftAt = new Date(model.checkout);
    result.roomCount = model.total_rooms;
    result.currency = model.currency;
    result.sum = model.total_sum;
    result.notesCount = model.notes_count;
    result.status = model.status;
    result.rooms = model.reservation_rooms;
    return result;
  }

  public static disableByStatus(mode: string, status: string): boolean {
    switch (mode) {
      case DashboardApiService.VIEW_MODE_ARRIVED:
      case DashboardApiService.VIEW_MODE_LIVING: {
        return status !== StatusEnum.CONFIRM;
      }
      case DashboardApiService.VIEW_MODE_LEFT: {
        return status !== StatusEnum.IN_HOUSE && status !== StatusEnum.CONFIRM;
      }
    }
  }

  public static sortByStatus(list: ReservationForViewModel[]): ReservationForViewModel[] {
    const result: ReservationForViewModel[] = [];
    const confirmed: ReservationForViewModel[] = [];
    const inHouse: ReservationForViewModel[] = [];
    const checkOut: ReservationForViewModel[] = [];
    const cancel: ReservationForViewModel[] = [];
    const noShow: ReservationForViewModel[] = [];
    for (const reservation of list) {
      switch (reservation.status) {
        case StatusEnum.CONFIRM: {
          confirmed.push(reservation);
          break;
        }
        case StatusEnum.IN_HOUSE: {
          inHouse.push(reservation);
          break;
        }
        case StatusEnum.CHECKOUT: {
          checkOut.push(reservation);
          break;
        }
        case StatusEnum.CANCEL: {
          cancel.push(reservation);
          break;
        }
        case StatusEnum.NO_SHOW: {
          noShow.push(reservation);
          break;
        }
      }
    }
    result.push(...confirmed);
    result.push(...inHouse);
    result.push(...checkOut);
    result.push(...cancel);
    result.push(...noShow);
    return result;
  }

  public static getFormattedStatus(status: string, model: ReservationForViewModel): string {
    switch (status) {
      case 'confirmed': {
        let result = null;
        if (model.guestArrivedAt === null && model.guestLeftAt === null) {
          result = StatusEnum.CONFIRM;
        }
        if (model.guestArrivedAt !== null && model.guestLeftAt === null) {
          result = StatusEnum.IN_HOUSE;
        }
        if (model.guestArrivedAt !== null && model.guestLeftAt !== null) {
          result = StatusEnum.CHECKOUT;
        }
        return result;
      }
      case 'cancelled':
      case 'canceled':
        return StatusEnum.CANCEL;
    }
  }

  public static getFormattedDate(date: string): Date {
    return date !== null ? new Date(date) : null;
  }

  public static getPropertyId(): string {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return String(hotel);
  }

  public getArrivedReservations(getFrom: Date, getBy: Date,
                                pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/arrived?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map(response => {
        return {
          reservations: response.data.data,
          total: response.data.total
        };
      })
    );
  }

  public setGuestAsArrived(reservationRoomId: number): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/reservation-room-living/${reservationRoomId}/arrived`;
    return this.httpClient.post<BaseResponseModel<GuestStatusResponseModel>>(url, null).pipe(
      map(response => {
        return response;
      })
    );
  }

  public getLeftReservations(getFrom: Date, getBy: Date,
                             pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/left?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map(response => {
        return {
          reservations: response.data.data,
          total: response.data.total
        };
      })
    );
  }

  public setGuestAsLeft(reservationRoomId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/reservation-room-living/${reservationRoomId}/left`;
    return this.httpClient.post<BaseResponseModel<GuestStatusResponseModel>>(url, null).pipe(
      map(response => {
        return response;
      })
    );
  }

  public getLivingReservations(getFrom: Date, getBy: Date,
                               pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/living?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((response) => {
        return {
          reservations: response.data.data,
          total: response.data.total
        };
      })
    );
  }

  getCancelledReservations(getFrom: Date, getBy: Date, pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/canceled?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<BaseResponseModel<CancellationResponseModel>>(url).pipe(
      map((response) => {
        const reservationsCount = response.data.reservations_count;
        const revenue = response.data.revenue;
        const total = response.data.reservations.total;
        return {
          reservationsCount, revenue, reservations: response.data.reservations.data, total
        };
      })
    );
  }

  getRebookingReservations(getFrom: Date, getBy: Date, pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/overload?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<BaseResponseModel<RebookingModel>>(url).pipe(
      map((response) => {
        const overload = response.data.overload;
        // tslint:disable-next-line:max-line-length
        const reservations = response.data.reservation_rooms === null || response.data.reservation_rooms.length === 0 ? [] : response.data.reservation_rooms.data;
        // tslint:disable-next-line:max-line-length
        const total = response.data.reservation_rooms === null || response.data.reservation_rooms.length === 0 ? [] : response.data.reservation_rooms.total;
        return {
          overload, reservations, total
        };
      })
    );
  }

  getConfirmedReservations(getFrom: Date, getBy: Date, pageSize: number, page: number): Observable<any> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/reservation-overview/confirmed?from=${from}&by=${by}&perPage=${pageSize}&page=${page}&sort=created_at&sort=status`;
    return this.httpClient.get<any>(url).pipe(
      map((response) => {
        const total = response.data.reservations.total;
        const bookedToday = response.data.reservations_count;
        const roomNights = response.data.room_nights;
        const revenue = response.data.revenue;
        const reservations = response.data.reservations.data;
        return {
          total, bookedToday, roomNights, revenue, reservations
        };
      })
    );
  }

  getOverallStatistic(getFrom: Date, getBy: Date): Observable<OverAllStatisticModel> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/statistics?from=${from}&by=${by}`;
    return this.httpClient.get<BaseResponseModel<OverAllStatisticModel>>(url).pipe(
      map(response => {
        return response.data;
      }));
  }

  getForecast(getFrom: Date, getBy: Date): Observable<ForecastResponseModel> {
    const from = Utils.convertDateToQuery(getFrom);
    const by = Utils.convertDateToQuery(getBy);
    // tslint:disable-next-line:max-line-length
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/dashboard/room-type-availability?from=${from}&by=${by}`;
    return this.httpClient.get<BaseResponseModel<ForecastResponseModel>>(url).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  getNote(bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/booking/${bookingId}/notes`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

  addNote(bookingId: number, note: string): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${DashboardApiService.getPropertyId()}/booking/${bookingId}/notes`;
    return this.httpClient.post<BaseResponseModel<any>>(url, {text: note}).pipe(
      map((res) => {
        return res.data;
      })
    );
  }

}
