import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

// Models
import {AuthDtoModel} from '@app/shared/services/auth/auth-dto.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {CleanerService} from '@app/shared/services/cleaner/cleaner.service';
import {environment} from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Environments
  DEV_ENV = environment;

  constructor(private http: HttpClient, private cleanerService: CleanerService) {
  }

  /* -------------------------------------------- USER ---------------------------------------------------*/

  // Вход пользавателя
  login(data: AuthDtoModel): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/user`;
    return this.http.post(url, data, {
      headers: new HttpHeaders({Authorization: this.getAppToken()})
    });
  }

  // Записываю токен пользавателя
  setUserToken(token, type) {
    return this.setToken(KeysEnum.USER_TOKEN, type, token);
  }

  // Записываю токен пользавателя на время сессии
  setTemporaryUserToken(token, type) {
    return this.setSessionToken(KeysEnum.USER_TOKEN, type, token);
  }

  // Получаю токен пользавателя из storage
  getUserToken(): string {
    return this.getToken(KeysEnum.USER_TOKEN) || this.getSessionToken(KeysEnum.USER_TOKEN);
  }

  /* -------------------------------------------- CRUD ---------------------------------------------------*/

  // Запись токена в locale storage
  setToken(item, type, token) {
    return localStorage.setItem(item, `${type} ${token}`);
  }

  // Запись токена в locale storage
  setSessionToken(item, type, token) {
    return sessionStorage.setItem(item, `${type} ${token}`);
  }

  // Чтение токена из locale storage
  getToken(item) {
    return localStorage.getItem(item);
  }

  // Чтение токена из session storage
  getSessionToken(item) {
    return sessionStorage.getItem(item);
  }

  // Удоляю token
  removeToken(item) {
    localStorage.removeItem(item);
    localStorage.setItem(item, null);
  }

  /* -------------------------------------------- App ---------------------------------------------------*/

// Получаю токен приложения
  authApp(): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/app`;
    return this.http.post(url, null, {
      headers: new HttpHeaders({
        Authorization: this.DEV_ENV.appToken
      })
    })
      .pipe(tap(({token_type, access_token}) => this.setAppToken(token_type, access_token)));
  }

  // Записываю токен приложения
  setAppToken(type, token) {
    return this.setToken(KeysEnum.APP_TOKEN, type, token);
  }

  // Получаю токен приложения
  getAppToken(): string {
    return this.getToken(KeysEnum.APP_TOKEN);
  }

  /* -------------------------------------------- STATE ---------------------------------------------------*/

  // Проверяю авторизацию
  isAuthenticated(): boolean {
    return this.getUserToken() !== 'null' && this.getUserToken() !== null;
  }

  // Выход пользавателя
  logOut(): Observable<any> {
    this.removeToken(KeysEnum.USER_TOKEN);
    this.cleanerService.cleanAll();
    const url = `${this.DEV_ENV.baseURL}/auth/user/logout`;
    return this.http.post(url, null);
  }

  // Проверяем есть ли токен приложения
  isAppTokenExist(): boolean {
    return this.getAppToken() !== 'null' && this.getAppToken() !== null;
  }

}
