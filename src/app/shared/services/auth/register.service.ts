import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

// Services
import {AuthService} from './auth.service';
import {environment} from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  // Environments
  DEV_ENV = environment;

  existEmailOnRegistration = null;
  emailOfReg = null;
  passOfReg = null;

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  // Регистрация отельера
  registerHotelier(data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/register`;
    this.emailOfReg = data.email;
    this.passOfReg = data.password;
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

  // Подтверждение электронной почты
  confirmEmail(token: string, role: string) {
    const url = `${this.DEV_ENV.baseURL}/auth/email-verification`;
    const data = {
      email: this.emailOfReg,
      role,
      token
    };
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

  resendConfirmCode(role: string) {
    const url = `${this.DEV_ENV.baseURL}/auth/email-verification/resend`;
    const data = {
      email: this.emailOfReg,
      role
    };
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

}
