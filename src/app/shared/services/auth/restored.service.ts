import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

// Services
import {AuthService} from './auth.service';
import {environment} from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestoredService {
  // Environments
  DEV_ENV = environment;

  isRestoreByEmail = true;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
  }

  // Запрос на востановление пароля
  restoredPasswordByEmail(data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/email-password/send`;
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

  restoredPasswordByPhone(data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/phone-password/send`;
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

  // Запрос на изменение пароля
  changePasswordByEmail(data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/email-password/reset`;
    return this.http.post(url, data,
      {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }

  changePasswordByPhone(data): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/auth/phone-password/reset`;
    return this.http.post(url, data, {
        headers: new HttpHeaders({
          Authorization: this.authService.getAppToken()
        })
      }
    );
  }


}
