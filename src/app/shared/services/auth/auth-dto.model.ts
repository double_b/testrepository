export class AuthDtoModel {
  constructor(
    public username: string,
    public password: string,
    public role: string
  ) {
  }
}
