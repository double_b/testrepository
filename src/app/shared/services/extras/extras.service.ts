import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// Environments
import {environment} from '@src/environments/environment';

// Models
import {CityModel} from '@app/shared/models/common/city.model';
import {CountryModel} from '@app/shared/models/common/country.model';
import {PropertyTypesModel} from '@app/shared/models/property-types.model';
import {HotelServiceModel} from '@app/shared/models/hotel-service.model';
import {GalleryTypesImageModel} from '@app/shared/models/gallery-types-image.model';
import {LanguageModel} from '@app/shared/models/common/language.model';
import {ServiceGroupModel} from '@app/shared/models/common/service-group.model';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {BedModel} from '../../models/room/bed.model';
import {FacilityGroupModel} from '@app/shared/models/common/facility-group.model';
import {FacilityModel} from '@app/shared/models/common/facility.model';
import {DocTypeModel} from '@app/shared/models/common/doc-type.model';

@Injectable({
  providedIn: 'root'
})
export class ExtrasService {

  // Environments
  DEV_ENV = environment;
  url = `${this.DEV_ENV.baseURL}/common`;

  constructor(private http: HttpClient) {
  }

  /* ----------------------- Types --------------------- */

  // Получаю ипы объектов
  propertiesType(): Observable<PropertyTypesModel[]> {
    return this.http.get<BaseResponseModel<PropertyTypesModel[]>>(`${this.url}/property-types`)
      .pipe(map((res) => res.data));
  }

  /* ----------------------- Rooms --------------------- */

  // Получаю room groups
  getGroupsRoom(): Observable<PropertyTypesModel[]> {
    return this.http.get<BaseResponseModel<PropertyTypesModel[]>>(`${this.url}/room-name-groups?`)
      .pipe(map((res) => res.data));
  }

  // Получаю room names
  getRoomNames(): Observable<PropertyTypesModel[]> {
    return this.http.get<BaseResponseModel<PropertyTypesModel[]>>(`${this.url}/room-names?`)
      .pipe(map((res) => res.data));
  }

  // Получаю facility groups
  getFacilityGroups(): Observable<FacilityGroupModel[]> {
    return this.http.get<BaseResponseModel<FacilityGroupModel[]>>(`${this.url}/facility-groups`)
      .pipe(map((res) => res.data));
  }

  // Получаю facility
  getFacility(): Observable<FacilityModel[]> {
    return this.http.get<BaseResponseModel<FacilityModel[]>>(`${this.url}/facilities`)
      .pipe(map((res) => res.data));
  }

  // Получаю room beds
  getBeds(): Observable<BedModel[]> {
    return this.http.get<BaseResponseModel<BedModel[]>>(`${this.url}/beds`)
      .pipe(map((res) => res.data));
  }

  /* ----------------------- Location --------------------- */

  // Города
  cities(): Observable<CityModel[]> {
    return this.http.get<BaseResponseModel<CityModel[]>>(`${this.url}/cities`)
      .pipe(map((res) => res.data));
  }

  // Страны
  countries(withCities: boolean = false): Observable<CountryModel[]> {
    return this.http.get<BaseResponseModel<CountryModel[]>>(`${this.url}/countries${withCities ? '?with=cities' : ''}`)
      .pipe(map((res) => res.data));
  }

  // Языки
  getLanguages(): Observable<LanguageModel[]> {
    return this.http.get<BaseResponseModel<LanguageModel[]>>(`${this.url}/languages`)
      .pipe(map((res) => res.data));
  }

  /* ----------------------- Services --------------------- */

  // Группы услуг
  servicesGroup(set = ''): Observable<ServiceGroupModel[]> {
    return this.http.get<BaseResponseModel<ServiceGroupModel[]>>(`${this.url}/service-groups${set}`)
      .pipe(map(res => res.data));
  }

  // Услуги
  servicesList(): Observable<HotelServiceModel[]> {
    return this.http.get<{ data: HotelServiceModel[] }>(`${this.url}/services`)
      .pipe(map(({data}) => data));
  }

  // Галерея
  galleryTypesImage(): Observable<GalleryTypesImageModel[]> {
    return this.http.get<BaseResponseModel<GalleryTypesImageModel[]>>(`${this.url}/image-names`)
      .pipe(map((res) => res.data));
  }

  // Типы доков
  docTypes(): Observable<DocTypeModel[]> {
    return this.http.get<BaseResponseModel<DocTypeModel[]>>(`${this.url}/document-types`)
      .pipe(map((res) => res.data));
  }

}
