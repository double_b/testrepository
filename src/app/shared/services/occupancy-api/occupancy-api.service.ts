import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {OccupancyModel} from '@app/shared/models/occupancy.model';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {OccupancyRequestModel} from '@app/shared/models/occupancy-request.model';

@Injectable({
  providedIn: 'root'
})
export class OccupancyApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getDiscounts(hotelId: number, roomTypeId: number, channelId: string, rateId: number): Observable<OccupancyModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/occupancy/${channelId}/${rateId}`;
    return this.httpClient.get<BaseResponseModel<OccupancyModel[]>>(url)
      .pipe(map(res => res.data));
  }

  saveDiscounts(hotelId: number, roomTypeId: number, channel: string, rate_id: number, occupancies: OccupancyModel[]): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/room-types/${roomTypeId}/occupancy`;
    const temp = new OccupancyRequestModel();
    temp.channel = channel;
    temp.rate_id = rate_id;
    temp.occupancy = occupancies;
    return this.httpClient.post(url, temp).pipe(map(response => response));
  }
}
