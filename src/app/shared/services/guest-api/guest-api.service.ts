import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GuestApiService {

  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getGuests(hotelId: number, params?: HttpParams): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guests`;
    return this.httpClient.get<BaseResponseModel<any>>(url, {params}).pipe(
      map((res) => res.data));
  }

  getSingleGuest(hotelId: number, guestId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guests/${guestId}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  updateGuest(hotelId: number, guestId: number, guest: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guests/${guestId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, guest).pipe(
      map((res) => res.data));
  }

  getGuestNotes(hotelId: number, guestId: number, isArchived: boolean): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guest/${guestId}/notes${isArchived ? '/archived' : ''}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  addGuestNote(hotelId: number, guestId: number, note: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guest/${guestId}/notes`;
    return this.httpClient.post<BaseResponseModel<any>>(url, note).pipe(
      map((res) => res.data));
  }

  updateGuestNote(hotelId: number, guestId: number, noteId: number, note: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guest/${guestId}/notes/${noteId}`;
    return this.httpClient.put<BaseResponseModel<any>>(url, note).pipe(
      map((res) => res.data));
  }

  deleteGuestNote(hotelId: number, guestId: number, noteId: number, forceDelete: boolean): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/guest/${guestId}/notes/${noteId}${forceDelete ? '/force-delete' : ''}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }
}
