import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservationApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  addReservation(hotelId: number, body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings`;
    return this.httpClient.post<BaseResponseModel<any>>(url, body).pipe(
      map((res) => res.data));
  }

  getReservations(hotelId: number, params?: HttpParams): Observable<any[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings`;
    return this.httpClient.get<any[]>(url, {params}).pipe(
      map((res) => res));
  }

  getSingleReservation(hotelId: number, bookingId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings/${bookingId}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  getGuestReservations(hotelId: number, guestId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings?guest_id=${guestId}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  addSingleGuestForReservation(hotelId: number, bookingId: number, body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings/${bookingId}/guest`;
    return this.httpClient.post<BaseResponseModel<any>>(url, body).pipe(
      map((res) => res.data));
  }

  deleteSingleGuestForReservation(hotelId: number, bookingId: number, guestId: number): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/bookings/${bookingId}/guest/${guestId}`;
    return this.httpClient.delete(url);
  }

  saveImportReservations(hotelId: number, data: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/import-reservations`;
    return this.httpClient.post<BaseResponseModel<any>>(url, data).pipe(
      map(response => response.data));
  }

  getReservationNotes(hotelId: number, bookingId: number, isArchived: boolean): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/booking/${bookingId}/notes${isArchived ? '/archived' : ''}`;
    return this.httpClient.get<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }

  addReservationNote(hotelId: number, bookingId: number, body: any): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/booking/${bookingId}/notes`;
    return this.httpClient.post<BaseResponseModel<any>>(url, body).pipe(
      map((res) => res.data));
  }

  deleteReservationNote(hotelId: number, bookingId: number, noteId: number, forceDelete: boolean): Observable<any> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/booking/${bookingId}/notes/${noteId}${forceDelete ? '/force-delete' : ''}`;
    return this.httpClient.delete<BaseResponseModel<any>>(url).pipe(
      map((res) => res.data));
  }
}
