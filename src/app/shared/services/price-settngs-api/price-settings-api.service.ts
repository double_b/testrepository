import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {CreateBasePricesBodyModel, CreateBasePricesModel, DeleteBasePriceModel, TourAgentRatesModel} from '@app/shared/models/price.model';

@Injectable({
  providedIn: 'root'
})
export class PriceSettingsApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getBasePriceList(hotelId: any): Observable<TourAgentRatesModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_rates?edit=true`;
    return this.httpClient.get<BaseResponseModel<TourAgentRatesModel[]>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  createBasePrices(hotelId: any, body: CreateBasePricesBodyModel): Observable<CreateBasePricesModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_rates`;
    return this.httpClient.post<BaseResponseModel<CreateBasePricesModel>>(url, body).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  updateBasePrices(hotelId: any, body: CreateBasePricesBodyModel, id): Observable<CreateBasePricesModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_rates/${id}`;
    return this.httpClient.patch<BaseResponseModel<CreateBasePricesModel>>(url, body).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  getBasePriceById(hotelId: any, id): Observable<TourAgentRatesModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_rates/${id}?edit=true`;
    return this.httpClient.get<BaseResponseModel<TourAgentRatesModel>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  deleteBasePrice(hotelId: any, id): Observable<DeleteBasePriceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_rates/${id}`;
    return this.httpClient.delete<any>(url).pipe(
      map(res => {
        return res;
      })
    );
  }
}
