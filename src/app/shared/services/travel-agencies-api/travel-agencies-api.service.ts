import {Injectable} from '@angular/core';
import {environment} from '@src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {BaseResponseModel} from '@app/shared/models/base-response.model';
import {ChannelModel} from '@app/shared/models/channel.model';
import {CreateBasePricesBodyModel, DeleteBasePriceModel, RoomTypesModel, TourAgentRatesModel, CreateBasePricesModel} from '@app/shared/models/price.model';
import {TravelAgenciesModel} from '@app/shared/models/travel-agencies-list.model';
import { ConstractsForCompanyModel } from '../../models/constracts-for-company.model';
import { AgencyInfoModel } from '../../models/agency-info.model';
import {BodyCreateTravelAgencyModel} from '@app/shared/models/body-create-travel-agency.model';
import {CrateTravelAgenciesModel} from '@app/shared/models/crate-travel-agencies.model';
import { BodyCreateContractModel } from '../../models/body-create-contract.model';

@Injectable({
  providedIn: 'root'
})
export class TravelAgenciesApiService {
  // Environments
  DEV_ENV = environment;

  constructor(private httpClient: HttpClient) {
  }

  getTravelAgenciesList(hotelId: any): Observable<TravelAgenciesModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/companies`;
    return this.httpClient.get<BaseResponseModel<TravelAgenciesModel[]>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  createTravelAgency(hotelId: any, body: BodyCreateTravelAgencyModel): Observable<CrateTravelAgenciesModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/companies`;
    return this.httpClient.post<BaseResponseModel<CrateTravelAgenciesModel>>(url, body).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  getAgencyInfo(hotelId: any, agencyId): Observable<AgencyInfoModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/companies/${agencyId}?contracts=&companyLegalInformation=`;
    return this.httpClient.get<BaseResponseModel<AgencyInfoModel>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  deleteTravelAgency(hotelId: any, id): Observable<DeleteBasePriceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/companies/${id}`;
    return this.httpClient.delete<BaseResponseModel<DeleteBasePriceModel>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  createContract(hotelId: any, body: BodyCreateContractModel): Observable<DeleteBasePriceModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_contracts`;
    return this.httpClient.post<BaseResponseModel<DeleteBasePriceModel>>(url, body).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  getContractsForCompany(hotelId: any, agencyId): Observable<ConstractsForCompanyModel[]> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_contracts/?company_id=${agencyId}`;
    return this.httpClient.get<BaseResponseModel<ConstractsForCompanyModel[]>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }

  getContractInfo(hotelId: any, id): Observable<ConstractsForCompanyModel> {
    const url = `${this.DEV_ENV.baseURL}/hotelier/${hotelId}/company_contracts/${id}`;
    return this.httpClient.get<BaseResponseModel<ConstractsForCompanyModel>>(url).pipe(
      map(res => {
        return res.data;
      })
    );
  }
}
