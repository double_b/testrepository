import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Components
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {NzModule} from '@app/shared/nz/nz.module';
import {PhotoManagerComponent} from './components/photo-manager/photo-manager.component';
import {DragulaModule} from 'ng2-dragula';
import {OnlyNumberDirective} from '@app/shared/helpers/only-numbers.directive';


@NgModule({
  declarations: [PhotoManagerComponent, OnlyNumberDirective],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    NzModule,
    DragulaModule
  ],
  exports: [
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    NzModule,
    DragulaModule,
    PhotoManagerComponent,
    OnlyNumberDirective
  ]
})
export class SharedModule {
}
