export class ConnectionSteps {

  static AUTHENTIFICATION = 'authentification';
  static AUTH = 'auth';
  static ROOM_TYPES_MAPPING = 'room_types_mapping';
  static CONNECTED = 'connected';

  authentification: false;
  auth: false;
  room_types_mapping: false;
  connected: false;
}
