import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from 'src/app/shared/services/notification/notification.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ChannelsService} from '@app/views/channels/channels.service';

@Component({
  selector: 'app-view-channel',
  templateUrl: './view-channel.component.html',
  styleUrls: ['./view-channel.component.scss']
})
export class ViewChannelComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private _router: Router,
              private notificationService: NotificationService,
              private channelsService: ChannelsService) {
  }

  channel_id: any = 1;
  isVisible = false;
  name;
  logo;
  description;
  currency;
  channelInfo;
  channelStatus;
  showAbout = true;
  roomsArray = [];
  errorStatus;
  errorMessage = 'Ошибка при подключении к Booking.com. Обратитесь в службу технической поддержки support@smartbooking.uz';
  status = 'error';
  roomsInfoArray;
  updateArrayNumbers = [];
  syncedAt;
  loadBtn = false;
  messageContainer = {
    connected: 'Подключено',
    in_progress: 'В процессе подключения',
    progress_error: 'Ошибка подключения',
    error: ''
  };

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.channel_id = this.route.snapshot.paramMap.get(RoutesEnum.ID).replace(' ', '');
      this.getChannelInfo(this.channel_id);
    });
    this.viewChannel();
  }

  provinceChange(room) {
    this.roomsArray.forEach(el => {
      if (room !== '-1') {
        if (this.updateArrayNumbers.find(element => element.room_type_id === el.id)) {
          el.visible = true;
        } else {
          el.visible = false;
        }
      }
    });
  }

  checkAuthyCode() {
    this.loadBtn = true;
    this.channelsService.patchChannel(this.channel_id).subscribe(res => {
      this.viewChannel();
    }, error => {
      // show message
      this.notificationService.errorMessage('При синхронизации данных произошла ошибка');
      this.viewChannel();
    });
  }

  private viewChannel() {
    this.channelsService.viewChannel(this.channel_id)
      .subscribe((response) => {
        this.loadBtn = false;
        this.syncedAt = response.synced_at;
      });
  }

  pushModelOnInit() {
    this.channelStatus.rooms.forEach(element => {
      this.updateArrayNumbers.push({
        channel_room_id: element.id,
        room_type_id: element.room_type_id,
      });
      this.roomsArray.find(el => el.id === element.room_type_id).visible = true;
    });
  }

  showModal() {
    this.isVisible = true;
  }

  handleOk() {
    this.channelsService.deleteChannel(this.channel_id)
      .subscribe((response) => {
        this.roomsInfoArray = response;
        this._router.navigate([RoutesEnum.HOTEL, this.channelsService.getPropertyId(), RoutesEnum.CHANNELS]).finally();
      });
    this.isVisible = false;
  }

  handleCancel() {
    this.isVisible = false;
  }

  showAboutFunction(status) {
    this.showAbout = status;
  }

  getChannelInfo(channelId) {
    this.channelsService.viewChannel(channelId)
      .subscribe((response) => {
        this.channelInfo = response.channel;
        this.channelStatus = response;
        this.status = response.status;
        if (response.status === 'progress_error') {
          this.errorStatus = true;
          this.errorMessage = response.message;
        }
        this.listRooms();
      });
  }

  listRooms() {
    this.channelsService.getRoomTypesUrl()
      .subscribe((response) => {
        this.roomsArray = response;
        this.roomsArray.unshift({
          empty: true,
          name: ' ',
          id: 0,
        });
        this.pushModelOnInit();
      });
  }

  stepConnectChannel() {
    for (let index = 0; index < this.updateArrayNumbers.length; index++) {
      if (this.updateArrayNumbers[index].room_type_id === 0) {
        document.getElementById(index.toString()).style.border = '1px solid red';
      } else {
        document.getElementById(index.toString()).style.border = '';
      }
    }
    if (this.updateArrayNumbers.find(el => el.room_type_id === 0)) {
      return;
    }
    const body = {
      mapping: this.updateArrayNumbers
    };
    this.channelsService.stepUpdateChannel(this.channel_id, body)
      .subscribe((response) => {
        this.notificationService.successMessage('Данные о канале успешно обновлены');
      });
  }

  backChannels() {
    this._router.navigate([RoutesEnum.HOTEL, this.channelsService.getPropertyId(), RoutesEnum.CHANNELS]).finally();
  }
}
