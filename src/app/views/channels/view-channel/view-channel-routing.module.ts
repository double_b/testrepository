import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

// Components
import {ViewChannelComponent} from '@app/views/channels/view-channel/view-channel.component';


const routes: Routes = [
  {
    path: '',
    component: ViewChannelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewChannelRoutingModule {
}
