import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Rout module
import {ViewChannelRoutingModule} from './view-channel-routing.module';

// Components
import {ViewChannelComponent} from '@app/views/channels/view-channel/view-channel.component';
import {NzAlertModule, NzButtonModule, NzGridModule, NzIconModule, NzMenuModule, NzModalModule, NzSelectModule} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';
import {SharedModule} from "@app/core/shared/shared.module";

@NgModule({
  declarations: [ViewChannelComponent],
    imports: [
        CommonModule,
        ViewChannelRoutingModule,
        NzMenuModule,
        NzAlertModule,
        NzGridModule,
        NzButtonModule,
        NzModalModule,
        NzIconModule,
        NzSelectModule,
        FormsModule,
        SharedModule
    ]
})
export class ViewChannelModule {
}
