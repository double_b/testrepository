import {Component, OnInit} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ConnectionSteps} from '@app/views/channels/connection.steps';
import {ChannelsService} from '@app/views/channels/channels.service';

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {

  isVisible = false;
  channelArray = [];
  channelArrayAdd = [];
  step = 'main';
  status = 'error';
  messageContainer = {
    connected: 'Подключено',
    in_progress: 'В процессе подключения',
    progress_error: 'Ошибка подключения',
    error: ''
  };

  constructor(private channelsService: ChannelsService) {
  }

  ngOnInit() {
    this.channelsService.getChannelsList()
      .subscribe((response: any) => {
        this.channelArray = response;
        this.status = response.status;
        this.channelArray.forEach(element => {
          if (element.id) {
            element.id = element.id.toString().replace(' ', '');
          }
        });
      });

    this.channelsService.getChannelsAvailable()
      .subscribe((response: any) => {
        this.channelArrayAdd = response;
        this.channelArrayAdd.forEach(element => {
          if (element.id) {
            element.id = element.id.toString().trim();
          }
        });
      });
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  getStatusRoute(id: string, status: string, step: string): string {
    if (status === 'in_progress' || status === 'progress_error') {
      return `${RoutesEnum.STEP}/${step}/${RoutesEnum.CHANNEL}/${id}`;
    } else {
      return `${RoutesEnum.VIEW_CHANNEL}/${id} `;
    }
  }

  getAuthentificationRoute(key: string): string {
    return `${RoutesEnum.CONNECTION}/${key}/${RoutesEnum.STEP}/${ConnectionSteps.AUTHENTIFICATION}`;
  }
}
