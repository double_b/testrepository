import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ChannelModel, CreateChanelModel, RoomTypesForChannelModel} from '@app/shared/models/channel.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ChannelApiService} from '@app/shared/services/channel-api/channel-api.service';

@Injectable({
  providedIn: 'root'
})
export class ChannelsService {

  constructor(private channelApiService: ChannelApiService) {
  }

  getPropertyId(): string {
    const hotel = JSON.parse(localStorage.getItem(KeysEnum.HOTEL_ID));
    return String(hotel);
  }

  getChannelsList(): Observable<ChannelModel[]> {
    return this.channelApiService.getChannelsList(this.getPropertyId());
  }

  getChannelsAvailable(): Observable<any> {
    return this.channelApiService.getChannelsAvailable(this.getPropertyId());
  }

  addChannel(body: CreateChanelModel): Observable<any> {
    return this.channelApiService.addChannel(this.getPropertyId(), body);
  }

  updateAuthyData(channelId, body: CreateChanelModel): Observable<any> {
    return this.channelApiService.updateAuthyData(this.getPropertyId(), channelId, body);
  }

  viewChannel(channelId): Observable<ChannelModel> {
    return this.channelApiService.viewChannel(this.getPropertyId(), channelId);
  }

  checkAuthyChannel(channelId, body): Observable<ChannelModel> {
    return this.channelApiService.checkAuthyChannel(this.getPropertyId(), channelId, body);
  }

  getRoomTypesUrl(): Observable<RoomTypesForChannelModel[]> {
    return this.channelApiService.getRoomTypesUrl(this.getPropertyId());
  }

  stepConnectChannel(channelId, body: CreateChanelModel): Observable<any> {
    return this.channelApiService.stepConnectChannel(this.getPropertyId(), channelId, body);
  }

  patchChannel(channelId): Observable<any> {
    return this.channelApiService.patchChannel(this.getPropertyId(), channelId);
  }

  deleteChannel(channelId): Observable<any> {
    return this.channelApiService.deleteChannel(this.getPropertyId(), channelId);
  }

  stepUpdateChannel(channelId, body: CreateChanelModel): Observable<any> {
    return this.channelApiService.stepUpdateChannel(this.getPropertyId(), channelId, body);
  }
}
