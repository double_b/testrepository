import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ChannelsComponent} from './channels.component';
import {RouterModule} from '@angular/router';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChannelsRoutingModule} from './channels-routing.module';
import {CustomPipesModule} from 'src/app/core/pipes/custom-pipes.module';
import {CommonModule} from '@angular/common';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';

@NgModule({
  declarations: [
    ChannelsComponent,
  ],
  imports: [
    RouterModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    FormsModule,
    ChannelsRoutingModule,
    RouterModule,
    CustomPipesModule,
    OldShared,
    CommonModule,
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChannelsModule {
}
