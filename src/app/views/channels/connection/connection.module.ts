import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Rout module
import {ConnectionRoutingModule} from './connection-routing.module';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';

// Components
import {ConnectionComponent} from '@app/views/channels/connection/connection.component';
import {NzAlertModule, NzButtonModule, NzGridModule, NzIconModule, NzSelectModule, NzStepsModule} from 'ng-zorro-antd';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ConnectionComponent],
  imports: [
    CommonModule,
    ConnectionRoutingModule,
    NzStepsModule,
    NzAlertModule,
    FormsModule,
    NzButtonModule,
    NzGridModule,
    NzIconModule,
    OldShared,
    NzSelectModule
  ]
})
export class ConnectionModule {
}
