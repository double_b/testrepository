import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

// Components
import {ConnectionComponent} from '@app/views/channels/connection/connection.component';


const routes: Routes = [
  {
    path: '',
    component: ConnectionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnectionRoutingModule {
}
