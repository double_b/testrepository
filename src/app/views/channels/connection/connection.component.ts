import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ConnectionSteps} from '@app/views/channels/connection.steps';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {CreateChanelModel} from '@app/shared/models/channel.model';
import {ChannelsService} from '@app/views/channels/channels.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {Utils} from '@app/shared/helpers/utils';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {

  showStep = new ConnectionSteps();
  step: any = ConnectionSteps.AUTHENTIFICATION;
  key: any = 1;
  CHANNEL_ID: any = 22;
  channel_property_id: any = 1;
  channel_auth_info;
  hotelId; // это id отеля в системе самого сайта бронирования, который мы вводим в инпут
  errorMessage;
  roomsArray = [];
  syncedAt;
  affermativeMessage = ConnectionSteps.CONNECTED;
  loadBtn;
  roomsInfoArray;
  updateArrayNumbers = [];
  channelWarningMessage: any;
  errorSyncMessage: string;
  needUpdate = false;
  validIdErrorMessage = '';
  createIsProcess = false;
  isVisible = false;

  constructor(private route: ActivatedRoute,
              private _router: Router,
              private channelsService: ChannelsService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.route.queryParams.subscribe(res => {
        if (res[RoutesEnum.HOTEL_ID]) {
          this.CHANNEL_ID = res[RoutesEnum.CHANNEL_ID];
          this.hotelId = res[RoutesEnum.HOTEL_ID];
          this.needUpdate = true;
        }
      });
      this.step = this.route.snapshot.paramMap.get(RoutesEnum.STEP_ID);
      if (this.step === 'connection') {
        this.step = 'connected';
      }
      if (this.route.snapshot.paramMap.get(RoutesEnum.KEY)) {
        this.key = this.route.snapshot.paramMap.get(RoutesEnum.KEY);
      }

      if (this.route.snapshot.paramMap.get(RoutesEnum.CHANNEL_ID)) {
        this.CHANNEL_ID = this.route.snapshot.paramMap.get(RoutesEnum.CHANNEL_ID).replace(' ', '');
        this.viewChannel();
      }

      if (this.step === ConnectionSteps.ROOM_TYPES_MAPPING) {
        this.listRooms();
      }
      if (this.step === ConnectionSteps.CONNECTED) {
        this.connectionRooms();
      }
    });
    this.switchStep(this.step);
    this.setHeader();
  }

  setHeader() {
    let array;
    this.channelsService.getChannelsList()
      .subscribe((response) => {
        array = response;
        this.channelsService.getChannelsAvailable()
          .subscribe((res) => {
            res.forEach(element => {
              array.push(element);
            });
            this.channelWarningMessage = array.find(element => element.key === this.key || element.channel_key === this.key);
          }, error => {
            this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
          });
      });
  }

  pushModelOnInit() {
    this.roomsInfoArray.rooms.forEach(element => {
      this.updateArrayNumbers.push({
        channel_room_id: element.id,
        room_type_id: null,
      });
    });
  }

  switchStep(step) {
    this.showStep.authentification = false;
    this.showStep.auth = false;
    this.showStep.room_types_mapping = false;
    this.showStep.connected = false;
    this.showStep[step] = true;
    this.step = step;
  }

  authyModel() {
    this.validIdErrorMessage = '';
    if (this.needUpdate) {
      const body = {
        channelPropertyId: this.hotelId
      };
      this.channelsService.updateAuthyData(this.CHANNEL_ID, body).subscribe(res => {
        this.CHANNEL_ID = res.data.id;
        this.channel_property_id = res.data.channel_property_id;
        this.channel_auth_info = res.data.channel.auth_info;
        this.nextStep(ConnectionSteps.AUTH);
      });
    } else {
      const body: CreateChanelModel = {
        channel_key: this.key,
        channel_property_id: this.hotelId
      };
      this.channelsService.addChannel(body)
        .subscribe((response) => {
          this.CHANNEL_ID = response.id;
          this.channel_property_id = response.channel_property_id;
          this.channel_auth_info = response.channel.auth_info;
          this.nextStep(ConnectionSteps.AUTH);
        }, error => {
          this.validIdErrorMessage = error.error.message;
        });
    }
  }

  viewChannel() {
    this.channelsService.viewChannel(this.CHANNEL_ID).subscribe((res) => {
      this.channel_property_id = res.channel_property_id;
      this.key = res.channel_key;
      this.setHeader();
    }, error => {
      this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
    });
  }

  checkAuthy() {
    const body = {
      channel_propert_id: this.channel_property_id
    };
    this.channelsService.checkAuthyChannel(this.CHANNEL_ID, body)
      .subscribe((response) => {
        this.checkAuthyCode();
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  checkAuthyCode() {
    this.loadBtn = true;
    this.channelsService.patchChannel(this.CHANNEL_ID).subscribe(res => {
      this.viewChannelProcess();
    }, error => {
      // show message
      // this.isVisible = true;
      this.notificationService.errorMessage('При синхронизации данных произошла ошибка');
      this.viewChannelProcess();
    });

  }

  private viewChannelProcess() {
    this.channelsService.viewChannel(this.CHANNEL_ID)
      .subscribe((response) => {
        this.syncedAt = response.synced_at;
        if (response.step === ConnectionSteps.ROOM_TYPES_MAPPING) {
          this.loadBtn = false;
          this.nextStep(ConnectionSteps.ROOM_TYPES_MAPPING);
        } else if (response.status === 'progress_error') {
          this.errorMessage = 'Не удалось получить доступ к указанному отелю. Убедитесь что выполнили все необходимые действия, указанные в инструкции.';
        } else {
          setTimeout(() => {
            this.checkAuthyCode();
          }, 5000);
        }
      }, error => {
        this.loadBtn = false;
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  listRooms() {
    this.channelsService.getRoomTypesUrl()
      .subscribe((response) => {
        this.roomsArray = response;
        this.infoRooms();
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  infoRooms() {
    this.channelsService.viewChannel(this.CHANNEL_ID)
      .subscribe((response) => {
        this.roomsInfoArray = response;
        this.pushModelOnInit();
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  provinceChange(room, index) {
    this.updateArrayNumbers[index].room_type_id = room.id;
    this.roomsArray.find(el => el.id === this.updateArrayNumbers[index].room_type_id).visible = false;
    this.roomsArray.find(el => el.id === room.id).visible = true;
    document.getElementById(index.toString()).style.border = '';
  }

  stepConnectChannel() {
    for (let index = 0; index < this.updateArrayNumbers.length; index++) {
      if (!Utils.isExist(this.updateArrayNumbers[index].room_type_id)) {
        document.getElementById(index.toString()).style.border = '1px solid red';
      } else {
        document.getElementById(index.toString()).style.border = '';
      }
    }
    if (this.updateArrayNumbers.find(el => !Utils.isExist(el.room_type_id))) {
      return;
    }

    const body = {
      mapping: this.updateArrayNumbers
    };

    this.createIsProcess = true;

    this.channelsService.stepConnectChannel(this.CHANNEL_ID, body)
      .subscribe((response) => {
        this.nextStep(ConnectionSteps.CONNECTED);
        this.createIsProcess = false;
      }, error => {
        this.createIsProcess = false;
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  connectionRooms() {
    this.channelsService.viewChannel(this.CHANNEL_ID)
      .subscribe((response) => {
        if (response.status === ConnectionSteps.CONNECTED) {
          this.affermativeMessage = response.status;
        } else {
          setTimeout(() => {
            this.connectionRooms();
          }, 10000);
        }
      }, error => {
        this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
      });
  }

  getHotelRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.hotelId}`;
  }

  tunePrice() {
    this._router.navigate([this.getHotelRoute(), RoutesEnum.CHESS]).finally();
  }

  nextStep(step) {
    this._router.navigate(
      [this.getHotelRoute(), RoutesEnum.CHANNELS, RoutesEnum.STEP, step, RoutesEnum.CHANNEL, this.CHANNEL_ID]).finally();
    this.switchStep(step);
  }

  backStep(step) {
    if (step === ConnectionSteps.AUTHENTIFICATION) {
      this.channelsService.deleteChannel(this.CHANNEL_ID)
        .subscribe((response) => {
          this._router.navigate([this.getHotelRoute(), RoutesEnum.CHANNELS, RoutesEnum.CONNECTION, this.key, RoutesEnum.STEP, step], {
            queryParams: {
              hotelId: this.channel_property_id,
              CHANNEL_ID: this.CHANNEL_ID
            }
          }).finally();
        }, error => {
          this.notificationService.errorMessage('Произошла неизвестная ошибка.', 'Повторите запрос позже.');
        });
      return;
      //  this.hotelierApiService.deleteChannel(1,this.CHANNEL_ID).subscribe(element=>{});
    } else {
      this._router.navigate(
        [this.getHotelRoute(), RoutesEnum.CHANNELS, RoutesEnum.STEP, step, RoutesEnum.CHANNEL, this.CHANNEL_ID]).finally();
    }
    this.switchStep(step);
  }

  backChannels() {
    this._router.navigate([this.getHotelRoute(), RoutesEnum.CHANNELS]).finally();
  }

  getCurrentStep(): any {
    switch (this.step) {
      case ConnectionSteps.AUTHENTIFICATION:
        return 0;
      case ConnectionSteps.AUTH:
        return 1;
      case ConnectionSteps.ROOM_TYPES_MAPPING:
        return 2;
      case ConnectionSteps.CONNECTED:
        return 4;
      default:
        return '';
    }
  }

  handleCancel() {
    this.isVisible = false;
  }

  handleOk() {
    this.channelsService.deleteChannel(this.CHANNEL_ID)
      .subscribe((response) => {
        window.history.back();
      });
    this.isVisible = false;
  }

}
