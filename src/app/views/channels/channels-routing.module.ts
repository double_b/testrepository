import {RouterModule, Routes} from '@angular/router';
import {ChannelsComponent} from './channels.component';
import {NgModule} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';

const routes: Routes = [
  {
    path: '',
    component: ChannelsComponent,
  },
  {
    path: `${RoutesEnum.STEP}/:${RoutesEnum.STEP_ID}/${RoutesEnum.CHANNEL}/:${RoutesEnum.CHANNEL_ID}`,
    loadChildren: () => import('./connection/connection.module')
      .then(m => m.ConnectionModule)
  },
  {
    path: `${RoutesEnum.CONNECTION}/:${RoutesEnum.KEY}/${RoutesEnum.STEP}/:${RoutesEnum.STEP_ID}`,
    loadChildren: () => import('./connection/connection.module')
      .then(m => m.ConnectionModule)
  },
  {
    path: `${RoutesEnum.VIEW_CHANNEL}/:${RoutesEnum.ID}`,
    loadChildren: () => import('./view-channel/view-channel.module')
      .then(m => m.ViewChannelModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChannelsRoutingModule {
}
