import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {GuestsService} from '@app/views/guests/guests.service';

@Component({
  selector: 'app-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.scss']
})
export class GuestsComponent {

  constructor(private router: Router, private guestService: GuestsService) {
  }

  public get isGuestDetailsPage(): boolean {
    return this.router.url.split('/').pop() === RoutesEnum.DETAILS;
  }

  public createNewReservation(): void {
    this.router.navigate([RoutesEnum.HOTEL, this.guestService.getPropertyId(), RoutesEnum.RESERVATION, RoutesEnum.CREATE]).finally();
  }
}
