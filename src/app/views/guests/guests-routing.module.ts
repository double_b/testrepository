import {RouterModule, Routes} from '@angular/router';
import {GuestsComponent} from './guests.component';
import {NgModule} from '@angular/core';
import {GuestsMainComponent} from './components/guests-main/guests-main.component';
import {GuestsDetailsComponent} from './components/guests-details/guests-details.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';

const routes: Routes = [
  {
    path: '',
    component: GuestsComponent,
    children: [
      {
        path: '',
        redirectTo: RoutesEnum.LIST,
      },
      {
        path: RoutesEnum.LIST,
        component: GuestsMainComponent,
      },
      {
        path: RoutesEnum.DETAILS,
        component: GuestsDetailsComponent,
      },
      {
        path: '**',
        redirectTo: RoutesEnum.LIST,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestsRoutingModule {
}
