import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {GuestsComponent} from './guests.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GuestsMainComponent} from './components/guests-main/guests-main.component';
import {GuestsRoutingModule} from './guests-routing.module';
import {GuestsDetailsComponent} from './components/guests-details/guests-details.component';
import {GuestsDetailsDocumentsTabComponent} from './components/guests-details-documents-tab/guests-details-documents-tab.component';
import {GuestsDetailsHistoryTabComponent} from './components/guests-details-history-tab/guests-details-history-tab.component';
import {GuestsDetailsMainTabComponent} from './components/guests-details-main-tab/guests-details-main-tab.component';
import {GuestsDetailsNotesTabComponent} from './components/guests-details-notes-tab/guests-details-notes-tab.component';
import {CustomPipesModule} from 'src/app/core/pipes/custom-pipes.module';
import {SharedModule as OldShared} from '@app/core/shared/shared.module';

@NgModule({
  declarations: [
    GuestsComponent,
    GuestsMainComponent,
    GuestsDetailsComponent,
    GuestsDetailsDocumentsTabComponent,
    GuestsDetailsHistoryTabComponent,
    GuestsDetailsMainTabComponent,
    GuestsDetailsNotesTabComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    FormsModule,
    GuestsRoutingModule,
    RouterModule,
    CustomPipesModule,
    OldShared
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuestsModule {
}
