import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ReservationApiService} from '@app/shared/services/reservation-api/reservation-api.service';
import {GuestApiService} from '@app/shared/services/guest-api/guest-api.service';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GuestsService {
  private userToEdit$ = new BehaviorSubject<number>(null); // guest id

  public get chosenUser(): BehaviorSubject<number> {
    return this.userToEdit$;
  }

  constructor(private reservationApiService: ReservationApiService, private guestApiService: GuestApiService) {
  }

  getSingleReservation(bookingId: number): Observable<any> {
    return this.reservationApiService.getSingleReservation(this.getPropertyId(), bookingId);
  }

  getGuests(params?: HttpParams): Observable<any> {
    return this.guestApiService.getGuests(this.getPropertyId(), params);
  }

  getSingleGuest(guestId: number): Observable<any> {
    return this.guestApiService.getSingleGuest(this.getPropertyId(), guestId);
  }

  getGuestData(guestId: number): Observable<any> {
    return this.reservationApiService.getGuestReservations(this.getPropertyId(), guestId);
  }

  updateGuest(guestId: number, guest: any): Observable<any> {
    return this.guestApiService.updateGuest(this.getPropertyId(), guestId, guest);
  }

  getGuestNotes(guestId: number, isArchived: boolean = false): Observable<any> {
    return this.guestApiService.getGuestNotes(this.getPropertyId(), guestId, isArchived);
  }

  addGuestNote(guestId: number, note: any): Observable<any> {
    return this.guestApiService.addGuestNote(this.getPropertyId(), guestId, note);
  }

  updateGuestNote(guestId: number, noteId: number, note: any): Observable<any> {
    return this.guestApiService.updateGuestNote(this.getPropertyId(), guestId, noteId, note);
  }

  deleteGuestNote(guestId: number, noteId: number, forceDelete: boolean = false): Observable<any> {
    return this.guestApiService.deleteGuestNote(this.getPropertyId(), guestId, noteId, forceDelete);
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }
}
