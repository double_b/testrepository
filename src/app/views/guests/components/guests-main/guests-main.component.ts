import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {GuestsService} from '../../guests.service';
import {FormControl} from '@angular/forms';
import {differenceInCalendarDays} from 'date-fns';
import {HttpParams} from '@angular/common/http';
import * as moment from 'moment';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ReplaySubject} from 'rxjs';
import {debounceTime, skip} from 'rxjs/operators';
import {ExtrasService} from '@app/shared/services/extras/extras.service';

@Component({
  selector: 'app-guests-main',
  templateUrl: './guests-main.component.html',
  styleUrls: ['./guests-main.component.scss']
})
export class GuestsMainComponent implements OnInit {

  @ViewChild('checkin')
  private checkinRef: ElementRef;

  @ViewChild('checkout')
  private checkoutRef: ElementRef;

  public checkinControl = new FormControl(null);
  public checkoutControl = new FormControl(null);

  public isExtendedSearch = true;
  public isGuestStatusVisible = false;
  public isGuestCountryVisible = false;

  public searchInputControl: FormControl = new FormControl('');

  public subscribeStartGetData = new ReplaySubject<any>(1);

  private countriesFilterSelected = 0;

  public guestList = [];

  public pageIndex = 1;
  public pageSize = 25;
  public totalElementsQty = 1;
  public isDataLoading = false;

  public guestStatusCheckOptions = [
    {label: 'Выбрать все', value: 'all', checked: false},
    {label: 'Новый гость', value: 'new-guest', checked: false},
    {label: 'Повторяющийся гость', value: 'recurrent-guest', checked: false}
  ];

  public guestCountryCheckOptions = [];

  constructor(private renderer2: Renderer2,
              private router: Router,
              private extrasService: ExtrasService,
              private guestsService: GuestsService) {

    this.extrasService.countries(true).subscribe(res => {
      this.guestCountryCheckOptions = res.map(item => {
        return {label: item.name, value: item.id};
      });
    });

    this.checkinControl.valueChanges.subscribe(newValue => {
      if (newValue != null && newValue !== '') {
        this.checkoutControl.setValue(this.getNextDay());
      } else {
        this.checkoutControl.reset();
      }
      this.toggleImagePlaceholder(newValue, 'checkin');
    });

    this.checkoutControl.valueChanges.subscribe(newValue => {
      this.toggleImagePlaceholder(newValue, 'checkout');
    });
  }

  public ngOnInit(): void {
    this.subscribeStartGetData
      .pipe(debounceTime(500), skip(0))
      .subscribe(item => {
        this.retrieveData();
      });
    this.retrieveData();
  }

  private getPreviousDay(): Date | null {
    const date = this.checkoutControl.value;
    if (date != null && date !== '') {
      return new Date(date.getTime() - 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  private getNextDay(): Date | null {
    const date = this.checkinControl.value;
    if (date != null && date !== '') {
      return new Date(date.getTime() + 24 * 60 * 60 * 1000);
    } else {
      return null;
    }
  }

  public changeStatusCheckboxes(newValue: boolean, checkboxIndex: number): void {
    this.guestStatusCheckOptions[checkboxIndex].checked = newValue;

    const allSelection: any = this.guestStatusCheckOptions.find(item => item.value === 'all');
    const newGuestSelection: any = this.guestStatusCheckOptions.find(item => item.value === 'new-guest');
    const reccurentGuestSelection: any = this.guestStatusCheckOptions.find(item => item.value === 'recurrent-guest');

    if (newValue && checkboxIndex === 0) {
      this.guestStatusCheckOptions.forEach((item: any) => item.checked = true);
    } else if (!newValue && checkboxIndex === 0) {
      this.guestStatusCheckOptions.forEach((item: any) => item.checked = false);
    } else if (newValue && checkboxIndex === 1 && reccurentGuestSelection.checked) {
      this.guestStatusCheckOptions.forEach((item: any) => item.checked = true);
    } else if (newValue && checkboxIndex === 2 && newGuestSelection.checked) {
      this.guestStatusCheckOptions.forEach((item: any) => item.checked = true);
    } else if (!newValue && (checkboxIndex === 1 || checkboxIndex === 2) && allSelection.checked) {
      this.guestStatusCheckOptions[0].checked = false;
    }
  }

  private checkKeywordForCountryName(keyword: string): number | null { // country id
    if (keyword == null) {
      return null;
    }
    const countryArr = this.guestCountryCheckOptions
      .filter(item => String(item.label).toLowerCase() === keyword.toLowerCase());
    if (countryArr.length === 0) {
      return null;
    }
    const countryId = countryArr.pop();
    return countryId.value;
  }

  disabledCheckinDate = (current: Date): boolean => {
    const checkoutDateNext = this.getPreviousDay();
    if (checkoutDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkoutDateNext, current) < 0;
  };

  disabledCheckoutDate = (current: Date): boolean => {
    const checkinDateNext = this.getNextDay();
    if (checkinDateNext == null) {
      return false;
    }

    return differenceInCalendarDays(checkinDateNext, current) > 0;
  };

  public setCountriesCheckboxes(checkboxes: any[]): void {
    this.guestCountryCheckOptions = checkboxes;
    this.countriesFilterSelected = checkboxes.filter(chbx => chbx.checked).length;
  }

  public getGuestTitle(itemName: 'status' | 'country'): string {
    let title;
    const checkedGuest = [];
    this.guestStatusCheckOptions.forEach(el => {
      if (el.checked) {
        checkedGuest.push(el);
      }
    });
    if (itemName === 'status') {
      title = checkedGuest !== [] && this.guestStatusCheckOptions.length > 0 ?
        `${checkedGuest.length} из ${this.guestStatusCheckOptions.length} выбранных`
        : '---';
      return title;
    } else {
      title = this.guestCountryCheckOptions != null && this.guestCountryCheckOptions.length > 0
        ? `${this.countriesFilterSelected} из ${this.guestCountryCheckOptions.length} выбранных`
        : '---';
      return title;
    }
  }

  public toggleImagePlaceholder(newValue: any, field: 'checkin' | 'checkout'): void {
    const datePicker = field === 'checkin'
      ? this.checkinRef : this.checkoutRef;
    if (datePicker == null) {
      return;
    }

    const datePickerInputNative = (<any> datePicker).picker.pickerInput.nativeElement;
    const backgroundProperty = newValue == null
      ? `url(datepicker-input-dahed-placeholder.svg)` : 'none';

    this.renderer2.setStyle(datePickerInputNative, 'background-image', backgroundProperty);
  }

  public seeGuestDetails(index: number): void {
    const selectedGuest = this.guestList[index];
    this.guestsService.chosenUser.next(selectedGuest.id);
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.guestsService.getPropertyId(), RoutesEnum.GUESTS, RoutesEnum.DETAILS]);
  }

  private prepareParams(): HttpParams {
    let params = new HttpParams();

    // 1) filters by countries
    const countries: any = this.guestCountryCheckOptions
      .filter(chbx => chbx.checked)
      .map(item => item.value); // country's id
    const areAllCountries = countries.length === 0;
    if (!areAllCountries) {
      // setting param as array
      countries.forEach((ctry, index) => params = params.set(`countries[${index}]`, ctry));
    }


    // 2) filters by dates
    const checkin = this.checkinControl.value;
    const checkout = this.checkoutControl.value;
    if (checkin != null) {
      const checkinProcessed = moment(checkin).format('YYYY-MM-DD');
      params = params.set('checkin', checkinProcessed);
    }
    if (checkout != null) {
      const checkoutProcessed = moment(checkout).format('YYYY-MM-DD');
      params = params.set('checkout', checkoutProcessed);
    }


    // 3) filters by is-new-guest status
    let chosenGuestStatus = null;
    const guestStatuses: any = this.guestStatusCheckOptions
      .filter(chbx => chbx.checked)
      .map(item => item.value); // 'all' | 'new-guest' | 'recurrent-guest'
    if (guestStatuses.length === 1) { // if one of the status is chosen
      chosenGuestStatus = guestStatuses.pop();
      const isNewGuest = chosenGuestStatus === 'new-guest';
      params = params.set('is_new_guest', `${isNewGuest}`);
    } else {
      chosenGuestStatus = 'all'; // if 'all' status (length === 3) or none of them (length == 0) is chosen (by default)
    }

    return params;
  }

  public searchDataByIndex(): void {
    this.retrieveData(this.pageIndex);
  }

  public retrieveData(page?: number): void {
    let params = this.prepareParams();
    params = params.set('perPage', `${this.pageSize}`);

    if (page != null) {
      params = params.set('page', `${page}`);
    }

    const value = this.searchInputControl.value;
    if (value != null && value !== '') {
      // check if keyword is country's name, otherwise other params
      const country = this.checkKeywordForCountryName(value);
      if (country != null) {
        params = params.set('countries[0]', `${country}`);
      } else {
        params = params.set('search_text', value);
      }
    }

    this.isDataLoading = true;
    this.applyFilters(params);
  }

  public searchData() {
    this.subscribeStartGetData.next();
  }

  private applyFilters(params?: HttpParams): void {
    this.isDataLoading = true;
    this.guestsService.getGuests(params)
      .subscribe(resp => {
        this.isDataLoading = false;
        this.totalElementsQty = resp.meta.total;
        this.pageIndex = resp.meta.current_page;
        this.guestList = resp.data;
      });
  }

  public clearAll(): void {
    this.checkinControl.reset();
    this.checkoutControl.reset();
    this.guestStatusCheckOptions.forEach(item => {
      delete (<any> item).checked;
      return item;
    });
    this.countriesFilterSelected = 0;
    this.guestCountryCheckOptions.forEach(item => {
      delete (<any> item).checked;
      return item;
    });
    this.getGuestTitle('country');
  }

  public toggleOtherFilters(): void {
    this.isExtendedSearch = !this.isExtendedSearch;
  }
}
