import {Component, Input} from '@angular/core';
import {of} from 'rxjs';
import {UploadXHRArgs} from 'ng-zorro-antd';
import {saveAs} from 'file-saver';

interface IFile {
  name: number;
  data: File;
}

@Component({
  selector: 'app-guests-details-documents-tab',
  templateUrl: './guests-details-documents-tab.component.html',
  styleUrls: ['./guests-details-documents-tab.component.scss']
})
export class GuestsDetailsDocumentsTabComponent {
  @Input()
  public guestId: number;

  public isLoadingModal = false;
  public isLoading = false;
  public imageUrl: any;
  public maxFileUpload = 10;
  public uploadedFiles: IFile[] = [];

  public isDeleteMessageModalVisible = false;

  public docIndexToDelete = null;

  private prepareMockedFiles(): void {
    for (let i = 0; i < 4; i++) {
      const tmpData = {
        test: [
          {
            data: 1
          },
          {
            data: 2
          },
          {
            data: 3
          },
          {
            data: 4
          },
        ]
      };
      const fileTemplate = JSON.stringify(tmpData);
      const blob = new Blob([fileTemplate], {type: 'text/plain; charset=utf-8'});
      const filename = Math.floor(Math.random() * 90000000000000) + 10000000000000;
      const file = new File([blob], `${filename}.txt`);

      const fileObj = {
        name: filename,
        data: file
      };
      this.uploadedFiles.push(fileObj);
    }
  }

  constructor() {
    this.prepareMockedFiles();
  }

  imageUploadingRequestHandler = (item: UploadXHRArgs) => {
    this.isLoadingModal = true;

    // @FIXME: temp logic
    //  must return Subscribtion
    of(item).subscribe(
      res => {
        this.isLoading = false;
        const file = res.file;
        file.status = 'done';
        file.response = '{"status": "success"}';
        this.loadFile(file);
      },
      err => {
        // this.utils.presentErrorResponseMessage(err);
      },
      () => {
        this.isLoadingModal = false;
      }
    );
  };

  public loadFile(file: any): void {
    const fileObj = {
      name: Math.floor(Math.random() * 90000000000000) + 10000000000000,
      data: file
    };
    this.uploadedFiles.push(fileObj);
  }

  public downloadFile(index: number): void {
    const file = this.uploadedFiles[index];
    if (file == null) {
      return;
    }
    saveAs(file.data, file.data.name);
  }

  public handleCancel(): void {
    this.isDeleteMessageModalVisible = false;
  }

  public handleOk(): void {
    if (this.docIndexToDelete != null) {
      this.uploadedFiles = this.uploadedFiles.filter((item, i) => i !== this.docIndexToDelete);
      this.isDeleteMessageModalVisible = false;
    }
  }

  public deleteFile(index: number): void {
    this.docIndexToDelete = index;
    this.isDeleteMessageModalVisible = true;
  }
}
