import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Patterns} from '@app/core/constants/validation-patterns';
import {GuestsService} from '../../guests.service';
import * as moment from 'moment';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ExtrasService} from '@app/shared/services/extras/extras.service';
import {CountryModel} from '@app/shared/models/common/country.model';
import {DocTypeModel} from '@app/shared/models/common/doc-type.model';

@Component({
  selector: 'app-guests-details-main-tab',
  templateUrl: './guests-details-main-tab.component.html',
  styleUrls: ['./guests-details-main-tab.component.scss']
})
export class GuestsDetailsMainTabComponent implements OnInit {
  @Input() guestId: number;

  public guestForm: FormGroup;
  public isExtraInfoPanelShown = false;
  public docTypes: DocTypeModel[] = [];
  public countriesList: CountryModel[] = [];

  public genderSelection = [
    {value: 0, label: 'Женский'},
    {value: 1, label: 'Мужской'},
  ];

  constructor(private extrasService: ExtrasService,
              private router: Router,
              private guestsService: GuestsService) {
    this.guestForm = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(Patterns.emailPattern)]),
      phone: new FormControl('', Validators.required),
      country_id: new FormControl(null, Validators.required),
      birthday: new FormControl(''),
      gender: new FormControl(''),
      inn: new FormControl(''),
      company_name: new FormControl(''),
      company_number: new FormControl(''),
      city_id: new FormControl(''),
      region: new FormControl(''),
      address: new FormControl(''),
      address2: new FormControl(''),
      zip_code: new FormControl(''),
      document_type_id: new FormControl(''),
      document_number: new FormControl(''),
      document_issued_at: new FormControl(''),
      document_country_id: new FormControl(''),
      document_expired_at: new FormControl(''),
    });

    this.extrasService.countries().subscribe(res => {
      this.countriesList = res;
    });

    this.extrasService.docTypes().subscribe(res => {
      this.docTypes = res;
    });
  }

  private static getCountryId(country: any): string {
    return country == null ? null : country.id;
  }

  private static parseDate(date: string | null): Date | string {
    if (date == null) {
      return null;
    }
    const processedDate = moment(date, 'YYYY-MM-DD');
    const result = processedDate.isValid()
      ? processedDate.toDate() : date;
    return result;
  }

  private initializeForm(value: any): void {
    const data = {
      first_name: value.first_name,
      last_name: value.last_name,
      email: value.email,
      phone: value.phone,
      country_id: GuestsDetailsMainTabComponent.getCountryId(value.country),
      birthday: GuestsDetailsMainTabComponent.parseDate(value.birthday),
      gender: value.gender,
      inn: value.inn,
      company_name: value.company_name,
      company_number: value.company_number,
      city_id: value.city_id,
      region: value.region,
      address: value.address,
      address2: value.address2,
      zip_code: value.zip_code,
      document_type_id: value.document_type_id,
      document_number: value.document_number,
      document_issued_at: GuestsDetailsMainTabComponent.parseDate(value.document_issued_at),
      document_country_id: value.document_country_id,
      document_expired_at: GuestsDetailsMainTabComponent.parseDate(value.document_expired_at),
    };

    this.guestForm.setValue(data);
  }

  public ngOnInit(): void {
    if (this.guestId != null) {
      this.guestsService.getSingleGuest(this.guestId).subscribe(res => {
        this.initializeForm(res);
      });
    } else {
      // by default the tab is 1st, on reload jumps to guest list
      this.router.navigate([RoutesEnum.HOTEL, this.guestsService.getPropertyId(), RoutesEnum.GUESTS, RoutesEnum.LIST]).finally();
    }
  }

  private formatDateForRequest(date): string | null {
    return date == null ? null : moment(date).format('YYYY-MM-DD');
  }

  public saveGuestData(): void {
    if (this.guestId == null) {
      return;
    }
    const formData = this.guestForm.value;
    formData.birthday = this.formatDateForRequest(formData.birthday);
    formData.document_issued_at = this.formatDateForRequest(formData.document_issued_at);
    formData.document_expired_at = this.formatDateForRequest(formData.document_expired_at);

    this.guestsService.updateGuest(this.guestId, formData).subscribe(data => {
      this.router.navigate([RoutesEnum.HOTEL, this.guestsService.getPropertyId(), RoutesEnum.GUESTS, RoutesEnum.LIST]).finally();
    });
  }

  public toggleExtraInfoPanel(): void {
    this.isExtraInfoPanelShown = !this.isExtraInfoPanelShown;
  }
}
