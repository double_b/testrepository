import {Component} from '@angular/core';
import {GuestsService} from '../../guests.service';

@Component({
  selector: 'app-guests-details',
  templateUrl: './guests-details.component.html',
  styleUrls: ['./guests-details.component.scss']
})
export class GuestsDetailsComponent {
  private id: number = null;

  public get guestId(): number | null {
    return this.id;
  }

  constructor(private guestsService: GuestsService) {
    this.guestsService.chosenUser.asObservable().subscribe(data => {
      this.id = data;
    });
  }
}
