import {Component, Input, OnChanges, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {Router} from '@angular/router';
import {ReservationListService} from '@app/layouts/reservation/reservation-list/reservation-list.service';
import {GuestsService} from '@app/views/guests/guests.service';

@Component({
  selector: 'app-guests-details-history-tab',
  templateUrl: './guests-details-history-tab.component.html',
  styleUrls: ['./guests-details-history-tab.component.scss']
})
export class GuestsDetailsHistoryTabComponent implements OnChanges, OnDestroy {
  @Input() guestId: number;
  public guestReservation = [];
  public searchControl: FormControl;

  private bookingSubscription: Subscription;

  constructor(private guestsService: GuestsService,
              private router: Router,
              private reservationListService: ReservationListService) {
    this.searchControl = new FormControl(null);
  }

  public ngOnChanges() {
    if (this.guestId != null) {
      this.loadGuestData(this.guestId);
    }
  }

  public ngOnDestroy() {
    if (this.bookingSubscription != null) {
      this.bookingSubscription.unsubscribe();
    }
  }

  private loadGuestData(guestId: number) {
    this.guestsService.getGuestData(guestId).subscribe(res => {
      this.guestReservation = res;
    });
  }

  private getBookingDataById(bookingId: number) {
    this.guestsService.getSingleReservation(bookingId).subscribe(res => {
      this.guestReservation = res;
    });
  }

  public searchByKeyWord(): void {
    const value = this.searchControl.value;
    if (value != null && value !== '') {
      const valueProcessed = Number(value);
      if (!isNaN(valueProcessed)) {
        this.getBookingDataById(valueProcessed);
      }
    } else {
      this.loadGuestData(this.guestId);
    }
  }

  public getProcessedDate(dateStr): string {
    const dateProcessed = moment(dateStr, 'YYYY-MM-DD');
    return dateProcessed.isValid() ? dateProcessed.format('DD.MM.YYYY') : dateStr;
  }

  public seeReservationDetails(data): void {
    this.reservationListService.bookingId.next(data.id);
    // tslint:disable-next-line:max-line-length
    this.router.navigate([`/${RoutesEnum.HOTEL}`, this.guestsService.getPropertyId(), RoutesEnum.RESERVATION, RoutesEnum.DETAILS]).finally();
  }
}
