import {Component, Input, OnChanges, OnDestroy} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {GuestsService} from '@app/views/guests/guests.service';

type Tab = 'active' | 'archived';

@Component({
  selector: 'app-guests-details-notes-tab',
  templateUrl: './guests-details-notes-tab.component.html',
  styleUrls: ['./guests-details-notes-tab.component.scss']
})
export class GuestsDetailsNotesTabComponent implements OnChanges, OnDestroy {
  @Input() public guestId: number;

  private archivedNotesGetSubscription: Subscription;
  private activeNotesGetSubscription: Subscription;
  private notesPostSubscription: Subscription;
  private activeNotesPostSubscription: Subscription;

  public activeNotes: any[] = [];
  public archivedNotes: any[] = [];

  public activeNotesTitle = 'Aктивные заметки';
  public archivedNotesTitle = 'Архивные заметки';

  public activeTab: Tab = 'active';

  public messageForm: FormGroup;

  public isDeleteMessageModalVisible = false;

  public noteToDelete = null;
  public noteToUpdate = null;

  public indexOfMsgToChange = null;
  public isMessageEdited = false;

  constructor(private guestsService: GuestsService) {
    this.messageForm = new FormGroup({
      message: new FormControl(''),
    });
  }

  public ngOnChanges() {
    if (this.guestId !== null) {
      this.retrieveAllNotes();
    }
  }

  public ngOnDestroy() {
    if (this.archivedNotesGetSubscription != null) {
      this.archivedNotesGetSubscription.unsubscribe();
    }
    if (this.activeNotesGetSubscription != null) {
      this.activeNotesGetSubscription.unsubscribe();
    }
    if (this.notesPostSubscription != null) {
      this.notesPostSubscription.unsubscribe();
    }
    if (this.activeNotesPostSubscription != null) {
      this.activeNotesPostSubscription.unsubscribe();
    }
  }

  public get isUpdateButtonDisabled() {
    return this.isMessageEdited &&
      (this.messageForm.get('message').value === '' || this.messageForm.get('message').value === null);
  }

  private addNote(noteText: string) {
    const payload = {text: noteText};
    const request$ = this.isMessageEdited && this.noteToUpdate != null
      ? this.guestsService.updateGuestNote(this.guestId, this.noteToUpdate.id, payload) // edit
      : this.guestsService.addGuestNote(this.guestId, payload); // add new
    this.notesPostSubscription = request$
      .subscribe(resp => {
        if (this.isMessageEdited) {
          this.isMessageEdited = false;
        }
        this.retrieveAllNotes();
      });
  }

  public get selectedNoteList(): any[] {
    return this.activeTab === 'active'
      ? this.activeNotes
      : this.archivedNotes;
  }

  public selectTab(tabName: 'active' | 'archived'): void {
    this.activeTab = tabName;
  }

  private processNotes(notes: any[]): any[] {
    return notes.map(item => {
      item.created_at = moment(item.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD.MM.YYYY (HH:mm)');
      return item;
    });
  }

  private retrieveAllNotes() {
    this.archivedNotesGetSubscription = this.guestsService.getGuestNotes(this.guestId).pipe(
      map(res => this.processNotes(res)))
      .subscribe(notes => {
        this.activeNotes = notes;
      });
    this.activeNotesGetSubscription = this.guestsService.getGuestNotes(this.guestId, true)
      .pipe(map(res => this.processNotes(res)))
      .subscribe(notes => {
        this.archivedNotes = notes;
      });
  }

  public handleCancel() {
    this.isDeleteMessageModalVisible = false;
  }

  public handleOk(): void {
    if (this.noteToDelete != null) {
      const request$ = this.activeTab === 'active'
        ? this.guestsService.deleteGuestNote(this.guestId, this.noteToDelete.id)
        : this.guestsService.deleteGuestNote(this.guestId, this.noteToDelete.id, true);
      request$.subscribe(resp => {
        this.retrieveAllNotes();
        this.isDeleteMessageModalVisible = false;
      });
    }
  }

  public deleteItem(post: any): void {
    this.noteToDelete = post;
    this.isDeleteMessageModalVisible = true;
  }

  public submitForm(): void {
    const value = this.messageForm.get('message').value;
    if (value == null || value === '') {
      return;
    }
    this.addNote(value);
    this.messageForm.reset();
  }

  public editItem(post: any): void {
    this.noteToUpdate = post;
    this.isMessageEdited = true;
    this.messageForm.get('message').setValue(post.text);
  }
}
