import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AccountManagementComponent} from './account-management.component';
import {AccountManagementRoutingModule} from './account-management-routing.module';
import {SharedModule} from '@app/core/shared/shared.module';

@NgModule({
  declarations: [
    AccountManagementComponent
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    AccountManagementRoutingModule
  ],
  exports: [],
  entryComponents: [],
})
export class AccountManagementModule {
}
