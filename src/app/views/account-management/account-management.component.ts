import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Patterns} from '@app/core/constants/validation-patterns';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.scss']
})
export class AccountManagementComponent {
  public isDeleteUser = false;
  public newUserModal = false;
  public isInviteUserVisible = false;
  public isInviteUserConfirmLoading = false;

  public inviteUserForm: FormGroup;

  public mockedUserList = [
    {
      user: {
        name: 'William Sheppard',
        phone: '264374932782',
        email: 'williamsheppard@gmail.com'
      },
      accessRight: '',
      lastLoginData: '01.07.2019 (12:45)'
    },
    {
      user: {
        name: 'William Sheppard',
        phone: '264374932782',
        email: 'williamsheppard@gmail.com'
      },
      accessRight: '',
      lastLoginData: '01.07.2019 (12:45)'
    },
    {
      user: {
        name: 'William Sheppard',
        phone: '264374932782',
        email: 'williamsheppard@gmail.com'
      },
      accessRight: '',
      lastLoginData: '01.07.2019 (12:45)'
    },
  ];

  public mockedDataForEditModel = [];
  public mockedParamsForEditModel = [
    {
      title: 'Календарь',
      enabled: true,
    },
    {
      title: 'Тарифы и Наличие мест',
      enabled: false,
    },
    {
      title: 'Брони и Заявки',
      enabled: true,
    },
    {
      title: 'Гости',
      enabled: true,
    },
    {
      title: 'Контрагенты',
      enabled: true,
    },
    {
      title: 'Гостиница',
      enabled: true,
    },
  ];

  public getMockedDataForEditModel(index: number): any[] {
    return this.mockedDataForEditModel[index];
  }

  public popupVisibility: any[] = [];
  public editModalVisibility: any[] = [];

  constructor(
    private fb: FormBuilder,
    private modal: NzModalService,
    private message: NzMessageService,
  ) {
    this.mockedUserList.forEach(user => {
      this.popupVisibility.push({visible: false});
      this.editModalVisibility.push({visible: false});
      this.mockedDataForEditModel.push(this.mockedParamsForEditModel);
    });

    this.inviteUserForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern(Patterns.emailPattern)]),
    });
  }

  public isPopupVisible(index: number): boolean {
    return this.popupVisibility[index].visible;
  }

  public isEditModalVisible(index: number): boolean {
    return this.editModalVisibility[index].visible;
  }

  public closeEditModal(index: number): void {
    this.editModalVisibility[index].visible = false;
  }

  public manageAccount(index: number): void {
    this.popupVisibility[index].visible = true;
  }

  public closePopup(index: number): void {
    this.popupVisibility[index].visible = false;
  }

  public edit(index: number): void {
    this.popupVisibility[index].visible = false;
    this.editModalVisibility[index].visible = true;
  }

  public grantRights(index: number): void {
    if (this.mockedUserList[index]['administrator']) {
      this.mockedUserList[index]['administrator'] = false;
      this.modal.warning({
        nzTitle: `<b>Отключение прав администратора</b>`,
        nzContent: `Вы точно хотите отключить права администратора для пользователя ${this.mockedUserList[index].user.name}?`,
        nzOkText: 'Отключить',
        nzCancelText: 'Отмена',
        nzOkType: 'danger',
        nzOnOk: () => {
          this.notAdministratorMessage(this.mockedUserList[index].user.name);
        },
      });
    } else {
      this.mockedUserList[index]['administrator'] = true;
      this.modal.warning({
        nzTitle: `<b>Новый администратор</b>`,
        nzContent: `Вы собираетесь предоставить права администратора и полный доступ к
                    Экстранету этого объекта пользователю ${this.mockedUserList[index].user.name}.`,
        nzOnOk: () => {
          this.setAdministratorMessage(this.mockedUserList[index].user.name);
        },
        nzCancelText: 'Cancel',
        nzOnCancel: () => console.log('Cancel')
      });
    }
  }

  public deleteUser(index: number): void {
    this.modal.warning({
      nzTitle: `<b>Удалить пользователя?</b>`,
      nzContent: `${this.mockedUserList[index].user.name} будет удален(а) в качестве пользователя для этого объекта. Продолжить?`,
      nzOkType: 'danger',
      nzOnOk: () => {
        this.deleteUserMessage(this.mockedUserList[index].user.name);
      },
      nzCancelText: 'Cancel',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  private deleteUserMessage(userName): void {
    this.message.success(`Вы удалили пользователя ${userName} из списка управления этим объектом`, {
      nzDuration: 5000
    });
  }

  private setAdministratorMessage(userName): void {
    this.message.success(`Вы предоставили права администратора пользователю ${userName}`, {
      nzDuration: 5000
    });
  }

  private notAdministratorMessage(userName): void {
    this.message.success(`Настройки доступа изменены — у пользователя ${userName} больше нет прав администратора`, {
      nzDuration: 5000
    });
  }

  private setPermissionsMessage(userName): void {
    this.message.success(`Права доступа для пользователя ${userName} сохранены`, {
      nzDuration: 5000
    });
  }

  public inviteUser(): void {
    this.isInviteUserVisible = true;
  }

  public handleInviteUserCancel(): void {
    this.isInviteUserVisible = false;
    this.inviteUserForm.reset();
  }

  public handleInviteUserOk(): void {
    this.isInviteUserVisible = false;

    for (const key in this.inviteUserForm.controls) {
      this.inviteUserForm.controls[key].markAsDirty();
      this.inviteUserForm.controls[key].updateValueAndValidity();
    }
    this.newUserModal = true;

    // @TODO: add logic later

    this.inviteUserForm.reset();
  }

  public handleEditModelCancel(index: number): void {
    this.editModalVisibility[index].visible = false;
  }

  public handleEditModelOk(index: number): void {
    this.editModalVisibility[index].visible = false;
    this.setPermissionsMessage(this.mockedUserList[index].user.name);
  }

  public search(): void {

  }

  public closeEditModalNewUser() {
    this.newUserModal = false;
  }

  public handleEditModelOkNewUser() {

  }
}
