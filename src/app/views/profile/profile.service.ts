import {Injectable} from '@angular/core';
import {ProfileApiService} from '@app/shared/services/profile-api/profile-api.service';
import {Observable} from 'rxjs';
import {ProfileModel} from '@app/shared/models/profile.model';
import {AuthService} from '@app/shared/services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private authService: AuthService, private profileApiService: ProfileApiService) {
  }

  getProfileData(): Observable<ProfileModel> {
    return this.profileApiService.getProfileData();
  }

  updateProfileData(body: any): Observable<any> {
    return this.profileApiService.updateProfileData(body);
  }

  updateEmail(body: any): Observable<any> {
    return this.profileApiService.updateEmail(body);
  }

  updatePassword(body: any): Observable<any> {
    return this.profileApiService.updatePassword(body);
  }

  updateAvatar(body: any): Observable<any> {
    return this.profileApiService.updateAvatar(body);
  }

  logout() {
    this.authService.logOut();
  }
}
