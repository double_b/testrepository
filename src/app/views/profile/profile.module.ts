import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NgZorroAntdModule} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ProfileComponent} from './profile.component';
import {ProfileNavigationPanelComponent} from './components/profile-navigation-panel/profile-navigation-panel.component';
import {ProfileMainTabComponent} from './components/profile-main-tab/profile-main-tab.component';
import {ProfilePersonalInfoTabComponent} from './components/profile-personal-info-tab/profile-personal-info-tab.component';
import {ProfilePasswordTabComponent} from './components/profile-password-tab/profile-password-tab.component';
import {ProfileEmailTabComponent} from './components/profile-email-tab/profile-email-tab.component';
import {ProfileContractsTabComponent} from './components/profile-contracts-tab/profile-contracts-tab.component';
import {FormatPricePipe} from 'src/app/core/pipes/format-price.pipe';
import {ProfileRoutingModule} from './profile-routing.module';
import {SharedModule} from '@app/core/shared/shared.module';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileNavigationPanelComponent,
    ProfileMainTabComponent,
    ProfilePersonalInfoTabComponent,
    ProfilePasswordTabComponent,
    ProfileEmailTabComponent,
    ProfileContractsTabComponent,
  ],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ProfileRoutingModule
  ],
  exports: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [FormatPricePipe]
})
export class ProfileModule {
}
