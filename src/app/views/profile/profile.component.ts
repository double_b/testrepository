import {Component, OnInit} from '@angular/core';
import {ProfileTab} from './models/profile-tab';
import {Profile} from 'src/app/core/models/profile';
import {NotificationService} from 'src/app/shared/services/notification/notification.service';
import { ProfileApiService } from "@app/shared/services/profile-api/profile-api.service";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  constructor(private profileService: ProfileApiService,
              private notificationService: NotificationService) {
  }

  public activeTab: ProfileTab = 'main';
  public headerTitle = 'Профиль';
  profileData: Profile;

  ngOnInit(): void {
    this.profileService.getProfileData().subscribe(res => {
      const formattPhone = this.formatNumber(res.phone);
      this.profileData = res;
      this.profileData.phone = formattPhone;
    }, err => {
      this.notificationService.errorMessage('Ошибка сервера');
    });
  }

  public activateTab(tabName: ProfileTab): void {
    this.activeTab = tabName;
  }

  changeProfileData(data: Profile) {
    data.phone = this.formatNumber(data.phone);
    this.profileData = data;
  }

  formatNumber(value: string): string {
    const nonCodeNumber = value.slice(3);
    const first = nonCodeNumber.slice(0, 2);
    const second = nonCodeNumber.slice(2, 5);
    const three = nonCodeNumber.slice(5, 7);
    const four = nonCodeNumber.slice(7, 9);
    return first + ' ' + second + ' ' + three + ' ' + four;
  }

}
