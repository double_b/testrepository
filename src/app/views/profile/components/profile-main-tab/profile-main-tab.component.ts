import {Component, Input, OnChanges} from '@angular/core';
import {Profile} from 'src/app/core/models/profile';
import * as moment from 'moment';

@Component({
  selector: 'app-profile-main-tab',
  templateUrl: './profile-main-tab.component.html',
  styleUrls: ['./profile-main-tab.component.scss']
})
export class ProfileMainTabComponent implements OnChanges {
  public isLoadingModal = false;
  public isLoading = false;
  public imageUrl: any;
  public lastSeenDateProcessed: string | null;

  @Input() userData: Profile;

  constructor() {
  }

  public ngOnChanges(): void {
    if (this.userData != null && (this.userData.last_seen != null || this.userData.last_seen !== '')) {
      const targetDate = moment(this.userData.last_seen, 'YYYY-DD-MM HH:mm:ss'); // 2019-20-12 12:00:00
      this.lastSeenDateProcessed = targetDate.isValid() ? targetDate.format('DD.MM.YYYY') : null;
    }
  }
}
