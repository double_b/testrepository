import {Component} from '@angular/core';

@Component({
  selector: 'app-profile-contracts-tab',
  templateUrl: './profile-contracts-tab.component.html',
  styleUrls: ['./profile-contracts-tab.component.scss']
})
export class ProfileContractsTabComponent {
  public isSuccessMessageShown = false;
  public message = `
    Вы запросили копию общих условий предоставления услуг.
    Мы отправим ее на вашу электронную почту в течение 2 рабочих дней.
  `;

  public requestContracts(): void {
    setTimeout(() => {
      this.isSuccessMessageShown = true;
    }, 1000);
  }
}
