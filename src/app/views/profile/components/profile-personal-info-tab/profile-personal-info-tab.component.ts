import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UploadXHRArgs} from 'ng-zorro-antd';
import {of} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Profile} from 'src/app/core/models/profile';
import {NotificationService} from 'src/app/shared/services/notification/notification.service';
import { ProfileApiService } from "@app/shared/services/profile-api/profile-api.service";

@Component({
  selector: 'app-profile-personal-info-tab',
  templateUrl: './profile-personal-info-tab.component.html',
  styleUrls: ['./profile-personal-info-tab.component.scss']
})
export class ProfilePersonalInfoTabComponent implements OnInit {
  public isLoadingModal = false;
  public isLoading = false;
  public imageUrl: any;
  @Input() userData: Profile;
  @Output() changeProfileData = new EventEmitter<Profile>();

  public userMainDataForm: FormGroup;

  constructor(private profileService: ProfileApiService,
              private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.userMainDataForm = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
    });

    this.userMainDataForm.get('phone').valueChanges.subscribe(value => {
      if (value && value !== '') {
        this.formatNumber(value);
      }
    });
    this.initializeForm(this.userData);
  }

  private initializeForm(userData: any): void {
    this.userMainDataForm.get('first_name').setValue(userData.first_name);
    this.userMainDataForm.get('last_name').setValue(userData.last_name);
    this.userMainDataForm.get('phone').setValue(userData.phone.trim());
  }

  private formatNumber(value: string) {
    if (value.slice(-1) !== ' ' && value.length === 3) {
      const first = value.slice(0, 2);
      const second = value.slice(2, 3);
      this.userMainDataForm.get('phone').patchValue(first + ' ' + second);
    }
    if (value.slice(-1) !== ' ' && value.length === 7) {
      const first = value.slice(0, 6);
      const second = value.slice(6, 7);
      this.userMainDataForm.get('phone').patchValue(first + ' ' + second);
    }
    if (value.slice(-1) !== ' ' && value.length === 10) {
      const first = value.slice(0, 9);
      const second = value.slice(9, 10);
      this.userMainDataForm.get('phone').patchValue(first + ' ' + second);
    }
    if (value.length > 12) {
      this.userMainDataForm.get('phone').patchValue(value.slice(0, 12));
    }
  }

  imageUploadingRequestHandler = (item: UploadXHRArgs) => {
    this.isLoadingModal = true;

    // @FIXME: temp logic
    //  must return Subscribtion
    of(item).subscribe(
      res => {
        this.isLoading = false;
        const imgFile = res.file;
        imgFile.status = 'done';
        this.updatePhoto(imgFile);
      },
      err => {
        // this.utils.presentErrorResponseMessage(err);
      },
      () => {
        this.isLoadingModal = false;
      }
    );
  };

  public updatePhoto(imageFile: any): void {
    const formData: FormData = new FormData();
    formData.append('avatar', imageFile);
    this.profileService.updateAvatar(formData).subscribe(res => {
      this.notificationService.successMessage('Фото было успешно обновлено');
      this.userData.avatar = res.data.avatar;
      this.changeProfileData.emit(this.userData);
    });
  }

  public submitForm() {
    if (this.userMainDataForm.invalid) {
      return;
    }
    const formData = this.userMainDataForm.value;
    formData.phone = '998' + formData.phone.replace(/\s/g, '');
    this.profileService.updateProfileData(formData).subscribe(res => {
      this.changeProfileData.emit(res.data);
      this.userMainDataForm.reset();
      this.notificationService.successMessage('Данные профиля были успешно обновлены');
      console.log(res.data);
      this.initializeForm(res.data);
    }, err => {
      this.addError(err.error.errors);
      this.notificationService.errorMessage('Не правильно заполнена форма');
    });
  }

  addError(errors) {
    if (!errors) {
      return;
    }
    // tslint:disable-next-line:forin
    for (let error in errors) {
      let errorsStr = '';
      errors[error].forEach(text => {
        errorsStr = errorsStr + text + ' ';
      });
      this.userMainDataForm.get(error).setErrors({serverError: errorsStr});
    }
  }

  onSubmited() {
    return this.userMainDataForm.valid && this.userMainDataForm.pristine;
  }
}
