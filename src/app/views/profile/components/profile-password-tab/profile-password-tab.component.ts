import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from 'src/app/shared/services/notification/notification.service';
import { ProfileApiService } from "@app/shared/services/profile-api/profile-api.service";


@Component({
  selector: 'app-profile-password-tab',
  templateUrl: './profile-password-tab.component.html',
  styleUrls: ['./profile-password-tab.component.scss']
})
export class ProfilePasswordTabComponent implements OnInit {

  public usePasswordForm: FormGroup;

  constructor(private fb: FormBuilder,
              private profileService: ProfileApiService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.usePasswordForm = this.fb.group({
      password: new FormControl('', [Validators.required]),
      new_password: new FormControl('', Validators.required),
      new_password_confirmation: new FormControl('', [Validators.required, this.confirmValidator]),
    });
  }

  public submitForm(): void {
    // tslint:disable-next-line:forin
    for (const key in this.usePasswordForm.controls) {
      this.usePasswordForm.controls[key].markAsDirty();
      this.usePasswordForm.controls[key].updateValueAndValidity();
    }

    if (this.usePasswordForm.invalid) {
      return;
    }
    const formData = this.usePasswordForm.value;
    this.profileService.updatePassword(formData).subscribe(res => {
      this.usePasswordForm.reset();
      this.notificationService.successMessage('Пароль был успешно изменен');
    }, err => {
      this.addError(err.error.errors);
      this.notificationService.errorMessage('Не правильно заполнена форма');
    });
  }

  addError(errors) {
    if (!errors) {
      return;
    }
    // tslint:disable-next-line:forin
    for (let error in errors) {
      let errorsStr = '';
      errors[error].forEach(text => {
        errorsStr = errorsStr + text + ' ';
      });
      this.usePasswordForm.get(error).setErrors({serverError: errorsStr});
    }
  }

  public validateConfirmPassword(): void {
    setTimeout(() => this.usePasswordForm.controls.confirm.updateValueAndValidity());
  }

  // custom validators

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return {error: true, required: true};
    } else if (control.value !== this.usePasswordForm.get('new_password').value) {
      return {confirm: true, error: true};
    }
    return {};
  };

  onSubmited() {
    return this.usePasswordForm.valid;
  }
}
