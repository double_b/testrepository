import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Profile} from 'src/app/core/models/profile';
import {NotificationService} from 'src/app/shared/services/notification/notification.service';
import {Patterns} from '@app/core/constants/validation-patterns';
import { ProfileApiService } from "@app/shared/services/profile-api/profile-api.service";

@Component({
  selector: 'app-profile-email-tab',
  templateUrl: './profile-email-tab.component.html',
  styleUrls: ['./profile-email-tab.component.scss']
})
export class ProfileEmailTabComponent implements OnChanges {
  public isEditModalVisible = false;
  public updateEmailForm: FormGroup;
  public passwordConfirmForm: FormGroup;

  public isPasswordConfirmModalError = false;
  public isGeneralServerError = false;

  public serverError = '';

  @Input() userData: Profile;
  @Output() changeProfileData = new EventEmitter<Profile>();

  constructor(private fb: FormBuilder,
              private notificationService: NotificationService,
              private profileService: ProfileApiService
  ) {
    this.updateEmailForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(Patterns.emailPattern)]),
    });

    this.passwordConfirmForm = this.fb.group({
      password: new FormControl('', [Validators.required]),
    });
  }

  public ngOnChanges() {
    if (this.userData != null && (this.userData.email != null || this.userData.email !== '')) {
      this.updateEmailForm.get('email').setValue(this.userData.email);
    }
  }

  public closeEditModal() {
    this.isEditModalVisible = false;
  }

  public handleEditModelCancel() {
    this.isEditModalVisible = false;
    this.serverError = '';
    this.isPasswordConfirmModalError = false;
    this.passwordConfirmForm.reset();
  }

  public handleEditModelOk() {
    const pass = this.passwordConfirmForm.get('password').value;
    const eMail = this.updateEmailForm.get('email').value;
    const body = {
      password: pass,
      email: eMail
    };
    this.isEditModalVisible = false;
    this.submitConfirmForm(body);
  }

  public submitForm() {
    // tslint:disable-next-line:forin
    for (const key in this.updateEmailForm.controls) {
      this.updateEmailForm.controls[key].markAsDirty();
      this.updateEmailForm.controls[key].updateValueAndValidity();
    }
    if (this.updateEmailForm.invalid) {
      return;
    }
    this.isEditModalVisible = true;
  }

  public submitConfirmForm(body) {
    this.isEditModalVisible = true; // force showing

    // tslint:disable-next-line:forin
    for (const key in this.passwordConfirmForm.controls) {
      this.passwordConfirmForm.controls[key].markAsDirty();
      this.passwordConfirmForm.controls[key].updateValueAndValidity();
    }

    if (this.passwordConfirmForm.invalid) {
      return;
    }

    this.profileService.updateEmail(body).subscribe(res => {
      this.changeProfileData.emit(res.data);
      this.isEditModalVisible = false;
      this.passwordConfirmForm.reset();
      this.notificationService.successMessage('E-mail был успешно обновлен');
    }, err => {
      if (err.error.errors != null) {
        if (err.error.errors.hasOwnProperty('password')) {
          this.isPasswordConfirmModalError = true;
          this.isGeneralServerError = false;
        } else {
          this.isGeneralServerError = true;
          this.isPasswordConfirmModalError = false;
          this.isEditModalVisible = false;
          this.passwordConfirmForm.reset();
        }
        this.addError(err.error.errors);
        this.notificationService.errorMessage('Неправильно заполнена форма');
      } else {
        this.isPasswordConfirmModalError = false;
        this.isGeneralServerError = false;
        this.isEditModalVisible = false;
        this.passwordConfirmForm.reset();
        this.notificationService.errorMessage('Неизвестная ошибка при запросе. Повторите действие позже');
        this.serverError = '';
      }
    });
  }

  public onEmailInputHandler(event: any): void {
    this.serverError = '';
    this.isGeneralServerError = false;
  }

  public onPasswordInputHandler(event: any): void {
    this.serverError = '';
    this.isPasswordConfirmModalError = false;
  }

  addError(errors) {
    if (!errors) {
      return;
    }
    // tslint:disable-next-line:forin
    for (let error in errors) {
      let errorsStr = '';
      errors[error].forEach(text => {
        errorsStr = errorsStr + text + ' ';
      });
      this.serverError = errorsStr;
    }
  }


  onSubmited() {
    return this.updateEmailForm.valid;
  }

  onConfirmSubmited() {
    return this.passwordConfirmForm.valid;
  }
}
