import {Component, EventEmitter, Output} from '@angular/core';
import {ProfileTab} from '../../models/profile-tab';
import {Router} from '@angular/router';
import {NavBarService} from '@app/shared/components/nav-bar/nav-bar.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ProfileService} from '@app/views/profile/profile.service';

interface ITab {
  name: string;
  content: string;
}

@Component({
  selector: 'app-profile-navigation-panel',
  templateUrl: './profile-navigation-panel.component.html',
  styleUrls: ['./profile-navigation-panel.component.scss']
})
export class ProfileNavigationPanelComponent {
  @Output() activatedTab = new EventEmitter<ProfileTab>();

  public isLogoutVisible = false;
  public selectedIndex = 0;
  public currentTab: ProfileTab = 'main';
  public tabs: ITab[] = [
    {
      name: 'main',
      content: 'Профиль',
    },
    {
      name: 'personal-info',
      content: 'Личные данные',
    },
    {
      name: 'password',
      content: 'Пароль',
    },
    {
      name: 'e-mail',
      content: 'E-mail',
    },
    {
      name: 'logout',
      content: 'Выйти',
    },
    {
      name: 'contracts',
      content: 'Договора',
    },
  ];

  constructor(private profileService: ProfileService,
              private router: Router,
              private navBarService: NavBarService) {
  }

  public activateTab(tabName: ProfileTab): void {
    this.currentTab = tabName;
    this.activatedTab.emit(tabName);
  }

  public closeLogoutModal(): void {
    this.isLogoutVisible = false;
  }

  public handleCancel(): void {
    this.isLogoutVisible = false;
  }

  public handleOk(): void {
    this.isLogoutVisible = false;
    this.profileService.logout();
    this.navBarService.clean();
    setTimeout(() => {
      this.router.navigate([RoutesEnum.AUTH, RoutesEnum.LOGIN]).finally();
    }, 100);
  }

  public logOut(): void {
    this.isLogoutVisible = true;
  }
}
