import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {ChessService} from '../chess.service';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import {DatesEnum} from '@app/core/constants/dates.enum';
import {ListEditModalModel, SimpleWeekDayModel} from '@app/views/chess/helpers/simple.model';


@Component({
  selector: 'app-chess-list-edit',
  templateUrl: './chess-list-edit.component.html',
  styleUrls: ['./chess-list-edit.component.scss']
})
export class ChessListEditComponent {
  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();
  @Input() mainValue: ListEditModalModel = new ListEditModalModel();
  @ViewChild('datesView', {static: true}) datesView: ElementRef;
  dateFormat = 'dd.MM.yyyy';
  daysOfWeek: SimpleWeekDayModel[] = [];
  visible = false;
  today = new Date();
  selectedDates: Date[] = [];

  selectParams = 1;
  price = null;
  roomType = null;

  isActive = true;

  isVisible = false;

  constructor(private chessService: ChessService) {
    // заполняем дни недели (чек боксы)
    DatesEnum.WEEK.forEach((day, i) => {
      this.daysOfWeek.push({name: day, val: true, index: i});
    });
    setTimeout(() => {
      this.isVisible = true;
    }, 100);
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };

  public closeCollapse() {
    setTimeout(() => {
      this.isVisible = false;
      this.onClose.emit();
    }, 1000);
  }

  public async savePrice() {
    const w = [];
    this.daysOfWeek.forEach(d => {
      if (d.val) {
        w.push(d.index);
      }
    });
    const dateFrom = this.chessService.formatDate(this.selectedDates[0], 'yyyy-mm-dd');
    const dateTo = this.chessService.formatDate(this.selectedDates[1], 'yyyy-mm-dd');
    this.chessService.changePrices(this.price.toString(),
      dateFrom, dateTo, this.mainValue.room,
      this.mainValue.room.rates.find(rate => rate.id === this.roomType), null, null, true).finally();
    this.closeCollapse();
  }

  onDateChange() {
    this.isActive = this.selectedDates.length <= 0;
  }
}
