import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChessService} from '../chess.service';
import {forkJoin, Subscription} from 'rxjs';
import {SimpleChannelModel, SimpleRoomTypeModel} from '@app/views/chess/helpers/simple.model';
import {ChessEnum} from '@app/views/chess/helpers/chess.enum';

@Component({
  selector: 'app-chess-sidebar',
  templateUrl: './chess-sidebar.component.html',
  styleUrls: ['./chess-sidebar.component.scss']
})
export class ChessSidebarComponent implements OnInit, OnDestroy {
  // подписки только для того, чтобы при дестрое от них отписаться
  channelsRoomsSub: Subscription = null;

  // ключ дефолтного канала
  DEFAULT_CHANNEL = 'smartbooking';
  DEFAULT_ROOM_TYPE = 0;
  DEFAULT_DATE_FORMAT = 'dd.MM.yyyy';

  channels: SimpleChannelModel[] = [];
  selectedChannel = this.DEFAULT_CHANNEL;

  rooms: SimpleRoomTypeModel[] = [];
  selectedRoomType = this.DEFAULT_ROOM_TYPE;

  dates: Date[] = [];

  /**
   * переключатель видимости тарифов по кол-ву гостей
   */
  pricesGuests = ChessService.DEF_SHOW_GUEST_PRICES;

  /**
   * переключатель видимости тарифов по мин кол-ву дней
   */
  restrictions = ChessService.DEF_SHOW_RESTRICTIONS;

  constructor(private chessService: ChessService) {
  }

  ngOnInit() {
    // добавляем дефолтный канал в список каналов
    this.channels.push({name: 'CHESS.RECEPTION_PRICE', key: this.DEFAULT_CHANNEL});
    // добавляем дефолтный тип номера (Все номера)
    this.rooms.push({id: this.DEFAULT_ROOM_TYPE, name: 'CHESS.ALL_ROOMS'});
    this.setDefaultDates();
    // забираю с сервера каналы продаж и типы номеров
    this.channelsRoomsSub = forkJoin([
      this.chessService.getChannels(),
      this.chessService.getRoomTypes()]).subscribe(res => {
      res[0].forEach(item => this.channels.push({key: item.channel_key, name: item.channel.name}));
      res[1].forEach(item => this.rooms.push({id: item.id, name: item.name}));
      this.rooms = this.rooms.sort((a, b) => {
        return a.id - b.id;
      });
      this.filterData();
    });
  }

  ngOnDestroy() {
    if (this.channelsRoomsSub != null) {
      this.channelsRoomsSub.unsubscribe();
    }
  }

  /**
   * задаем первоначальные даты
   * по дефолту период - один месяц
   */
  setDefaultDates() {
    const today = new Date();
    this.dates.push(today);
    this.dates.push(new Date(today.getFullYear(), today.getMonth() + 1, today.getDate()));
  }

  public changePricesGuests(): void {
    this.chessService.updateVisible(ChessEnum.OCCUPANCY, this.pricesGuests);
  }

  public changeRestrictions(): void {
    this.chessService.updateVisible(ChessEnum.RESTRICTION, this.restrictions);
  }

  public filterData(): void {
    this.chessService.doFilter({
      allRooms: this.rooms,
      currentRoomType: this.selectedRoomType,
      channel: this.selectedChannel,
      dateFrom: this.dates[0],
      dateTo: this.dates[1]
    }, true);
  }

}
