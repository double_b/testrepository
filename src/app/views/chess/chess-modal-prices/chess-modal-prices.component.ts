import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ChessService} from '../chess.service';
import {PriceModalModel, SimpleOccupancyModel} from '@app/views/chess/helpers/simple.model';
import {Utils} from '@app/shared/helpers/utils';
import {OccupancyModel} from '@app/shared/models/occupancy.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-chess-modal-prices',
  templateUrl: './chess-modal-prices.component.html',
  styleUrls: ['./chess-modal-prices.component.scss']
})
export class ChessModalPricesComponent implements OnInit {
  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input() mainModel: PriceModalModel = new PriceModalModel();
  maxGuest: number;
  occupancies: SimpleOccupancyModel[] = [];
  isVisible = false;
  isLoading = false;

  discountSub: Subscription = null;

  constructor(private chessService: ChessService) {
  }

  nameGuests(guests) {
    if (guests < 10 || guests >= 20) {
      if (guests % 10 === 1) {
        return 'CHESS.GUEST';
      } else if (guests % 10 > 1 && guests % 10 < 5) {
        return 'CHESS.GUEST_TWO';
      } else {
        return 'CHESS.GUESTS';
      }
    } else {
      return 'CHESS.GUESTS';
    }
  }

  ngOnInit() {
    this.isVisible = true;
    this.isLoading = true;
    this.getDiscounts();
  }

  getDiscounts() {
    this.discountSub = this.chessService.getDiscounts(this.mainModel.roomTypeId, this.mainModel.rateId).subscribe(res => {
      this.maxGuest = Math.max.apply(Math, res.map(o => {
        return o.guests;
      }));
      this.occupancies = [];
      for (let i = this.maxGuest; i > 0; i--) {
        const occupancy = res.find(item => item.guests === i);
        const temp = new SimpleOccupancyModel();
        temp.isLocal = false;
        temp.status = false;
        temp.guests = i;
        temp.validState = '';
        temp.mode = 0;
        temp.amount = null;
        const tempLocal = new SimpleOccupancyModel();
        tempLocal.isLocal = true;
        tempLocal.status = false;
        tempLocal.guests = i;
        tempLocal.validState = '';
        tempLocal.mode = 0;
        tempLocal.amount = null;
        if (Utils.isExist(occupancy)) {
          temp.status = occupancy.amount !== 0;
          temp.mode = occupancy.mode;
          temp.amount = occupancy.amount;

          tempLocal.status = occupancy.discount_local === 1;
          tempLocal.mode = occupancy.mode_local;
          tempLocal.amount = occupancy.amount_local;
        }
        this.occupancies.push(temp);
        this.occupancies.push(tempLocal);
      }
      this.isLoading = false;
    });
  }

  changeStatus(occupancy: SimpleOccupancyModel) {
    if (!occupancy.status) {
      this.occupancies.find(i => i.guests === occupancy.guests && i.isLocal).status = false;
    }
  }

  handleOk() {
    const tempOccupancies: OccupancyModel[] = [];
    for (const item of this.occupancies) {
      if (item.isLocal) {
        continue;
      }
      if (!item.isLocal && item.guests === this.maxGuest) {
        continue;
      }
      const temp = new OccupancyModel();
      temp.guests = item.guests;
      temp.mode = item.mode;
      temp.amount = Utils.isExist(item.amount) ? item.amount : 0;
      const itemLocal = this.occupancies.find(i => i.guests === item.guests && i.isLocal);
      if (item) {
        temp.discount_local = itemLocal.status ? 1 : 0;
        temp.mode_local = itemLocal.mode;
        temp.amount_local = itemLocal.amount;
      }
      tempOccupancies.push(temp);
    }
    this.chessService.saveDiscounts(this.mainModel.roomTypeId, this.mainModel.rateId,
      tempOccupancies, this.mainModel.dateFrom, this.mainModel.dateTo);
    this.handleCancel();
  }

  handleCancel() {
    if (this.discountSub != null) {
      this.discountSub.unsubscribe();
    }
    this.isVisible = false;
    this.closeEvent.emit();
  }

}

