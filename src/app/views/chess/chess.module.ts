import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';

import {ChessComponent} from './chess.component';
import {ChessSidebarComponent} from './chess-sidebar/chess-sidebar.component';
import {ChessTableComponent} from './chess-table/chess-table.component';
import {ChessModalPricesComponent} from './chess-modal-prices/chess-modal-prices.component';
import {ChessListEditComponent} from './chess-list-edit/chess-list-edit.component';
import localeFr from '@angular/common/locales/fr';
import {NZ_WAVE_GLOBAL_CONFIG} from 'ng-zorro-antd';
import {DragScrollModule} from 'ngx-drag-scroll';
import {DragulaModule} from 'ng2-dragula';
import {ClickOutsideModule} from 'ng4-click-outside';
import {ChessFilterPipe} from './helpers/chess-filter.pipe';

import {ChessRoutingModule} from './chess-routing.module';
import {SharedModule as OldSharedModule} from '@app/core/shared/shared.module';
import {SharedModule} from '@app/shared/shared.module';
import {SetPriceModalComponent} from './chess-table/set-price-modal/set-price-modal.component';
import {CustomPipesModule} from '@app/core/pipes/custom-pipes.module';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    ChessComponent,
    ChessSidebarComponent,
    ChessTableComponent,
    ChessModalPricesComponent,
    ChessFilterPipe,
    ChessListEditComponent,
    SetPriceModalComponent
  ],
  imports: [
    CommonModule,
    DragScrollModule,
    DragulaModule,
    ClickOutsideModule,
    ChessRoutingModule,
    SharedModule,
    OldSharedModule,
    CustomPipesModule
  ],
  exports: [
    ChessComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    {
      provide: NZ_WAVE_GLOBAL_CONFIG, useValue: {
        disabled: true
      }
    },
    {provide: LOCALE_ID, useValue: 'fr-FR'},
  ]
})
export class ChessModule {
}
