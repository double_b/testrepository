import {Component, EventEmitter, Input, Output} from '@angular/core';
import {GroupChangeModalModel} from '@app/views/chess/helpers/simple.model';

@Component({
  selector: 'app-set-price-modal',
  templateUrl: './set-price-modal.component.html',
  styleUrls: ['./set-price-modal.component.scss']
})
export class SetPriceModalComponent {

  @Input() modal: GroupChangeModalModel = new GroupChangeModalModel();
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSave: EventEmitter<GroupChangeModalModel> = new EventEmitter<GroupChangeModalModel>();

  save() {
    this.onSave.emit(this.modal);
  }

  cancel() {
    this.onCancel.emit();
  }

}
