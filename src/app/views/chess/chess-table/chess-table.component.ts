import {Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {ChessService} from '../chess.service';
import {
  DisplayStatusModel,
  GroupChangeModalModel,
  ListEditModalModel,
  MonthModel,
  PriceModalModel,
  SimpleRateModel,
  TableData,
  TableResModel
} from '@app/views/chess/helpers/simple.model';
import {ChessEnum} from '@app/views/chess/helpers/chess.enum';
import {DatesEnum} from '@app/core/constants/dates.enum';
import {RoomInventoryModel} from '@app/shared/models/room/room-inventory.model';
import {Subscription} from 'rxjs';
import {Utils} from '@app/shared/helpers/utils';
import {RoutesEnum} from '@app/core/constants/routes.enum';

@Component({
  selector: 'app-chess-table',
  templateUrl: './chess-table.component.html',
  styleUrls: ['./chess-table.component.scss']
})
export class ChessTableComponent implements OnInit, OnDestroy {
  @ViewChild('scrollOne', {static: true}) scrollOne: ElementRef;
  @ViewChild('loader', {static: true}) loader: ElementRef;
  @ViewChild('roomsView', {static: true}) roomsView: ElementRef;
  @ViewChild('loaderData', {static: true}) loaderData: ElementRef;
  @ViewChild('modalChangePrices', {static: true}) modalChangePrices: ElementRef;

  @Output() listEditClick: EventEmitter<ListEditModalModel> = new EventEmitter<ListEditModalModel>();

  months: MonthModel[] = [];

  modal: GroupChangeModalModel = new GroupChangeModalModel();
  displayModal = false;

  priceModal: PriceModalModel = new PriceModalModel();
  displayPriceModal = false;

  rooms: TableResModel[] = [];
  selectorActive = false;
  dateForUpdateIndex = 0;
  roomUpdate = 0;

  displayOccupancy = ChessService.DEF_SHOW_RESTRICTIONS;
  displayRestrictions = ChessService.DEF_SHOW_RESTRICTIONS;

  parentRow: Element = null;

  selectedObject: any = null;

  loaderActive = false;

  // даты - период данных во всей таблице
  tableDateFrom = new Date();
  tableDateTo = new Date();

  pressed = false;
  selectedRow: any = null;
  selectActive = false;

  private lastSelectIndex = 0;
  private lastIndex = 0;
  private firstIndex = 0;
  private lastChangeIndex = -1;
  private chosenIndexes = [];
  private rowType = null;

  private loadingData = true;
  scrolledBlock = 0;

  serviceSubscriptions: Subscription[] = [];

  private static findElemBySlag(e, slug): Element {
    try {
      if (e.path) {
        const path = e.path;
        let i = 0;
        while (path[i].localName !== slug) {
          i++;
        }
        return path[i];
      } else {
        let parent = e.target;
        while (parent.localName !== slug && parent) {
          parent = parent.parentElement;
        }
        return parent;
      }
    } catch {
      return null;
    }
  }

  constructor(public chessService: ChessService,
              private renderer: Renderer2) {
    this.serviceSubscriptions.push(this.chessService.filter.subscribe(data => this.update(data)));
    this.serviceSubscriptions.push(this.chessService.updateDay.subscribe(data => this.updateDay(data)));
    this.serviceSubscriptions.push(this.chessService._updateVisible.subscribe(data => {
      if (data.type === 'occupancy') {
        this.displayOccupancy = data.result;
      } else {
        this.displayRestrictions = data.result;
      }
    }));
    this.serviceSubscriptions.push(this.chessService.updateRoom.subscribe(data => {
      this.updateRoomType(data);
    }));
  }

  ngOnInit() {
    this.displayLoader(true);
  }

  ngOnDestroy() {
    this.serviceSubscriptions.forEach(sub => sub.unsubscribe());
  }

  displayLoader(val: boolean) {
    const loader: Element = this.loader.nativeElement;
    this.loaderActive = val;
    if (val) {
      loader.classList.add('active');
    } else {
      loader.classList.remove('active');
    }
  }

  // сбрасываем выбранные ячейки
  destroySelector(e, slug) {
    let container: any = ChessTableComponent.findElemBySlag(e, slug);
    if (!container) {
      container = document;
    }
    const elements = container.querySelectorAll('.selected');
    elements.forEach(el => {
      el.classList.remove('selected');
      el.classList.remove('first');
      el.classList.remove('last');
    });
    this.displayModalMin(false);
    this.chosenIndexes = [];
  }

  // строим строку с датами на самом верху страницы - откорректировано double_b
  private datesLineUpdate(dateFrom: Date, dateTo: Date) {
    this.months = [];

    this.tableDateFrom = dateFrom;
    this.tableDateTo = dateTo;
    let dayFrom = dateFrom.getDate();

    for (let yyyy = dateFrom.getFullYear(); yyyy <= dateTo.getFullYear(); yyyy++) {
      for (let mm = dateFrom.getMonth(); mm <= dateTo.getMonth(); mm++) {
        let dayPerMonth = new Date(yyyy, mm + 1, 0).getDate();
        const month: MonthModel = {
          year: yyyy,
          name: DatesEnum.MONTHS[mm],
          days: []
        };
        if (yyyy === dateTo.getFullYear() && mm === dateTo.getMonth()) {
          dayPerMonth = dateTo.getDate();
        }
        for (let dd = dayFrom; dd <= dayPerMonth; dd++) {
          const wD = new Date(yyyy, mm, dd - 1).getDay();
          month.days.push({
            isDayOff: wD === 5 || wD === 6,
            name: DatesEnum.WEEK_DOT[wD],
            day: dd
          });
        }
        this.months.push(month);
        dayFrom = 1;
      }
    }
  }

  // сохранение новых параметров при закрытии модалки (которая открывается при выделении нескольких ячеек на одной строке)
  public saveData(modal: GroupChangeModalModel) {
    this.displayLoader(true);
    switch (modal.type) {
      case ChessEnum.PRICE_MODAL: {
        const dateFrom = modal.room.dates[this.firstIndex];
        const dateTo = modal.room.dates[this.lastIndex];
        this.chessService.changePrices(modal.price.toString(),
          dateFrom, dateTo, modal.room, modal.rate, null, modal.selectParams).finally();
        break;
      }
      case ChessEnum.NUMB_MODAL: {
        const dateFrom = modal.room.dates[this.firstIndex];
        const dateTo = modal.room.dates[this.lastIndex];
        this.chessService.changeNumbers(modal.numb.toString(), dateFrom, dateTo, modal.room, null, modal.selectParams).finally();
        break;
      }
      case ChessEnum.RESERVATION_MODAL: {
        // если отмечено Удалить мин срок до бронирования - то сбрасываем счетчик перед отправкой
        let reservation = modal.reservation;
        if (modal.selectParams === 1) {
          reservation = 0;
        }
        this.chessService.changeMinReservation(reservation.toString(), this.firstIndex, this.lastIndex, modal.room, modal.rate).finally();
        break;
      }
      case ChessEnum.LIVE_MODAL: {
        // если отмечено Удалить мин срок проживания - то сбрасываем счетчик перед отправкой
        let live = modal.live;
        if (modal.selectParams === 1) {
          live = 1;
        }
        this.chessService.changeMinStay(live.toString(), this.firstIndex, this.lastIndex, modal.room, modal.rate).finally();
        break;
      }
    }
    this.closeModal();
  }

  public closeModal() {
    this.destroySelector(null, 'table');
  }

  public update(data: TableData) {
    if (data == null) {
      this.displayLoader(false);
      return;
    }
    this.displayLoader(true);
    this.rooms = [];
    this.rooms.push(data.res);

    setTimeout(() => {
      const roomView: Element = this.roomsView.nativeElement;
      if (this.chessService.filterData.currentRoomType === 0) {
        if (roomView.children[1].clientHeight < roomView.scrollHeight) {
          this.loadNextRoom();
        }
      } else {
        roomView.removeEventListener('scroll', this.loadData);
      }
    }, 600);

    this.datesLineUpdate(data.dateFrom, data.dateTo);

    this.displayLoader(false);
    if (this.chessService.filterData.currentRoomType === 0) {
      this.loaderData.nativeElement.classList.remove('hidden');
    }
  }

  private loadNextRoom() {
    this.loadingData = false;
    if (this.rooms.length === this.chessService.filterData.allRooms.length - 1) {
      this.loaderData.nativeElement.classList.add('hidden');
      return;
    }
    const nextId = this.chessService.filterData.allRooms[this.rooms.length + 1].id;
    if (this.rooms.filter(x => x.room_type.id === nextId).length === 0) {
      const dateFrom = new Date(this.rooms[0].dates[0]);
      const dateTo = new Date(this.rooms[0].dates[this.rooms[0].dates.length - 1]);
      const tempSub = this.chessService.getCalendarData(nextId, this.chessService.filterData.channel, dateFrom, dateTo)
        .subscribe(nextRoom => {
          nextRoom.displayStatuses = this.chessService.updateStatuses(nextRoom);
          // nextRoom = this.chessService.validData(nextRoom);
          this.rooms.push(nextRoom);
          this.loadingData = true;
          const roomView: Element = this.roomsView.nativeElement;
          if (roomView.children[this.rooms.length] && roomView.children[this.rooms.length].clientHeight < roomView.scrollHeight - 40) {
            this.loadNextRoom();
          }
          if (this.rooms.length === this.chessService.filterData.allRooms.length - 1) {
            this.loaderData.nativeElement.classList.add('hidden');
          }
        });
      this.serviceSubscriptions.push(tempSub);
    } else {
      this.loadingData = true;
    }
  }

  public async loadData(e) {
    const roomsView = e.target;
    const lastRoom = roomsView.children[roomsView.children.length - 2];
    if (lastRoom.offsetHeight + lastRoom.offsetTop < roomsView.offsetHeight + roomsView.scrollTop + 50 &&
      this.rooms.length <= this.chessService.filterData.allRooms.length - 2 && this.loadingData) {
      this.loadNextRoom();
    }
  }

  public displayModalMin(val) {
    this.displayModal = val;
    this.modal = null;
  }

  // открываем боковое окно для редактирования списком
  public displayListEdit(room, room_index) {
    this.roomUpdate = room_index;
    const listEdit = new ListEditModalModel();
    listEdit.room = room;
    listEdit.tableStartDate = this.tableDateFrom;
    listEdit.tableEndDate = this.tableDateTo;
    this.listEditClick.emit(listEdit);
  }

  updateScroll(e) {
    const scrollOne = this.scrollOne.nativeElement as HTMLElement;
    const roomsView = this.roomsView.nativeElement;
    if (this.scrolledBlock === 1 && e.target.classList.contains('dates')) {
      for (let i = 0; i < this.rooms.length; i++) {
        roomsView.children[2].children[i].children[0].children[1].children[1].scrollLeft = scrollOne.scrollLeft;
      }
    } else if (this.scrolledBlock === 2 && e.target.classList.contains('table')) {
      scrollOne.scrollLeft = e.target.scrollLeft;
      for (let i = 0; i < this.rooms.length; i++) {
        if (roomsView.children[2].children[i].children[0].children[1].children[1].scrollLeft !== scrollOne.scrollLeft) {
          roomsView.children[2].children[i].children[0].children[1].children[1].scrollLeft = scrollOne.scrollLeft;
        }
      }
    }
  }

  changeScrollBlock(type) {
    this.scrolledBlock = type;
  }

  updateDay(data: any): void {
    let d = 0;
    for (let k = this.firstIndex; k <= this.lastIndex; k++) {
      if (data.statuses.length > 0) {
        this.rooms[this.roomUpdate].statuses[k] = data.statuses[d];
      }
      if (data.restrictions.length > 0) {
        this.rooms[this.roomUpdate].restrictions[k] = data.restrictions[d];
      }
      if (data.booked.length > 0) {
        this.rooms[this.roomUpdate].booked[k] = data.booked[d];
      }
      if (data.rooms.length > 0) {
        this.rooms[this.roomUpdate].rooms[k] = data.rooms[d];
      }
      let i = 0;
      this.rooms[this.roomUpdate].rates.forEach(element => {
        if (data.rates[i]) {
          if (data.rates[i].min_advance_res.length > 0) {
            element.min_advance_res[k] = data.rates[i].min_advance_res[d];
          }
          if (data.rates[i].min_stay.length > 0) {
            element.min_stay[k] = data.rates[i].min_stay[d];
          }
          if (data.rates[i].statuses.length > 0) {
            element.statuses[k] = data.rates[i].statuses[d];
          }
          if (data.rates[i].restrictions.length > 0) {
            element.restrictions[k] = data.rates[i].restrictions[d];
          }
          if (data.rates[i].prices.length > 0) {
            element.prices[k] = data.rates[i].prices[d];
          }
          let j = 0;
          if (data.rates[i].occupancyLocal.length > 0) {
            element.occupancyLocal.forEach(o => {
              o.prices[k] = data.rates[i].occupancyLocal[j].prices[d];
              j++;
            });
          }
          j = 0;
          if (data.rates[i].occupancy.length > 0) {
            element.occupancy.forEach(o => {
              o.prices[k] = data.rates[i].occupancy[j].prices[d];
              j++;
            });
          }
          i++;
        }
      });
      this.rooms[this.roomUpdate].displayStatuses = this.chessService.updateStatuses(this.rooms[this.roomUpdate]);
      d++;
    }

    this.displayLoader(false);
  }

  public changeStatus(i, room_index): void {
    this.displayLoader(true);
    const date = this.rooms[room_index].dates[i];
    const room_id = this.rooms[room_index].room_type.id;
    this.roomUpdate = room_index;
    this.firstIndex = i;
    this.lastIndex = i;
    const inventory = new RoomInventoryModel();
    inventory.start = this.rooms[room_index].dates[i];
    inventory.end = this.rooms[room_index].dates[i];
    if (
      this.rooms[room_index].statuses[i] === ChessEnum.OK_STATUS ||
      this.rooms[room_index].statuses[i] === ChessEnum.NO_ROOMS_STATUS ||
      this.rooms[room_index].statuses[i] === ChessEnum.NO_PRICES_STATUS ||
      this.rooms[room_index].statuses[i] === ChessEnum.SOLD_OUT_STATUS) {
      const tempSub = this.chessService.openOrCloseRoom(ChessEnum.CLOSE, room_id, inventory).subscribe(res => {
        this.chessService.updateFilter(date, date, room_id);
      });
      this.serviceSubscriptions.push(tempSub);
    } else {
      const tempSub = this.chessService.openOrCloseRoom(ChessEnum.OPEN, room_id, inventory).subscribe(res => {
        this.chessService.updateFilter(date, date, room_id);
      });
      this.serviceSubscriptions.push(tempSub);
    }
  }

  public showModalPrices(rate: SimpleRateModel, id: number, room_index) {
    this.roomUpdate = room_index;
    this.priceModal = new PriceModalModel();
    this.priceModal.roomTypeId = id;
    this.priceModal.rateId = rate.id;
    this.priceModal.rateName = rate.name;
    this.priceModal.dateFrom = this.tableDateFrom;
    this.priceModal.dateTo = this.tableDateTo;
    this.displayPriceModal = true;
  }

  public onPriceModalClose() {
    this.displayPriceModal = false;
    this.priceModal = null;
  }

  public updateRoomType(data: TableResModel) {
    for (let i = 0; i < data.dates.length; i++) {
      this.rooms[this.roomUpdate].dates.forEach((date, j) => {
        if (date === data.dates[i]) {
          this.rooms[this.roomUpdate].booked[j] = data.booked[i];
          this.rooms[this.roomUpdate].restrictions[j] = data.restrictions[i];
          this.rooms[this.roomUpdate].room_type = data.room_type;
          this.rooms[this.roomUpdate].rooms[j] = data.rooms[i];
          this.rooms[this.roomUpdate].statuses[j] = data.statuses[i];
          this.rooms[this.roomUpdate].rates.forEach(rate => {
            const tempRate = data.rates.find(item => item.id === rate.id);
            if (Utils.isExist(tempRate)) {
              rate.name = tempRate.name;
              rate.prices[j] = tempRate.prices[i];
              rate.min_stay[j] = tempRate.min_stay[i];
              rate.restrictions[j] = tempRate.restrictions[i];
              rate.min_advance_res[j] = tempRate.min_advance_res[i];
              rate.statuses[j] = tempRate.statuses[i];
              rate.occupancy.forEach((occupancy, k) => {
                const tempOccupancy = tempRate.occupancy.find(item => item.guests === occupancy.guests);
                if (Utils.isExist(tempOccupancy)) {
                  occupancy.prices[j] = tempOccupancy.prices[i];
                }
              });
              rate.occupancyLocal.forEach((occupancy, k) => {
                const tempOccupancy = tempRate.occupancyLocal.find(item => item.guests === occupancy.guests);
                if (Utils.isExist(tempOccupancy)) {
                  occupancy.prices[j] = tempOccupancy.prices[i];
                }
              });
            }
          });
        }
      });
    }
  }

  // настраиваем маленькую модалку при выделении нескольких ячеек в одной строке
  public async mouseUp(e) {
    this.pressed = false;
    let el: any;
    let input: any;
    if (e) {
      el = ChessTableComponent.findElemBySlag(e, 'td');
    } else {
      el = this.selectedRow.children[this.lastSelectIndex];
    }
    if (this.firstIndex !== this.lastIndex) {
      this.displayModalMin(true);
      switch (this.rowType) {
        case ChessEnum.PRICES_ROW: {
          const newPrice = this.selectedRow.children[this.firstIndex].children[0].value;
          let tempSelectParams = 1;
          if (this.selectedObject.rate.statuses[this.firstIndex] === 'closed') {
            tempSelectParams = 0;
          }
          this.modal = new GroupChangeModalModel();
          this.modal.type = ChessEnum.PRICE_MODAL;
          this.modal.title = this.selectedObject.room.room_type.name;
          this.modal.title2 = this.selectedObject.rate.name;
          this.modal.fromDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.firstIndex]), 'dd mmmm yyyy');
          this.modal.toDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.lastIndex]), 'dd mmmm yyyy');
          this.modal.selectParams = tempSelectParams;
          this.modal.price = newPrice;
          break;
        }
        case ChessEnum.ROOM_NUM_ROW: {
          const newNumb = this.selectedRow.children[this.firstIndex].children[0].value;
          this.modal = new GroupChangeModalModel();
          this.modal.type = ChessEnum.NUMB_MODAL;
          this.modal.title = this.selectedObject.room.room_type.name;
          this.modal.fromDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.firstIndex]), 'dd mmmm yyyy');
          this.modal.toDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.lastIndex]), 'dd mmmm yyyy');
          this.modal.selectParams = 1;
          this.modal.numb = newNumb;
          break;
        }
        case ChessEnum.RESERVATION_ROW: {
          const newReservation = this.selectedRow.children[this.firstIndex].children[0].value;
          let tempSelectParams = 0;
          if (!newReservation) {
            tempSelectParams = 1;
          }
          this.modal = new GroupChangeModalModel();
          this.modal.type = ChessEnum.RESERVATION_MODAL;
          this.modal.title = 'CHESS.MIN_BOOKING_PERIOD';
          this.modal.fromDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.firstIndex]), 'dd mmmm yyyy');
          this.modal.toDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.lastIndex]), 'dd mmmm yyyy');
          this.modal.selectParams = tempSelectParams;
          this.modal.reservation = newReservation;
          break;
        }
        case ChessEnum.LIVE_ROW: {
          const newLive = this.selectedRow.children[this.firstIndex].children[0].value;
          let temSelectParams = 0;
          if (!newLive) {
            temSelectParams = 1;
          }
          this.modal = new GroupChangeModalModel();
          this.modal.type = ChessEnum.LIVE_MODAL;
          this.modal.title = this.selectedObject.room.room_type.name;
          this.modal.fromDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.firstIndex]), 'dd mmmm yyyy');
          this.modal.toDate = this.chessService.formatDate(new Date(this.selectedObject.room.dates[this.lastIndex]), 'dd mmmm yyyy');
          this.modal.selectParams = temSelectParams;
          this.modal.live = newLive;
          break;
        }
      }
      this.modal.room = this.selectedObject.room;
      this.modal.rate = this.selectedObject.rate;
      let tr: any;
      if (e) {
        tr = ChessTableComponent.findElemBySlag(e, 'tr');
      } else {
        tr = this.selectedRow;
      }
      const firstEl = tr.children[this.firstIndex];
      const modalElement: Element = this.modalChangePrices.nativeElement;
      let roomViewTop = tr;
      while (!roomViewTop.classList.contains('room')) {
        roomViewTop = roomViewTop.parentElement;
      }
      this.renderer.setStyle(modalElement, 'top', (roomViewTop.offsetTop - 15 + tr.offsetTop + firstEl.offsetHeight) + 'px');
      this.renderer.setStyle(modalElement, 'left', (firstEl.offsetLeft + 220) + 'px');
    } else {
      input = el.children[0];
      input.value = el.innerText;
      input.classList.remove('hidden');
      input.select();
    }
  }

  // сохранение изменений ячейки при потере фокуса в строке с кол-ом строк
  public saveChangesPriceRoomNumRow(e, p, index, room, room_index) {
    this.saveChangesPrice(e, p, index, ChessEnum.ROOM_NUM_ROW, room, room_index);
  }

  // сохранение изменений ячейки при потере фокуса в строке с ценами по тарифу
  public saveChangesPricePricesRow(e, p, index, room, room_index, rate) {
    const newVal = e.target.value.toString().replace(/\s/g, '');
    if (p === 0 && newVal === '0') {
      return;
    }
    this.saveChangesPrice(e, p, index, ChessEnum.PRICES_ROW, room, room_index, rate);
  }

  // сохранение изменений ячейки при потере фокуса в строке с мин сроком проживания
  public saveChangesMinStayRow(e, p, index, room, room_index, rate) {
    // если указано 1, то есть мин срок проживания по дефолту - то ничего отправлять на сервер не нужно
    const newVal = e.target.value.toString().replace(/\s/g, '');
    if (p === 1 && newVal === '1') {
      return;
    }
    this.saveChangesPrice(e, p, index, ChessEnum.LIVE_ROW, room, room_index, rate);
  }

  // сохранение изменений ячейки при потере фокуса в строке с мин окном бронирования
  public saveChangesMinAdvanceRow(e, p, index, room, room_index, rate) {
    // если указано 0, то есть мин окно бронирования по дефолту - то ничего отправлять на сервер не нужно
    const newVal = e.target.value.toString().replace(/\s/g, '');
    if (p === 0 && newVal === '0') {
      return;
    }
    this.saveChangesPrice(e, p, index, ChessEnum.RESERVATION_ROW, room, room_index, rate);
  }

  // сохранение изменений ячейки при потере фокуса
  public saveChangesPrice(e, p, index, type, room, room_index, rate?) {
    e.target.classList.add('hidden');
    if (this.rowType !== type && this.lastChangeIndex !== index) {
      e.target.parentElement.classList.remove('selected');
      e.target.parentElement.classList.remove('last');
      e.target.parentElement.classList.remove('first');
    }
    // tslint:disable-next-line:radix
    const newVal = e.target.value.toString().replace(/\s/g, '');
    let oldVal;
    if (p) {
      // tslint:disable-next-line:radix
      oldVal = p.toString().replace(/\s/g, '');
      if (oldVal === '-') {
        oldVal = null;
      }
    }
    this.roomUpdate = room_index;
    this.rowType = type;
    this.lastChangeIndex = index;

    if (newVal !== oldVal && newVal) {
      this.displayLoader(true);
      this.firstIndex = index;
      this.lastIndex = index;
      switch (type) {
        case ChessEnum.PRICES_ROW: {
          const dateFrom = room.dates[index];
          const dateTo = room.dates[index];
          this.chessService.changePrices(newVal, dateFrom, dateTo, room, rate).finally();
          break;
        }
        case ChessEnum.ROOM_NUM_ROW: {
          const dateFrom = room.dates[index];
          const dateTo = room.dates[index];
          this.chessService.changeNumbers(newVal, dateFrom, dateTo, room).finally();
          break;
        }
        case ChessEnum.RESERVATION_ROW: {
          this.chessService.changeMinReservation(newVal, index, index, room, rate).finally();
          break;
        }
        case ChessEnum.LIVE_ROW: {
          this.chessService.changeMinStay(newVal, index, index, room, rate).finally();
          break;
        }
      }
    }
  }

  // при нажатии мышки на ячейку в строке с мин окном бронирования
  onMinAdvanceRowMouseDown(event: any, room: TableResModel, rate: SimpleRateModel) {
    this.mouseDown(event, ChessEnum.RESERVATION_ROW, {room, rate}).finally();
  }

  // при нажатии мышки на ячейку в строке с мин сроком проживания
  onMinLiveRowMouseDown(event: any, room: TableResModel, rate: SimpleRateModel) {
    this.mouseDown(event, ChessEnum.LIVE_ROW, {room, rate}).finally();
  }

  // при нажатии мышки на ячейку в строке с кол-ом номеров
  onRoomNumRowMouseDown(event: any, room: TableResModel, room_num: number) {
    this.mouseDown(event, ChessEnum.ROOM_NUM_ROW, {room, room_num}).finally();
  }

  // при нажатии мышки на ячейку в строке с ценами
  onPriceRowMouseDown(event: any, room: TableResModel, rate: SimpleRateModel) {
    this.mouseDown(event, ChessEnum.PRICES_ROW, {room, rate}).finally();
  }

  // при нажатии мышки на ячейку
  public async mouseDown(e, rowType, object) {
    e.preventDefault();
    const values = this.roomsView.nativeElement.querySelectorAll('.input');
    values.forEach(input => {
      if (!input.classList.contains('hidden')) {
        input.classList.add('hidden');
      }
    });

    this.selectedRow = ChessTableComponent.findElemBySlag(e, 'tr');
    this.pressed = true;
    const el: any = ChessTableComponent.findElemBySlag(e, 'td');
    this.destroySelector(e, 'table');
    if (rowType === this.rowType) {
      if (e.shiftKey) {
        if (!el.classList.contains('selected')) {
          this.chosenIndexes.push(el.cellIndex);
          if (this.lastSelectIndex < el.cellIndex) {
            this.selectedRow.children[this.lastIndex].classList.remove('last');
            for (let i = this.lastIndex; i < el.cellIndex; i++) {
              this.selectedRow.children[i].classList.add('selected');
            }
            el.classList.add('selected');
            el.classList.add('last');
            this.lastIndex = el.cellIndex;
          } else {
            this.selectedRow.children[this.firstIndex].classList.remove('first');
            for (let i = this.firstIndex; i > el.cellIndex; i--) {
              this.selectedRow.children[i].classList.add('selected');
            }
            el.classList.add('selected');
            el.classList.add('first');
            this.firstIndex = el.cellIndex;
          }
        }
        this.lastSelectIndex = el.cellIndex;
      } else {
        el.classList.add('selected');
        el.classList.add('first');
        el.classList.add('last');

        this.lastSelectIndex = el.cellIndex;
        this.lastIndex = el.cellIndex;
        this.firstIndex = el.cellIndex;
      }
    } else {
      el.classList.add('selected');
      el.classList.add('first');
      el.classList.add('last');
      this.lastSelectIndex = el.cellIndex;
      this.lastIndex = el.cellIndex;
      this.firstIndex = el.cellIndex;
    }

    this.selectActive = true;
    this.selectedObject = object;
    this.rowType = rowType;
    this.lastChangeIndex = el.cellIndex;
  }

  // при наведении мышки на ячейку в строке с мин окном бронирования
  onMinAdvanceRowMouseEnter(event: any) {
    this.mouseEnter(event, ChessEnum.RESERVATION_ROW);
  }

  // при наведении мышки на ячейку в строке с мин сроком проживания
  onMinLiveRowMouseEnter(event: any) {
    this.mouseEnter(event, ChessEnum.LIVE_ROW);
  }

  // при наведении мышки на ячейку в строке с ценами по тарифу
  onPriceRowMouseEnter(event: any) {
    this.mouseEnter(event, ChessEnum.PRICES_ROW);
  }

  // при наведении мышки на ячейку в строке с кол-ом номеров
  onRoomNumRowMouseEnter(event: any) {
    this.mouseEnter(event, ChessEnum.ROOM_NUM_ROW);
  }

  // при наведении мышки на ячейку
  public mouseEnter(e, rowType) {
    const thisRow = ChessTableComponent.findElemBySlag(e, 'tr');
    if (this.pressed && this.selectedRow === thisRow) {
      this.rowType = rowType;
      window.getSelection().removeAllRanges();
      const el: any = ChessTableComponent.findElemBySlag(e, 'td');
      const index: number = el.cellIndex;
      let nextElem;
      let prevElem;
      if (index !== 0) {
        prevElem = this.selectedRow.children[index - 1];
      }
      if (index < this.selectedRow.children.length) {
        nextElem = this.selectedRow.children[index + 1];
      }

      if (!el.classList.contains('selected')) {
        this.chosenIndexes.push(index);
        if (this.firstIndex < index) {
          prevElem.classList.remove('last');

          for (let i = this.firstIndex; i <= index; i++) {
            thisRow.children[i].classList.add('selected');
            thisRow.children[i].classList.remove('last');
          }
          el.classList.add('last');
          this.lastIndex = el.cellIndex;
        } else {
          nextElem.classList.remove('first');
          for (let j = this.lastSelectIndex; j >= index; j--) {
            thisRow.children[j].classList.add('selected');
            thisRow.children[j].classList.remove('first');
          }
          el.classList.add('first');
          this.firstIndex = el.cellIndex;
        }
      } else {
        this.chosenIndexes = this.chosenIndexes.filter(chosen => chosen !== index);
        if (this.lastSelectIndex > index) {
          for (let k = this.lastSelectIndex; k > index; k--) {
            thisRow.children[k].classList.remove('last');
            thisRow.children[k].classList.remove('selected');
          }
          el.classList.add('last');
          this.lastIndex = el.cellIndex;
        } else {
          prevElem.classList.remove('first');
          prevElem.classList.remove('selected');
          el.classList.add('first');
          this.firstIndex = el.cellIndex;
        }
      }
      this.lastSelectIndex = index;
      return false;
    }
  }

  getStatusClass(status: DisplayStatusModel): string {
    let result = 'color';
    if (status.class === 1 || status.class === 3) {
      result += ' last';
    }
    if (status.class === 2 || status.class === 3) {
      result += ' first';
    }
    switch (status.val) {
      case 0: {
        result += ' red';
        break;
      }
      case 1: {
        result += ' yellow';
        break;
      }
      case 2: {
        result += ' green';
        break;
      }
    }
    return result;
  }

  getEditHotelRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.chessService.getPropertyId()}/${RoutesEnum.EDIT}/${RoutesEnum.TYPES}`;
  }

  onScroll(event) {
    console.log(event)
  }
}
