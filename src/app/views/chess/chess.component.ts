import {Component, OnDestroy} from '@angular/core';
import {ListEditModalModel} from '@app/views/chess/helpers/simple.model';
import {ChessService} from '@app/views/chess/chess.service';

@Component({
  selector: 'app-chess',
  templateUrl: './chess.component.html'
})
export class ChessComponent implements OnDestroy {
  listEditVisible = false;
  listEditValue = new ListEditModalModel();

  constructor(public chessService: ChessService) {
  }

  ngOnDestroy() {
    this.chessService.clear();
  }

  onListEditOpen(listEdit: ListEditModalModel) {
    this.listEditValue = listEdit;
    this.listEditVisible = true;
  }

  onListEditClose() {
    this.listEditValue = null;
    this.listEditVisible = false;
  }
}
