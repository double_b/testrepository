import {EventEmitter, Injectable} from '@angular/core';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {ChannelApiService} from '@app/shared/services/channel-api/channel-api.service';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {ChannelModel} from '@app/shared/models/channel.model';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';
import {DatesEnum} from '@app/core/constants/dates.enum';
import {PriceApiService} from '@app/shared/services/price-api/price-api.service';
import {PriceModel} from '@app/shared/models/price.model';
import {FilterModel, SimpleRateModel, SimpleRoomModel, TableData, TableResModel} from '@app/views/chess/helpers/simple.model';
import {RoomInventoryModel} from '@app/shared/models/room/room-inventory.model';
import {CalendarApiService} from '@app/shared/services/calendar-api/calendar-api.service';
import {ChessEnum} from '@app/views/chess/helpers/chess.enum';
import {OccupancyModel} from '@app/shared/models/occupancy.model';
import {OccupancyApiService} from '@app/shared/services/occupancy-api/occupancy-api.service';

@Injectable({
  providedIn: 'root'
})
export class ChessService {

  static DEF_SHOW_GUEST_PRICES = false;
  static DEF_SHOW_RESTRICTIONS = false;

  today = new Date();
  filter: EventEmitter<TableData> = new EventEmitter();
  updateDay: EventEmitter<any> = new EventEmitter();
  _updateVisible: EventEmitter<any> = new EventEmitter();
  updateRoom: EventEmitter<any> = new EventEmitter();
  filterData: FilterModel = new FilterModel();

  private readonly mainLoading = new BehaviorSubject<boolean>(false);
  readonly $mainLoading = this.mainLoading.asObservable();

  subscriptions: Subscription[] = [];

  constructor(private roomsService: RoomsService,
              private occupancyApiService: OccupancyApiService,
              private channelApiService: ChannelApiService,
              private priceApiService: PriceApiService,
              private calendarApiService: CalendarApiService) {
  }

  getChannels(): Observable<ChannelModel[]> {
    return this.channelApiService.getChannelsList(this.getPropertyId());
  }

  getRoomTypes(): Observable<RoomsTypesListModel[]> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId());
  }

  getDiscounts(roomTypeID: number, rateId: number): Observable<OccupancyModel[]> {
    return this.occupancyApiService.getDiscounts(this.getPropertyId(), roomTypeID, this.filterData.channel, rateId);
  }

  // overridden changePriceOld method by double_b
  public async changePrices(price: string,
                            dateFrom: string,
                            dateTo: string,
                            room: SimpleRoomModel,
                            rate: SimpleRateModel,
                            w: number[] = [],
                            status: number = null,
                            updateOneRoom?: boolean) {
    if (w === null) {
      w = [];
    }
    this.mainLoading.next(true);
    // создаем объект об изменении цены
    const priceModel = new PriceModel();
    priceModel.channel = this.filterData.channel;
    // tslint:disable-next-line:radix
    priceModel.price = parseInt(price.replace(/\s/g, ''));
    priceModel.rate_id = rate.id;
    priceModel.start = dateFrom;
    priceModel.end = dateTo;
    priceModel.w = w;
    // отправляем новые цены на сервер и ждем пустого ответа
    await this.priceApiService.savePrices(this.getPropertyId(), room.room_type.id, priceModel).toPromise();
    if (status !== null) {
      // после ответа необходимо открыть или закрыть дни
      // но при открытии/закрытии цена не должна быть указана
      // и поэтому чтоб не было конфликтов в бд обнуляем цену - и отправляем на сервер тот же объект цены
      priceModel.price = null;
      if (status === ChessEnum.OPEN) {
        await this.priceApiService.openPriceDay(this.getPropertyId(), room.room_type.id, priceModel).toPromise();
      } else if (status === ChessEnum.CLOSE) {
        await this.priceApiService.closePriceDay(this.getPropertyId(), room.room_type.id, priceModel).toPromise();
      }
    }
    // после открытия/закрытия дней обновляем параметры фильтров - почему пока не знаю
    this.updateFilter(dateFrom, dateTo, room.room_type.id, updateOneRoom);
  }

  // overridden changeNumbersOld method by double_b
  public async changeNumbers(newNumb: string,
                             dateFrom: string,
                             dateTo: string,
                             room: SimpleRoomModel,
                             w: number[] = [],
                             status: number = null) {
    this.mainLoading.next(true);
    if (w === null) {
      w = [];
    }
    const inventory = new RoomInventoryModel();
    // tslint:disable-next-line:radix
    inventory.rooms = parseInt(newNumb);
    inventory.start = dateFrom;
    inventory.end = dateTo;
    inventory.w = w;
    await this.roomsService.changeRoomInventory(this.getPropertyId(), room.room_type.id, inventory).toPromise();
    if (status !== null) {
      inventory.rooms = null;
      await this.openOrCloseRoom(status, room.room_type.id, inventory).toPromise();
    }
    // после открытия/закрытия дней обновляем параметры фильтров - почему пока не знаю
    this.updateFilter(dateFrom, dateTo, room.room_type.id);
  }

  openOrCloseRoom(status: number, roomTypeId: number, inventory: RoomInventoryModel): Observable<any> {
    if (status === ChessEnum.OPEN) {
      return this.roomsService.openRoom(this.getPropertyId(), roomTypeId, inventory);
    } else if (status === ChessEnum.CLOSE) {
      return this.roomsService.closeRoom(this.getPropertyId(), roomTypeId, inventory);
    }
  }

  // overridden changeMinReservationOld method by double_b
  public async changeMinReservation(newReserv: string,
                                    indexFrom: number,
                                    indexTo: number,
                                    room: SimpleRoomModel,
                                    rate: SimpleRateModel) {
    this.mainLoading.next(true);
    const inventory = new RoomInventoryModel();
    inventory.channel = this.filterData.channel;
    inventory.rate_id = rate.id;
    // tslint:disable-next-line:radix
    let reserv = parseInt(newReserv);
    if (reserv < 0) {
      reserv = 0;
    }
    inventory.min_advance_res = reserv;
    inventory.start = room.dates[indexFrom];
    inventory.end = room.dates[indexTo];
    await this.roomsService.updateMinAdvance(this.getPropertyId(), room.room_type.id, inventory).toPromise();
    // после открытия/закрытия дней обновляем параметры фильтров - почему пока не знаю
    this.updateFilter(room.dates[indexFrom], room.dates[indexTo], room.room_type.id);
  }

  // overridden changeMinReservationOld method by double_b
  public async changeMinStay(newStay: string,
                             indexFrom: number,
                             indexTo: number,
                             room: SimpleRoomModel,
                             rate: SimpleRateModel) {
    this.mainLoading.next(true);
    // tslint:disable-next-line:radix
    let stay = parseInt(newStay);
    if (stay < 1) {
      stay = 1;
    }
    const inventory = new RoomInventoryModel();
    inventory.channel = this.filterData.channel;
    inventory.rate_id = rate.id;
    inventory.min_stay = stay;
    inventory.start = room.dates[indexFrom];
    inventory.end = room.dates[indexTo];
    await this.roomsService.updateMinStay(this.getPropertyId(), room.room_type.id, inventory).toPromise();
    // после открытия/закрытия дней обновляем параметры фильтров - почему пока не знаю
    this.updateFilter(room.dates[indexFrom], room.dates[indexTo], room.room_type.id);
  }

  updateFilter(dateFrom: string, dateTo: string, roomTypeId: number, roomUpdate?: boolean) {
    const filterData = this.filterData;
    filterData.dateFrom = new Date(dateFrom);
    filterData.dateTo = new Date(dateTo);
    filterData.currentRoomType = roomTypeId;
    this.doFilter(filterData, false, roomUpdate);
  }

  public updateVisible(type, result): void {
    this._updateVisible.emit({type, result});
  }

  public doFilter(data: FilterModel, updateFilterData?: boolean, updateRoom?: boolean) {
    if (updateFilterData) {
      this.filterData = data;
    }
    let id = 0;
    if (data.allRooms.length === 1) {
      this.filter.emit(null);
      id = 0;
      return;
    }

    if (data.currentRoomType === 0) {
      id = data.allRooms[1].id;
    } else {
      id = data.currentRoomType;
    }

    const tempSub = this.getCalendarData(id, data.channel, data.dateFrom, data.dateTo).subscribe(res => {
      res.displayStatuses = this.updateStatuses(res);
      if (updateFilterData) {
        const tableData = new TableData();
        tableData.dateFrom = data.dateFrom;
        tableData.dateTo = data.dateTo;
        tableData.res = res;
        this.filter.emit(tableData);
        this.mainLoading.next(false);
        return null;
      } else if (updateRoom) {
        this.updateRoom.emit(res);
        this.mainLoading.next(false);
        return res;
      } else {
        this.updateDay.emit(res);
        this.mainLoading.next(false);
        return null;
      }
    });
    this.subscriptions.push(tempSub);
  }

  getCalendarData(roomTypeId: number, channel: string, dateFrom: Date, dateTo: Date): Observable<TableResModel> {
    return this.calendarApiService.getCalendarListData(this.getPropertyId(), roomTypeId, channel, dateFrom, dateTo);
  }

  private parseVal(val): number {
    switch (val) {
      case 'no_rooms':
      case 'no_prices':
      case 'sold_out':
        return 1;
      case 'ok':
        return 2;
      case 'closed':
        return 0;
      default:
        return undefined;
    }
  }

  public updateStatuses(data) {
    const statuses = [];
    let val = this.parseVal(data.statuses[0]);
    let tempClass = 0;
    for (let i = 0; i < data.statuses.length; i++) {
      const nextVal = this.parseVal(data.statuses[i + 1]);
      const prevVal = this.parseVal(data.statuses[i - 1]);
      const thisVal = this.parseVal(data.statuses[i]);


      tempClass = 2;
      if (thisVal === val && i + 1 !== data.statuses.length && i !== 0) {
        tempClass = 0;
      }
      if (nextVal !== val && nextVal !== undefined) {
        tempClass = 1;
        if (prevVal !== val) {
          tempClass = 3;
        }
        val = nextVal;
      }
      if (nextVal === undefined) {
        tempClass = 1;
      }

      if (prevVal !== undefined) {
        if (tempClass === 1 && prevVal !== thisVal) {
          tempClass = 3;
        }
        if (tempClass === 2 && nextVal !== thisVal) {
          tempClass = 3;
        }
        if (tempClass !== 3 && prevVal !== thisVal) {
          tempClass = 2;
        }
        if (tempClass === 3 && nextVal === thisVal) {
          tempClass = 2;
        }
      } else {
      }
      statuses.push({
        val: thisVal,
        class: tempClass
      });
    }
    return statuses;
  }

  public formatDate(date: Date, format: string): string {
    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    const year = date.getFullYear();
    if (day.length < 2) {
      day = '0' + day;
    }
    if (month.length < 2) {
      month = '0' + month;
    }
    switch (format) {
      case 'yyyy-mm-dd':
        return `${year}-${month}-${day}`;
      case 'dd mmmm yyyy':
        return `${day} ${DatesEnum.MONTHS_LC[date.getMonth()]} ${year}`;
      default:
        return `${day}.${month}.${year}`;
    }
  }

  public saveDiscounts(roomTypeID: number, rateID: number, occupancies: OccupancyModel[], dateFrom: Date, dateTo: Date) {
    this.mainLoading.next(true);
    const tempSub = this.occupancyApiService.saveDiscounts(this.getPropertyId(), roomTypeID, this.filterData.channel, rateID, occupancies)
      .subscribe(res => {
        const tempSub1 = this.getCalendarData(roomTypeID, this.filterData.channel, dateFrom, dateTo).subscribe(data => {
          data.displayStatuses = this.updateStatuses(data);
          this.updateRoom.emit(data);
          this.mainLoading.next(false);
        });
        this.subscriptions.push(tempSub1);
      });
    this.subscriptions.push(tempSub);
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }

  clear() {
    this.subscriptions.forEach(sub => {
      if (sub != null) {
        sub.unsubscribe();
      }
    });
  }
}
