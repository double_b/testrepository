export class ChessEnum {
  static OCCUPANCY = 'occupancy';
  static RESTRICTION = 'restrictions';

  static PRICE_MODAL = 'price';
  static NUMB_MODAL = 'numb';
  static RESERVATION_MODAL = 'reservation';
  static LIVE_MODAL = 'live';

  static PRICES_ROW = 'prices';
  static ROOM_NUM_ROW = 'room_numbs';
  static RESERVATION_ROW = 'reservation';
  static LIVE_ROW = 'live';

  static OPEN = 1;
  static CLOSE = 0;

  static OK_STATUS = 'ok';
  static NO_ROOMS_STATUS = 'no_rooms';
  static NO_PRICES_STATUS = 'no_prices';
  static SOLD_OUT_STATUS = 'sold_out';
}
