import {Pipe, PipeTransform} from '@angular/core';
import {SimpleOccupancyModel} from '@app/views/chess/helpers/simple.model';

@Pipe({
  name: 'filter'
})
export class ChessFilterPipe implements PipeTransform {
  transform(occupancies: SimpleOccupancyModel[], isLocal: boolean = true) {
    return occupancies.filter(item => item.isLocal === isLocal);
  }
}
