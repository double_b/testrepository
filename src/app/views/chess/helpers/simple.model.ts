import {ChessEnum} from '@app/views/chess/helpers/chess.enum';
import {RoomTypeModel} from '@app/shared/models/room';

export class PriceModalModel {
  roomTypeId: number;
  rateId: number;
  rateName: string;
  dateFrom = new Date();
  dateTo = new Date();
}

export class FilterModel {
  allRooms: SimpleRoomTypeModel[] = [];
  currentRoomType = 0;
  channel: string = null;
  dateFrom = new Date();
  dateTo = new Date();
}

export class SimpleChannelModel {
  name: string;
  key: string;
}

export class SimpleRoomTypeModel {
  id: number;
  name: string;
}

export class MonthModel {
  year: number;
  name: string;
  days: DayModel[] = [];
}

export class DayModel {
  isDayOff: boolean;
  name: string;
  day: number;
}

export class GroupChangeModalModel {
  type: string;
  title: string;
  title2?: string;
  fromDate?: string;
  toDate?: string;
  selectParams: number;
  numb?: number;
  price?: number;
  reservation?: number;
  live?: number;
  room?: SimpleRoomModel;
  rate?: SimpleRateModel;

  isPriceOrNumb(): boolean {
    return this.isPrice() || this.isNumb();
  }

  isPrice(): boolean {
    return this.type === ChessEnum.PRICE_MODAL;
  }

  isNumb(): boolean {
    return this.type === ChessEnum.NUMB_MODAL;
  }

  isReservation(): boolean {
    return this.type === ChessEnum.RESERVATION_MODAL;
  }

  isLive(): boolean {
    return this.type === ChessEnum.LIVE_MODAL;
  }
}

export class ListEditModalModel {
  room: TableResModel;
  tableStartDate: Date;
  tableEndDate: Date;
}

export class SimpleRoomModel {
  room_type: RoomTypeModel;
  dates: string[] = [];
}

export class SimpleRateModel {
  id: number;
  name: string;
  min_advance_res: any[] = [];
  min_stay: any[] = [];
  statuses: any;
  restrictions: any;
  prices: any;
  occupancyLocal: SimpleOccupancyModel[];
  occupancy: SimpleOccupancyModel[];
}

export class TableData {
  dateFrom: Date;
  dateTo: Date;
  res: TableResModel;
}

export class TableResModel {
  booked: number[] = [];
  dates: string[] = [];
  restrictions: number[] = [];
  rooms: number[] = [];
  statuses: string[] = [];
  rates: SimpleRateModel[] = [];
  room_type: RoomTypeModel;
  displayStatuses: DisplayStatusModel[] = [];
}

export class DisplayStatusModel {
  val: number;
  class: number;
}

export class SimpleOccupancyModel {
  guests: number;
  prices?: number[] = [];

  mode?: number;
  status?: boolean;
  amount?: number;
  isLocal?: boolean;
  validState?: string;
}

export class SimpleWeekDayModel {
  name: string;
  val: boolean;
  index: number;
}
