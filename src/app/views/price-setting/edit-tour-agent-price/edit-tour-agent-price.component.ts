import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-tour-agent-price-edit',
  templateUrl: './edit-tour-agent-price.component.html',
  styleUrls: ['./edit-tour-agent-price.component.scss']
})
export class EditTourAgentPriceComponent implements OnInit {

  public typeId: any;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.typeId = params.type_id;
    });
  }
}
