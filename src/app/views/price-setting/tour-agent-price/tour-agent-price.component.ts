import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {PriceSettingsService} from '@app/views/price-setting/price-settings.service';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {Router} from '@angular/router';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import * as moment from 'moment';

@Component({
  selector: 'app-tour-agent-price',
  templateUrl: './tour-agent-price.component.html',
  styleUrls: ['./tour-agent-price.component.scss']
})
export class TourAgentPriceComponent implements OnInit {
  @ViewChild('endDatePicker') endDatePicker;
  @ViewChild('success_template') successTemplateNotification: TemplateRef<{}>;

  public currentType: any;

  @Input() set typeId(type: any) {
    if (type) {
      this.currentType = type;
      this.getDataByTypeId(type);
      this.editMode = true;
    }
  }

  selectedDoubleRoom = false;
  selectedSharedRoom = false;
  selectedTripleRoom = false;
  validateForm: FormGroup;
  dateRange = [];
  editMode = false;
  dataById: any;

  roomTypes: any[] = [];
  createIsProcess = false;
  basePriceData: any;

  startValue: Date | null = null;
  endValue: Date | null = null;
  openedEnd = false;
  daysForRooms: any[] = [];

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(this.startValue, current) > 0;
  };

  constructor(private fb: FormBuilder,
              private location: Location,
              private priceSettingsService: PriceSettingsService,
              private router: Router,
              private notificationService: NotificationService) {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      start_date: [null, [Validators.required]],
      end_date: [null, [Validators.required]],
      min_stay_day: [false, []],
      room_types: new FormArray([]),
      table_data: [null, []],
      min_stay: ['', []],
    });
  }

  ngOnInit() {
    if (!this.editMode) {
      this.getRoomTypesList();
    }
  }

  private getRoomTypesList() {
    this.priceSettingsService.getRoomTypesList().subscribe(res => {
      this.initRoomTypes(res);
    });
  }

  private getDataByTypeId(typeId) {
    this.priceSettingsService.getBasePriceById(typeId).subscribe(res => {
      this.basePriceData = res;
      this.fillData(res);
    });
  }

  private fillData(data) {
    this.dataById = data;
    this.validateForm = this.fb.group({
      name: [data.name, [Validators.required]],
      start_date: [data.start_date, [Validators.required]],
      end_date: [data.end_date, [Validators.required]],
      min_stay_day: [data.min_stay ? true : false, []],
      room_types: new FormArray([]),
      table_data: [null, []],
      min_stay: [data.min_stay, []],
    });

    this.dateRange = [new Date(data.start_date), new Date(data.end_date)];
    this.startValue = new Date(data.start_date);
    this.endValue = new Date(data.end_date);

    this.getDisableDays();
    this.getRoomTypesList();

  }

  private setSelectedRoomTypes() {
    const ids = this.dataById.prices.map(el => {
      return el.room_type_id;
    });
    const unique = [...new Set(ids)];
    unique.forEach(el => {
      const foundIndex = this.roomTypes.findIndex(type => {
        return type.id === el;
      });
      if (foundIndex > -1) {
        const prices = [];
        this.dataById.prices.forEach(price => {
          if (price.room_type_id === this.roomTypes[foundIndex].id) {
            prices.push(price);
          }
        });
        this.roomTypes[foundIndex].prices = prices;
        this.validateForm.get('room_types')['controls'][foundIndex].setValue(true);
      }
    });
  }

  private initRoomTypes(types) {
    this.roomTypes = types;
    const typesArray = this.validateForm.get('room_types') as FormArray;
    types.forEach(el => {
      typesArray.push(new FormControl(false));
    });

    if (this.dataById) {
      this.setSelectedRoomTypes();
    }
  }

  onDateChange(even) {
    this.validateForm.controls.start_date.setValue(even[0]);
    this.validateForm.controls.end_date.setValue(even[1]);
  }

  onStartDateChange(even) {
    this.startValue = even;
    this.validateForm.controls.start_date.setValue(even);
    const tomorrow = new Date(even);
    if (!this.endValue || this.startValue > this.endValue) {
      tomorrow.setDate(tomorrow.getDate() + 1);
      this.endValue = tomorrow;
      this.validateForm.controls.end_date.setValue(tomorrow);
    }
    this.getDisableDays();
  }

  onEndDateChange(even) {
    this.endValue = even;
    this.validateForm.controls.end_date.setValue(even);
    this.getDisableDays();
  }

  private getDisableDays() {
    if (this.startValue && this.endValue) {
      const currentDay = new Date(this.startValue);
      const endDay = new Date(this.endValue);
      const days = [];
      while (currentDay <= endDay) {
        days.push(moment(currentDay).isoWeekday());
        currentDay.setDate(currentDay.getDate() + 1);
      }
      this.daysForRooms = [...new Set(days)];
    }
  }

  submitForm() {
    // tslint:disable-next-line:forin
    const checkboxRoomTypes = this.validateForm.get('room_types').value.find(el => {
      return el === true;
    });
    if (!checkboxRoomTypes) {
      this.notificationService.errorMessage('Вы не выбрали тип комнаты');
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.get('min_stay_day').value === true && !this.validateForm.get('min_stay').value) {
      this.notificationService.errorMessage('Вы не ввели минимальное количество дней');
      return;
    }

    if (this.validateForm.valid) {
      const body = {
        name: this.validateForm.get('name').value,
        property_id: this.priceSettingsService.getPropertyId(),
        start_date: this.validateForm.get('start_date').value,
        end_date: this.validateForm.get('end_date').value,
        prices: this.getPrices(),
        min_stay: this.validateForm.get('min_stay_day').value ? this.validateForm.get('min_stay').value : null
      };

      this.createIsProcess = true;

      if (!this.editMode) {
        this.priceSettingsService.createBasePrices(body).subscribe(res => {
          this.createIsProcess = false;
          // tslint:disable-next-line:max-line-length
          this.router.navigate([RoutesEnum.HOTEL, this.priceSettingsService.getPropertyId(), RoutesEnum.PRICE_SET, RoutesEnum.LIST]).finally();
        }, error => {
          this.createIsProcess = false;
        });
      } else {
        this.priceSettingsService.updateBasePrices(body, this.dataById.id).subscribe(res => {
          this.createIsProcess = false;
          this.notificationService.successMessage('Успешно!', 'Ваши данные были сохранены.');
          // tslint:disable-next-line:max-line-length
          this.router.navigate([RoutesEnum.HOTEL, this.priceSettingsService.getPropertyId(), RoutesEnum.PRICE_SET, RoutesEnum.LIST]).finally();
        }, error => {
          this.createIsProcess = false;
        });
      }
    } else {
      window.scrollTo(0, 0);
    }
  }

  private getPrices() {
    const prices: any[] = [];
    this.roomTypes.forEach(type => {
      if (type.tableData) {
        type.tableData.forEach(row => {
          if (!row.monday.disabled && row.monday.val) {
            let price = row.monday.val;
            if (typeof row.monday.val === 'string') {
              price = row.monday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 0,
            });
          }
          if (!row.tuesday.disabled && row.tuesday.val) {
            let price = row.tuesday.val;
            if (typeof row.tuesday.val === 'string') {
              price = row.tuesday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 1,
            });
          }
          if (!row.wednesday.disabled && row.wednesday.val) {
            let price = row.wednesday.val;
            if (typeof row.wednesday.val === 'string') {
              price = row.wednesday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 2,
            });
          }
          if (!row.thursday.disabled && row.thursday.val) {
            let price = row.thursday.val;
            if (typeof row.thursday.val === 'string') {
              price = row.thursday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 3,
            });
          }
          if (!row.friday.disabled && row.friday.val) {
            let price = row.friday.val;
            if (typeof row.friday.val === 'string') {
              price = row.friday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 4,
            });
          }
          if (!row.saturday.disabled && row.saturday.val) {
            let price = row.saturday.val;
            if (typeof row.saturday.val === 'string') {
              price = row.saturday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 5,
            });
          }
          if (!row.sunday.disabled && row.sunday.val) {
            let price = row.sunday.val;
            if (typeof row.sunday.val === 'string') {
              price = row.sunday.val.replace(/\s+/g, '');
            }
            prices.push({
              price: Number(price),
              guest_quantity: row.icons.length,
              local: row.guest,
              room_type_id: type.id,
              w: 6,
            });
          }
        });
      }
    });
    return prices;
  }

  goBack() {
    this.location.back();
  }

  changeDoubleRoom(event, i) {
    this.selectedDoubleRoom = !this.selectedDoubleRoom;
    this.roomTypes[i].tableData = [];
  }

  changeSharedRoom(event) {
    this.selectedSharedRoom = !this.selectedSharedRoom;
  }

  changeTripleRoom(event) {
    this.selectedTripleRoom = !this.selectedTripleRoom;
  }

  public setChangedData(event, index) {
    this.roomTypes[index].tableData = event;
  }

  public setMinStayDay() {
    if (this.validateForm.get('min_stay_day').value === false) {
      this.validateForm.get('min_stay').setValue(null);
    }
  }
}
