import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PriceSettingComponent} from './price-setting.component';
import {AllTourPriceComponent} from './all-tour-price/all-tour-price.component';
import {TourAgentPriceComponent} from './tour-agent-price/tour-agent-price.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {EditTourAgentPriceComponent} from '@app/views/price-setting/edit-tour-agent-price/edit-tour-agent-price.component';

const routes: Routes = [
  {
    path: '',
    component: PriceSettingComponent,
    redirectTo: RoutesEnum.LIST
  },
  {
    path: RoutesEnum.LIST,
    component: AllTourPriceComponent
  },
  {
    path: RoutesEnum.CREATE,
    component: TourAgentPriceComponent
  },
  {
    path: RoutesEnum.EDIT + '/:type_id',
    component: EditTourAgentPriceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PriceSettingRoutingModule {
}
