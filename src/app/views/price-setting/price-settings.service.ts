import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {PriceSettingsApiService} from '@app/shared/services/price-settngs-api/price-settings-api.service';
import {CreateBasePricesModel, DeleteBasePriceModel, TourAgentRatesModel} from '@app/shared/models/price.model';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';

@Injectable({
  providedIn: 'root'
})
export class PriceSettingsService {

  constructor(private priceSettingsApiService: PriceSettingsApiService, private roomsService: RoomsService) {
  }

  getBasePriceList(): Observable<TourAgentRatesModel[]> {
    return this.priceSettingsApiService.getBasePriceList(this.getPropertyId());
  }

  getRoomTypesList(): Observable<RoomsTypesListModel[]> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId());
  }

  createBasePrices(body): Observable<CreateBasePricesModel> {
    return this.priceSettingsApiService.createBasePrices(this.getPropertyId(), body);
  }

  updateBasePrices(body, id): Observable<CreateBasePricesModel> {
    return this.priceSettingsApiService.updateBasePrices(this.getPropertyId(), body, id);
  }

  getBasePriceById(id): Observable<TourAgentRatesModel> {
    return this.priceSettingsApiService.getBasePriceById(this.getPropertyId(), id);
  }

  deleteBasePrice(id): Observable<DeleteBasePriceModel> {
    return this.priceSettingsApiService.deleteBasePrice(this.getPropertyId(), id);
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }
}
