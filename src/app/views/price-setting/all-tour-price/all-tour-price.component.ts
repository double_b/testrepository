import {Component, OnInit} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {PriceSettingsService} from '@app/views/price-setting/price-settings.service';
import {TourAgentRatesModel} from '@app/shared/models/price.model';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';

@Component({
  selector: 'app-all-tour-price',
  templateUrl: './all-tour-price.component.html',
  styleUrls: ['./all-tour-price.component.scss']
})
export class AllTourPriceComponent implements OnInit {
  tableData: TourAgentRatesModel[] = [];

  isVisibleDelete = false;
  dataToDelete;
  deleteLoading = false;

  tariffList: any[] = [];
  roomTypes: RoomsTypesListModel[] = [];

  constructor(private priceSettingsService: PriceSettingsService) {
  }

  ngOnInit() {
    this.priceSettingsService.getRoomTypesList().subscribe(res => {
      this.roomTypes = res;
      this.getList();
    });
  }

  private getList() {
    this.priceSettingsService.getBasePriceList().subscribe(res => {
      this.tariffList = res;
      const array = [];
      this.tableData = [];

      this.tariffList.forEach(el => {
        array.push(this.setPrices(el));
      });

      this.tableData = [...array];
    });
  }

  private setPrices(el) {
    const array_types = [];

    const types = el.prices.map(price => {
      return price.room_type_id;
    });
    const unique = [...new Set(types)];

    unique.forEach(type_id => {
      let prices = {};
      prices = el.prices.filter(price => {
        return price.room_type_id === type_id;
      });

      const found_type = this.roomTypes.find(type => {
        return type.id === type_id;
      });

      const new_found_type = Object.assign({prices}, found_type);
      array_types.push(new_found_type);
    });

    return {
      id: el.id,
      name: el.name,
      date: new Date(el.start_date).toLocaleDateString() + ' — ' + new Date(el.end_date).toLocaleDateString(),
      min: el.min_stay,
      types: array_types,
      basic_price: el.basic_price
    };
  }


  prepareDataToDelete(data) {
    this.dataToDelete = data;
    this.isVisibleDelete = true;
  }

  cancelDelete() {
    this.dataToDelete = null;
    this.isVisibleDelete = false;
  }

  deleteElm() {
    this.priceSettingsService.deleteBasePrice(this.dataToDelete.id).subscribe(res => {
      delete this.dataToDelete;
      this.isVisibleDelete = false;
      this.deleteLoading = false;
      this.getList();
    });
  }

  getPriceCreateRoute(): string {
    return `/${RoutesEnum.HOTEL}/${this.priceSettingsService.getPropertyId()}/${RoutesEnum.PRICE_SET}/${RoutesEnum.CREATE}`;
  }

}
