import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PriceSettingComponent} from './price-setting.component';
import {TourAgentPriceComponent} from './tour-agent-price/tour-agent-price.component';
import {RouterModule} from '@angular/router';
import {
  NzAlertModule,
  NzButtonModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzFormModule,
  NzIconModule,
  NzInputModule,
  NzInputNumberModule,
  NzModalModule,
  NzRadioModule,
  NzStepsModule,
  NzTableModule
} from 'ng-zorro-antd';
import {SharedModule} from '@app/core/shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AllTourPriceComponent} from './all-tour-price/all-tour-price.component';
import {PriceSettingRoutingModule} from './price-setting-routing.module';
import {EditTourAgentPriceComponent} from "@app/views/price-setting/edit-tour-agent-price/edit-tour-agent-price.component";

@NgModule({
  declarations: [
    PriceSettingComponent,
    TourAgentPriceComponent,
    EditTourAgentPriceComponent,
    AllTourPriceComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NzFormModule,
    SharedModule,
    ReactiveFormsModule,
    NzStepsModule,
    NzRadioModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzInputNumberModule,
    NzInputModule,
    NzAlertModule,
    NzButtonModule,
    NzTableModule,
    NzIconModule,
    FormsModule,
    NzModalModule,
    PriceSettingRoutingModule
  ],
  exports: [PriceSettingComponent]
})
export class PriceSettingModule {
}
