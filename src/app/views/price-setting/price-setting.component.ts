import {Component} from '@angular/core';

@Component({
  selector: 'app-price-setting',
  template: '<router-outlet></router-outlet>'
})
export class PriceSettingComponent {
}
