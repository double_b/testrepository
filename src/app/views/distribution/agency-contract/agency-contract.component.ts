import {Component, DoCheck, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {DistributionService} from '@app/views/distribution/distribution.service';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'app-agency-contract',
  templateUrl: './agency-contract.component.html',
  styleUrls: ['./agency-contract.component.scss']
})
export class AgencyContractComponent implements OnInit, DoCheck, OnDestroy {
  @ViewChild('endDatePicker') endDatePicker;
  paramsSubscription: Subscription;
  agencyId;
  currentStep = 0;
  isLoading = false;
  cancellations = [];
  taxes = {nds: false, city_tax: null};
  tableData;
  foodData;
  validateForm: FormGroup;
  agencyInfo: any;
  selectedDates: Date[] = [];
  name;
  tooltipMessage = 'Для заключения договора необходимо выбрать тарифы с ценами и номерами на все выбранные даты заключения договора';

  startValue: Date | null = null;
  endValue: Date | null = null;
  openedEnd = false;
  countDaysInYear;
  selectedDaysFromDatepicker: any;
  selectedDays = 0;
  selectedDaysArray: any[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private distributionService: DistributionService,
              private notificationService: NotificationService
  ) {
    this.validateForm = this.fb.group({
      created_date: [null, [Validators.required]],
      contract_number: ['', [Validators.required]],
    });
  }

  ngDoCheck() {
    switch (this.currentStep) {
      case 0 :
        this.name = 'Заключение договора - выбор цен';
        break;
      case 1 :
        this.name = 'Заключение договора - условия';
        break;
      case 2 :
        this.name = 'Заключение договора - подтверждение';
        break;
    }
  }

  ngOnInit() {
    this.paramsSubscription = this.route.paramMap.subscribe(res => {
      this.agencyId = res.get(RoutesEnum.ID);
      this.getAgencyInfo();
    });
    this.getSelectedDays();
  }

  public getSelectedDays() {
    if (!this.startValue || !this.endValue) {
      const currentYear = moment().year();
      // @ts-ignore
      this.countDaysInYear = (new Date(currentYear, 11, 31 + 1) - new Date(currentYear, 0, 1)) / 86400000;
      this.selectedDaysFromDatepicker = this.getDaysInYear(currentYear);
    } else {
      const startYear = moment(this.startValue).year();
      const endYear = moment(this.endValue).year();

      const startMonth = this.startValue.getMonth();
      const endMonth = this.endValue.getMonth();

      const startDay = this.startValue.getDate();
      const endDay = this.endValue.getDate();
      // @ts-ignore
      this.countDaysInYear = (new Date(endYear, endMonth, endDay + 1) - new Date(startYear, startMonth, startDay)) / 86400000;
      this.selectedDaysFromDatepicker = this.getDaysForDatepicker();
    }
    this.selectedDays = this.setSelectedDaysCount();
  }

  private getDaysInYear(currentYear) {
    let endDate = new Date(currentYear, 11, 31);
    let currentDay = new Date(currentYear, 0, 1);
    let days = [];
    while (currentDay <= endDate) {
      days.push(moment(currentDay).format('L'));
      currentDay.setDate(currentDay.getDate() + 1);
    }
    return days;
  }

  private getDaysForDatepicker() {
    let endDate = new Date(this.endValue);
    endDate.setDate(endDate.getDate() + 1);
    let currentDay = new Date(this.startValue);
    let days = [];
    while (currentDay <= endDate) {
      days.push(moment(currentDay).format('L'));
      currentDay.setDate(currentDay.getDate() + 1);
    }
    return days;
  }

  private getAgencyInfo() {
    this.distributionService.getAgencyInfo(this.agencyId).subscribe(res => {
      this.agencyInfo = res;
    });
  }

  ngOnDestroy() {
    if (this.paramsSubscription != null) {
      this.paramsSubscription.unsubscribe();
    }
  }

  leftBtn() {
    switch (true) {
      case this.currentStep === 0:
        // tslint:disable-next-line:max-line-length
        this.router.navigate([RoutesEnum.HOTEL, this.distributionService.getPropertyId(), RoutesEnum.DISTRIBUTION, RoutesEnum.AGENCY, this.agencyId]).finally();
        break;
      case this.currentStep === 1 || this.currentStep === 2:
        this.currentStep = this.currentStep - 1;
    }
  }

  rightBtn() {
    switch (true) {
      case this.currentStep === 0:
        this.currentStep = this.currentStep + 1;
        window.scrollTo(0, 0);
        break;
      case this.currentStep === 1:
        if (this.cancellations.length > 0) {
          this.isLoading = true;
          setTimeout(() => {
            this.isLoading = false;
            this.currentStep = this.currentStep + 1;
            window.scrollTo(0, 0);
          }, 1000);
        } else {
          this.notificationService.errorMessage('Добавьте как минимум 1 условие уннуляции');
        }
        break;
      case this.currentStep === 2:
        this.createContract();
        break;
    }
  }

  private createContract() {
    if (!this.validateForm.get('contract_number').value) {
      this.notificationService.errorMessage('Укажите номер договора');
      return;
    }
    if (!this.startValue || !this.endValue) {
      this.notificationService.errorMessage('Выберите даты заключения договора');
      return;
    }

    let body = this.agencyInfo.company_legal_information;
    delete body.id;
    delete body.contract_number;
    delete body.okonh;
    body.name = this.agencyInfo.name;
    body.rates = this.getPrices();
    body.rules = this.getRules();
    body.city_tax = this.taxes.city_tax ? Number(this.taxes.city_tax.replace('%', '')) : null;
    body.tax = this.taxes.nds ? 18 : null;
    body.number = this.validateForm.get('contract_number').value;
    body.start = this.startValue;
    body.end = this.endValue;

    if (!this.foodData || !this.foodData.include_food) {
      body.breakfast = false;
      body.lunch = false;
      body.dinner = false;
    } else {
      body.breakfast = this.foodData.breakfast;
      body.lunch = this.foodData.lunch;
      body.dinner = this.foodData.dinner;
    }

    this.distributionService.createContract(body).subscribe(res => {
      this.notificationService.successMessage('Успешно!', 'Контракт был сохранен.');
      // tslint:disable-next-line:max-line-length
      this.router.navigate([RoutesEnum.HOTEL, this.distributionService.getPropertyId(), RoutesEnum.DISTRIBUTION, RoutesEnum.AGENCY, this.agencyId]).finally();
    });
  }

  private getPrices() {
    let rates = [];
    this.tableData.forEach(el => {
      let prices = [];
      el.types.forEach(type => {
        prices = prices.concat(type.prices);
      });
      el.prices = prices;
      const dates = el.date.split('—');
      el.start_date = this.getDate(dates[0].trim());
      el.end_date = this.getDate(dates[1].trim());
      el.min_stay = el.min;
      rates.push(el);
    });
    return rates;
  }

  private getDate(date) {
    let today;
    today = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
    if (today === 'Invalid date') {
      today = moment(date, 'MM.DD.YYYY').format('YYYY-MM-DD');
    }
    return today;
  }

  private getRules() {
    let rules = [];
    this.cancellations.forEach(el => {
      rules.push(el.body);
    });
    return rules;
  }

  setCancellations(event) {
    this.cancellations = event;
  }

  setTaxes(event) {
    this.taxes = event;
  }

  setTableData(event) {
    this.tableData = event;
    this.setSelectedDays();
  }

  setFoodData(event) {
    this.foodData = event;
  }

  private setSelectedDays() {
    const ratesDays = [];
    this.tableData.forEach(element => {
      const dates = element.date.split('—');
      const start_date = this.getDate(dates[0].trim());
      let end_date = this.getDate(dates[1].trim());

      let endDate = new Date(end_date);
      let currentDay = new Date(start_date);
      let days = [];
      while (currentDay <= endDate) {
        days.push(moment(currentDay).format('L'));
        currentDay.setDate(currentDay.getDate() + 1);
      }
      ratesDays.push(...days);
    });
    this.selectedDaysArray = ratesDays;
    this.selectedDays = this.setSelectedDaysCount();
  }

  private setSelectedDaysCount() {
    let _self = this;
    const array = this.selectedDaysFromDatepicker.filter(function(obj) {
      return _self.selectedDaysArray.indexOf(obj) >= 0;
    });
    return array.length;
  }

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      const year = new Date(this.startValue).getFullYear();
      if (this.startValue) {
        this.endValue = new Date(year, 11, 31);
      }
    }
  }
}
