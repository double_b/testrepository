import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-agency-contract-third-step',
  templateUrl: './agency-contract-third-step.component.html',
  styleUrls: ['./agency-contract-third-step.component.scss']
})
export class AgencyContractThirdStepComponent {
  @Input() cancellationsData;
  @Input() taxes;
  @Input() tableData;
  @Input() foodData;
}
