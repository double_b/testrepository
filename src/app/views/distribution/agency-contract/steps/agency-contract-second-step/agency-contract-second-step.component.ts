import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-agency-contract-second-step',
  templateUrl: './agency-contract-second-step.component.html',
  styleUrls: ['./agency-contract-second-step.component.scss']
})
export class AgencyContractSecondStepComponent implements OnInit {
  @Input() taxes;
  @Input() cancellationsData;
  @Output() cancellationsEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() taxesEmitter: EventEmitter<any> = new EventEmitter<any>();

  cancellations = [];
  cancellationEdit;
  isVisible = false;
  cancellationIndex;
  validateCancellationsForm: FormGroup;

  percentOptions: any[] = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    if (this.cancellationsData) {
      this.cancellations = this.cancellationsData;
    }
    this.validateForm = this.fb.group({
      nds: [this.taxes.nds ? this.taxes.nds : false, []],
      breakfast: [null, []],
      city_tax: [this.taxes.city_tax ? true : false, []],
      city_tax_total: [{value: this.taxes.city_tax ? this.taxes.city_tax : '', disabled: this.taxes.city_tax ? false : true}, []],
      city_tax_percent: [null, []],
      city_tax_include: [null, []],
    });
    this.initCancellationsForm();
  }

  initCancellationsForm() {
    this.validateCancellationsForm = this.fb.group({
      days: [this.cancellationEdit ? this.cancellationEdit.body.days : '', [Validators.required]],
      cancel_commission: [this.cancellationEdit ? this.cancellationEdit.body.cancel_commission : '', [Validators.required]],
      commission: [this.cancellationEdit ? this.cancellationEdit.body.commission : null, [Validators.required]],
      prepayment: [this.cancellationEdit ? this.cancellationEdit.body.prepayment : null, [Validators.required]]
    });
  }

  addAnnulateRow(body) {
    this.cancellations.push(
      {
        body: {
          cancel_commission: body.cancel_commission,
          days: body.days,
          prepayment: body.prepayment,
          commission: body.commission
      },
    }
    );
    this.cancellations = [...this.cancellations];
    this.cancellationsEmitter.emit(this.cancellations);
  }

  deleteCancellation(index) {
    this.cancellations.splice(index, 1);
    this.cancellations = [...this.cancellations];
    this.cancellationsEmitter.emit(this.cancellations);
  }

  editCancellation(index) {
    this.cancellationEdit = this.cancellations[index];
    this.cancellationIndex = index;
    this.showModal();
  }

  changeTaxes() {
    if (this.validateForm.get('city_tax').value) {
      this.validateForm.controls.city_tax_total.enable();
    } else {
      this.validateForm.controls.city_tax_total.disable();
    }
    const body = {
      nds: this.validateForm.get('nds').value,
      city_tax: this.validateForm.get('city_tax').value ? this.validateForm.get('city_tax_total').value + '%' : null,
    };
    this.taxesEmitter.emit(body);
  }

  handleCancel() {
    this.isVisible = false;
  }

  handleOk() {
    this.isVisible = false;
  }

  showModal() {
    this.initCancellationsForm();
    this.isVisible = true;
  }

  saveAnnulate() {
    const body = this.validateCancellationsForm.getRawValue();
    if (this.cancellationEdit) {
      const result = {body};
      const newDateArray = [...this.cancellations];
      newDateArray[this.cancellationIndex] = result;
      this.cancellations = newDateArray;
      this.cancellationEdit = null;
      this.cancellationIndex = null;
    } else {
      this.addAnnulateRow(body);
    }
    this.handleOk();
    this.cancellationsEmitter.emit(this.cancellations);
  }

}
