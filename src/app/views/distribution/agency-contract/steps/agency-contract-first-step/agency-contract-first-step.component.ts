import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-agency-contract-first-step',
  templateUrl: './agency-contract-first-step.component.html',
  styleUrls: ['./agency-contract-first-step.component.scss']
})
export class AgencyContractFirstStepComponent implements OnInit {
  @Output() tableDataEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() foodDataEmitter: EventEmitter<any> = new EventEmitter<any>();
  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.validateForm = this.fb.group({
      include_food: [false, []],
      breakfast: [null, []],
      lunch: [null, []],
      dinner: [null, []],
      all: [null, []],
    });
  }

  ngOnInit() {
    this.setFoodData();
  }

  checkAllFood() {
    this.validateForm.controls.breakfast.setValue(this.validateForm.value.all);
    this.validateForm.controls.lunch.setValue(this.validateForm.value.all);
    this.validateForm.controls.dinner.setValue(this.validateForm.value.all);
    this.setFoodData();
  }

  setTableData(event) {
    this.tableDataEmitter.emit(event);
  }

  setFoodData() {
    const body = this.validateForm.getRawValue();
    this.foodDataEmitter.emit(body);
  }
}
