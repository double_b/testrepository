import {Component, OnInit} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {DistributionService} from '@app/views/distribution/distribution.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '@app/shared/services/notification/notification.service';
import {Patterns} from '@app/core/constants/validation-patterns';

@Component({
  selector: 'app-travel-agencies',
  templateUrl: './travel-agencies.component.html',
  styleUrls: ['./travel-agencies.component.scss']
})
export class TravelAgenciesComponent implements OnInit {

  public isVisible = false;
  isVisibleDelete = false;
  dataToDelete;

  radioValue = 0;
  validateForm: FormGroup;

  agencyList: any[] = [];


  constructor(private distributionService: DistributionService, private fb: FormBuilder, private notificationService: NotificationService) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      tour_agent: [false, []],
      contr_agent: [true, []],
      company_name: ['', [Validators.required]],
      address: ['', [Validators.required]],
      phone: ['', []],
      bank_account: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(20), Validators.pattern('[0-9]*')]],
      bank_account_usd: ['', [Validators.maxLength(20), Validators.minLength(20), Validators.pattern('[0-9]*')]],
      oked: ['', []],
      bank: ['',],
      mfo: ['',],
      inn: ['',],
      okonh: ['', []],
      swift_code: ['', []],
      director_name: ['', [Validators.required]],
      email: ['', [Validators.pattern(Patterns.emailPattern)]],
      city: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.getTravelAgenciesList();
  }

  private getTravelAgenciesList() {
    this.distributionService.getTravelAgenciesList().subscribe(res => {
      this.agencyList = res;
    });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;

    const body = this.validateForm.getRawValue();
    if (this.radioValue === 0) {
      body.contr_agent = true;
      body.tour_agent = false;
    } else {
      body.contr_agent = false;
      body.tour_agent = true;
    }

    this.distributionService.createTravelAgency(body).subscribe(res => {
      this.getTravelAgenciesList();
      this.validateForm.reset();
      this.notificationService.successMessage(`Контрагент добавлен!`);
    });
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  getAgencyRoute(agencyId: any): string {
    return `/${RoutesEnum.HOTEL}/${this.distributionService.getPropertyId()}/${RoutesEnum.DISTRIBUTION}/${RoutesEnum.AGENCY}/${agencyId}`;
  }

  deleteAgency(agencyId: any) {
    this.dataToDelete = agencyId;
    this.isVisibleDelete = true;
  }

  cancelDelete() {
    this.dataToDelete = null;
    this.isVisibleDelete = false;
  }

  deleteElm() {
    this.isVisibleDelete = false;
    this.distributionService.deleteTravelAgency(this.dataToDelete).subscribe(res => {
      delete this.dataToDelete;
      this.getTravelAgenciesList();
    });
  }
}
