import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {TravelAgenciesApiService} from '@app/shared/services/travel-agencies-api/travel-agencies-api.service';
import {DeleteBasePriceModel} from '@app/shared/models/price.model';
import {KeysEnum} from '@app/core/constants/keys.enum';
import {TravelAgenciesModel} from '@app/shared/models/travel-agencies-list.model';
import {ConstractsForCompanyModel} from '@app/shared/models/constracts-for-company.model';
import {AgencyInfoModel} from '@app/shared/models/agency-info.model';
import {BodyCreateTravelAgencyModel} from '@app/shared/models/body-create-travel-agency.model';
import {CrateTravelAgenciesModel} from '@src/app/shared/models/crate-travel-agencies.model';
import {BodyCreateContractModel} from '@app/shared/models/body-create-contract.model';
import {RoomsService} from '@app/shared/services/rooms/rooms.service';
import {RoomsTypesListModel} from '@app/shared/models/rooms-types-list.model';

@Injectable({
  providedIn: 'root'
})
export class DistributionService {

  data = {
    agencyId: new BehaviorSubject<any>(null as any),
  };

  constructor(private travelAgenciesApiService: TravelAgenciesApiService, private roomsService: RoomsService) {
  }

  getTravelAgenciesList(): Observable<TravelAgenciesModel[]> {
    return this.travelAgenciesApiService.getTravelAgenciesList(this.getPropertyId());
  }

  createTravelAgency(body: BodyCreateTravelAgencyModel): Observable<CrateTravelAgenciesModel> {
    return this.travelAgenciesApiService.createTravelAgency(this.getPropertyId(), body);
  }

  getAgencyInfo(id): Observable<AgencyInfoModel> {
    return this.travelAgenciesApiService.getAgencyInfo(this.getPropertyId(), id);
  }

  deleteTravelAgency(id): Observable<DeleteBasePriceModel> {
    return this.travelAgenciesApiService.deleteTravelAgency(this.getPropertyId(), id);
  }

  createContract(body: BodyCreateContractModel): Observable<DeleteBasePriceModel> {
    return this.travelAgenciesApiService.createContract(this.getPropertyId(), body);
  }

  getContractsForCompany(id): Observable<ConstractsForCompanyModel[]> {
    return this.travelAgenciesApiService.getContractsForCompany(this.getPropertyId(), id);
  }

  getContractInfo(id): Observable<ConstractsForCompanyModel> {
    return this.travelAgenciesApiService.getContractInfo(this.getPropertyId(), id);
  }

  getRoomTypes(): Observable<RoomsTypesListModel[]> {
    return this.roomsService.getRoomsTypeList(this.getPropertyId());
  }

  getPropertyId() {
    const hotelID = localStorage.getItem(KeysEnum.HOTEL_ID);
    return Number(hotelID);
  }

}
