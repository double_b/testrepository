import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DistributionComponent} from './distribution.component';
import {RouterModule} from '@angular/router';
import {TravelAgenciesComponent} from './travel-agencies/travel-agencies.component';
import {SharedModule, SharedModule as OldShared} from '@app/core/shared/shared.module';
import {
  NzAlertModule,
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule,
  NzDatePickerModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzRadioModule,
  NzSelectModule,
  NzStepsModule,
  NzTableModule
} from 'ng-zorro-antd';
import {AgencyComponent} from './agency/agency.component';
import {AgencyContractComponent} from './agency-contract/agency-contract.component';
import {ContractsComponent} from './contracts/contracts.component';
import {AgencyContractFirstStepComponent} from './agency-contract/steps/agency-contract-first-step/agency-contract-first-step.component';
import {AgencyContractSecondStepComponent} from './agency-contract/steps/agency-contract-second-step/agency-contract-second-step.component';
import {AgencyContractThirdStepComponent} from './agency-contract/steps/agency-contract-third-step/agency-contract-third-step.component';
import {ReactiveFormsModule} from '@angular/forms';
import {DistributionRoutingModule} from './distribution-routing.module';
import {ContractComponent} from '@app/views/distribution/contract/contract.component';

@NgModule({
  declarations: [
    DistributionComponent,
    TravelAgenciesComponent,
    AgencyComponent,
    AgencyContractComponent,
    ContractsComponent,
    AgencyContractFirstStepComponent,
    AgencyContractSecondStepComponent,
    AgencyContractThirdStepComponent,
    ContractComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    NzRadioModule,
    NzFormModule,
    NzGridModule,
    NzTableModule,
    NzAlertModule,
    NzCardModule,
    NzStepsModule,
    ReactiveFormsModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzSelectModule,
    OldShared,
    DistributionRoutingModule
  ],
  exports: [DistributionComponent]
})
export class DistributionModule {
}
