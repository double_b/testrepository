import {Component} from '@angular/core';

@Component({
  selector: 'app-distribution',
  template: '<router-outlet></router-outlet>',
  styleUrls: []
})
export class DistributionComponent {
}
