import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {DistributionService} from '@app/views/distribution/distribution.service';

@Component({
  selector: 'app-agency',
  templateUrl: './agency.component.html',
  styleUrls: ['./agency.component.scss']
})
export class AgencyComponent implements OnInit, OnDestroy {
  paramsSubscription: Subscription = null;
  agencyId;
  agencyInfo: any;
  contractsList: any[] = [];
  isDataLoading = true;

  constructor(private route: ActivatedRoute,
              private distributionService: DistributionService
  ) {
  }

  ngOnInit() {
    this.paramsSubscription = this.route.paramMap.subscribe(res => {
      this.agencyId = res.get(RoutesEnum.ID);
      this.getAgencyInfo();
    });
  }

  private getAgencyInfo() {
    this.distributionService.getAgencyInfo(this.agencyId).subscribe(res => {
      this.agencyInfo = res;
    });
    this.distributionService.getContractsForCompany(this.agencyId).subscribe(res => {
      this.contractsList = res;
      this.isDataLoading = false;
    });
  }

  ngOnDestroy() {
    if (this.paramsSubscription != null) {
      this.paramsSubscription.unsubscribe();
    }
  }

  getContractRoute(): string {
    // tslint:disable-next-line:max-line-length
    return `/${RoutesEnum.HOTEL}/${this.distributionService.getPropertyId()}/${RoutesEnum.DISTRIBUTION}/${RoutesEnum.CONTRACT}/${this.agencyId}`;
  }

  getViewContractRoute(contract): string {
    // tslint:disable-next-line:max-line-length
    return `/${RoutesEnum.HOTEL}/${this.distributionService.getPropertyId()}/${RoutesEnum.DISTRIBUTION}/${RoutesEnum.VIEW_CONTRACT}/${contract.id}`;
  }

}
