import {Component} from '@angular/core';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent {

  contractsData = [
    {
      name: 'Lorem',
      number: 23123,
      conclusion_date: '12.01.2019',
      valid_until: '31.12.2019',
      status: 'active'
    },
    {
      name: 'Lorem',
      number: 23123,
      conclusion_date: '12.01.2019',
      valid_until: '31.12.2019',
      status: 'await'
    },
    {
      name: 'Lorem',
      number: 23123,
      conclusion_date: '12.01.2019',
      valid_until: '31.12.2019',
      status: 'closed'
    }
  ];

  contractsData2 = [
    {
      name: 'Lorem',
      number: 23123,
      conclusion_date: '12.01.2019',
      valid_until: '31.12.2019',
      status: 'active'
    },
    {
      name: 'sobaka',
      number: 23123,
      conclusion_date: '12.01.2019',
      valid_until: '31.12.2019',
      status: 'await'
    }
  ];
}
