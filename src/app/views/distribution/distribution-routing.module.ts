import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DistributionComponent} from './distribution.component';
import {TravelAgenciesComponent} from './travel-agencies/travel-agencies.component';
import {AgencyComponent} from './agency/agency.component';
import {AgencyContractComponent} from './agency-contract/agency-contract.component';
import {ContractsComponent} from './contracts/contracts.component';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ContractComponent} from '@app/views/distribution/contract/contract.component';

const routes: Routes = [
  {
    path: '',
    component: DistributionComponent,
    redirectTo: RoutesEnum.TRAVEL_AGENCIES
  },
  {
    path: RoutesEnum.TRAVEL_AGENCIES,
    component: TravelAgenciesComponent
  },
  {
    path: `${RoutesEnum.AGENCY}/:${RoutesEnum.ID}`,
    component: AgencyComponent,
  },
  {
    path: `${RoutesEnum.CONTRACT}/:${RoutesEnum.ID}`,
    component: AgencyContractComponent
  },
  {
    path: RoutesEnum.CONTRACTS,
    component: ContractsComponent
  },
  {
    path: `${RoutesEnum.VIEW_CONTRACT}/:${RoutesEnum.ID}`,
    component: ContractComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistributionRoutingModule {
}
