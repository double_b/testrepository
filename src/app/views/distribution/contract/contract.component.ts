import {Component, OnInit} from '@angular/core';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {ActivatedRoute} from '@angular/router';
import {DistributionService} from '@app/views/distribution/distribution.service';
import {TourAgentRatesModel} from '@app/shared/models/price.model';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {

  tableData: TourAgentRatesModel[] = [];
  contract_id: any;
  contract: any;
  roomTypes: any[] = [];
  tariffList: any[] = [];
  cancellationsData: any[] = [];

  startValue: any;
  endValue: any;

  constructor(private route: ActivatedRoute,
              private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(res => {
      this.contract_id = res.get(RoutesEnum.ID);
      this.distributionService.getContractInfo(this.contract_id).subscribe(data => {
        this.contract = data;
        this.cancellationsData = this.contract.rules;
        this.startValue = this.contract.start;
        this.endValue = this.contract.end;
        this.getRoomTypes();
      });
    });
  }

  private getRoomTypes() {
    this.distributionService.getRoomTypes().subscribe(res => {
      this.roomTypes = res;
      this.processPrices();
    });
  }

  private processPrices() {
    this.contract.prices = [];
    this.contract.rates.forEach(el => {
      el.prices.map(price => {
        price.rate_id = el.id;
      });
      this.contract.prices = this.contract.prices.concat(el.prices);
    });
    let array = [];

    this.contract.rates.forEach(el => {
      array.push(this.setPrices(el));
    });

    this.tableData = [...array];
  }

  private setPrices(el) {
    let array_types = [];

    el.prices = this.contract.prices.filter(price => {
      return el.id === price.rate_id;
    });

    const types = el.prices.map(price => {
      return price.room_type_id;
    });
    let unique = [...new Set(types)];

    unique.forEach(type_id => {
      let prices = {};
      prices = el.prices.filter(price => {
        return price.room_type_id === type_id;
      });

      let found_type = this.roomTypes.find(type => {
        return type.id === type_id;
      });

      let new_found_type = Object.assign({prices: prices}, found_type);
      array_types.push(new_found_type);
    });

    return {
      id: el.id,
      name: el.name,
      date: new Date(el.start).toLocaleDateString() + ' — ' + new Date(el.end).toLocaleDateString(),
      min: el.min_stay,
      types: array_types
    };
  }
}
