import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule, registerLocaleData} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Routs
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';

// Interceptors
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './core/interceptors/auth.interceptor';

// Environment
import {environment} from '../environments/environment';

// Components
import {AppComponent} from './app.component';

// Multi languages
import {NgZorroAntdModule, NZ_I18N, NzOverlayModule, ru_RU} from 'ng-zorro-antd';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import ru from '@angular/common/locales/ru';

import {AgmCoreModule} from '@agm/core';

import {DragulaModule} from 'ng2-dragula';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';

import {ToastrModule} from 'ngx-toastr';

import {SharedModule} from './core/shared/shared.module';

// Modules
import {CalendarStore} from '@src/app/core/store/calendar.store';
import {ScrollStore} from '@src/app/core/store/scroll.store';

// Pipes
import {CustomPipesModule} from './core/pipes/custom-pipes.module';

registerLocaleData(ru);

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    GooglePlaceModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgZorroAntdModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NzOverlayModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsApiKey,
      libraries: ['places']
    }),
    DragulaModule.forRoot(),
    SharedModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      maxOpened: 3
    }),
    CustomPipesModule
  ],
  providers: [
    {
      provide: NZ_I18N,
      useValue: ru_RU,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    CalendarStore,
    ScrollStore
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
