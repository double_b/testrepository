import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {RoutesEnum} from '@app/core/constants/routes.enum';
import {IsAuthenticatedGuard} from '@app/core/services/guards/is-authenticated-guard/is-authenticated-guard.service';


const routes: Routes = [
  {
    path: '',
    redirectTo: RoutesEnum.AUTH,
    pathMatch: 'full'
  },
  {
    path: RoutesEnum.AUTH,
    loadChildren: () => import('./layouts/auth/auth.module')
      .then(m => m.AuthModule)
  },
  {
    path: RoutesEnum.HOTEL,
    canActivate: [IsAuthenticatedGuard],
    loadChildren: () => import('./layouts/hotels/hotel-layout.module')
      .then(m => m.HotelLayoutModule)
  },
  {
    path: RoutesEnum.MESSAGES,
    canActivate: [IsAuthenticatedGuard],
    loadChildren: () => import('./views/messages/messages.module')
      .then(m => m.MessagesModule)
  },
  {
    path: RoutesEnum.PROFILE,
    canActivate: [IsAuthenticatedGuard],
    loadChildren: () => import('./views/profile/profile.module')
      .then(m => m.ProfileModule)
  },
  {
    path: RoutesEnum.ACC_MANAGE,
    canActivate: [IsAuthenticatedGuard],
    loadChildren: () => import('./views/account-management/account-management.module')
      .then(m => m.AccountManagementModule)
  },
  {
    path: RoutesEnum.REVIEWS,
    canActivate: [IsAuthenticatedGuard],
    loadChildren: () => import('./views/messages/messages.module')
      .then(m => m.MessagesModule)
  },

  {
    path: '**',
    loadChildren: () => import('./core/shared/not-found/not-found.module')
      .then(m => m.NotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
